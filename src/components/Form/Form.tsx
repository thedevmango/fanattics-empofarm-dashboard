import React from "react";
import { Form } from "reactstrap";

interface IForm {
  children?: React.ReactNode;
  className?: string;
}

export default ({ children, className = "" }: IForm) => {
  return <Form className={"component-form " + className}>{children}</Form>;
};
