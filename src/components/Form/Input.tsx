import React from "react";
import { FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText } from "reactstrap";
import TextCounter from "./TextCounter";

interface IInput {
  append?: string;
  checked?: boolean;
  children?: React.ReactNode;
  className?: string;
  containerStyle?: React.CSSProperties;
  defaultValue?: any;
  defaultChecked?: boolean;
  disabled?: boolean;
  label?: string;
  max?: number | string | undefined;
  min?: number | string | undefined;
  maxLength?: number;
  name?: string;
  placeholder?: string;
  prepend?: any;
  onBlur?: any;
  onChange?: any;
  onFocus?: any;
  onKeyPress?: any;
  type?:
    | "text"
    | "textarea"
    | "number"
    | "password"
    | "file"
    | "select"
    | "radio"
    | "checkbox"
    | "date";
  value?: any;
}

export default ({
  append,
  checked,
  children,
  className = "",
  defaultValue,
  defaultChecked,
  disabled = false,
  label,
  max,
  min,
  maxLength,
  name,
  placeholder,
  prepend,
  onBlur = () => null,
  onChange = () => null,
  onFocus = () => null,
  onKeyPress = () => null,
  type,
  value,
  containerStyle,
}: IInput) => {
  return (
    <FormGroup style={{ marginBottom: label ? "1.5rem" : 0, ...containerStyle }}>
      {!label ? null : <Label>{label}</Label>}
      <InputGroup
        className={
          type === "radio" || type === "checkbox" ? "input-group-hide" : ""
        }
      >
        {!prepend ? null : (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>{prepend}</InputGroupText>
          </InputGroupAddon>
        )}

        <Input
          checked={checked}
          className={`component-input ${className}`}
          defaultValue={defaultValue}
          defaultChecked={defaultChecked}
          disabled={disabled}
          max={max}
          min={min}
          maxLength={maxLength}
          name={name}
          onBlur={onBlur}
          onChange={(e) => onChange(e)}
          onFocus={onFocus}
          onKeyPress={onKeyPress}
          placeholder={placeholder}
          type={type}
          value={value}
        >
          {children}
        </Input>

        {type === "file" ? <label className="component-btn component-btn-secondary">Browse File</label> : null}

        {!append ? null : (
          <InputGroupAddon addonType="append">
            <InputGroupText>{append}</InputGroupText>
          </InputGroupAddon>
        )}
      </InputGroup>

      {!maxLength ? null : <TextCounter length={value.length} maxLength={maxLength} />}
    </FormGroup>
  );
};
