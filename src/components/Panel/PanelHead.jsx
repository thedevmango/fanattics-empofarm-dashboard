import React from "react";

export default ({ children, bordered }) => {
  return (
    <div className={`head ${bordered ? "bordered" : null}`}>{children}</div>
  );
};
