import React from "react";

export default ({ children, bordered }) => {
  return (
    <div className={`component-panel-body ${bordered ? "bordered" : null}`}>
      {children}
    </div>
  );
};
