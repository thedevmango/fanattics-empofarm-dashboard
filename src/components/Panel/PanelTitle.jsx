import React from "react";

export default ({ children, size = "medium", bold }) => {
  return (
    <div className={`title-${size} ${bold ? "bold" : null}`}>{children}</div>
  );
};
