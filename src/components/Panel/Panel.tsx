import React, { ReactNode } from "react";

interface IPanel extends React.HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  padding?: "16" | "24";
}

export default ({ children, padding = "24", className, ...props }: IPanel) => {
  return (
    <div {...props} className={`component-panel component-panel-${padding}  ${className}`}>
      {children}
    </div>
  );
};
