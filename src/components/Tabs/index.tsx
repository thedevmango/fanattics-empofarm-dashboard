import React from "react";
import { BodyText } from "../Typography";

interface ITabs {
  activeTab: string | string[];
  className?: string;
  counter?: number[];
  options: string[];
  setActiveTab: any;
  slug?: any[];
}

export default ({ activeTab, className, counter, options, setActiveTab, slug = [] }: ITabs) => {
  const boldIndicator = (option: string, idx: number) => {
    if (slug.length > 0) {
      if (slug[idx] === activeTab) return true;
    } else if (option === activeTab) return true;
    return false;
  };

  return (
    <div className={`component-tab-container ${className}`}>
      {options.map((option: string, idx) => {
        return (
          <BodyText
            key={idx}
            className="tab-item"
            onClick={() => (slug ? setActiveTab(slug[idx]) : setActiveTab(option))}
            bold={boldIndicator(option, idx)}
          >
            {option}&nbsp;{!counter ? null : !counter[idx] ? null : `(${counter[idx]})`}
          </BodyText>
        );
      })}
    </div>
  );
};
