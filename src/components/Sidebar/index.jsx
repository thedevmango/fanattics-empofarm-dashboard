/* eslint-disable no-unused-vars */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { SIDEBAR_OPEN } from "../../redux/types/sidebar";
import { useLocation } from "react-router-dom";

import { ListGroup, ListGroupItem } from "reactstrap";
import { AiOutlineShopping, AiOutlineControl } from "react-icons/ai";
import { RiCustomerService2Line } from "react-icons/ri";
import {
  IconCart,
  IconLogistic,
  IconMoney,
  IconPeople,
  IconSales,
  IconWarehouse,
} from "../../assets/Icons";

export default function Sidebar() {
  const location = useLocation();
  const dispatch = useDispatch();
  const selected = useSelector((state) => state.sidebar.selected);

  const SidebarItem = ({ selected, children, icon, disabled = false }) => {
    return (
      <ListGroupItem
        tag="button"
        onMouseEnter={() => dispatch({ type: SIDEBAR_OPEN, payload: children })}
        disabled={disabled}
        // active={selected === children}
        active={location.pathname.includes(`/${children}/`)}
        action
      >
        <div className="icon">{icon}</div>
        <div className="text">{children}</div>
      </ListGroupItem>
    );
  };

  return (
    <nav className="dashboard-sidebar">
      <ListGroup className="empo-sidebar">
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconCart />}
        >
          purchasing
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconSales />}
        >
          sales
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconPeople />}
        >
          katalog user
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconWarehouse />}
        >
          inventory
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconLogistic />}
        >
          logistik
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<IconMoney />}
        >
          finance
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<RiCustomerService2Line />}
        >
          layanan pelanggan
        </SidebarItem>
        <SidebarItem
          // disabled
          selected={selected}
          icon={<AiOutlineControl />}
        >
          kontrol
        </SidebarItem>
      </ListGroup>
    </nav>
  );
}
