/* eslint-disable no-unused-vars */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { SIDEBAR_SELECT_CHILD, SIDEBAR_CLOSE } from "../../redux/types/sidebar";
import { ListGroup, ListGroupItem } from "reactstrap";

export default function ChildSidebar({ isOpen }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const selectedMenu = useSelector((state) => state.sidebar.selectedMenu);
  const selectedChild = useSelector((state) => state.sidebar.selectedChild);
  const toggleMenu = (menu) =>
    dispatch({ type: SIDEBAR_SELECT_CHILD, payload: [selectedMenu, menu] });

  function closeSidebar() {
    return dispatch({ type: SIDEBAR_CLOSE });
  }

  const SidebarItem = ({ selected, slug, onClick, children, disabled = false }) => {
    function handleClick() {
      // if (selected === children) return null;
      history.push(`/${selectedMenu.split(" ").join("-")}/${slug}`);
      return onClick(children);
    }

    return (
      <ListGroupItem
        tag="button"
        disabled={disabled}
        action
        onClick={handleClick}
        active={selected === children}
      >
        <div className="text">{children}</div>
      </ListGroupItem>
    );
  };

  return (
    <nav onMouseLeave={closeSidebar} className={`dashboard-sidebar child ${isOpen ? "open" : ""}`}>
      <ListGroup className="empo-sidebar">
        {selectedMenu === "purchasing" && (
          <>
            <SidebarItem slug="dashboard" onClick={toggleMenu} selected={selectedChild}>
              Dashboard
            </SidebarItem>
            <SidebarItem slug="tender" onClick={toggleMenu} selected={selectedChild}>
              Tender
            </SidebarItem>
            <SidebarItem slug="akuisisi-produk" onClick={toggleMenu} selected={selectedChild}>
              Akuisisi Produk
            </SidebarItem>
            <SidebarItem
              slug="commodity-price-setting"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Commodity Price Setting
            </SidebarItem>
          </>
        )}

        {selectedMenu === "sales" && (
          <>
            <SidebarItem slug="product-catalog" onClick={toggleMenu} selected={selectedChild}>
              Product Catalog
            </SidebarItem>
            <SidebarItem
              slug="commodity-stock-setting"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Comodity Stock Setting
            </SidebarItem>
            <SidebarItem
              slug="commodity-price-setting"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Comodity Price Setting
            </SidebarItem>
            <SidebarItem
              slug="transactional-activities"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Transactional Activities
            </SidebarItem>
            <SidebarItem slug="price-index-analytics" onClick={toggleMenu} selected={selectedChild}>
              Price Index Analytics
            </SidebarItem>
            <SidebarItem slug="sales-analytics" onClick={toggleMenu} selected={selectedChild}>
              Sales Analytics
            </SidebarItem>
          </>
        )}

        {selectedMenu === "katalog user" && (
          <>
            <SidebarItem slug="admin-list" onClick={toggleMenu} selected={selectedChild}>
              Admin List
            </SidebarItem>
            <SidebarItem slug="user-catalog" onClick={toggleMenu} selected={selectedChild}>
              User Catalog
            </SidebarItem>
          </>
        )}

        {selectedMenu === "inventory" && (
          <>
            <SidebarItem slug="dashboard" onClick={toggleMenu} selected={selectedChild}>
              Dashboard
            </SidebarItem>
            <SidebarItem slug="commodity/catalog" onClick={toggleMenu} selected={selectedChild}>
              Comodity Catalog
            </SidebarItem>
            <SidebarItem
              slug="commodity/flow-history"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Comodity Flow History
            </SidebarItem>
            <SidebarItem
              slug="transactional-activities"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Transactional Activities
            </SidebarItem>
          </>
        )}

        {selectedMenu === "logistik" && (
          <>
            <SidebarItem slug="inbound" onClick={toggleMenu} selected={selectedChild}>
              Inbound
            </SidebarItem>
            <SidebarItem slug="outbound" onClick={toggleMenu} selected={selectedChild}>
              Outbound
            </SidebarItem>
            <SidebarItem slug="mutasi-stok" onClick={toggleMenu} selected={selectedChild}>
              Mutasi Stok
            </SidebarItem>
          </>
        )}

        {selectedMenu === "finance" && (
          <>
            <SidebarItem slug="dashboard" onClick={toggleMenu} selected={selectedChild}>
              Dashboard
            </SidebarItem>
            <SidebarItem
              slug="purchasing-transaction"
              onClick={toggleMenu}
              selected={selectedChild}
            >
              Purchasing Transaction
            </SidebarItem>
            <SidebarItem slug="sales-transaction" onClick={toggleMenu} selected={selectedChild}>
              Sales Transaction
            </SidebarItem>
            <SidebarItem slug="#" onClick={toggleMenu} selected={selectedChild}>
              Pengaturan Biaya Operasional
            </SidebarItem>
            <SidebarItem slug="cancellation" onClick={toggleMenu} selected={selectedChild}>
              Pembatalan Transaksi
            </SidebarItem>
          </>
        )}

        {selectedMenu === "layanan pelanggan" && (
          <>
            <SidebarItem slug="purchasing-support" onClick={toggleMenu} selected={selectedChild}>
              purchasing support
            </SidebarItem>
            <SidebarItem slug="sales-support" onClick={toggleMenu} selected={selectedChild}>
              Sales Support
            </SidebarItem>
            <SidebarItem slug="technical-support" onClick={toggleMenu} selected={selectedChild}>
              Technical Support
            </SidebarItem>
          </>
        )}

        {selectedMenu === "kontrol" && (
          <>
            <SidebarItem slug="inbound-qc" onClick={toggleMenu} selected={selectedChild}>
              Inbound QC
            </SidebarItem>
            <SidebarItem slug="outbound-qc" onClick={toggleMenu} selected={selectedChild}>
              Outbound QC
            </SidebarItem>
          </>
        )}
      </ListGroup>
    </nav>
  );
}
