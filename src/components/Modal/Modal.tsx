import React from "react";
import { Modal } from "reactstrap";

interface IModal extends React.HTMLAttributes<HTMLElement> {
  [key: string]: any;
  isOpen: boolean;
  children?: React.ReactNode;
  autoFocus?: boolean;
  size?: "sm" | "md" | "lg" | "auto";
  toggle?: React.KeyboardEventHandler<any> | React.MouseEventHandler<any>;
  backdrop?: boolean | "static";
  scrollable?: boolean;
  className?: string;
  fade?: boolean;
  centered?: boolean;
  width?: number;
}

export default ({
  toggle,
  isOpen,
  backdrop,
  className = "",
  children,
  centered = true,
  fade,
  scrollable,
  size = "md",
}: IModal) => {
  return (
    <Modal
      backdrop={backdrop}
      centered={centered}
      fade={fade}
      scrollable={scrollable}
      className={`custom-modal custom-modal-${size} ${className}`}
      toggle={toggle}
      isOpen={isOpen}
    >
      {children}
    </Modal>
  );
};
