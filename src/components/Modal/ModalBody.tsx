import React, { ReactNode } from "react";
import { ModalBody } from "reactstrap";

interface IModal {
  className?: string;
  children: ReactNode;
}

export default ({ className, children }: IModal) => {
  return <ModalBody className={className}>{children}</ModalBody>;
};
