import React, { ReactNode } from "react";
import { ModalHeader } from "reactstrap";

interface IModal {
  accent: boolean;
  className: string;
  children: ReactNode;
}

export default ({ accent = false, className = "", children }: IModal) => {
  let accentStyle = "";
  if (accent) accentStyle = "modal-header-accent ";
  return (
    <ModalHeader tag="div" className={`${accentStyle}${className}`}>
      {children}
    </ModalHeader>
  );
};
