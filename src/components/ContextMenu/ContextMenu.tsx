import React from "react";

export default ({
  children,
  showMenu,
  yPos,
  xPos,
}: {
  children: React.ReactNode;
  showMenu: boolean;
  yPos: string;
  xPos: string;
}) => {
  return showMenu ? (
    <div className="context-menu-container" style={{ top: yPos, left: xPos }}>
      {children}
    </div>
  ) : (
    <></>
  );
};
