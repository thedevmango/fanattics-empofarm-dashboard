import { useCallback, useEffect, useState } from "react";

export default (data?: any) => {
  const [xPos, setXPos] = useState("0px");
  const [yPos, setYPos] = useState("0px");
  const [showMenu, setShowMenu] = useState(false);
  const [contextData, setContextData] = useState<any>(undefined);

  const handleClick = useCallback(() => {
    if (showMenu) {
      setShowMenu(false);
      setContextData(undefined);
    }
  }, [showMenu]);

  const handleContextMenu = useCallback(
    (e) => {
      e.preventDefault();

      setXPos(`${e.pageX}px`);
      setYPos(`${e.pageY}px`);
      setShowMenu(true);
      setContextData(data);
    },
    [setXPos, setYPos, data]
  );

  useEffect(() => {
    document.addEventListener("click", handleClick);
    document.addEventListener("contextmenu", handleContextMenu);
    return () => {
      document.addEventListener("click", handleClick);
      document.removeEventListener("contextmenu", handleContextMenu);
    };
  });

  return { xPos, yPos, showMenu, contextData };
};
