import React, { ReactNode, CSSProperties } from "react";
import { useHistory } from "react-router-dom";

interface IButton extends React.HTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  disabled?: boolean;
  bold?: boolean;
  active?: boolean;
  to?: string;
  onClick?: any;
  theme?:
    | "primary"
    | "secondary"
    | "danger"
    | "text"
    | "icon"
    | "action"
    | "link"
    | "action-table"
    | "icon-only";
  className?: string;
  containerStyle?: CSSProperties;
  block?: boolean;
  split?: boolean;
}

export default ({
  active,
  bold,
  children,
  className = "",
  containerStyle = {},
  disabled,
  onClick = () => null,
  // textStyle,
  theme,
  to,
  split,
  block,
  ...props
}: IButton) => {
  const history = useHistory();

  return (
    <button
      {...props}
      className={`component-btn component-btn-${theme} ${className} ${bold ? "bold" : ""} ${
        disabled ? "disabled" : ""
      } ${active ? "active" : ""} ${split ? "component-btn-split" : ""}`}
      style={containerStyle}
      onClick={to ? () => history.push(to) : onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};
