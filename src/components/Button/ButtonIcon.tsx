import React, { ReactNode, CSSProperties } from "react";

interface IButtonIcon {
  children: ReactNode;
  split?: boolean;
  type?: "prepend" | "append" | "split";
  className?: string;
  style?: CSSProperties;
  theme?: "primary" | "secondary";
}

export default ({ children, className, type, style, theme }: IButtonIcon) => {
  return (
    <span
      style={{ ...style }}
      className={`component-btn-icon component-btn-${theme} component-btn-icon-${type} ${className}`}
    >
      <div className="component-btn-icon-split-border" />
      {children}
    </span>
  );
};
