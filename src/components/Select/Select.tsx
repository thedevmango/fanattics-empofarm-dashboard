import React, { ReactNode } from 'react'

interface ISelect extends React.HTMLAttributes<HTMLSelectElement>{
    children: ReactNode;
    disabled?: boolean;
    active?: boolean;
    className?: string;
}

export default ({ 
    children, 
    active,
    className,
    disabled,
    ...props
}: ISelect) => { 
    return(
        <select {...props} className={`component-select ${className}`} >{children}</select>
    )
}