import React from "react";
import { useHistory } from "react-router-dom";
import { BsChevronDown } from "react-icons/bs";
import "../../../assets/styles/components/_button.scss";

export default ({
  id,
  children,
  disabled = false,
  width,
  bold,
  active,
  to = undefined,
  onClick = undefined,
  theme,
  className,
  icon = false,
  append = false,
  dropdown = false,
  containerStyle,
  textStyle,
  iconStyle,
}) => {
  const history = useHistory();

  const themeStyle = (theme) => {
    let classname = "btn ";
    switch (theme) {
      case "danger":
        classname += "btn-danger ";
        break;
      case "primary":
        classname += "btn-primary ";
        break;
      case "secondary":
        classname += "btn-secondary ";
        break;
      case "link":
        classname += "btn-link ";
        break;
      case "pivot-stack":
        classname += "btn-pivot-stack ";
        break;
      default:
        break;
    }
    if (className) {
      classname += `${className} `;
    }
    if (active) {
      classname += "active ";
    }
    return classname;
  };

  return (
    <button
      id={id}
      disabled={disabled}
      onClick={onClick ? onClick : () => (to ? history.push(to) : null)}
      style={{ width, ...containerStyle }}
      className={themeStyle(theme)}>
      {icon ? (
        <span className="prepend" style={{ ...iconStyle }}>
          {icon}
        </span>
      ) : null}
      <span style={{ fontWeight: bold ? 600 : 400, ...textStyle }} className="text">
        {children}
      </span>
      {dropdown ? (
        <span className="dropdown">
          <BsChevronDown strokeWidth={1} />
        </span>
      ) : null}
      {append ? <span className="append">{append}</span> : null}
    </button>
  );
};
