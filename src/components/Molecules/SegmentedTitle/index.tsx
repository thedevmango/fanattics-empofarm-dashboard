import React from "react";
import { PaneHeader } from "../../Typography";

interface ISegmentedTitle {
  activeTitle: string;
  className?: string;
  location?: any;
  onClick: any;
  slug?: string[];
  titles: string[];
}

export default ({
  activeTitle,
  className = "",
  location,
  onClick,
  slug,
  titles,
}: ISegmentedTitle) => {
  return (
    <div className={`component-segmented-title ${className}`}>
      {titles.map((title, idx) => {
        return (
          <PaneHeader
            key={idx}
            onClick={() => (slug ? onClick({ value: slug[idx] }) : onClick({ value: title }))}
            bold={slug ? location.pathname.includes(slug[idx]) : activeTitle === title}
            accent={slug ? location.pathname.includes(slug[idx]) : activeTitle === title}
            className="title-item"
          >
            {title}
          </PaneHeader>
        );
      })}
    </div>
  );
};
