import React from "react";
import { BsDownload } from "react-icons/bs";
import { FileDownload } from "../../../configs/FileDownload";
import { Button } from "../../Button";
import { BodyText, MetadataText, SubjectTitle } from "../../Typography";

interface IDataTransaksiItem {
  accent?: boolean;
  children: React.ReactNode;
  url?: string;
  title: string;
}

export default ({ accent = false, children, url = "", title }: IDataTransaksiItem) => {
  const downloadInvoice = () => {
    const filename = (children || "filename") + ".pdf";
    FileDownload(url, filename);
  };

  return (
    <>
      <div className="info-title">
        <MetadataText color="#6E6E6E">{title}</MetadataText>
      </div>
      <div className="info-value">
        {!url ? (
          <SubjectTitle color={accent ? "#69984C" : "#292929"}>{children}</SubjectTitle>
        ) : (
          <div className="d-flex align-items-center">
            <BodyText accent>{children || "-"}</BodyText>
            <Button onClick={downloadInvoice} containerStyle={{ padding: 5 }} theme="icon-only">
              <BsDownload size={12} />
            </Button>
          </div>
        )}
      </div>
    </>
  );
};
