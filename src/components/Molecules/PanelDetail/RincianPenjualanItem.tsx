/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { formatquantity } from "../../../configs/formatnumber";
import { BodyText, HeaderText } from "../../Typography";
import { Card } from "../../Card";
import LaporanInventoryManager from "./LaporanInventoryManager";

interface IRincianPenjualanItem {
  data: any;
  type: "i_courier" | "i_qc" | "o_courier" | "o_qc" | "sales";
}

export default ({ data, type = "i_courier" }: IRincianPenjualanItem) => {
  return data.map(
    (
      item: any,
      // : {
      //   imageUrl: any;
      //   productName: string;
      //   sku: string;
      //   farmerName: string;
      //   packagingType: string;
      //   quantity: any;
      //   pricePerQuantity: any;
      //   totalPrice: any;
      //   packagingCost: any;
      //   inventoryReport: any;
      // },
      i: any
    ) => {
      return (
        <Card key={i} className="rincian-penjualan-item">
          <div className="body">
            <div className="image-wrapper">
              <img style={{ width: 84, height: 84, objectFit: 'cover' }} src={item.product_sku.product_variant.imageUrl} alt="product" />
            </div>

            <div className="product-info">
              <div className="product-info-title">
                <HeaderText accent bold>
                  {item.product_sku.product_variant.productNam}
                </HeaderText>
              </div>
              <div className="product-info-details">
                <div className="details-item">
                  <BodyText color="#605E5C">SKU</BodyText>
                  <BodyText color="#323130" bold>
                    {item.product_sku.sku}
                  </BodyText>
                </div>
                <div className="details-item">
                  <BodyText color="#605E5C">Farmer</BodyText>
                  <BodyText color="#323130" bold>
                    {item.product_sku.farmer.user.fullName}
                  </BodyText>
                </div>
                <div className="details-item">
                  <BodyText color="#605E5C">Pilihan Packaging</BodyText>
                  <BodyText color="#323130" bold>
                    {item.packagingUnit}
                  </BodyText>
                </div>
                <div className="details-item">
                  <BodyText color="#605E5C">Total Pembelian</BodyText>
                  <BodyText color="#323130" bold>
                    {formatquantity(item.quantity)}&nbsp;Kg
                  </BodyText>
                </div>
              </div>

              {type === "o_courier" || type === "o_qc" ? null : (
                <div className="product-info-details">
                  <div className="details-item">
                    <BodyText color="#605E5C">Harga per Satuan</BodyText>
                    <BodyText color="#323130" bold>
                      Rp&nbsp;{formatquantity(item.pricePerQuantity)}/Kg
                    </BodyText>
                  </div>
                  <div className="details-item">
                    <BodyText color="#605E5C">Harga Total</BodyText>
                    <BodyText color="#323130" bold>
                      Rp&nbsp;{formatquantity(item.totalPrice)}
                    </BodyText>
                  </div>
                  <div className="details-item">
                    <BodyText color="#605E5C">Harga Packaging</BodyText>
                    <BodyText color="#323130" bold>
                      Rp&nbsp;{formatquantity(item.packagingCost)}/{item.packagingType}
                    </BodyText>
                  </div>
                </div>
              )}
            </div>
          </div>

          <div className="accordion">
            {/*  */}
            {/* <LaporanInventoryManager data={item} /> */}
          </div>
        </Card>
      );
    }
  );
};
