import React from "react";
import { MetadataText, SubjectTitle } from "../../Typography";
import { Button } from "../../Button";
import { BsDownload } from "react-icons/bs";

interface IDataTenderItem {
  accent?: boolean;
  children: React.ReactNode;
  onClick?: any;
  title: string;
}

export default ({ accent = false, children, onClick, title }: IDataTenderItem) => {
  return (
    <>
      <div className="info-title">
        <MetadataText color="#6E6E6E">{title}</MetadataText>
      </div>
      <div className="info-value">
        <SubjectTitle color={accent ? "#69984C" : "#292929"}>{children}</SubjectTitle>
        {!onClick ? null : (
          <Button onClick={onClick} theme="action">
            <BsDownload />
          </Button>
        )}
      </div>
    </>
  );
};
