import React from "react";
import { Panel } from "../../Panel";
import { PaneHeader, GreetingTitle } from "../../Typography";
import { formatquantity } from "../../../configs/formatnumber";

interface IRincianTender {
  children: React.ReactNode;
  className?: string;
  cover: any;
  productName: string;
  totalNominal?: number;
}

export default ({ children, className = "", cover, totalNominal, productName }: IRincianTender) => {
  return (
    <Panel padding="24" className={`mol-rincian-tender ${className}`}>
      <PaneHeader className="mb-4" color="#404040" bold>
        Rincian Tender
      </PaneHeader>

      <div className="content">
        <div className="image-wrapper">
          <img src={cover} alt="product" />
        </div>

        <div className="tender-info">
          <PaneHeader className="col pl-0 pb-3" color="#404040">
            {productName}
          </PaneHeader>
          {children}
        </div>
      </div>

      <div className="total-nominal-tender">
        <PaneHeader color="#605E5C">Total Harga Negosiasi</PaneHeader>
        <GreetingTitle accent>
          {totalNominal ? `Rp ${formatquantity(totalNominal)}` : <>&ndash;</>}
        </GreetingTitle>
      </div>
    </Panel>
  );
};
