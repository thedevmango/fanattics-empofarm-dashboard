import React from "react";
import { Link } from "react-router-dom";
import { Panel } from "../../Panel";
import { BodyText, HeaderText, MetadataText } from "../../Typography";
import { Colors } from "../../../constants";

interface IUpdateStatusTrx {
  children: React.ReactNode;
  timestamp?: string;
  className?: string;
  link?: any;
}

export default ({ children, timestamp, className, link }: IUpdateStatusTrx) => {
  return (
    <Panel padding="16" className={`mol-update-status ${className}`}>
      <div className="content">
        <BodyText color={Colors.gray400}>Update Status Transaksi</BodyText>
        <HeaderText style={{ marginTop: 5 }}>{children}</HeaderText>
      </div>
      <div className="content">
        <MetadataText color={Colors.gray500}>{timestamp?.split(" ")[1]}</MetadataText>
        <MetadataText color={Colors.gray500}>{timestamp?.split(" ")[0]}</MetadataText>
        {!link ? null : (
          <Link to={link}>
            <MetadataText className="see-detail" color="#00BCF2">
              Lihat Detail
            </MetadataText>
          </Link>
        )}
      </div>
    </Panel>
  );
};
