import React from "react";
import { Panel } from "../../Panel";
import { PaneHeader } from "../../Typography";

interface IDataTransaksi {
  children: React.ReactNode;
  className?: string;
}

export default ({ children, className = "" }: IDataTransaksi) => {
  return (
    <Panel padding="24" className={`mol-data-transaksi ${className}`}>
      <div style={{ borderBottomWidth: 0, paddingBottom: 0 }} className="header">
        <PaneHeader color="#404040" bold>
          Data Transaksi
        </PaneHeader>
      </div>

      <div style={{ paddingTop: 0 }} className="content">
        {children}
      </div>
    </Panel>
  );
};
