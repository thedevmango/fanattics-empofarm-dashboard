import React from "react";
import { Panel } from "../../Panel";
import { PaneHeader } from "../../Typography";

interface IDataTender {
  children: React.ReactNode;
  className?: string;
}

export default ({ children, className = "" }: IDataTender) => {
  return (
    <Panel padding="24" className={`mol-data-tender ${className}`}>
      <div className="header">
        <PaneHeader color="#404040" bold>
          Data Tender
        </PaneHeader>
      </div>

      <div className="content">{children}</div>
    </Panel>
  );
};
