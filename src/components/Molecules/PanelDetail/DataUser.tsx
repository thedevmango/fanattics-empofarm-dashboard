import React from "react";
import { Panel } from "../../Panel";
import { HeaderText, MetadataText, SubjectTitle } from "../../Typography";

interface IDataUser {
  address: string;
  className?: string;
  contact: string;
  name: string;
  type: "Farmer" | "Seller" | "Buyer";
}

export default ({ address, className, contact, name, type }: IDataUser) => {
  return (
    <Panel padding="24" className={`mol-data-user ${className}`}>
      <div className="header">
        <HeaderText color="#404040" bold>
          {`Data ${type}`}
        </HeaderText>
      </div>

      <div className="content">
        <div className="info-title">
          <MetadataText bold>{`Nama ${type}`}</MetadataText>
        </div>
        <div className="info-value">
          <SubjectTitle accent>{name}</SubjectTitle>
        </div>

        <div className="info-title">
          <MetadataText bold>Nomor Kontak</MetadataText>
        </div>
        <div className="info-value">
          <SubjectTitle color="#323130">{contact}</SubjectTitle>
        </div>

        <div className="info-title">
          <MetadataText bold>{`Alamat ${type}`}</MetadataText>
        </div>
        <div className="info-value">
          <SubjectTitle color="#323130">{address}</SubjectTitle>
        </div>
      </div>
    </Panel>
  );
};
