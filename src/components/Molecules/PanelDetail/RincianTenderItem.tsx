import React from "react";
import { BodyText, HeaderText } from "../../Typography";

interface ITenderItem {
  className?: string;
  title: string;
  children: any;
}

export default ({ className, title, children }: ITenderItem) => {
  return (
    <div className={`tender-item ${className}`}>
      <BodyText className="tender-item-title" color="#404040" bold>
        {title}
      </BodyText>
      <HeaderText className="tender-item-value" color="#303030">
        {children}
      </HeaderText>
    </div>
  );
};
