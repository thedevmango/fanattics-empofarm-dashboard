import React from "react";
import { Panel } from "../../Panel";
import { PaneHeader, GreetingTitle, HeaderText, BodyText, SubjectTitle } from "../../Typography";
import { formatquantity } from "../../../configs/formatnumber";

interface IRincianPenjualan {
  children: React.ReactNode;
  className?: string;
  insuranceFee?: number;
  packagingCost?: number;
  shippingCost?: number;
  totalNominal?: number;
  type: "i_courier" | "i_qc" | "o_courier" | "o_qc";
}

export default ({
  children,
  className = "",
  insuranceFee,
  packagingCost,
  shippingCost,
  totalNominal,
  type = "i_courier"
}: IRincianPenjualan) => {
  return (
    <Panel padding="24" className={`mol-rincian-penjualan ${className}`}>
      <div className="header">
        <HeaderText color="#6E6E6E" bold>
          Rincian Penjualan
        </HeaderText>
      </div>

      <div className="content">{children}</div>

      {type === "o_courier" || type === "o_qc" ? null : (
        <div className="footer">
          <div className="biaya-tambahan">
            <div className="title">
              <HeaderText color="#000000">Biaya Tambahan</HeaderText>
            </div>
            <div className="value">
              <div className="value-item">
                <BodyText color="#605E5C">Biaya Packaging</BodyText>
                <SubjectTitle color="#323130">
                  {!packagingCost ? <>&ndash;</> : `Rp ${formatquantity(packagingCost)}`}
                </SubjectTitle>
              </div>
              <div className="value-item">
                <BodyText color="#605E5C">Asuransi</BodyText>
                <SubjectTitle color="#323130">
                  {!insuranceFee ? <>&ndash;</> : `Rp ${formatquantity(insuranceFee)}`}
                </SubjectTitle>
              </div>
              <div className="value-item">
                <BodyText color="#605E5C">Biaya Pengiriman</BodyText>
                <SubjectTitle color="#323130">
                  {!shippingCost ? <>&ndash;</> : `Rp ${formatquantity(shippingCost)}`}
                </SubjectTitle>
              </div>
            </div>
          </div>

          <div className="total-nominal-transaksi">
            <div className="title">
              <PaneHeader color="#605E5C">Total Nominal Penjualan</PaneHeader>
            </div>
            <div className="value">
              <GreetingTitle bold accent>
                {!totalNominal ? <>&ndash;</> : `Rp ${formatquantity(totalNominal)}`}
              </GreetingTitle>
            </div>
          </div>
        </div>
      )}
    </Panel>
  );
};
