import React from "react";
import { Button } from "../../Button";
import { Panel } from "../../Panel";
import { MetadataText } from "../../Typography";

interface Props {
  className?: string;
  onDownload?: () => any;
  source?: string;
}

export default ({ className = "", onDownload, source }: Props) => {
  return (
    <Panel className={`mol-qr-download ${className}`}>
      <div className="qr-wrapper">
        {!source ? (
          <div className="qr-image-dummy" />
        ) : (
          <img src={source} alt="qr-img" className="qr-image" />
        )}
      </div>
      <Button onClick={onDownload} theme="link">
        <MetadataText accent>Download</MetadataText>
      </Button>
    </Panel>
  );
};
