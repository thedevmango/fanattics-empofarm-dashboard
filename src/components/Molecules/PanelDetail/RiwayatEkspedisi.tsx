import React from "react";
import moment from "moment";
import { Panel } from "../../Panel";
import { PaneHeader, SubjectTitle, MetadataText, BodyText } from "../../Typography";
import { gray500, gray400, gray800 } from "../../../constants/Colors";
import { enumDeliveryInbound } from "../../../configs/enums";

interface IRiwayatEkspedisi {
  className?: string;
  courrierOfficer?: string;
  data: any;
}

export default ({ className, courrierOfficer, data }: IRiwayatEkspedisi) => {
  return (
    <Panel padding="24" className={`mol-riwayat-ekspedisi ${className}`}>
      <PaneHeader bold color={gray500}>
        Riwayat Status Ekspedisi
      </PaneHeader>
      <div className="officer-name">
        <SubjectTitle color={gray400}>Petugas Kurir :</SubjectTitle>
        &nbsp;&nbsp;
        <SubjectTitle color={gray400}>{!data ? "" : data.courierName}</SubjectTitle>
      </div>

      {!data
        ? null
        : data.logs.map((item: any, i: number) => {
                return (
                  <div key={i} className="content">
                    <div className="bullet" />
                    <div className="riwayat-ekspedisi-item">
                      <div className="item-info">
                        <SubjectTitle color={gray800} bold>
                          {enumDeliveryInbound(item.deliveryStatus)}
                        </SubjectTitle>
                        <SubjectTitle color={gray800} bold>
                          &nbsp;&mdash;&nbsp;
                        </SubjectTitle>
                        <SubjectTitle color={gray800}>{data.courierName}</SubjectTitle>
                      </div>
                      {!item.description ? null : (
                        <div className="item-description">
                          <BodyText color={gray500}>{item.description}</BodyText>
                        </div>
                      )}
                      <div className="item-timestamp">
                        <MetadataText color={gray500}>
                          {moment(item.createdAt).format("DD MMMM YYYY, H:mm")}&nbsp;WIB
                        </MetadataText>
                      </div>
                    </div>
                  </div>
                );
                  })}
    </Panel>
  );
};
