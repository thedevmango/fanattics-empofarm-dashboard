/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useRef, useState } from "react";
import { FiChevronDown, FiChevronRight } from "react-icons/fi";
import { formatquantity } from "../../../configs/formatnumber";
import Collapse from "../../Collapse";
import { Panel } from "../../Panel";
import { BodyText, HeaderText, MetadataText } from "../../Typography";

interface ILaporanInventoryManager {
  data: any;
}

export default ({ data }: ILaporanInventoryManager) => {
  const [isCollapsed, setIsCollapsed] = useState(false);
  const toggleCollapse = () => setIsCollapsed(!isCollapsed);

  // const handleClickOutside = (e: Event) => {
  //   if (node.current && !node.current.contains(e.target as Node)) {
  //     setIsCollapsed(false);
  //   }
  // };

  // useEffect(() => {
  //   if (isCollapsed) {
  //     document.addEventListener("mousedown", handleClickOutside);
  //   } else {
  //     document.removeEventListener("mousedown", handleClickOutside);
  //   }

  //   return () => {
  //     document.removeEventListener("mousedown", handleClickOutside);
  //   };
  // }, [isCollapsed]);

  return (
    <div className="collapsible-info">
      <div className="collapse-header" onClick={toggleCollapse}>
        <BodyText>Laporan Inventory Manager</BodyText>
        {isCollapsed ? <FiChevronDown /> : <FiChevronRight />}
      </div>
      <Collapse isOpen={isCollapsed}>
        <div className="collapse-wrapper">
          <div className="collapse-body">
            {data.inventoryReport.realStockQuantity && data.inventoryReport.realStockStatus ? (
              <Panel className="collapse-body-content">
                <div className="stock-info stock-info-percentage">
                  <BodyText bold color="#000">
                    {Math.round((data.inventoryReport.realStockQuantity / data.quantity) * 100)}%
                  </BodyText>
                </div>
                <div className="stock-info stock-info-stock">
                  <BodyText color="#6E6E6E">Stok Real</BodyText>
                  <HeaderText color="#292929">
                    {formatquantity(data.inventoryReport.realStockQuantity)}
                    &nbsp;Kg
                  </HeaderText>
                </div>
                <div className="stock-info stock-info-status">
                  <MetadataText
                    color={data.inventoryReport.realStockStatus === "paid" ? "#59BA53" : "#E05454"}
                  >
                    {data.inventoryReport.realStockStatus === "paid"
                      ? "Sudah Dibayar"
                      : data.inventoryReport.realStockStatus === "unpaid"
                      ? "Belum Dibayar"
                      : null}
                  </MetadataText>
                </div>
              </Panel>
            ) : null}
          </div>

          <div className="collapse-body">
            {data.inventoryReport.consigmentStockQuantity &&
            data.inventoryReport.consigmentStockStatus ? (
              <Panel className="collapse-body-content">
                <div className="stock-info stock-info-percentage">
                  <BodyText bold color="#000">
                    {Math.round(
                      (data.inventoryReport.consigmentStockQuantity / data.quantity) * 100
                    )}
                    %
                  </BodyText>
                </div>
                <div className="stock-info stock-info-stock">
                  <BodyText color="#6E6E6E">Stok Konsinyasi</BodyText>
                  <HeaderText color="#292929">
                    {formatquantity(data.inventoryReport.consigmentStockQuantity)}
                    &nbsp;Kg
                  </HeaderText>
                </div>
                <div className="stock-info stock-info-status">
                  <MetadataText
                    color={
                      data.inventoryReport.consigmentStockStatus === "paid" ? "#59BA53" : "#E05454"
                    }
                  >
                    {data.inventoryReport.consigmentStockStatus === "paid"
                      ? "Sudah Dibayar"
                      : data.inventoryReport.consigmentStockStatus === "unpaid"
                      ? "Belum Dibayar"
                      : null}
                  </MetadataText>
                </div>
              </Panel>
            ) : null}
          </div>
        </div>
      </Collapse>
    </div>
  );
};
