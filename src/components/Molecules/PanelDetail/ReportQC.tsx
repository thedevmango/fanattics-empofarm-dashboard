import React, { useState } from "react";
import { Panel } from "../../Panel";
import { HeaderText, MetadataText, BodyText, SubjectTitle } from "../../Typography";
import { gray500, gray700 } from "../../../constants/Colors";
import { BsChevronDown, BsChevronRight } from "react-icons/bs";
import Collapse from "../../Collapse";
import { Button } from "../../Button";

interface IReportQC {
  children?: React.ReactNode;
  className?: string;
  containerStyle?: React.CSSProperties;
  description?: string;
  // isCollapsed?: boolean;
  itemName?: string;
  officerName?: string;
  // toggleCollapse?: any;
  handleDownloadReport?: any;
  handlePickOfficer?: any;
  type: "Inbound" | "Outbound";
  qcPassed?: boolean;
}

export default ({
  children,
  className,
  containerStyle = {},
  description,
  // isCollapsed = false,
  itemName,
  officerName,
  // toggleCollapse,
  handleDownloadReport,
  handlePickOfficer,
  type,
  qcPassed,
}: IReportQC) => {
  const [isCollapsed, setIsCollapsed] = useState(false);
  const toggleCollapse = () => setIsCollapsed(!isCollapsed);

  return (
    <Panel style={containerStyle} padding="24" className={`mol-qc-report ${className}`}>
      <HeaderText bold color={gray500}>
        {`Laporan ${type} QC`}
      </HeaderText>

      <div className="qc-officername">
        <MetadataText color={gray500}>Petugas QC&nbsp;&nbsp;:&nbsp;&nbsp;</MetadataText>
        {officerName ? <MetadataText color={gray500}>{officerName}</MetadataText> : null}
      </div>

      {type === "Outbound" ? null : type === "Inbound" && children ? (
        <div onClick={toggleCollapse} className="header header-collapse">
          <div className="title-wrapper">
            <BodyText>{itemName}</BodyText>
            {!qcPassed ? null : (
              <div className="qc-badge">
                <MetadataText accent>QC Passed</MetadataText>
              </div>
            )}
          </div>
          {isCollapsed ? <BsChevronDown /> : <BsChevronRight />}
        </div>
      ) : null}

      {type === "Outbound" && children ? (
        <>
          <div className="content content-dark">{children}</div>
          {!handleDownloadReport ? null : (
            <Button className="btn-download-qc-report" onClick={handleDownloadReport} theme="link">
              Download QC Report
            </Button>
          )}
          {!handlePickOfficer ? null : (
            <Button className="btn-pick-officer" onClick={handlePickOfficer} theme="primary">
              Pilih Petugas QC
            </Button>
          )}
        </>
      ) : children ? (
        <Collapse isOpen={isCollapsed}>
          <div className="content">
            {children}

            {description ? (
              <div className="mt-3 mb-4 d-flex flex-column">
                <BodyText color={gray500}>Keterangan</BodyText>
                <SubjectTitle className="mt-1" color={gray700}>
                  {description}
                </SubjectTitle>
              </div>
            ) : null}
          </div>
          {!handleDownloadReport ? null : (
            <Button className="btn-download-qc-report" onClick={handleDownloadReport} theme="link">
              Download QC Report
            </Button>
          )}
        </Collapse>
      ) : null}

      {!handlePickOfficer ? null : (
        <Button className="btn-pick-officer" onClick={handlePickOfficer} theme="primary">
          Pilih Petugas QC
        </Button>
      )}
    </Panel>
  );
};
