import React from "react";
import { MetadataText, BodyText, SubjectTitle } from "../../Typography";
import { GoVerified } from "react-icons/go";
import { gray500, gray700, gray800 } from "../../../constants/Colors";

interface IReportQCItem {
  signedBy: string;
  timestamp: string;
  title: "Quality" | "Quantity" | "Packaging" | "Document";
  valueActual?: string;
  valueOffer?: string;
  verified: boolean;
}

export default ({
  signedBy,
  timestamp,
  title,
  valueActual,
  valueOffer,
  verified,
}: IReportQCItem) => {
  return (
    <>
      <div className="mol-reportqc-item">
        <div className="info">
          <div className="info-title">
            <MetadataText color={gray500} className="mr-2" bold>
              {title}
            </MetadataText>
            {!verified ? null : <GoVerified color="#00B294" size={14} />}
          </div>
          <MetadataText className="info-signed-by mb-3" color={gray800}>
            {`Signed by: ${!verified ? "-" : signedBy}`}
          </MetadataText>
        </div>

        {!verified ? null : (
          <div className="timestamp">
            <MetadataText color={gray500}>{timestamp.split(" ")[1]}</MetadataText>
            <MetadataText color={gray500}>{timestamp.split(" ")[0]}</MetadataText>
          </div>
        )}
      </div>

      {!valueActual && !valueOffer ? null : (
        <div className="mol-reportqc-item-details">
          <div className="details-offer">
            <BodyText color={gray500}>Offer</BodyText>
            <SubjectTitle color={gray700}>{valueOffer}</SubjectTitle>
          </div>
          <div className="details-actual">
            <BodyText color={gray500}>Actual</BodyText>
            <SubjectTitle color={gray700}>{valueActual}</SubjectTitle>
          </div>
        </div>
      )}
    </>
  );
};
