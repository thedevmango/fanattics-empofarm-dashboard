import React from "react";
import { FiCheck } from "react-icons/fi";
import { Button } from "../../Button";
import { Modal, ModalBody } from "../../Modal";
import { BodyText, GreetingTitle, HeaderText } from "../../Typography";

interface IConfirmUploadedFile {
  className?: string;
  isLoading: boolean;
  isOpen: boolean;
  onDelete: any;
  onDeleteText?: string;
  onSave: any;
  onSaveText?: string;
  selectedFile: any;
  title?: string;
  toggle?: any;
}

export default ({
  className,
  isLoading,
  isOpen,
  onDelete,
  onDeleteText = "Hapus",
  onSave,
  onSaveText = "Simpan",
  selectedFile,
  toggle,
  title = "File Uploaded!",
}: IConfirmUploadedFile) => {
  return (
    <Modal toggle={toggle} isOpen={isOpen} size="md">
      <ModalBody className={`mol-popup-confirm-uploaded-file ${className}`}>
        <GreetingTitle accent>{title}</GreetingTitle>

        <div className="icon-wrapper">
          <FiCheck color="#69984C" strokeWidth={4} size={80} />
        </div>
        {!selectedFile ? null : (
          <HeaderText className="text-center">{selectedFile.path}</HeaderText>
        )}

        <div className="button-wrapper">
          <Button onClick={onDelete} theme="secondary" className="button-hapus">
            {isLoading ? (
              <div className="spinner-wrapper">
                <div role="status" className="spinner-border spinner-border-sm">
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ) : (
              <BodyText accent>{onDeleteText}</BodyText>
            )}
          </Button>
          <Button onClick={onSave} theme="primary" className="button-simpan">
            <BodyText color="#fff">{onSaveText}</BodyText>
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
