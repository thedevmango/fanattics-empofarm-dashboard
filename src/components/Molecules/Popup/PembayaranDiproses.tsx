import React from "react";
import { GreetingTitle, HeaderText } from "../../Typography";
import { Modal, ModalBody } from "../../Modal";
import { Button } from "../../Button";
import { primaryBrand } from "../../../constants/Colors";

interface Props {
  children: React.ReactNode;
  onClick: any;
  isOpen: boolean;
}

export default ({ isOpen, children, onClick }: Props) => {
  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalBody className="d-flex flex-column align-items-center pt-5">
        <GreetingTitle bold>Pembayaran Diproses!</GreetingTitle>

        <div className="mt-3 mb-5">
          <HeaderText className="text-center">
            Pembayaran dengan ID Transaksi&nbsp;
            <span style={{ color: primaryBrand }}>{children}</span>
            &nbsp;akan dilanjutkan oleh divisi Finance
          </HeaderText>
        </div>

        <Button onClick={onClick} theme="link">
          Tutup
        </Button>
      </ModalBody>
    </Modal>
  );
};
