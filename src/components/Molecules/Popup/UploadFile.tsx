import React from "react";
import { IoMdClose } from "react-icons/io";
import { formatquantity } from "../../../configs/formatnumber";
import { Colors } from "../../../constants";
import { Button, ButtonIcon } from "../../Button";
import { Input } from "../../Form";
import { Modal, ModalBody } from "../../Modal";
import { HeaderText, PaneHeader } from "../../Typography";

interface IUploadFile {
  children: React.ReactNode;
  className?: string;
  changeDate?: any;
  isOpen: boolean;
  onClose: any;
  onSubmit: any;
  onSubmitText?: string;
  selectedFile: any;
  title?: string;
  toggle?: any;
}

export default ({
  children,
  className,
  changeDate,
  isOpen,
  onClose,
  onSubmit,
  onSubmitText = "Upload",
  selectedFile,
  title = "Upload file",
  toggle,
}: IUploadFile) => {
  return (
    <Modal toggle={toggle} isOpen={isOpen} size="lg">
      <ModalBody className={`mol-popup-upload-file ${className}`}>
        {toggle ? null : (
          <div className="button-wrapper">
            <Button onClick={onClose} className="button-close">
              <ButtonIcon>
                <IoMdClose size={16} />
              </ButtonIcon>
            </Button>
          </div>
        )}

        <PaneHeader color={Colors.gray500}>{title}</PaneHeader>

        <div className="mt-3 py-2 d-flex flex-column flex-wrap align-items-center w-75">
          {selectedFile ? (
            <HeaderText className="text-center">
              {selectedFile.path} &mdash; {formatquantity(selectedFile.size)} bytes
            </HeaderText>
          ) : (
            children
          )}
        </div>

        {changeDate ? (
          <div className="mt-3 mb-1 d-flex align-items-center justify-content-center">
            <Input
              label="Due Date"
              containerStyle={{ width: 250 }}
              type="date"
              onChange={(e: any) => changeDate(e.target.value)}
            />
          </div>
        ) : null}

        <div className="my-3">
          <Button disabled={!selectedFile} onClick={onSubmit} theme="primary">
            {onSubmitText}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
