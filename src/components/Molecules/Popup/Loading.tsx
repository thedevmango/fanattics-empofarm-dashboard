import React from "react";
import { Modal, ModalBody } from "../../Modal";
import { GreetingTitle, PaneHeader } from "../../Typography";

interface IPopupLoading {
  className?: string;
  isOpen: boolean;
  title?: string;
}

export default ({ className, isOpen, title = "Mengupload File" }: IPopupLoading) => {
  return (
    <Modal isOpen={isOpen} size="md">
      <ModalBody className={`mol-popup-loading ${className}`}>
        <GreetingTitle accent>{title}</GreetingTitle>

        <div className="spinner-wrapper">
          <div role="status" className="spinner-border">
            <span className="sr-only">Loading...</span>
          </div>
        </div>

        <PaneHeader>Harap tunggu beberapa saat</PaneHeader>
        <PaneHeader>karena kami sedang</PaneHeader>
        <PaneHeader>memproses</PaneHeader>
      </ModalBody>
    </Modal>
  );
};
