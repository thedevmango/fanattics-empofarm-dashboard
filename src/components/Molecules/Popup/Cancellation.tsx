/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { Button } from "../../Button";
import { Modal, ModalBody } from "../../Modal";
import { BodyText, SubjectTitle } from "../../Typography";
import Spinner from "../Button/Spinner";

interface Props {
  isOpen: boolean;
  loading?: boolean;
  onSubmit: any;
  toggle: any;
}

export default ({ isOpen, loading, onSubmit, toggle }: Props) => {
  const [maxCount, setMaxCount] = useState(400);
  const [value, setValue] = useState("");

  function handleChange(e: any) {
    setValue(e.target.value);
  }
  function textColor(val: number) {
    if (val <= maxCount) return "#C8C8C8";
    return "#CB3B0E";
  }
  function stringCounter(val: number) {
    if (!value) return 0;
    if (val <= maxCount) return val;
    return maxCount - val;
  }

  function onCancel() {
    setValue("");
    toggle();
  }

  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalBody className="modal-cancellation">
        <SubjectTitle bold>Informasi Permintaan Pembatalan</SubjectTitle>

        <div className="form-textarea">
          <textarea
            onChange={handleChange}
            name="remarks"
            id="remarks-cancel"
            rows={15}
            placeholder=""
          />

          <div className="text-counter">
            <BodyText color={textColor(value.length)}>
              {`${stringCounter(value.length)}/${maxCount} Characters`}
            </BodyText>
          </div>
        </div>

        <div className="button-wrapper">
          <Button
            onClick={onCancel}
            theme="secondary"
            containerStyle={{ width: 125, marginLeft: "auto" }}
          >
            Cancel
          </Button>
          <Button
            disabled={value.length > maxCount}
            onClick={() => onSubmit(value)}
            theme="primary"
            containerStyle={{ width: 125, marginLeft: 16 }}
          >
            {loading ? <Spinner /> : "Submit"}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
