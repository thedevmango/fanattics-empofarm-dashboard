import React from "react";
import { IoIosRadioButtonOff } from "react-icons/io";
import { Button, ButtonIcon } from "../../Button";
import Spinner from "./Spinner";

export default ({ loading = false, onClick }: { loading?: boolean; onClick?: () => any }) => {
  return (
    <Button onClick={onClick} containerStyle={{ minWidth: 245 }} theme="primary">
      DOWNLOAD HALAMAN INI
      <ButtonIcon type="append">{loading ? <Spinner /> : <IoIosRadioButtonOff />}</ButtonIcon>
    </Button>
  );
};
