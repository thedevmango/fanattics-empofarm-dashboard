import React from "react";
import { useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import { Colors } from "../../../constants";
import { Button, ButtonIcon } from "../../Button";

interface IBackButton {
  className?: string;
  children: React.ReactNode;
  onClick?: any;
}

export default ({ className = "", children, onClick }: IBackButton) => {
  const history = useHistory();
  return (
    <Button
      className={`back-button ${className}`}
      onClick={onClick ? onClick : () => history.goBack()}
      theme="text"
    >
      <ButtonIcon type="prepend">
        <FiArrowLeft color={Colors.primaryBrand} />
      </ButtonIcon>
      {children}
    </Button>
  );
};
