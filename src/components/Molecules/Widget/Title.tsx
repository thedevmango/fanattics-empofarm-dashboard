import React from "react";
import { HeaderText, SubjectTitle, BodyText } from "../../Typography";

interface ITitle {
  bold?: boolean;
  children: React.ReactNode;
  size: "lg" | "md" | "sm";
}

export default ({ bold, children, size }: ITitle) => {
  return (
    <div className="widget-header-title">
      {size === "lg" ? (
        <HeaderText bold={bold}>{children}</HeaderText>
      ) : size === "md" ? (
        <SubjectTitle bold={bold}>{children}</SubjectTitle>
      ) : (
        <BodyText bold={bold}>{children}</BodyText>
      )}
    </div>
  );
};
