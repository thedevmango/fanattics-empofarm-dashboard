import React from "react";
import { PageTitle, MetadataText } from "../../Typography";
import { formatquantity, formatpriceunit } from "../../../configs/formatnumber";

interface ITotalValue {
  children: React.ReactNode;
  type: "percentage" | "priceunit" | "quantity";
}

export default ({ type, children }: ITotalValue) => {
  return type === "percentage" ? (
    <PageTitle>{`${children}%`}</PageTitle>
  ) : type === "priceunit" ? (
    <>
      <PageTitle>{`Rp${formatpriceunit(children)[0]}`}</PageTitle>
      <MetadataText>{formatpriceunit(children)[1]}</MetadataText>
    </>
  ) : (
    <PageTitle>{formatquantity(children)}</PageTitle>
  );
};
