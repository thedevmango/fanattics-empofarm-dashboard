import React from "react";
import { MetadataText, BodyText } from "../../Typography";
import { FiTrendingDown, FiTrendingUp, FiArrowDown, FiArrowUp } from "react-icons/fi";

interface ITrendPercentage {
  className?: string;
  small?: boolean;
  icon: "arrow" | "trend";
  value: number;
}

export default ({ className = "", small = false, icon = "trend", value }: ITrendPercentage) => {
  const Typography = ({ children, className, color, small }) => {
    return small ? (
      <MetadataText className={className} color={color}>
        {children}
      </MetadataText>
    ) : (
      <BodyText className={className} color={color}>
        {children}
      </BodyText>
    );
  };

  return (
    <Typography
      small={small}
      className={`percentage-item ${className}`}
      color={value < 0 ? "#D92C2C" : "#6FA332"}
    >
      {icon !== "arrow" ? null : value < 0 ? <FiArrowDown /> : <FiArrowUp />}
      &nbsp;{Math.abs(value)}%&nbsp;
      {icon !== "trend" ? null : value < 0 ? <FiTrendingDown /> : <FiTrendingUp />}
    </Typography>
  );
};
