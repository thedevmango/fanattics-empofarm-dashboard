import React from "react";
import { Card } from "../../Card";
import Title from "./Title";
import Dropdown from "./Dropdown";
import TotalValue from "./TotalValue";
import Percentage from "./Percentage";

interface IWidgetPanel {
  isActive?: string;
  borderColor?: "primary" | "danger" | "warning" | "none";
  children: React.ReactNode;
  className?: string;
  dropdownItems?: string[];
  dropdownOpen?: boolean;
  dropdownToggle: any;
  elevationDepth?: "" | "4" | "8" | "16" | "64";
  onClick?: any;
  size: "sm" | "md" | "lg";
  timeframe: string;
  title: string;
  trendPercentage: number;
  trendIcon: "arrow" | "trend";
  totalValue: number;
  type: "percentage" | "priceunit" | "quantity";
}

export default ({
  isActive,
  borderColor = "none",
  children,
  className = "",
  dropdownItems = [],
  dropdownOpen = false,
  dropdownToggle,
  elevationDepth = "",
  onClick,
  size = "sm",
  timeframe,
  title,
  trendPercentage = 0,
  trendIcon = "trend",
  totalValue = 0,
  type = "quantity",
}: IWidgetPanel) => {
  return (
    <Card
      onClick={() => (onClick ? onClick(title) : null)}
      borderColor={borderColor}
      elevationDepth={elevationDepth}
      className={`mol-widget mol-widget-card ${isActive === title ? "active" : ""} ${className}`}
    >
      <div className="widget-header">
        <Title bold size={size}>
          {title}
        </Title>

        {!dropdownItems.length ? null : (
          <Dropdown items={dropdownItems} isOpen={dropdownOpen} toggle={dropdownToggle}>
            {timeframe}
          </Dropdown>
        )}
      </div>

      {!totalValue ? null : (
        <div className="widget-total-value">
          <TotalValue type={type}>{totalValue}</TotalValue>
        </div>
      )}

      {!trendPercentage ? null : (
        <div className="widget-percentage">
          <Percentage icon={trendIcon} value={trendPercentage} />
        </div>
      )}

      <div className="body">{children}</div>
    </Card>
  );
};
