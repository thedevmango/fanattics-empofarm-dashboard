import React from "react";
import { Panel } from "../../Panel";
import Title from "./Title";
import Dropdown from "./Dropdown";
import TotalValue from "./TotalValue";
import Percentage from "./Percentage";

interface IWidgetPanel {
  children: React.ReactNode;
  className?: string;
  dropdownItems?: string[];
  dropdownOpen?: boolean;
  dropdownToggle: any;
  size: "sm" | "md" | "lg";
  timeframe: string;
  title: string;
  trendPercentage: number;
  trendIcon: "arrow" | "trend";
  totalValue: number;
  type: "percentage" | "priceunit" | "quantity";
}

export default ({
  children,
  className = "",
  dropdownItems = [],
  dropdownOpen = false,
  dropdownToggle,
  size = "sm",
  timeframe,
  title,
  trendPercentage = 0,
  trendIcon = "trend",
  totalValue = 0,
  type = "quantity",
}: IWidgetPanel) => {
  return (
    <Panel padding="16" className={`mol-widget mol-widget-panel ${className}`}>
      <div className="widget-header">
        <Title size={size}>{title}</Title>

        {!dropdownItems.length ? null : (
          <Dropdown items={dropdownItems} isOpen={dropdownOpen} toggle={dropdownToggle}>
            {timeframe}
          </Dropdown>
        )}
      </div>

      {!totalValue ? null : <TotalValue type={type}>{totalValue}</TotalValue>}

      {!trendPercentage ? null : <Percentage icon={trendIcon} small value={trendPercentage} />}

      <div className="body">{children}</div>
    </Panel>
  );
};
