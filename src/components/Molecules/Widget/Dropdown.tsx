import React from "react";
import { Button, ButtonIcon } from "../../Button";
import { VscChevronDown } from "react-icons/vsc";

interface IDropdown {
  children: React.ReactNode;
  isOpen: boolean;
  items: string[];
  toggle: any;
}

export default ({ children, isOpen, items, toggle }: IDropdown) => {
  return (
    <div className="dropdown widget-header-dropdown">
      <Button className="dropdown-togglee dropdown-toggle-btn" onClick={toggle} theme="text">
        <span style={{ color: "#00BCF2" }}>{children}</span>
        <ButtonIcon type="append">
          <VscChevronDown color="#00BCF2" />
        </ButtonIcon>
      </Button>

      <div className={`dropdown-menu ${isOpen ? "show" : ""}`}>
        {items.map((item, idx) => {
          return (
            <div
              key={idx}
              onClick={() => toggle({ payload: item })}
              className={`dropdown-item ${children === item ? "active" : ""}`}
            >
              {item}
            </div>
          );
        })}
      </div>
    </div>
  );
};
