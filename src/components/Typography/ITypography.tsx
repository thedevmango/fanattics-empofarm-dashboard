import { ReactNode, CSSProperties } from "react";

export interface ITypography extends React.HTMLAttributes<HTMLDivElement> {
  bold?: boolean;
  className?: string;
  style?: CSSProperties;
  accent?: boolean;
  color?: string;
  children: ReactNode;
}
