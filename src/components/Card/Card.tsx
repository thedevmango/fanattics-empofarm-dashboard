import React, { ReactNode } from "react";

interface ICard extends React.HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  elevationDepth?: "" | "4" | "8" | "16" | "64";
  borderColor?: "primary" | "danger" | "warning" | "none";
}

export default ({
  children,
  className,
  style,
  borderColor = "none",
  elevationDepth = "",
  ...props
}: ICard) => {
  return (
    <div
      {...props}
      style={style}
      className={`component-card component-depth-${elevationDepth} component-card-border-${borderColor} ${className}`}
    >
      {children}
    </div>
  );
};
