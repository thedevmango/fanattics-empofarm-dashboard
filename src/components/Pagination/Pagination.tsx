/* eslint-disable no-unused-vars */
import React from "react";
import { GoChevronLeft, GoChevronRight } from "react-icons/go";

interface IPagination {
  next: any;
  prev: any;
  jump: any;
  pages: any;
  currentPage: any;
  maxPage: any;
}

export default ({ next, prev, jump, pages, currentPage, maxPage }: IPagination) => {
  return (
    <ul className="pagination">
      <li className={`prev arrow ${currentPage === 1 ? "disabled" : ""}`}>
        <button disabled={currentPage === 1 ? true : false} onClick={prev}>
          <GoChevronLeft />
        </button>
      </li>

      {pages.map((page, index) => (
        <li key={index} className={`number ${currentPage === page ? "active" : ""}`}>
          <button onClick={() => (page > 0 ? jump(page) : null)}>{page > 0 ? page : "..."}</button>
        </li>
      ))}

      <li className={`next arrow ${currentPage === maxPage || !maxPage ? "disabled" : ""}`}>
        <button disabled={currentPage === maxPage ? true : false} onClick={next}>
          <GoChevronRight />
        </button>
      </li>
    </ul>
  );
};
