import React from "react";

interface ITableBody extends React.HTMLAttributes<HTMLTableSectionElement> {
  children: React.ReactNode;
  ref?: any;
}

export default ({ children, ref, ...props }: ITableBody) => {
  return (
    <tbody ref={ref} {...props}>
      {children}
    </tbody>
  );
};
