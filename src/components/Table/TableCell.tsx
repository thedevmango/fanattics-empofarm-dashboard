import React from "react";

interface ITableCell extends React.HTMLAttributes<HTMLTableDataCellElement> {
  children: React.ReactNode;
  colSpan?: number;
}

export default ({ children, colSpan, ...props }: ITableCell) => {
  return (
    <td {...props} colSpan={colSpan}>
      {children}
    </td>
  );
};
