import React from "react";

interface ITable extends React.HTMLAttributes<HTMLTableElement> {
  children: React.ReactNode;
  className?: string;
  bordered?: boolean;
}

export default ({ children, className = "", bordered, ...props }: ITable) => {
  return (
    <table
      {...props}
      className={`component-table ${className} ${bordered ? "bordered" : null}`}
    >
      {children}
    </table>
  );
};
