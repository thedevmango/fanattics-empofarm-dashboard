import React from "react";

interface ITableHeadCell extends React.HTMLAttributes<HTMLTableHeaderCellElement> {
  children: React.ReactNode;
  colSpan?: number;
}

export default ({ children, colSpan, ...props }: ITableHeadCell) => {
  return (
    <th {...props} colSpan={colSpan}>
      {children}
    </th>
  );
};
