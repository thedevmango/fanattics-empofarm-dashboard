import React from "react";

interface ITableHead extends React.HTMLAttributes<HTMLTableSectionElement> {
  children: React.ReactNode;
  dark?: boolean;
}

export default ({ children, dark, ...props }: ITableHead) => {
  return (
    <thead {...props} className={`${dark ? "dark" : null}`}>
      {children}
    </thead>
  );
};
