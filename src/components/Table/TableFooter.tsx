import React from "react";

interface ITableFooter extends React.HTMLAttributes<HTMLTableSectionElement> {
  children: React.ReactNode;
}

export default ({ children, ...props }: ITableFooter) => {
  return <tfoot {...props}>{children}</tfoot>;
};
