import React from "react";

interface ICollapse {
  children: React.ReactNode;
  className?: string;
  isOpen: boolean;
}

export default ({ children, className = "", isOpen }: ICollapse) => {
  return (
    <div className={`component-collapse ${isOpen ? "show" : ""} ${className}`}>{children}</div>
  );
};
