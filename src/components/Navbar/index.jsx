/* eslint-disable no-lone-blocks */
/* eslint-disable no-unused-vars */
import React, { Fragment, useState } from "react";
import { Navbar, Nav, NavItem, Modal, ModalBody, NavbarBrand } from "reactstrap";
import { FaCompress, FaUserCircle } from "react-icons/fa";
import { MdPowerSettingsNew, MdSettings, MdSearch } from "react-icons/md";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { LogoutAction } from "../../redux/actions/auth";
import { Button } from "../Button";
import Logo from "../../assets/images/logo-empofarm-light.png";
import Profpict from "../../assets/images/profpict.jpg";

export default function NavBar() {
  const dispatch = useDispatch();
  const history = useHistory();
  const login = useSelector((state) => state.auth.login);
  // const login = true;
  const [logout, setLogout] = useState(false);
  const toggleLogout = () => setLogout(!logout);

  const ModalLogout = () => {
    const handleLogout = () => {
      dispatch(LogoutAction());
      setLogout(false);
    };
    return (
      <Modal centered isOpen={logout} size="sm">
        <ModalBody>
          <center>Are you sure to logout?</center>
          <br />
          <div className="d-flex justify-content-center">
            <Button onClick={handleLogout} theme="secondary" className="mr-1" width={75}>
              Yes
            </Button>
            <Button onClick={toggleLogout} theme="secondary" className="ml-1" width={75}>
              No
            </Button>
          </div>
        </ModalBody>
      </Modal>
    );
  };

  return (
    <Fragment>
      <ModalLogout />

      <Navbar expand="lg">
        <NavbarBrand href="/">
          <img width={100} src={Logo} alt="empofarm" />
        </NavbarBrand>
        <Nav className="ml-auto" navbar>
          {login ? (
            <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
              <NavItem>
                <button onClick={() => history.push("/dashboard/settings")}>
                  <MdSettings size={15} />
                  <span>Setting</span>
                </button>
              </NavItem>
              <NavItem>
                <button onClick={() => setLogout(true)}>
                  <MdPowerSettingsNew size={15} />
                  <span>Log Out</span>
                </button>
              </NavItem>
              <NavItem>
                <div className="profile">
                  <div className="role">Admin</div>
                  <div className="name">Syafruddin</div>
                  <div className="image">
                    <img src={Profpict} alt="" />
                  </div>
                </div>
              </NavItem>
            </div>
          ) : (
            <NavItem>
              <Link to="/login">
                <button>
                  <FaCompress />
                  <span>Log In</span>
                </button>
              </Link>
            </NavItem>
          )}
        </Nav>
      </Navbar>
    </Fragment>
  );
}
