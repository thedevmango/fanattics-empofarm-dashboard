export const sumByKey = (data: any, key: string) => {
  return data.reduce((a: number, b: number) => a + (b[key] || 0), 0);
};
