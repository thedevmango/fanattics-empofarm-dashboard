import { DeliveryStatus, OutboundStatus } from "../../constants";

export const enumTenderStatus = (value) => {
  if (value === "on-purchasing") return "Menunggu Respon Purchasing";

  if (value === "logistic-inbound-hold") return "Menunggu Konfirmasi Inbound Logistic";
  if (value === "on-logistic-inbound") return "Dalam Proses Inbound Logistic";
  if (value === "on-arrival-tray") return "Menunggu Respon Inbound QC";
  if (value === "on-qc-inbound") return "Dalam Proses Inbound QC";

  if (value === "nominal-value-input") return "Menunggu Nominal Purchasing";
  if (value === "finance-approval") return "Menunggu Approval Finance";
  if (value === "payment-request") return "Menunggu Pembayaran";
  if (value === "final-value-submitted") return "Uang sudah ditransfer";

  if (value === "item-stored") return "Berhasil";
  if (value === "iqc-passed") return "Lolos QC";
  if (value === "iqc-rejected") return "Gagal QC";
  return "404";
};

export const enumIQCStatus = (value) => {
  switch (value) {
    case "on-purchasing":
      return "Menunggu Respon Purchasing";

    case "logistic-inbound-hold":
      return "Menunggu Konfirmasi Inbound Logistic";
    case "on-logistic-inbound":
      return "Dalam Proses Inbound Logistic";

    case "on-arrival-tray":
      return "Menunggu Respon Inbound QC";
    case "on-qc-inbound":
      return "Dalam Proses Inbound QC";

    case "iqc-rejected":
    case "iqc-passed":
    case "nominal-value-input":
    case "finance-approval":
    case "final-value-submitted":
    case "item-stored":
      return "Selesai Proses Inbound QC";

    default:
      return "404";
  }
};

export const enumCourierStatus = (value) => {
  switch (value) {
    case "on-purchasing":
      return "Menunggu Respon Purchasing";

    case "logistic-inbound-hold":
      return "Menunggu Konfirmasi Inbound Logistic";
    case "on-logistic-inbound":
      return "Dalam Proses Inbound Logistic";

    case "on-arrival-tray":
    case "on-qc-inbound":
    case "iqc-rejected":
    case "iqc-passed":
    case "nominal-value-input":
    case "finance-approval":
    case "final-value-submitted":
    case "item-stored":
      return "Selesai Proses Inbound Logistic";

    default:
      return "404";
  }
};

export const enumDeliveryInbound = (value) => {
  const {
    waitingLogisticConfirmation,
    logisticOnPickup,
    arrivedOnFarm,
    logisticLoading,
    logisticHauling,
    arrivedOnDestination,
  } = DeliveryStatus.inbound;
  if (value === waitingLogisticConfirmation) return "Menunggu Konfirmasi Kurir";
  if (value === logisticOnPickup) return "Kurir Dalam Perjalanan Pick Up";
  if (value === arrivedOnFarm) return "Kurir Tiba di Farm";
  if (value === logisticLoading) return "Kurir Bongkar Muatan";
  if (value === logisticHauling) return "Kurir Dalam Pengiriman";
  if (value === arrivedOnDestination) return "Kurir Tiba di Gudang Empofarm";
};

export const enumOutboundStatus = (val) => {
  const {
    salesNew,
    salesUnconfirmed,
    salesConfirmed,
    salesComposition,
    salesConsolidation,
    salesConsolidated,
    outboundOutboundQC,
    oqcApproval,
    oqcRejected,
    outboundChamber,
    outboundDelivery,
    salesCompleted,
    salesFailed,
  } = OutboundStatus;

  if (val === salesNew) return "Sales Order Request - UNPAID";
  if (val === salesUnconfirmed) return "Sales Order Request - PAID";
  if (val === salesConfirmed) return "Menunggu Konfirmasi Inventory Manager";
  if (val === salesComposition) return "Menunggu Konfirmasi Inventory Manager";
  if (val === salesConsolidation) return "TPC Diproses Inventory Manager";
  if (val === salesConsolidated) return "Menunggu Diproses Outbound QC";
  if (val === outboundOutboundQC) return "Menunggu Hasil Outbound QC";
  if (val === oqcApproval) return "Outbound QC Diterima";
  if (val === oqcRejected) return "Outbound QC Ditolak";
  if (val === outboundChamber) return "Menunggu Kurir";
  if (val === outboundDelivery) return "Kurir Dalam Pengiriman";
  if (val === salesCompleted) return "Transaksi Berhasil";
  if (val === salesFailed) return "Transaksi Gagal";
};