export const enumType = (value) => {
  if (value === "DS") return "Direct Selling";
  if (value === "CSE") return "Consignment Empofarm";
  if (value === "CSF") return "Consignment Farm";
  if (value === "PAR") return "Product Acquisition Request";
  return "";
};

export const enumTypeCategory = (value) => {
  if (value === "DS" || value === "CSE") return "New Product";
  return "";
};