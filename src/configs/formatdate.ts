import moment from "moment";
export const formatDate = (val: string, format: string) => moment(val).format(format)