export const pricetag = (val) => {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    minimumFractionDigits: 0,
  }).format(val);
};

export const formatnumber = (val) => {
  return new Intl.NumberFormat("id-ID", {
    minimumFractionDigits: 0,
  }).format(val);
};

export const formatquantity = (val) => {
  return new Intl.NumberFormat("id-ID", {
    minimumFractionDigits: 0,
  }).format(val);
};

export const formatpriceunit = (num) => {
  let numbers = new Intl.NumberFormat("id-ID").format(num).split(".");
  let number =
    numbers.length > 1
      ? Math.round(parseInt(numbers.join(""), 10) / Math.pow(1000, numbers.length - 1))
      : numbers[0];
  let unit = numbers.length > 1 ? ["ribu", "juta", "milyar"][numbers.length - 2] : null;
  return [number, unit];
};
