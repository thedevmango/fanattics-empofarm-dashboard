import { sign, decode } from "jsonwebtoken";
// import { readFileSync } from "fs";

export const CreateJWTToken = (payload) => {
  /**
   * NOTE!
   * - CHANGE THE privateKEY with your string secret code
   *   but, make sure it's the same as your publicKEY in jwt-auth.js
   */
  //? let privateKEY = readFileSync("./private.key", "utf8");
  //? return sign(payload, privateKEY, { expiresIn: "6h", algorithm: "RS256" });

  return sign(payload, "mamamia", { expiresIn: "1d" });
};

export const DecodeToken = (payload) => {
  return decode(payload);
};
