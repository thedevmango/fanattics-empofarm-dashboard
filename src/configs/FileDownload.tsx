import Axios from "axios";

export const FileDownload = (data: string, filename: string) => {
  Axios.get(data, { responseType: "blob" })
    .then((res) => {
      const url = window.URL.createObjectURL(new Blob([res.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", filename);
      document.body.appendChild(link);
      link.click();
    })
    .catch((err) => console.log(err));
};
