import React from "react";

export default () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="12"
      fill="currentColor"
      viewBox="0 0 14 12">
      <path
        fill="currentColor"
        d="M3 10.667h1.334V12H3.001v-1.333zM7 0L.335 3.333V12h1.333V6.667h10.667V12h1.333V3.333L7.001 0zM4.335 5.333H1.667V4h2.667v1.333zm4 0H5.667V4h2.667v1.333zm4 0H9.667V4h2.667v1.333zM3.001 8h1.333v1.333H3.001V8zm2.666 0h1.334v1.333H5.667V8zm0 2.667h1.334V12H5.667v-1.333zm2.667 0h1.333V12H8.334v-1.333z"></path>
    </svg>
  );
};
