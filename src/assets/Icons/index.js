export { default as IconCart } from "./IconCart";
export { default as IconLogistic } from "./IconLogistic";
export { default as IconMoney } from "./IconMoney";
export { default as IconPeople } from "./IconPeople";
export { default as IconSales } from "./IconSales";
export { default as IconWarehouse } from "./IconWarehouse";
