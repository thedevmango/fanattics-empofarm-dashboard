/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { Switch, Route, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "js-cookie";
import { ReLoginAction } from "./redux/actions/auth";

import NavBar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import ChildSidebar from "./components/Sidebar/child";

import Login from "./pages/Login";
import ResetPassword from "./pages/ResetPassword";
import PengaturanProduk from "./pages/Produk/Pengaturan";
import TenderMasuk from "./pages/Transaksi/TenderMasuk/index";
import PurchasingFlow from "./pages/Transaksi/PurchasingFlow";
import Inventory from "./pages/Inventory";
import Purchasing from "./pages/Purchasing";
import Finance from "./pages/Finance";
import Kontrol from "./pages/Kontrol";
import Sandbox from "./pages/Sandbox";
import SandboxKenang from "./pages/SandboxKenang";
import Sales from "./pages/Sales";
import KatalogUser from "./pages/KatalogUser";
import PurchaseOrder from "./pages/PurchaseOrder";
import SandboxAya from "./pages/Sandbox/index";
import Logistik from "./pages/Logistik";

export default function App() {
  const location = useLocation();
  const dispatch = useDispatch();

  const [isNavbarHidden, setIsNavbarHidden] = useState(true);
  const [isSidebarHidden, setIsSidebarHidden] = useState(true);

  const selectedMenu = useSelector((state) => state.sidebar.selectedMenu);
  const login = useSelector((state) => state.auth.login);

  useEffect(() => {
    if (location.pathname === "/login" || location.pathname === "/reset-password") {
      setIsNavbarHidden(true);
      setIsSidebarHidden(true);
    } else {
      setIsNavbarHidden(false);
      setIsSidebarHidden(false);
    }
    let token = Cookies.get("cookie");
    if (token) {
      dispatch(ReLoginAction(token));
    }
  }, [location.pathname]);

  return (
    <div className="App">
      {!isNavbarHidden && <NavBar />}
      {!isSidebarHidden && <Sidebar />}
      <ChildSidebar isOpen={selectedMenu.length} />

      <Switch>
        <Route path="/login" exact component={Login} />
        <Route path="/reset-password" exact component={ResetPassword} />
      </Switch>

      {!login ? null : (
        <div className="App-content">
          <div className="main-container">
            <Switch>
              <Route path="/" exact component={Login} />
              <Route path="/sandbox" exact component={Sandbox} />
              <Route path="/sandbox-ken" exact component={SandboxKenang} />
              <Route path="/pengaturan-produk" component={PengaturanProduk} />
              <Route path="/tender-masuk" component={TenderMasuk} />
              <Route path="/purchasing-flow" component={PurchasingFlow} />
              <Route path="/inventory" component={Inventory} />
              <Route path="/purchasing" component={Purchasing} />
              <Route path="/finance" component={Finance} />
              <Route path="/kontrol" component={Kontrol} />
              <Route path="/sales" component={Sales} />
              <Route path="/logistik" component={Logistik} />
              <Route path="/purchase-order" component={PurchaseOrder} />
              <Route path="/katalog-user" component={KatalogUser} />
              <Route path="/sandbox-aya" component={SandboxAya} />
            </Switch>
          </div>
        </div>
      )}
    </div>
  );
}
