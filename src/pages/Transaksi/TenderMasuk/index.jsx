import React, { useState } from "react";
import Searchbar from "../../../components/Searchbar";
import { Table } from "reactstrap";

export default function TenderMasuk(props) {
  //* SEARCH FUNCTION
  const [searchQuery, setSearchQuery] = useState("");
  const [searching, setSearching] = useState(false);
  const handleSearchQuery = (text) => setSearchQuery(text);
  // if (searchQuery) {
  //   data = data.filter((item) => {
  //     return (
  //       item.nama.toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1 ||
  //       item.treniId.toString().indexOf(searchQuery.toLowerCase()) !== -1
  //     );
  //   });
  // }

  return (
    <div className="tender-masuk">
      <div className="header mb-4">
        <div className="header-title">Transaksi - Tender</div>
      </div>

      <div className="content">
        <div className="content-header d-flex flex-row flex-wrap mb-3">
          <div className="pivot-btn flex-grow-1 d-flex flex-row">
            <div>Tender Masuk</div>
            <div>Tender Disetujui</div>
            <div>Tender Diambil</div>
            <div>Tender Diverifikasi</div>
            <div>Tender Selesai</div>
            <div>Tender Ditolak</div>
          </div>

          <div className="search-wrapper">
            <Searchbar
              placeholder="Cari Farmer atau Kode Transaksi"
              onChange={handleSearchQuery}
              searching={searching}
              setSearching={setSearching}
              searchQuery={searchQuery}
              setSearchQuery={setSearchQuery}
            />
          </div>
        </div>

        <div className="content-body">
          <Table hover>
            <thead>
              <tr>
                <th>Kode Transaksi</th>
                <th>Nama Farmer</th>
                <th>Kategori Barang</th>
                <th>Tanggal Masuk</th>
                <th>Bentuk Kerjasama</th>
                <th>Tipe Penyimpanan</th>
                <th>Total Transaksi</th>
              </tr>
            </thead>
          </Table>
        </div>
      </div>
    </div>
  );
}
