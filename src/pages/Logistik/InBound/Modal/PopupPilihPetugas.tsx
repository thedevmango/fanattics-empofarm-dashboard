import Axios from "axios";
import React, { useEffect, useState } from "react";
import { Button } from "../../../../components/Button";
import { Modal, ModalBody } from "../../../../components/Modal";
import Searchbar from "../../../../components/Searchbar";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../../components/Table";
import { API_URL } from "../../../../constants";

interface Props {
  handleAssignCourier: any;
  id: number;
  isOpen: boolean;
  toggle?: () => boolean;
}

export default ({ handleAssignCourier, id, isOpen, toggle }: Props) => {
  const [data, setData] = useState([]);
  const [selected, setSelected] = useState(-1);
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const getCourierList = async () => {
    try {
      const { data } = await Axios.get(`${API_URL}/users/couriers`);
      setData(data.result);
      setFilteredData(data.result);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (!data.length) return;
    if (searchQuery) {
      const results = data.filter((item: any) => {
        return (
          item.fullName.toLowerCase().includes(searchQuery.toLowerCase()) ||
          item.officerId.toLowerCase().includes(searchQuery.toLowerCase())
        );
      });
      console.log("TEEESSS", results);
      return setFilteredData(results);
    } else {
      return setFilteredData(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchQuery]);

  useEffect(() => {
    getCourierList();
  }, []);

  // console.log(searchQuery, data);
  return (
    <Modal size="lg" isOpen={isOpen} toggle={toggle}>
      <ModalBody>
        <Searchbar
          placeholder="Cari nama petugas atau ID"
          className="mb-3"
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />

        <div style={{ height: 400, overflow: "auto" }}>
          <Table>
            <TableHead style={{ boxShadow: "none" }}>
              <TableRow>
                <TableHeadCell style={{ position: "sticky", top: 0 }}>Nama Kurir</TableHeadCell>
                <TableHeadCell style={{ position: "sticky", top: 0 }}>ID</TableHeadCell>
                <TableHeadCell style={{ textAlign: "right", position: "sticky", top: 0 }}>
                  Status
                </TableHeadCell>
              </TableRow>
            </TableHead>
            <TableBody style={{ boxShadow: "none" }}>
              {filteredData.map((item: any, i: number) => {
                return (
                  <TableRow
                    key={item.id}
                    onClick={() => setSelected(item.id)}
                    style={{ backgroundColor: selected === item.id ? "#e1eadb" : "transparent" }}
                  >
                    <TableCell>{item.courierName}</TableCell>
                    <TableCell>{item.courierCode}</TableCell>
                    <TableCell style={{ textAlign: "right" }}>0 tugas</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>

        <div className="d-flex justify-content-end mt-5">
          <Button onClick={() => handleAssignCourier(selected)} theme="primary">
            Pilih Petugas Kurir
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
