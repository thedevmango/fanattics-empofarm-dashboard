/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { IoMdPin, IoMdRadioButtonOff,  } from "react-icons/io";
import { RiMotorbikeFill } from 'react-icons/ri';
import { Button, ButtonIcon } from "../../../../components/Button";
import { BackButton } from "../../../../components/Molecules";
import {
  BodyText,
  HeaderText,
  PageTitle,
  PaneHeader,
  SubjectTitle,
} from "../../../../components/Typography";
import { API_URL, DeliveryStatus, GMAPS_API, InboundStatus } from "../../../../constants";
import PopupPilihPetugas from "../Modal/PopupPilihPetugas";
import {
  DataTender,
  DataTenderItem,
  DataUser,
  ReportQC,
  RincianTenderItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { Panel } from "../../../../components/Panel";
import { formatquantity } from "../../../../configs/formatnumber";
import GoogleMapReact from "google-map-react";
import { enumTenderStatus } from "../../../../configs/enums";
import Axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { getShipmentLogs } from "../../../../redux/actions/logs";
import { FaTruck } from "react-icons/fa";

export default (props) => {
  const dispatch = useDispatch();
  const parentData = props.location.state.data;
  const { LogisticDone } = InboundStatus;

  const shipmentLogs = useSelector((state) => state.logs.shipmentLogs);

  //* LOCAL STATE
  const [data, setData] = useState(undefined);
  const [isModalOpen, setIsModalOpen] = useState(false);

  //* FUNCTION
  const toggleModalPetugas = () => setIsModalOpen(!isModalOpen);

  const getDetailsInfo = async () => {
    try {
      const details = await Axios.get(`${API_URL}/purchases/list`, {
        params: {
          id: parentData.id,
          include: ["product_variant", "farmer", "shipment_log", "courier"],
        },
      });
      setData(details.data.result[0]);
    } catch (err) {
      console.log(err);
    }
  };

  const handleAssignCourier = async (selected) => {
    try {
      const resCourier = await Axios.patch(`${API_URL}/purchases/${parentData.id}/assign-courier`, {
        courierId: selected,
      });
      console.log(resCourier);
      getDetailsInfo();
      getShipmentLogs({ data: data, type: "inbound" });
      setIsModalOpen(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (!data) return;
    dispatch(getShipmentLogs({ data: data, type: "inbound" }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    getDetailsInfo();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isModalOpen]);

  const apiIsLoaded = (map, maps) => {
    const directionsService = new maps.DirectionsService();
    const directionsDisplay = new maps.DirectionsRenderer();
    directionsService.route(
      {
        origin: { lat: parentData.farmer.latitude, lng: parentData.farmer.longitude },
        destination: {
          lat: parentData.farmer.latitude + 0.01,
          lng: parentData.farmer.longitude + 0.01,
        },
        // destination: 'Piña MZ3 LT8, Ampliacion Lopez Portillo, 13400 Ciudad de México, CDMX, Mexico',
        travelMode: "DRIVING",
      },
      (response, status) => {
        if (status === "OK") {
          directionsDisplay.setDirections(response);
          // console.log(response.routes[0].overview_path, "Rute");
          const routePolyline = new maps.Polyline({
            path: response.routes[0].overview_path,
            strokeColor: "#5491F5",
            strokeOpacity: 1.0,
            strokeWeight: 3,
          });
          routePolyline.setMap(map);
        } else {
          window.alert("Directions request failed due to " + status);
        }
      }
    );
  };

  console.log("INBOUND data", parentData);
  console.log("INBOUND", shipmentLogs);
  if (!data)
    return (
      <div className="logistik logistik-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="logistik logistik-details">
      <Helmet>
        <title>Inbound Details | Logistik</title>
      </Helmet>

      <PopupPilihPetugas
        id={parentData.id}
        handleAssignCourier={handleAssignCourier}
        toggle={toggleModalPetugas}
        isOpen={isModalOpen}
      />

      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="d-flex align-items-center justify-content-between mb-4">
        <PageTitle>Detail Transaksi Penjualan</PageTitle>

        {data.courrierId ? null : (
          <Button onClick={toggleModalPetugas} containerStyle={{ minWidth: 220 }} theme="primary">
            PILIH KURIR
            <ButtonIcon type="append">
              <IoMdRadioButtonOff />
            </ButtonIcon>
          </Button>
        )}
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(data.updatedAt).format("DD/MM/YYY H:mm")} link={false}>
            <HeaderText>{enumTenderStatus(data.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Farmer"
            name={parentData.farmer.user.fullName}
            contact={parentData.farmer.farmTelephoneNumber}
            address={parentData.farmer.farmAddress}
          />

          <DataTender>
            <DataTenderItem title="Tanggal Pengajuan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTenderItem>
            <DataTenderItem title="Kode Transaksi">{parentData.transactionCode}</DataTenderItem>
          </DataTender>

          {LogisticDone.includes(parentData.status) ? (
            <ReportQC type="Inbound" officerName={"Dedi"}></ReportQC>
          ) : null}
        </div>

        <div className="col-8">
          <Panel padding="24" className="mol-rincian-tender mol-rincian-pengiriman">
            <PaneHeader className="mb-4" color="#404040" bold>
              Rincian Pengiriman
            </PaneHeader>

            <div className="content">
              <div className="image-wrapper">
                <img src={parentData.product_variant.imageUrl} alt="product" />
              </div>
              <div className="tender-info">
                <PaneHeader className="col pl-0 pb-3" color="#404040">
                  {`${parentData.product_variant.subcategory.name} ${parentData.product_variant.productName}`}
                </PaneHeader>

                <RincianTenderItem title="Category">
                  {parentData.product_variant.subcategory.category.categoryName}
                </RincianTenderItem>
                <RincianTenderItem title="Sub Category">
                  {parentData.product_variant.subcategory.name}
                </RincianTenderItem>
                <RincianTenderItem title="Varian">
                  {parentData.product_variant.productName}
                </RincianTenderItem>
                <RincianTenderItem title="Kuantitas Barang">
                  {formatquantity(parentData.quantity)} Kilogram
                </RincianTenderItem>
                <RincianTenderItem title="Area">-</RincianTenderItem>
              </div>
            </div>

            <PaneHeader className="mt-4 mb-3" color="#404040" bold>
              Peta Lokasi
            </PaneHeader>

            <div className="d-flex mb-3">
              <table>
                <tbody>
                  {shipmentLogs ? (
                    <>
                      <TableItem title="Nama Kurir" value={shipmentLogs.courierName} />
                      <TableItem
                        title="Berangkat"
                        value={moment(shipmentLogs.createdAt).format("DD-MM-YYYY  H:mm:ss")}
                      />
                    </>
                  ) : (
                    <>
                      <TableItem title="Nama Kurir" value="Belum Terpilih" color="#EA4300" />
                      <TableItem title="Berangkat" value="--:--:--  --:--:----" />
                    </>
                  )}

                  {shipmentLogs &&
                  shipmentLogs.logs.length &&
                  LogisticDone.includes(parentData.status) ? (
                    <TableItem
                      title="Pengiriman Selesai"
                      value={moment(
                        shipmentLogs.logs[shipmentLogs.logs.length - 1].createdAt
                      ).format("DD-MM-YYYY  H:mm:ss")}
                    />
                  ) : (
                    <TableItem title="Pengiriman Selesai" value="--:--:--  --:--:----" />
                  )}
                </tbody>
              </table>
            </div>

            <div style={{ height: 400, width: "100%" }} className="map-wrapper">
              <GoogleMapReact
                bootstrapURLKeys={{ key: GMAPS_API }}
                defaultCenter={{
                  lat: data.courrierId
                    ? parentData.farmer.latitude + 0.005
                    : parentData.farmer.latitude,
                  lng: data.courrierId
                    ? parentData.farmer.longitude + 0.005
                    : parentData.farmer.longitude,
                }}
                defaultZoom={15}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={
                  data.courrierId ? ({ map, maps }) => apiIsLoaded(map, maps) : null
                }
              >
                <Marker lat={parentData.farmer.latitude} lng={parentData.farmer.longitude} />
                {data.courrierId ? (
                  <MarkerTruck
                    lat={parentData.farmer.latitude + 0.01}
                    lng={parentData.farmer.longitude + 0.01}
                  />
                ) : null}
              </GoogleMapReact>
            </div>
          </Panel>

          {shipmentLogs ? <RiwayatEkspedisi data={shipmentLogs} /> : null}
        </div>
      </div>
    </div>
  );
};

const Marker = () => (
  <div>
    <IoMdPin color="D74028" size={30} />
  </div>
);
const MarkerTruck = () => (
  <div>
    <FaTruck color="D74028" size={25} />
  </div>
);

const CourierMarker = () => (
  <div>
    <RiMotorbikeFill color="D74028" size={30} />
  </div>
)

const TableItem = ({ color, title, value }) => {
  return (
    <tr>
      <td style={{ width: 120 }}>
        <BodyText bold>{title}</BodyText>
      </td>
      <td>
        <BodyText>:</BodyText>
      </td>
      <td>
        <SubjectTitle color={color}>{value}</SubjectTitle>
      </td>
    </tr>
  );
};
