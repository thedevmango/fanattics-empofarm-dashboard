/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import moment from "moment";
import { BsChevronRight } from "react-icons/bs";
import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { Button } from "../../../components/Button";
import Tabs from "../../../components/Tabs";
import { useDispatch, useSelector } from "react-redux";
import { getDeliveryList } from "../../../redux/actions";
import { DeliveryStatus, InboundStatus } from "../../../constants";
import { SegmentedTitle } from "../../../components/Molecules";
import { iCourier_P_TAB } from "../../../redux/types";
import { Panel } from "../../../components";

export default (props) => {
  const dispatch = useDispatch();
  const { LogisticIncoming, LogisticOnProcess, LogisticDone } = InboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state) => state.i_courier.tab);
  const data = useSelector((state) => state.i_courier.deliveryList);
  const loading = useSelector((state) => state.i_courier.loading);

  //* FUNCTION
  const toggleTab = (selectedTab) => dispatch({ type: iCourier_P_TAB, payload: selectedTab });

  const toggleTitle = ({ value }) => {
    if (value === "inbound") props.history.push(`/logistik/inbound`);
    if (value === "outbound") props.history.push(`/logistik/outbound`);
    if (value === "mutasi-stok") return null;
  };

  //* FETCH DATA
  const getItemList = () =>
    dispatch(getDeliveryList({ status: tab, include: ["shipment_log", "courier"] }));

  useEffect(() => {
    getItemList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tab]);

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  // console.log("LOG Inbound", data);
  return (
    <div className="logistik logistik-inbound">
      <Helmet>
        <title>Inbound | Logistic</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        titles={["Inbound", "Outbound", "Mutasi Stok"]}
        slug={["inbound", "outbound", "mutasi-stok"]}
        onClick={toggleTitle}
      />

      <Tabs
        className="col-7 pl-0 mb-3"
        options={["Delivery Order", "On Going Delivery", "Finished Delivery"]}
        slug={[LogisticIncoming, LogisticOnProcess, LogisticDone]}
        activeTab={tab}
        setActiveTab={toggleTab}
      />

      <Panel>
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Tanggal Request</TableHeadCell>
              <TableHeadCell>Nama Item</TableHeadCell>
              <TableHeadCell>Nama Farmer</TableHeadCell>
              <TableHeadCell>Area</TableHeadCell>
              <TableHeadCell colSpan={2}>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, history, match, tab }) => {
  if (!data) return null;
  return data.map((item, idx) => {
    // const link = `${match.path}/detail/${item.product_variant.productName
    //   .split(" ")
    //   .join("-")
    //   .toLowerCase()}`;
    const link = `${match.path}/details`;
    const nextClick = () => history.push(link, { data: item });

    return (
      <TableRow key={idx} onClick={nextClick}>
        <TableCell>{moment(item.createdAt).format("MMM D, YYYY hh:mm")}</TableCell>
        <TableCell>
          {`${item.product_variant.subcategory.name} ${item.product_variant.productName}`}
        </TableCell>
        <TableCell>{item.farmer.user.fullName}</TableCell>
        <TableCell>{item.codeArea || "-"}</TableCell>
        <TableCell>&nbsp;</TableCell>
        <TableCell style={{ width: 50 }}>
          <Button containerStyle={{ backgroundColor: "transparent" }} theme="action-table">
            <BsChevronRight />
          </Button>
        </TableCell>
      </TableRow>
    );
  });
};

// const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
// const requestDate = ["13/09/2020", "12/09/2020", "11/09/2020"];
// const category = ["Hard", "Soft", "Liquid", "Equipment"];
// const subCategory = ["Jagung", "Kelapa", "Beras Putih", "Kopi", "Gabah", "Kacang Almond"];
// const quantity = ["500 Kg", "1.000 Kg", "1.500 Kg", "2.000 Kg"];
// const dummyDataTable = [
//   ["CV. Burhan", "proccessed", "Regiom A"],
//   ["Farm Cikole", "proccessed", "Regiom A"],
//   ["CV. Burhan", "proccessed", "Regiom A"],
//   ["RPA Sierad", "proccessed", "Regiom A"],
//   ["Moms and Pops", "proccessed", "Region B"],
//   ["RPA Sierad", "proccessed", "Region B"],
//   ["Moms and Pops", "proccessed", "Region B"],
//   ["RPA Sierad", "proccessed", "Region B"],
//   ["CV. Burhan", "proccessed", "Region C"],
//   ["CV. Burhan", "proccessed", "Region C"],
//   ["CV. Burhan", "proccessed", "Region C"],
//   ["CV. Burhan", "proccessed", "Region C"],
//   ["Moms and Pops", "ordered", "Regiom A"],
//   ["RPA Sierad", "ordered", "Regiom A"],
//   ["Moms and Pops", "ordered", "Regiom A"],
//   ["Farm Cikole", "ordered", "Regiom A"],
//   ["Moms and Pops", "ordered", "Regiom A"],
//   ["CV. Burhan", "ordered", "Region B"],
//   ["CV. Burhan", "ordered", "Region B"],
//   ["Farm Cikole", "ordered", "Region B"],
//   ["CV. Burhan", "ordered", "Region B"],
//   ["CV. Burhan", "ordered", "Region B"],
//   ["Farm Cikole", "ordered", "Region C"],
//   ["Farm Cikole", "ordered", "Region C"],
//   ["Moms and Pops", "ordered", "Region C"],
//   ["CV. Burhan", "ordered", "Region C"],
//   ["CV. Burhan", "ordered", "Region C"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["Moms and Pops", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["Farm Cikole", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region C"],
//   ["CV. Burhan", "finished", "Region C"],
//   ["Moms and Pops", "finished", "Region C"],
//   ["Moms and Pops", "finished", "Region C"],
//   ["CV. Burhan", "finished", "Region C"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["Farm Cikole", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region D"],
//   ["CV. Burhan", "finished", "Region D"],
// ].map((item, idx) => {
//   return {
//     id: idx + 1,
//     buyerName: item[0],
//     trxStatus: item[1],
//     codeArea: item[2],
//     location: "Lokasi Pengambilan",
//     description: "Keterangan",
//     transactionCode: "ALEPHOENTR975",
//     category: randomizeData(category),
//     subCategory: randomizeData(subCategory),
//     quantity: randomizeData(quantity),
//     requestDate: randomizeData(requestDate),
//   };
// });
