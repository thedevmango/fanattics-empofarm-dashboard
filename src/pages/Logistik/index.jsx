/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";

import InBound from "./Inbound";
import InboundDetails from "./Inbound/Details";
import OutBound from "./Outbound";
import OnboundDetails from "./Outbound/Details";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/inbound`} exact component={InBound} />
      <Route path={`${props.match.path}/inbound/details`} exact component={InboundDetails} />

      <Route path={`${props.match.path}/outbound`} exact component={OutBound} />
      <Route path={`${props.match.path}/outbound/details`} exact component={OnboundDetails} />
    </>
  );
};
