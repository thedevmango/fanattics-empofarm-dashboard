/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { IoMdRadioButtonOff } from "react-icons/io";
import { Button, ButtonIcon } from "../../../../components/Button";
import { BackButton } from "../../../../components/Molecules";
import { HeaderText, PageTitle, PaneHeader, SubjectTitle } from "../../../../components/Typography";
import { API_URL, OutboundStatus } from "../../../../constants";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  ReportQC,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { Panel } from "../../../../components/Panel";
import PopupPilihPetugas from "../Modal/PopupPilihPetugas";
import RincianPenjualan from "./RincianPenjualan";
import RincianPenjualanItem from "./RincianPenjualanItem";
import { useDispatch, useSelector } from "react-redux";
import {
  getOQCOfficer,
  getSalesDelivery,
  // getShipmentLogs,
} from "../../../../redux/actions/o_courier";
import Axios from "axios";
import { getShipmentLogs } from "../../../../redux/actions/logs";

export default (props) => {
  const dispatch = useDispatch();
  const tab = props.location.state.tab;
  const parentData = props.location.state.data;
  const { CourierIncoming, outboundChamber } = OutboundStatus;

  const details = useSelector((state) => state.o_courier.deliveryDetails);
  const shipmentLogs = useSelector((state) => state.logs.shipmentLogs);
  const oqc_officer = useSelector((state) => state.o_courier.oqc_officer);

  //* LOCAL STATE
  const [isModalOpen, setIsModalOpen] = useState(false);

  //* FUNCTION
  const toggleModalPetugas = () => setIsModalOpen(!isModalOpen);

  const handleAssignCourier = async (selected) => {
    try {
      const { data } = await Axios.put(`${API_URL}/sales/${parentData.id}`, {
        courierId: selected,
        status: "outbound-delivery",
      });
      console.log(data.message);
      dispatch(getSalesDelivery({ id: parentData.id }));
      setIsModalOpen(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (!details) return;
    if (details.oqcOfficerId) dispatch(getOQCOfficer(details.oqcOfficerId));
    if (details.courierId) dispatch(getShipmentLogs({ data: details, type: "outbound" }));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [details]);

  useEffect(() => {
    dispatch(getSalesDelivery({ id: parentData.id }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  console.log(details);

  if (!details || !oqc_officer)
    return (
      <div className="logistik logistik-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="logistik logistik-details">
      <Helmet>
        <title>Outbound Details | Logistik</title>
      </Helmet>

      <PopupPilihPetugas
        handleAssignCourier={handleAssignCourier}
        toggle={toggleModalPetugas}
        isOpen={isModalOpen}
      />

      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="d-flex align-items-center justify-content-between mb-4">
        <PageTitle>Detail Transaksi Penjualan</PageTitle>

        {details.status === outboundChamber ? (
          <Button onClick={toggleModalPetugas} containerStyle={{ minWidth: 220 }} theme="primary">
            PILIH KURIR PENGIRIMAN
            <ButtonIcon type="append">
              <IoMdRadioButtonOff />
            </ButtonIcon>
          </Button>
        ) : null}
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx
            timestamp={moment(details.updatedAt).format("DD/MM/YYY H:mm")}
            link={false}
          >
            <HeaderText>{details.status}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Buyer"
            name={parentData.buyer.user.fullName}
            contact={parentData.buyer.user.phoneNumber}
            address={parentData.buyer.user.address}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {parentData.transactionCode}
            </DataTransaksiItem>
          </DataTransaksi>

          <ReportQC type="Outbound" officerName={oqc_officer.fullName}></ReportQC>
        </div>

        <div className="col-8">
          <RincianPenjualan parentData={parentData}>
            {parentData.sale_transaction_details.map((item, i) => {
              return <RincianPenjualanItem key={i} item={item} />;
            })}
          </RincianPenjualan>

          <Panel className="mb-3">
            <div className="header pb-3 border-bottom">
              <PaneHeader bold>Kelengkapan Dokumen</PaneHeader>
            </div>
            <div className="mb-3"></div>

            <div className="body">
              <Panel
                style={{
                  padding: "18px 8px",
                  boxShadow: `0px 0.3px 0.9px rgba(0, 0, 0, 0.1),
                  0px 1.6px 3.6px rgba(0, 0, 0, 0.13)`,
                }}
                className="mb-3 d-flex flex-column"
              >
                <SubjectTitle color="#605E5C">Shipmen List</SubjectTitle>
                <HeaderText color="#107C10">Daftar Pengiriman.pdf</HeaderText>
              </Panel>
              <Panel
                style={{
                  padding: "18px 8px",
                  boxShadow: `0px 0.3px 0.9px rgba(0, 0, 0, 0.1),
                  0px 1.6px 3.6px rgba(0, 0, 0, 0.13)`,
                }}
                className="mb-3 d-flex flex-column"
              >
                <SubjectTitle color="#605E5C">Surat Jalan</SubjectTitle>
                <HeaderText color="#107C10">Surat Jalan 20123184.pdf</HeaderText>
              </Panel>
            </div>
          </Panel>

          {CourierIncoming.includes(details.status) ? null : (
            <RiwayatEkspedisi data={shipmentLogs} />
          )}
        </div>
      </div>
    </div>
  );
};
