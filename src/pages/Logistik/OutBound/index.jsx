/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import moment from "moment";
import { BsChevronRight } from "react-icons/bs";
import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { Button } from "../../../components/Button";
import Tabs from "../../../components/Tabs";
import { OutboundStatus } from "../../../constants";
import { SegmentedTitle } from "../../../components/Molecules";
import { Panel } from "../../../components/Panel";
import { useDispatch, useSelector } from "react-redux";
import { oCourier_P_TAB } from "../../../redux/types";
import { getSalesDelivery } from "../../../redux/actions/o_courier";

export default (props) => {
  const dispatch = useDispatch();
  const { CourierIncoming, CourierInProcess, CourierDone } = OutboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state) => state.o_courier.tab);
  const data = useSelector((state) => state.o_courier.deliveryList);

  //* FUNCTION
  const setTab = (tab) => dispatch({ type: oCourier_P_TAB, payload: tab });

  const toggleTitle = ({ value }) => {
    if (value === "inbound") props.history.push(`/logistik/inbound`);
    if (value === "outbound") props.history.push(`/logistik/outbound`);
    if (value === "mutasi-stok") return null;
  };

  const fetchData = () => {
    dispatch(getSalesDelivery({ status: tab }));
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tab]);

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  console.log(data);
  return (
    <div className="logistik logistik-outbound">
      <Helmet>
        <title>Out Bound | Logistic</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        titles={["Inbound", "Outbound", "Mutasi Stok"]}
        slug={["inbound", "outbound", "mutasi-stok"]}
        onClick={toggleTitle}
      />

      <Panel>
        <Tabs
          className="col-7 pl-0 mb-4"
          options={["Delivery Order", "On Going Delivery", "Finished Delivery"]}
          slug={[CourierIncoming, CourierInProcess, CourierDone]}
          activeTab={tab}
          setActiveTab={setTab}
        />

        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Nama Pemesan</TableHeadCell>
              <TableHeadCell>Area Pengiriman</TableHeadCell>
              <TableHeadCell>Keterangan</TableHeadCell>
              <TableHeadCell style={{ textAlign: "right" }}>Tanggal Request</TableHeadCell>
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, match, history, tab }) => {
  if (!data) return null;
  return data.map((item, idx) => {
    const link = `${match.path}/details`;
    const nextClick = () => history.push(link, { data: item, tab: tab });

    return (
      <TableRow key={idx} onClick={nextClick}>
        <TableCell>{item.transactionCode}</TableCell>
        <TableCell>{item.buyer.user.fullName}</TableCell>
        <TableCell>{"-"}</TableCell>
        <TableCell>{item.description}</TableCell>
        <TableCell style={{ textAlign: "right" }}>
          {moment(item.createdAt).format("DD/MM/YYYY")}
        </TableCell>
        <TableCell style={{ width: 50, borderLeft: "0 solid #E1E1E1" }}>
          <Button theme="action-table">
            <BsChevronRight />
          </Button>
        </TableCell>
      </TableRow>
    );
  });
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const requestDate = [
  "2020-09-20T12:59:39.349Z",
  "2020-09-28T12:59:39.349Z",
  "2020-09-12T12:59:39.349Z",
];
const subCategory = ["Jagung", "Kelapa", "Beras Putih", "Kopi", "Gabah", "Kacang Almond"];
const dummyDataTable = [
  ["CV. Burhan", "proccessed", "Jawa 1"],
  ["Farm Cikole", "proccessed", "Jawa 1"],
  ["CV. Burhan", "proccessed", "Jawa 1"],
  ["RPA Sierad", "proccessed", "Jawa 1"],
  ["Moms and Pops", "proccessed", "Sumatra 2"],
  ["RPA Sierad", "proccessed", "Sumatra 2"],
  ["Moms and Pops", "proccessed", "Sumatra 2"],
  ["RPA Sierad", "proccessed", "Sumatra 2"],
  ["CV. Burhan", "proccessed", "Sulawesi 3"],
  ["CV. Burhan", "proccessed", "Sulawesi 3"],
  ["CV. Burhan", "proccessed", "Sulawesi 3"],
  ["CV. Burhan", "proccessed", "Sulawesi 3"],
  ["Moms and Pops", "ordered", "Jawa 1"],
  ["RPA Sierad", "ordered", "Jawa 1"],
  ["Moms and Pops", "ordered", "Jawa 1"],
  ["Farm Cikole", "ordered", "Jawa 1"],
  ["Moms and Pops", "ordered", "Jawa 1"],
  ["CV. Burhan", "ordered", "Sumatra 2"],
  ["CV. Burhan", "ordered", "Sumatra 2"],
  ["Farm Cikole", "ordered", "Sumatra 2"],
  ["CV. Burhan", "ordered", "Sumatra 2"],
  ["CV. Burhan", "ordered", "Sumatra 2"],
  ["Farm Cikole", "ordered", "Sulawesi 3"],
  ["Farm Cikole", "ordered", "Sulawesi 3"],
  ["Moms and Pops", "ordered", "Sulawesi 3"],
  ["CV. Burhan", "ordered", "Sulawesi 3"],
  ["CV. Burhan", "ordered", "Sulawesi 3"],
  ["CV. Burhan", "finished", "Kalimantan 4"],
  ["CV. Burhan", "finished", "Kalimantan 4"],
  ["Moms and Pops", "finished", "Kalimantan 4"],
  ["CV. Burhan", "finished", "Kalimantan 4"],
  ["Farm Cikole", "finished", "Kalimantan 4"],
  ["CV. Burhan", "finished", "Sulawesi 3"],
  ["CV. Burhan", "finished", "Sulawesi 3"],
  ["Moms and Pops", "finished", "Sulawesi 3"],
  ["Moms and Pops", "finished", "Sulawesi 3"],
  ["CV. Burhan", "finished", "Sulawesi 3"],
  ["CV. Burhan", "finished", "Jawa 5"],
  ["CV. Burhan", "finished", "Jawa 5"],
  ["Farm Cikole", "finished", "Jawa 5"],
  ["CV. Burhan", "finished", "Jawa 5"],
  ["CV. Burhan", "finished", "Jawa 5"],
].map((item, idx) => {
  return {
    id: idx + 1,
    buyer: {
      id: idx + 1,
      user: {
        id: idx + 2,
        fullName: item[0],
        phoneNumber: "08987654321",
        address: "Jalan Cinta, Bandung",
      },
    },
    trxStatus: item[1],
    codeArea: item[2],
    location: "Lokasi Pengambilan",
    description: randomizeData(["Direct Delivery", "Split Delivery"]),
    transactionCode: "1053149099999218",
    oqc_officer: {
      fullName: randomizeData(["Agus", "Ryan"]),
    },
    oqc_report: randomizeData(["accepted", "rejected"]),
    deliveryStatus: randomizeData([
      "waiting-logistic-confirmation",
      "logistic-on-pickup",
      "logistic-hauling-port",
      "arrived-on-port",
      "logistic-unloading",
      "logistic-hauling-buyer",
      "arrived-on-destination",
    ]),
    status: randomizeData([
      // TUGAS MASUK
      "outbound-chamber",

      // DALAM PROSES
      "outbound-delivery",

      // SELESAI
      "sales-completed",
      "sales-failed",
    ]),
    documentList: [
      '{"fileName": "Manifest List Document","fileUrl": "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"}',
      '{"fileName": "Packaging List Document","fileUrl": "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"}',
      '{"fileName": "Product Composition Document","fileUrl": "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"}',
    ],
    subCategory: randomizeData(subCategory),
    sale_transaction_details: [
      {
        id: 6,
        salesTransactionId: 3,
        productSkuId: 6,
        basePrice: 1800000,
        salePrice: 1500000,
        quantity: 30,
        packagingUnit: "Botol",
        packagingQuantity: 10,
        productCompositionStatus: null,
        createdAt: "2020-09-28T12:59:39.385Z",
        updatedAt: "2020-09-28T12:59:39.385Z",
        product_sku: {
          id: 6,
          farmerId: 2,
          farmer: {
            user: {
              fullName: "Joko Tole",
            },
          },
          sku: "779106321",
          variantId: 6,
          price: 60000,
          draftStock: 500,
          directSellStock: 250,
          consignmentStock: 250,
          createdAt: "2020-09-28T12:59:39.370Z",
          updatedAt: "2020-09-28T12:59:39.370Z",
          product_variant: {
            id: 6,
            subcategor: 1,
            productNam: "Sapi",
            realStock: 500,
            draftStock: 250,
            govPrice: null,
            empofarmPr: null,
            imageUrl:
              "https://www.thetimes.co.uk/imageserver/image/%2Fmethode%2Ftimes%2Fprod%2Fweb%2Fbin%2F1acc71d8-5638-11e9-b872-7488e2315159.jpg?crop=6016%2C3384%2C0%2C313&resize=1180",
            createdAt: "2020-09-28T12:59:39.365Z",
            updatedAt: "2020-09-28T12:59:39.365Z",
          },
        },
      },
      {
        id: 7,
        salesTransactionId: 3,
        productSkuId: 3,
        basePrice: 1650000,
        salePrice: 1500000,
        quantity: 55,
        packagingUnit: "Karung",
        packagingQuantity: 10,
        productCompositionStatus: null,
        createdAt: "2020-09-28T12:59:39.385Z",
        updatedAt: "2020-09-28T12:59:39.385Z",
        product_sku: {
          id: 3,
          farmerId: 1,
          farmer: {
            user: {
              fullName: "Ryan Gosling",
            },
          },
          sku: "824579818",
          variantId: 3,
          price: 30000,
          draftStock: 500,
          directSellStock: 250,
          consignmentStock: 250,
          createdAt: "2020-09-28T12:59:39.370Z",
          updatedAt: "2020-09-28T12:59:39.370Z",
          product_variant: {
            id: 3,
            subcategor: 2,
            productNam: "Grade 1",
            realStock: 500,
            draftStock: 250,
            govPrice: null,
            empofarmPr: null,
            imageUrl:
              "https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/image_nodes/tomatoes_helios4eos_gettyimages-edit.jpeg?itok=4KrW14a4",
            createdAt: "2020-09-28T12:59:39.365Z",
            updatedAt: "2020-09-28T12:59:39.365Z",
          },
        },
      },
    ],
    createdAt: randomizeData(requestDate),
  };
});
