/* eslint-disable no-unused-vars */
import React from "react";
import { BodyText, HeaderText } from "../../../components/Typography";
import ProfilePic from "../../../assets/images/profpict.jpg";
import { Card } from "../../../components/Card";

export default (props) => {
  return (
    <div
      style={{
        border: "1px solid #D2E0C9",
        borderRadius: "8px",
        padding: "16px",
        margin: "8px 0px",
      }}
    >
      <Card className="d-flex flex-row">
        <div>
          <img
            src={ProfilePic}
            style={{ width: "84px", height: "84px" }}
            alt=""
          />
        </div>
        <div className="ml-2">
          <HeaderText bold accent>
            Madu Murni
          </HeaderText>
          <div className="d-flex flex-row flex-wrap row mt-2">
            <div className="d-flex flex-column col-3">
              <BodyText>SKU</BodyText>
              <BodyText bold>081920192 0912 09120</BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText>Farmer</BodyText>
              <BodyText bold>Joko Tole</BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText>Pilihan Packaging</BodyText>
              <BodyText bold>Botol</BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText>Total Pembelian</BodyText>
              <BodyText bold>500 Botol</BodyText>
            </div>
          </div>
        </div>
      </Card>
    </div>
  );
};
