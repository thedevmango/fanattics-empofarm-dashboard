/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { Button } from "../../../../components/Button";
import { BackButton } from "../../../../components/Molecules";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  ReportQC,
  ReportQCItem,
  RincianTender,
  RincianTenderItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { HeaderText, PageTitle } from "../../../../components/Typography";
import { API_URL, InboundStatus } from "../../../../constants";
import { formatquantity } from "../../../../configs/formatnumber";
import PopupPilihPetugas from "../../Modal/PopupPilihPetugas";
import PopupKonfirmasiQC from "../../Modal/PopupKonfirmasiQC";
import Axios from "axios";
import { enumIQCStatus, enumTenderStatus } from "../../../../configs/enums";

export default (props: any) => {
  const parentData = props.location.state.data;
  const { QCIncoming, QCDone } = InboundStatus;

  const [data, setData] = useState<any>(undefined);
  const [shipmentLogs, setShipmentLogs] = useState<any>(undefined);
  const [iqcReports, setIqcReports] = useState<any>(undefined);
  const [popupPickOfficer, setPopupPickOfficer] = useState(false);
  const [popupConfirmQC, setPopupConfirmQC] = useState(false);

  //* FUNCTION
  const togglePopupPickOfficer = () => setPopupPickOfficer(!popupPickOfficer);
  const togglePopupConfirmQC = () => setPopupConfirmQC(!popupConfirmQC);

  const handleSubmitConfirmQC = async () => {
    try {
      const { data } = await Axios.put(`${API_URL}/purchases/${parentData.id}`, {
        status: "nominal-value-input",
      });
      console.log(data.message);
      setPopupConfirmQC(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleDownloadReport = () => {
    return alert("downloaded");
  };

  // console.log("DATA", data);
  // console.log("REPORTS", iqcReports);
  const getDetailsInfo = async () => {
    try {
      const details = await Axios.get(`${API_URL}/quality-controls/purchasing-list`, {
        params: {
          id: parentData.id,
          // include: ["courier", "shipment_log"],
        },
      });
      const resVariant = await Axios.get(
        `${API_URL}/products/variants?id=${parentData.productVariantId}`
      );
      const variant = resVariant.data.result[0];
      const resSubcategory = await Axios.get(
        `${API_URL}/products/subcategories?id=${variant.subcategoryId}`
      );
      const subcategory = resSubcategory.data.result[0];
      const resCategory = await Axios.get(
        `${API_URL}/products/categories?id=${subcategory.categoryId}`
      );
      const category = resCategory.data.result[0];
      const result = {
        ...details.data.result[0],
        product_variant: {
          ...variant,
          subcategory: {
            ...subcategory,
            category: {
              ...category,
            },
          },
        },
      };
      setData(result);
    } catch (err) {
      console.log(err);
    }
  };

  const getShipmentLogs = async () => {
    if (!data) return;
    if (!data.courrierId) return;
    try {
      const courier = await Axios.get(`${API_URL}/users/couriers?id=${data.courrierId}`);
      const logs = await Axios.get(`${API_URL}/logs/shipments`, {
        params: { purchasingTransactionId: data.id },
      });
      setShipmentLogs({ ...courier.data.result[0], logs: logs.data.result });
    } catch (err) {
      setShipmentLogs(undefined);
      console.log(err);
    }
  };

  const getQCReports = async () => {
    if (!data) return;
    if (!data.iqcOfficerId) return;
    try {
      const resOfficer = await Axios.get(`${API_URL}/users/admins?id=${data.iqcOfficerId}`);
      const resReports = await Axios.get(`${API_URL}/quality-controls/inbounds`, {
        params: { purchasingTransactionId: parentData.id },
      });
      const reports = resReports.data.result;
      console.log("tess", reports);
      setIqcReports({ admin: resOfficer.data.result[0], reports: reports[reports.length - 1] });
    } catch (err) {
      setIqcReports(undefined);
      console.log(err);
    }
  };

  const handleAssignOfficer = async (selected: number) => {
    try {
      const { data } = await Axios.put(`${API_URL}/purchases/${parentData.id}`, {
        iqcOfficerId: selected,
        status: "on-qc-inbound",
      });
      console.log(data.message);
      getDetailsInfo();
      getShipmentLogs();
      getQCReports();
      setPopupPickOfficer(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getShipmentLogs();
    getQCReports();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    getDetailsInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [popupPickOfficer]);

  console.log("tes", data);
  // if ((!data && !iqcReports) || (QCDone.includes(parentData.status) && !iqcReports) || !iqcReports)
  if (!data && !iqcReports)
    return (
      <div className="qc-inbound qc-inbound-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="qc-inbound qc-inbound-details">
      <Helmet>
        <title>Inbound Details | Kontrol</title>
      </Helmet>

      <PopupPilihPetugas
        {...props}
        type="iqc"
        handleAssignOfficer={handleAssignOfficer}
        isOpen={popupPickOfficer}
        toggle={togglePopupPickOfficer}
      />

      <PopupKonfirmasiQC
        onSubmit={handleSubmitConfirmQC}
        isOpen={popupConfirmQC}
        toggle={togglePopupConfirmQC}
      />

      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="d-flex align-items-center justify-content-between mb-4">
        <PageTitle>Quality Control Order Detail</PageTitle>

        {QCIncoming.includes(parentData.status) ? (
          <Button containerStyle={{ minWidth: 200 }} theme="primary">
            DOWNLOAD HALAMAN INI
          </Button>
        ) : QCDone.includes(parentData.status) && parentData.status === "iqc-passed" ? (
          <Button onClick={togglePopupConfirmQC} containerStyle={{ minWidth: 160 }} theme="primary">
            Verifikasi Hasil QC
          </Button>
        ) : null}
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx
            timestamp={moment(parentData.createdAt).format("DD/MM/YYY H:mm")}
            link={false}
          >
            <HeaderText>{enumIQCStatus(data.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Farmer"
            name={parentData.farmer.user.fullName}
            contact={parentData.farmer.telephoneNumber}
            address={parentData.farmer.farmAddress}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {parentData.transactionCode}
            </DataTransaksiItem>
          </DataTransaksi>

          <ReportQC
            type="Inbound"
            officerName={!data.iqcOfficerId || !iqcReports ? undefined : iqcReports.admin.fullName}
            itemName={
              QCDone.includes(parentData.status)
                ? `${data.product_variant.subcategory.name} ${data.product_variant.productName}`
                : undefined
            }
            qcPassed={
              !iqcReports
                ? false
                : iqcReports.reports
                ? iqcReports.reports.status !== "reject"
                : false
            }
            handleDownloadReport={handleDownloadReport}
            handlePickOfficer={!data.iqcOfficerId ? togglePopupPickOfficer : false}
            description={
              !iqcReports ? false : iqcReports.reports ? iqcReports.reports.notes : false
            }
          >
            {QCDone.includes(parentData.status) && iqcReports ? (
              <>
                <ReportQCItem
                  title="Quantity"
                  signedBy={iqcReports.admin.fullName}
                  timestamp={moment(iqcReports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                  // verified={iqcReports.reports.status !== "reject"}
                  verified
                  valueOffer={`${parentData.quantity} Kg`}
                  valueActual={`${iqcReports.reports.quantity} Kg`}
                />
                <ReportQCItem
                  title="Quality"
                  signedBy={iqcReports.admin.fullName}
                  timestamp={moment(iqcReports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                  // verified={iqcReports.reports.status !== "reject"}
                  verified
                  valueOffer={parentData.product_variant.productName}
                  valueActual={iqcReports.reports.quality}
                />
              </>
            ) : null}
          </ReportQC>
        </div>

        <div className="col-8">
          <RincianTender
            cover={data.product_variant.imageUrl}
            productName={`${data.product_variant.subcategory.name} ${data.product_variant.productName}`}
            className="pb-5"
          >
            <RincianTenderItem title="Category">
              {data.product_variant.subcategory.category.categoryName}
            </RincianTenderItem>
            <RincianTenderItem title="Sub Category">
              {data.product_variant.subcategory.name}
            </RincianTenderItem>
            <RincianTenderItem title="Varian">{data.product_variant.productName}</RincianTenderItem>
            <RincianTenderItem title="SKU">{data.product_sku.sku}</RincianTenderItem>
            <RincianTenderItem title="Kuantitas Barang">
              {formatquantity(data.quantity)} Kilogram
            </RincianTenderItem>
            <RincianTenderItem title="Deskripsi Barang">
              {data.description || "-"}
            </RincianTenderItem>
          </RincianTender>

          {shipmentLogs ? <RiwayatEkspedisi data={shipmentLogs} /> : null}
        </div>
      </div>
    </div>
  );
};

const dummyEkspedisiItem = (
  status: string,
  location: string,
  timestamp: any,
  description?: string
) => ({
  status,
  location,
  timestamp,
  description,
});
const dummyEkspedisi = [
  dummyEkspedisiItem("picking", "-", moment(), "Diambil menggunakan truk"),
  dummyEkspedisiItem("picked", "Farm Joko Tole", moment().add(3, "d")),
  dummyEkspedisiItem("sending", "Logistik", moment().add(4, "d")),
  dummyEkspedisiItem("arrived", "Empofarm", moment().add(6, "d")),
];
