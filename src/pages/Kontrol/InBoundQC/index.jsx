/* eslint-disable no-unused-vars */
import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import moment from "moment";
import { BsChevronRight } from "react-icons/bs";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  TableFooter,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { Button } from "../../../components/Button";
import Tabs from "../../../components/Tabs";
import { BodyText, MetadataText, PageTitle } from "../../../components/Typography";
import { InboundStatus } from "../../../constants";
import { gray500 } from "../../../constants/Colors";
import BadgeQC from "../BadgeQC";
import { useDispatch, useSelector } from "react-redux";
import { IQC_P_TAB } from "../../../redux/types";
import { getTaskList } from "../../../redux/actions/i_qc";

export default (props) => {
  const dispatch = useDispatch();
  const { QCIncoming, QCOnProcess, QCDone } = InboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state) => state.i_qc.tab);
  const data = useSelector((state) => state.i_qc.taskList);

  //* FUNCTION
  const toggleTab = (tab) => dispatch({ type: IQC_P_TAB, payload: tab });

  const fetchData = () => {
    dispatch(getTaskList({ status: tab }));
  };

  //* FETCH DATA
  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tab]);

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  // console.log(data);
  return (
    <div className="qc-inbound">
      <Helmet>
        <title>Inbound | Kontrol</title>
      </Helmet>

      <PageTitle className="mb-4">Inbound Quality Control</PageTitle>

      <Tabs
        className="col-7 pl-0 mb-3"
        options={["QC Order", "In Progress", "Finished QC"]}
        slug={[QCIncoming, QCOnProcess, QCDone]}
        activeTab={tab}
        setActiveTab={toggleTab}
      />

      <Table bordered>
        <TableHead>
          <TableRow>
            <TableHeadCell>Kode Transaksi</TableHeadCell>
            <TableHeadCell>Nama Farmer</TableHeadCell>
            <TableHeadCell>Nama Item</TableHeadCell>
            <TableHeadCell>Lokasi</TableHeadCell>

            {tab === QCIncoming ? (
              <TableHeadCell style={{ textAlign: "right" }}>Tanggal Permintaan</TableHeadCell>
            ) : (
              <TableHeadCell>QC Admin</TableHeadCell>
            )}

            {tab === QCDone ? <TableHeadCell>Inbound QC Report</TableHeadCell> : null}

            <TableHeadCell>&nbsp;</TableHeadCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <RenderBodyTable {...props} tab={tab} data={currentData()} />
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell colSpan={10}>
              <Pagination
                next={next}
                prev={prev}
                jump={jump}
                pages={pages}
                currentPage={currentPage}
                maxPage={maxPage}
              />
            </TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </div>
  );
};

const RenderBodyTable = ({ data, history, match, tab }) => {
  const { QCIncoming, QCDone } = InboundStatus

  if (!data) return null;
  return data.map((item, idx) => {
    const link = `${match.path}/details`
    const nextClick = () => history.push(link, { data: item })
    
    const qcReports = item.iqc_reports[item.iqc_reports.length - 1];

    return (
      <TableRow key={idx} onClick={nextClick}>
        <TableCell>{item.transactionCode}</TableCell>
        <TableCell>{item.farmer.user.fullName}</TableCell>
        <TableCell>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <BodyText>
              {`${item.product_variant.subcategory.name} ${item.product_variant.productName}`}
            </BodyText>
            <MetadataText color={gray500}>
              {item.product_variant.subcategory.category.categoryName}
            </MetadataText>
          </div>
        </TableCell>
        <TableCell>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {/* //TODO LOCATION SHOULD BE ADDED SOON */}
            <BodyText>{"-"}</BodyText>
            <MetadataText color={gray500}>{"-"}</MetadataText>
          </div>
        </TableCell>

        {tab === QCIncoming ? (
          <TableCell style={{ textAlign: "right" }}>
            {moment(item.createdAt).format("DD/MM/YYYY")}
          </TableCell>
        ) : (
          <TableCell>{item.iqcOfficer.fullName}</TableCell>
        )}

        {tab === QCDone ? (
          <TableCell>
            {!qcReports ? null : qcReports.status === "reject" ? (
              <BadgeQC color="danger">Ditolak</BadgeQC>
            ) : (
              <BadgeQC color="primary">Diterima</BadgeQC>
            )}
          </TableCell>
        ) : null}

        <TableCell style={{ width: 50, borderLeft: "0 solid #E1E1E1" }}>
          <Button theme="action-table">
            <BsChevronRight size={12} />
          </Button>
        </TableCell>
      </TableRow>
    );
  });
};
