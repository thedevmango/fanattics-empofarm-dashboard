import React from "react";
import { Button, ButtonIcon } from "../../components/Button";
import { BsFillLightningFill } from "react-icons/bs";

interface Props {
  children: React.ReactNode;
  color: "primary" | "danger" | "warning";
  style: React.CSSProperties;
}

export default function BadgeQC({ children, color, style }: Props) {
  return (
    <Button
      theme="primary"
      containerStyle={{
        borderRadius: 4,
        padding: "2px 8px",
        backgroundColor:
          color === "primary" ? "#107C10" : color === "danger" ? "#D92C2C" : "#FFD335",
        minWidth: 90,
        color: color === "warning" ? "#000" : "#FFF",
        ...style,
      }}
    >
      <ButtonIcon type="prepend">
        <BsFillLightningFill color={color === "warning" ? "#000" : "#FFF"} />
      </ButtonIcon>
      {children}
    </Button>
  );
}
