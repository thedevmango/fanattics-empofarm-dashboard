/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";
import InBoundQC from "./InBoundQC";
import InBoundQCDetails from "./InBoundQC/Details";
import OutBoundQC from "./OutBoundQC";
import OutBoundQCDetails from "./OutBoundQC/Details";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/inbound-qc`} exact component={InBoundQC} />
      <Route path={`${props.match.path}/inbound-qc/details`} component={InBoundQCDetails} />
      <Route path={`${props.match.path}/outbound-qc`} exact component={OutBoundQC} />
      <Route path={`${props.match.path}/outbound-qc/details`} component={OutBoundQCDetails} />
    </>
  );
};
