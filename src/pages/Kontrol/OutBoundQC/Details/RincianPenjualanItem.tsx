/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import React, { useEffect, useState } from "react";
import { BodyText, Card, HeaderText } from "../../../../components";
import LaporanInventoryManager from "../../../../components/Molecules/PanelDetail/LaporanInventoryManager";
import { formatquantity } from "../../../../configs/formatnumber";
import { API_URL } from "../../../../constants";

interface Props {
  item: any;
}

export default ({ item }: Props) => {
  const [data, setData] = useState<any>(undefined);

  const getDetails = async () => {
    try {
      let userId = item.product_sku.farmer.userId;
      let variantId = item.product_sku.variantId;
      const resUser = await Axios.get(`${API_URL}/users?id=${userId}`);
      let user = resUser.data.result[0];
      const resVariant = await Axios.get(`${API_URL}/products/variants?id=${variantId}`);
      let variant = resVariant.data.result[0];
      const resSubcategory = await Axios.get(
        `${API_URL}/products/subcategories?id=${variant.subcategoryId}`
      );
      let subcategory = resSubcategory.data.result[0];

      setData({
        ...item,
        product_sku: {
          ...item.product_sku,
          farmer: {
            ...item.product_sku.farmer,
            user: {
              ...user,
            },
          },
          product_variant: {
            ...variant,
            subcategory: {
              ...subcategory,
            },
          },
        },
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getDetails();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log("item", item);
  // console.log("data", data);
  if (!data) return null;
  return (
    <Card className="rincian-penjualan-item">
      <div className="body">
        <div className="image-wrapper">
          <img
            style={{ width: 84, height: 84, objectFit: "cover" }}
            src={data.product_sku.product_variant.imageUrl}
            alt="product"
          />
        </div>

        <div className="product-info">
          <div className="product-info-title">
            <HeaderText accent bold>
              {`${data.product_sku.product_variant.subcategory.name} ${data.product_sku.product_variant.productName}`}
            </HeaderText>
          </div>
          <div className="product-info-details">
            <div className="details-item">
              <BodyText color="#605E5C">SKU</BodyText>
              <BodyText color="#323130" bold>
                {item.product_sku.sku}
              </BodyText>
            </div>
            <div className="details-item">
              <BodyText color="#605E5C">Farmer</BodyText>
              <BodyText color="#323130" bold>
                {data.product_sku.farmer.user.fullName}
              </BodyText>
            </div>
            <div className="details-item">
              <BodyText color="#605E5C">Pilihan Packaging</BodyText>
              <BodyText color="#323130" bold>
                {item.packagingUnit}
              </BodyText>
            </div>
            <div className="details-item">
              <BodyText color="#605E5C">Total Pembelian</BodyText>
              <BodyText color="#323130" bold>
                {formatquantity(item.quantity)}&nbsp;Kg
              </BodyText>
            </div>
          </div>

          <div className="product-info-details">
            <div className="details-item">
              <BodyText color="#605E5C">Harga per Satuan</BodyText>
              <BodyText color="#323130" bold>
                Rp&nbsp;{formatquantity(item.product_sku.price)}/Kg
              </BodyText>
            </div>
            <div className="details-item">
              <BodyText color="#605E5C">Harga Total</BodyText>
              <BodyText color="#323130" bold>
                Rp&nbsp;{formatquantity(item.quantity * item.product_sku.price)}
              </BodyText>
            </div>
            <div className="details-item">
              <BodyText color="#605E5C">Harga Packaging</BodyText>
              <BodyText color="#323130" bold>
                Rp&nbsp;{formatquantity(item.packagingCost)}/Kg
              </BodyText>
            </div>
          </div>
        </div>
      </div>

      <div className="accordion">{/* <LaporanInventoryManager data={item} /> */}</div>
    </Card>
  );
};
