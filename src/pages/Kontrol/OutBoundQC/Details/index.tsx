/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import {
  BackButton,
  Button,
  ButtonIcon,
  HeaderText,
  PageTitle,
  PaneHeader,
  Panel,
  SubjectTitle,
} from "../../../../components";
import { IoMdRadioButtonOff } from "react-icons/io";
import { API_URL, OutboundStatus } from "../../../../constants";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  ReportQC,
  ReportQCItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import PopupPilihPetugas from "../../Modal/PopupPilihPetugas";
import PopupKonfirmasiQC from "../../Modal/PopupKonfirmasiQC";
import RincianPenjualan from "./RincianPenjualan";
import RincianPenjualanItem from "./RincianPenjualanItem";
import Axios from "axios";

export default (props: any) => {
  const parentData = props.location.state.data;
  const { QCIncoming, QCInprocess, QCDone } = OutboundStatus;

  //* LOCAL STATE
  const [popupPickOfficer, setPopupPickOfficer] = useState(false);
  const [popupConfirmQC, setPopupConfirmQC] = useState(false);
  const togglePopupPickOfficer = () => setPopupPickOfficer(!popupPickOfficer);
  const togglePopupConfirmQC = () => setPopupConfirmQC(!popupConfirmQC);

  const [data, setData] = useState<any>(undefined);
  const [shipmentLogs, setShipmentLogs] = useState<any>(undefined);
  const [oqcReports, setOqcReports] = useState<any>(undefined);

  //* FUNCTION
  const getDetailsInfo = async () => {
    try {
      const details = await Axios.get(`${API_URL}/sales`, {
        params: { id: parentData.id },
      });
      // const resVariant = await Axios.get(`${API_URL}/products/variants?id=${parentData.productVariantId}`);
      // const variant = resVariant.data.result[0]
      // const resSubcategory = await Axios.get(`${API_URL}/products/subcategories?id=${variant.subcategoryId}`);
      // const subcategory = resSubcategory.data.result[0]
      // const resCategory = await Axios.get(`${API_URL}/products/categories?id=${subcategory.categoryId}`);
      // const category = resCategory.data.result[0]
      // const result = {
      //   ...details.data.result[0],
      //   product_variant: {
      //     ...variant,
      //     subcategory: {
      //       ...subcategory,
      //       category: {
      //         ...category
      //       }
      //     }
      //   }
      // };
      const result = { 
        ...details.data.result[0]
      }
      setData(result);
    } catch (err) {
      console.log(err);
    }
  };

  const handleAssignOfficer = async (selected: number) => {
    try {
      const { data } = await Axios.put(`${API_URL}/sales/${parentData.id}`, {
        oqcOfficerId: selected,
        status: "outbound-outboundqc"
      })
      console.log(data.message)
      getDetailsInfo()
      getShipmentLogs()
      getQCReports()
      setPopupPickOfficer(false)
    } catch (err) {
      console.log(err)
    }
  }

  const getShipmentLogs = async () => {
    if (!data) return;
    if (!data.courierId) return;
    try {
      const courier = await Axios.get(`${API_URL}/users/couriers?id=${data.courierId}`);
      const logs = await Axios.get(`${API_URL}/logs/shipments`, {
        params: { salesTransactionId: data.id },
      });
      setShipmentLogs({ ...courier.data.result[0], logs: logs.data.result });
    } catch (err) {
      setShipmentLogs(undefined);
      console.log(err);
    }
  };

  const getQCReports = async () => {
    if (!data) return
    if (!data.oqcOfficerId) return
    try {
      const resOfficer = await Axios.get(`${API_URL}/users/admins?id=${data.oqcOfficerId}`)
      setOqcReports({ admin: resOfficer.data.result[0] })
    } catch (err) {
      setOqcReports(undefined)
      console.log(err)
    }
  }

  const handlePickOfficer = () => setPopupPickOfficer(true);
  const handleDownloadReport = () => alert("downloaded")

  const handleConfirmQC = async () => {
    console.log("this is chamber")
    Axios.put(`${API_URL}/sales/${parentData.id}`, {
      status: "outbound-chamber"
    }).then(() => {
      getDetailsInfo()
      setPopupConfirmQC(false)
    }).catch((err) => console.log(err))
  }

  const handleCancelQC = async () => {
    console.log("this is rejected")
    Axios.put(`${API_URL}/sales/${parentData.id}`, {
      status: "oqc-rejected"
    }).then(() => {
      getDetailsInfo()
      setPopupConfirmQC(false)
    }).catch((err) => console.log(err))
  }

  useEffect(() => {
    getShipmentLogs();
    getQCReports()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    getDetailsInfo()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log("parent", parentData);
  // console.log("details", data);
  // console.log("logs", shipmentLogs);
  console.log("reports", oqcReports);
  if (!data) return (
    <div className="qc-outbound qc-outbound-details">
      <Helmet>
        <title>loading...</title>
      </Helmet>
      <BackButton>Kembali ke Transactional Activities</BackButton>
    </div>
  )
  return (
    <div className="qc-outbound qc-outbound-details">
      <Helmet>
        <title>Outbound Details | Kontrol</title>
      </Helmet>

      <PopupPilihPetugas
        {...props}
        type="oqc"
        handleAssignOfficer={handleAssignOfficer}
        isOpen={popupPickOfficer}
        toggle={togglePopupPickOfficer}
      />

      <PopupKonfirmasiQC
        onSubmit={handleConfirmQC}
        isOpen={popupConfirmQC}
        toggle={togglePopupConfirmQC}
        onCancel={handleCancelQC}
      />

      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="d-flex align-items-center justify-content-between mb-4">
        <PageTitle>Quality Control Order Detail</PageTitle>

        <div className="button-wrapper d-flex">
          <Button containerStyle={{ minWidth: 220 }} theme="primary">
            DOWNLOAD HALAMAN INI
            <ButtonIcon type="append">
              <IoMdRadioButtonOff />
            </ButtonIcon>
          </Button>

          {data.status === "oqc-approval" ? (
            <Button
              onClick={togglePopupConfirmQC}
              containerStyle={{ minWidth: 220, marginLeft: 10 }}
              theme="primary"
            >
              VERIFIKASI HASIL QC
            </Button>
          ) : null}
        </div>
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(parentData.createdAt).format("DD/MM/YYY H:mm")}>
            <HeaderText>{data.status}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Buyer"
            name={parentData.buyer.user.fullName}
            contact={parentData.buyer.user.phoneNumber}
            address={parentData.buyer.user.address}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {parentData.transactionCode || "-"}
            </DataTransaksiItem>
          </DataTransaksi>

          <ReportQC
            type="Outbound"
            officerName={
              !data.oqcOfficerId || !oqcReports ? undefined : oqcReports.admin.fullName
            }
            handleDownloadReport={handleDownloadReport}
            handlePickOfficer={!data.oqcOfficerId ? handlePickOfficer : false}
          >
            {QCDone.includes(parentData.status) ? (
              <>
                <ReportQCItem
                  title="Quantity"
                  signedBy={parentData.oqc_officer.fullName}
                  timestamp={"07/09/2020 15:15"}
                  verified
                />
                <ReportQCItem
                  title="Quality"
                  signedBy={parentData.oqc_officer.fullName}
                  timestamp={"07/09/2020 15:15"}
                  verified
                />
                <ReportQCItem
                  title="Packaging"
                  signedBy={parentData.oqc_officer.fullName}
                  timestamp={"07/09/2020 15:15"}
                  verified
                />
                <ReportQCItem
                  title="Document"
                  signedBy={parentData.oqc_officer.fullName}
                  timestamp={"07/09/2020 15:15"}
                  verified
                />
              </>
            ) : null}
          </ReportQC>
        </div>

        <div className="col-8">
          <RincianPenjualan parentData={parentData}>
            {parentData.sale_transaction_details.map((item: any, i: number) => {
              return <RincianPenjualanItem key={i} item={item} />;
            })}
          </RincianPenjualan>

          <Panel className="mb-3">
            <div className="header pb-3 border-bottom">
              <PaneHeader bold>Kelengkapan Dokumen</PaneHeader>
            </div>
            <div className="mb-3"></div>

            <div className="body">
              <Panel
                style={{
                  padding: "18px 8px",
                  boxShadow: `0px 0.3px 0.9px rgba(0, 0, 0, 0.1),
                  0px 1.6px 3.6px rgba(0, 0, 0, 0.13)`,
                }}
                className="mb-3 d-flex flex-column"
              >
                <SubjectTitle color="#605E5C">Shipmen List</SubjectTitle>
                <HeaderText color="#107C10">Daftar Pengiriman.pdf</HeaderText>
              </Panel>
              <Panel
                style={{
                  padding: "18px 8px",
                  boxShadow: `0px 0.3px 0.9px rgba(0, 0, 0, 0.1),
                  0px 1.6px 3.6px rgba(0, 0, 0, 0.13)`,
                }}
                className="mb-3 d-flex flex-column"
              >
                <SubjectTitle color="#605E5C">Surat Jalan</SubjectTitle>
                <HeaderText color="#107C10">Surat Jalan 20123184.pdf</HeaderText>
              </Panel>
            </div>
          </Panel>

          {shipmentLogs ? <RiwayatEkspedisi data={shipmentLogs} /> : null}
        </div>
      </div>
    </div>
  );
};

const dummyEkspedisiItem = (
  status: string,
  location: string,
  timestamp: any,
  description?: string
) => ({
  status,
  location,
  timestamp,
  description,
});
const dummyEkspedisi = [
  dummyEkspedisiItem("picking", "-", moment(), "Diambil menggunakan truk"),
  dummyEkspedisiItem("picked", "Farm Joko Tole", moment().add(3, "d")),
  dummyEkspedisiItem("sending", "Logistik", moment().add(4, "d")),
  dummyEkspedisiItem("arrived", "Empofarm", moment().add(6, "d")),
];
