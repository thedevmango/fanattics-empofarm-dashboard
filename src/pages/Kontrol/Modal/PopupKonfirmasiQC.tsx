/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { MdClose } from "react-icons/md";
import { BodyText, Button, Modal, ModalBody, PaneHeader } from "../../../components";

interface Props {
  isOpen: boolean;
  onCancel?: () => void;
  onSubmit?: () => void;
  toggle?: () => void;
}

export default ({ isOpen, onCancel, onSubmit, toggle }: Props) => {
  return (
    <Modal size="lg" isOpen={isOpen} style={{ padding: "24px 32px", borderRadius: 2 }}>
      <ModalBody>
        <div className="mb-4 d-flex justify-content-between">
          <PaneHeader bold>Konfirmasi Hasil QC</PaneHeader>

          <Button
            containerStyle={{
              padding: 0,
              backgroundColor: "transparent",
            }}
            onClick={toggle}
            theme="text"
          >
            <MdClose size={16} />
          </Button>
        </div>

        <BodyText color="#605E5C">
          Dengan menyutujui hasil QC ini, transaksi akan dilanjutkan ke proses berikutnya.
        </BodyText>

        <div className="button-wrapper mt-4 d-flex">
          <Button onClick={onSubmit} containerStyle={{ marginLeft: "auto" }} theme="primary">
            Setuju
          </Button>
          <Button
            onClick={onCancel ? onCancel : toggle}
            containerStyle={{ marginLeft: 5 }}
            theme="secondary"
          >
            Tidak
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
