/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import { INVENTORY_P_TAB } from "../../../redux/types/inventory";
import { Pagination, usePagination } from "../../../components/Pagination";
import { PageTitle } from "../../../components/Typography";
import Searchbar from "../../../components/Searchbar";
import { Panel } from "../../../components/Panel";
import { Form } from "../../../components/Form";
import Tabs from "../../../components/Tabs";
import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../components/Table";
import { Button } from "../../../components/Button";
import { BsChevronRight } from "react-icons/bs";
import { OutboundStatus } from "../../../constants";
import { getTPCList } from "../../../redux/actions/inventory";

export default (props) => {
  const dispatch = useDispatch();
  const { TPCIncoming, TPCInprocess, TPConQC, TPCDone } = OutboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state) => state.inventory.tab);
  const data = useSelector((state) => state.inventory.TPCList);

  //* LOCAL STATE
  const [title, setTitle] = useState("");
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  //* FUNCTION
  const toggleTab = (selectedTab) => dispatch({ type: INVENTORY_P_TAB, payload: selectedTab });

  //* FETCH DATA
  useEffect(() => {
    if (tab === TPCIncoming) setTitle("Outbound Request");
    if (tab === TPCInprocess) setTitle("TPC Diproses");
    if (tab === TPConQC) setTitle("TPC dalam Outbound QC");
    if (tab === TPCDone) setTitle("TPC Selesai");
  }, [tab]);

  useEffect(() => {
    if (!data.length) return setFilteredData([]);
    if (searchQuery) {
      const results = data.filter((item) => {
        return item.buyer.user.fullName.toLowerCase().includes(searchQuery.toLowerCase());
      });
      return setFilteredData(results);
    } else {
      return setFilteredData(data);
    }
  }, [searchQuery, data]);

  useEffect(() => {
    dispatch(getTPCList({ status: tab }));
  }, [tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  // console.log(filteredData);
  return (
    <div className="inventory inventory-activities">
      <Helmet>
        <title>{`${title} | Inventory`}</title>
      </Helmet>

      <PageTitle>Outbound Inventory</PageTitle>

      <div className="row justify-content-between mt-4">
        <Tabs
          options={["Outbound Request", "TPC Diproses", "TPC dalam Outbound QC", "TPC Selesai"]}
          slug={[TPCIncoming, TPCInprocess, TPConQC, TPCDone]}
          activeTab={tab}
          setActiveTab={toggleTab}
          className="col-9"
        />

        <Form className="col-3">
          <Searchbar
            placeholder="Cari nama pembeli"
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          />
        </Form>
      </div>

      <Panel className="content mt-4">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Tanggal Permintaan</TableHeadCell>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Kode Invoice</TableHeadCell>
              <TableHeadCell>Nama Pemesan</TableHeadCell>
              {tab === TPCIncoming || tab === TPCInprocess ? (
                <TableHeadCell style={{ textAlign: "right" }}>Jumlah Item</TableHeadCell>
              ) : (
                <TableHeadCell style={{ textAlign: "right" }}>Admin QC</TableHeadCell>
              )}
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, history, match, tab }) => {
  const { TPCIncoming, TPCInprocess } = OutboundStatus;

  if (!data.length) return null;
  return data.map((item, i) => {
    const link = tab === TPCIncoming ? `${match.path}/sales-order` : `${match.path}/details`
    const nextClick = () => history.push(link, { data: item })
    
    return (
      <TableRow key={i} onClick={nextClick}>
        <TableCell>{moment(item.createdAt).format("DD/MM/YYYY")}</TableCell>
        <TableCell>{item.transactionCode || "-"}</TableCell>
        <TableCell>{item.invoiceCode || "-"}</TableCell>
        <TableCell>{item.buyer.user.fullName}</TableCell>
        {tab === TPCIncoming || tab === TPCInprocess ? (
          <TableCell style={{ textAlign: 'right'}}>{item.sale_transaction_details.length} Item</TableCell>
        ) : (
          <TableCell style={{ textAlign: 'right'}}>{item.oqc_officer.fullName || "-"}</TableCell>
        )}
        <TableCell style={{ width: 50, borderLeftColor: "#e1e1e1" }}>
          <Button theme="action-table">
            <BsChevronRight />
          </Button>
        </TableCell>
      </TableRow>
    );
  });
};
