/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { BodyText, Card, HeaderText } from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";
import { primary, secondary } from "../../../../constants/Colors";
import sales from "../../../../redux/reducers/sales";

export default ({ data }: { data: any }) => {
  return !data
    ? null
    : data.map((item: any, i: number) => {
        return (
          <Card key={i} className="rincian-penjualan-item">
            <div className="body">
              <div className="image-wrapper">
                <img
                  style={{ width: 84, height: 84, objectFit: "cover" }}
                  src={item.product_sku.product_variant.imageUrl}
                  alt="product"
                />
              </div>

              <div className="product-info">
                <div className="product-info-title">
                  <HeaderText accent bold>
                    {`${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`}
                  </HeaderText>
                </div>

                <div className="product-info-details">
                  <InfoDetailsItem title="SKU">{item.product_sku.sku}</InfoDetailsItem>
                  <InfoDetailsItem title="Farmer">
                    {item.product_sku.farmer.user.fullName}
                  </InfoDetailsItem>
                  <InfoDetailsItem title="Pilihan Packaging">{item.packagingUnit}</InfoDetailsItem>
                  <InfoDetailsItem title="Total Pembelian">
                    {formatquantity(item.quantity)} Kg
                  </InfoDetailsItem>
                </div>

                <div className="product-info-details">
                  <InfoDetailsItem title="Harga per Kuantitas">
                    Rp {formatquantity(item.salePrice)}/Kg
                  </InfoDetailsItem>
                  <InfoDetailsItem title="Harga Total">
                    Rp {formatquantity(item.salePrice * item.quantity)}
                  </InfoDetailsItem>
                  <InfoDetailsItem title="Harga Packaging">
                    Rp {formatquantity(item.packagingPrice)}/Kg
                  </InfoDetailsItem>
                </div>
              </div>
            </div>
          </Card>
        );
      });
};

const InfoDetailsItem = ({ children, title }) => {
  return (
    <div className="details-item">
      <BodyText color={secondary}>{title}</BodyText>
      <BodyText color={primary} bold>
        {children}
      </BodyText>
    </div>
  );
};
