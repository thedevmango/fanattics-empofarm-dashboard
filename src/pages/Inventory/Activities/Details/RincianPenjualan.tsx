/* eslint-disable react-hooks/exhaustive-deps */
import Axios from "axios";
import React, { useEffect, useState } from "react";
import {
  BodyText,
  GreetingTitle,
  HeaderText,
  PaneHeader,
  Panel,
  SubjectTitle,
} from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";
import { API_URL } from "../../../../constants";
import { gray500 } from "../../../../constants/Colors";
import RincianPenjualanItem from "./RincianPenjualanItem";

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  className?: string;
  parentData: any;
}

export default ({ className, parentData }: Props) => {
  const [saleDetails, setSaleDetails] = useState([]);

  function fetchSalesDetails(id: number) {
    Axios.get(`${API_URL}/sales/sales-details?salesTransactionId=${id}`)
      .then((res) => setSaleDetails(res.data.result))
      .catch((err) => console.log(err.response));
  }

  useEffect(() => {
    fetchSalesDetails(parentData.id);
  }, []);

  console.log("saleDetails", saleDetails);
  if (!saleDetails) return null;
  return (
    <Panel className={`mol-rincian-penjualan ${className}`}>
      <div className="header">
        <HeaderText bold color={gray500}>
          Rincian Penjualan
        </HeaderText>
      </div>

      <div className="content">
        <RincianPenjualanItem data={saleDetails} />
      </div>

      <div className="footer">
        <div className="biaya-tambahan">
          <div className="title">
            <HeaderText color="#000">Biaya Tambahan</HeaderText>
          </div>

          <div className="value">
            <ValueItem title="Biaya Packaging">Rp {formatquantity(undefined)}</ValueItem>
            <ValueItem title="Asuransi">Rp {formatquantity(undefined)}</ValueItem>
            <ValueItem title="Biaya Pengiriman">Rp {formatquantity(undefined)}</ValueItem>
          </div>
        </div>

        <div className="total-nominal-transaksi">
          <div className="title">
            <PaneHeader color="#605E5C">Total Nominal Penjualan</PaneHeader>
          </div>
          <div className="value">
            <GreetingTitle bold accent>
              {!parentData.totalPrice ? (
                <>&ndash;</>
              ) : (
                `Rp ${formatquantity(parentData.totalPrice)}`
              )}
            </GreetingTitle>
          </div>
        </div>
      </div>
    </Panel>
  );
};

const ValueItem = ({ children, title }) => (
  <div className="value-item">
    <BodyText color="#605E5C">{title}</BodyText>
    <SubjectTitle color="#323130">{children}</SubjectTitle>
  </div>
);
