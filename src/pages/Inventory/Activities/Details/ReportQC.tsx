import Axios from "axios";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { ReportQC, ReportQCItem } from "../../../../components/Molecules/PanelDetail";
import { API_URL, OutboundStatus } from "../../../../constants";

export default ({ parentData }: { parentData: any }) => {
  const QCDone = OutboundStatus.QCDone;

  const [oqcReports, setoqcReports] = useState<any>([]);

  function getOQCReports(id: number) {
    const link = `${API_URL}/quality-controls/outbounds?salesTransactionDetailId=${id}`;
    Axios.get(link)
      .then((res) => setoqcReports(res.data.result))
      .catch((err) => console.log(err));
  }

  const timestamper = (val: string) => moment(val).format("DD/MM/YYYY H:mm");

  useEffect(() => {
    getOQCReports(parentData.sale_transaction_details[0].id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ReportQC
      type="Outbound"
      officerName={parentData.oqcOfficerId ? parentData.oqc_officer.fullName : undefined}
    >
      {QCDone.includes(parentData.status) && oqcReports.length > 0 ? (
        <>
          <ReportQCItem
            title="Quantity"
            signedBy={parentData.oqc_officer.fullName}
            timestamp={timestamper(oqcReports[0].createdAt)}
            verified
          />
          <ReportQCItem
            title="Quality"
            signedBy={parentData.oqc_officer.fullName}
            timestamp={timestamper(oqcReports[0].createdAt)}
            verified
          />
          <ReportQCItem
            title="Packaging"
            signedBy={parentData.oqc_officer.fullName}
            timestamp={timestamper(oqcReports[0].createdAt)}
            verified
          />
          <ReportQCItem
            title="Document"
            signedBy={parentData.oqc_officer.fullName}
            timestamp={timestamper(oqcReports[0].createdAt)}
            verified
          />
        </>
      ) : null}
    </ReportQC>
  );
};
