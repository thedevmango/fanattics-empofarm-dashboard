/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import {
  BackButton,
  Button,
  DownloadButton,
  HeaderText,
  MetadataText,
  PageTitle,
  Panel,
} from "../../../../components";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  QRDownload,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { useDispatch, useSelector } from "react-redux";
import { getTPCList } from "../../../../redux/actions/inventory";
import { getShipmentLogs } from "../../../../redux/actions/logs";
import { OutboundStatus } from "../../../../constants";
import { enumOutboundStatus } from "../../../../configs/enums";
import ReportQC from "./ReportQC";
import RincianPenjualan from "./RincianPenjualan";
import DocumentPanel from "./DocumentPanel";

export default (props: any) => {
  const dispatch = useDispatch();
  const parentData = props.location.state.data;
  const { TPCInprocess } = OutboundStatus;

  const TPCDetails = useSelector((state: any) => state.inventory.TPCDetails);
  const shipmentLogs = useSelector((state: any) => state.logs.shipmentLogs);

  useEffect(() => {
    if (!TPCDetails) return;
    dispatch(getShipmentLogs({ data: TPCDetails, type: "outbound" }));
  }, [TPCDetails]);

  useEffect(() => {
    dispatch(getTPCList({ id: parentData.id }));
  }, []);

  console.log("TPCDetails", TPCDetails);
  if (!TPCDetails)
    return (
      <div className="inventory inventory-kitchen">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="inventory inventory-details">
      <Helmet>
        <title>Outbound Inventory Details</title>
      </Helmet>
      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="d-flex align-items-center justify-content-between mb-4">
        <PageTitle>Detail Transaksi Pemesanan</PageTitle>

        <DownloadButton />
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx
            timestamp={moment(TPCDetails.updatedAt).format("DD/MM/YYY H:mm")}
            link={false}
          >
            <HeaderText>{enumOutboundStatus(TPCDetails.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Buyer"
            name={TPCDetails.buyer.user.fullName}
            contact={TPCDetails.buyer.user.phoneNumber}
            address={TPCDetails.buyer.user.address}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(TPCDetails.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {TPCDetails.transactionCode || "#N/A"}
            </DataTransaksiItem>
            <DataTransaksiItem title="Bentuk Transaksi">Sales Order (SO)</DataTransaksiItem>
          </DataTransaksi>

          <QRDownload onDownload={() => alert("downloaded")} />

          {TPCInprocess.includes(parentData.status) ? null : <ReportQC parentData={parentData} />}
        </div>

        <div className="col-8">
          <RincianPenjualan parentData={TPCDetails} />

          <DocumentPanel />

          <RiwayatEkspedisi data={shipmentLogs} />
        </div>
      </div>
    </div>
  );
};
