/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useReducer, useState } from "react";
import {
  Button,
  Form,
  HeaderText,
  Input,
  PaneHeader,
  Panel,
  SubjectTitle,
} from "../../../../components";

export default (props: any) => {
  // const init_state: any = { files: [] };
  // const reducer = (state = init_state, { type, payload }) => {
  //   switch (type) {
  //     case "add":
  //       return { ...state, files: [...state.files, payload] };
  //     case "delete":
  //       return { ...state, files: state.files.filter((item: any) => item.filename !== payload) };
  //     default:
  //       return state;
  //   }
  // };
  // const [stateReducer, setReducer] = useReducer(reducer, init_state);

  const [documentList, setDocumentList] = useState<any[]>([]);
  const [documentFile, setDocumentFile] = useState<any>(undefined);
  const [documentTitle, setDocumentTitle] = useState<string>("");

  const handleChangeFile = (event: any) => {
    const fileUploaded = event.target.files[0];
    setDocumentFile(fileUploaded);
  };
  const handleChangeTitle = (event: any) => {
    const fileName = event.target.value;
    setDocumentTitle(fileName);
  };
  const handleSubmitFile = () => {
    const file = documentFile;
    file.title = documentTitle;
    setDocumentList([...documentList, file]);
    setDocumentFile(undefined);
    setDocumentTitle("");
  };

  // console.log(documentList, documentFile);
  return (
    <Panel className="mb-3">
      <div className="header pb-3 border-bottom">
        <PaneHeader bold>Kelengkapan Dokumen</PaneHeader>
      </div>

      <div className="body mt-3">
        {documentList.map((item, i) => {
          return (
            <UploadedDocument key={i} title={item.title || ""}>
              {item.name || ""}
            </UploadedDocument>
          );
        })}

        {/* <div className="row my-3">
          <div className="col-6 d-flex flex-column">
            <SubjectTitle color="#605E5C">Surat Jalan</SubjectTitle>
            <HeaderText color="#107C10">Surat Jalan 20123184.pdf</HeaderText>
          </div>
          <div className="col-6 d-flex">
            <div className="ml-auto">
              <Button theme="secondary">Ganti Dokumen</Button>
            </div>
          </div>
        </div> */}

        <InputDocument
          value={documentTitle}
          onChangeFile={handleChangeFile}
          onChangeName={handleChangeTitle}
        />

        <div className="d-flex justify-content-center">
          <Button
            disabled={!documentFile || !documentTitle}
            onClick={handleSubmitFile}
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </div>
    </Panel>
  );
};

const InputDocument = ({ value, onChangeFile, onChangeName }) => {
  return (
    <div className="row my-3">
      <div className="col-6">
        <Form>
          <Input value={value} onChange={onChangeName} type="text" placeholder="Nama File" />
        </Form>
      </div>
      <div className="col-6 d-flex">
        <div className="ml-auto">
          <input onChange={onChangeFile} id="input-doc" type="file" style={{ display: "none" }} />
          <label htmlFor="input-doc" className="component-btn component-btn-secondary">
            Upload Dokumen
          </label>
        </div>
      </div>
    </div>
  );
};

const UploadedDocument = ({
  children,
  onClick,
  title,
}: {
  children: React.ReactNode;
  onClick?: () => any;
  title: string;
}) => {
  return (
    <div className="row my-3">
      <div className="col-6 d-flex flex-column">
        <SubjectTitle color="#605E5C">{title}</SubjectTitle>
        <HeaderText color="#107C10">{children}</HeaderText>
      </div>
      <div className="col-6 d-flex">
        <div className="ml-auto">
          <Button onClick={onClick} theme="secondary">
            Ganti Dokumen
          </Button>
        </div>
      </div>
    </div>
  );
};
