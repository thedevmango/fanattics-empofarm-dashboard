import React from "react";
import { BodyText, Button, Card, PaneHeader } from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";
import { gray500, gray800, primaryBrand } from "../../../../constants/Colors";

export default ({
  border,
  item,
  onClick,
}: {
  border?: boolean;
  item: any;
  onClick?: () => any;
}) => {
  return (
    <>
      <Card
        onClick={onClick}
        className="rincian-penjualan-item"
        style={{
          borderColor: border ? primaryBrand : "transparent",
          cursor: "pointer",
        }}
      >
        <div className="body">
          <div className="image-wrapper">
            <img
              style={{ width: 84, height: 84, objectFit: "cover" }}
              src={item.product_sku.product_variant.imageUrl}
              alt="product"
            />
          </div>

          <div className="product-info">
            <div className="product-info-title">
              <PaneHeader>
                {`${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`}
              </PaneHeader>
            </div>

            <div className="product-info-details">
              <InfoDetailsItem title="SKU">{item.product_sku.sku}</InfoDetailsItem>
              <InfoDetailsItem title="Kuantitas">
                {formatquantity(item.quantity)} Kg
              </InfoDetailsItem>

              <div className="details-item" style={{ flex: 1 }}>
                <BodyText color={gray800}>Komposisi</BodyText>
                <Button
                  containerStyle={{
                    backgroundColor: "transparent",
                    marginTop: 6,
                    borderRadius: 4,
                    borderWidth: 1,
                    borderStyle: "solid",
                    borderColor: "#78A25E",
                  }}
                >
                  Terverifikasi
                </Button>
              </div>
            </div>

            <div className="product-info-details">
              <InfoDetailsItem title="Packaging">{item.packagingUnit}</InfoDetailsItem>
              <InfoDetailsItem title="Farmer">
                {item.product_sku.farmer.user.fullName}
              </InfoDetailsItem>
            </div>
          </div>
        </div>
      </Card>
      <div style={{ borderTop: "1px solid #E1E1E1", margin: "0 5px" }}></div>
    </>
  );
};

const InfoDetailsItem = ({ children, title }) => {
  return (
    <div className="details-item">
      <BodyText color={gray500}>{title}</BodyText>
      <BodyText color={gray800} bold>
        {children}
      </BodyText>
    </div>
  );
};
