/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import moment from "moment";
import { HeaderText, PageTitle, Panel, SubjectTitle } from "../../../../components";
import { gray500, gray800, primaryBrand, secondary } from "../../../../constants/Colors";

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  className?: string;
  data: any;
}

export default ({ className, data }: Props) => {
  return (
    <Panel className={className}>
      <PageTitle>Sales Order</PageTitle>

      <div className="mt-4">
        <div className="d-flex flex-row flex-wrap">
          <SalesOrderItem title="Kode Transaksi">{data.transactionCode || "#N/A"}</SalesOrderItem>
          <SalesOrderItem title="Performa Invoice" color={primaryBrand}>
            {data.invoiceCode || "#N/A"}
          </SalesOrderItem>
          <SalesOrderItem title="Tanggal Permintaan">
            {data.createdAt ? moment(data.createdAt).format("dddd, DD/MM/YYYY") : "#N/A"}
          </SalesOrderItem>
          <SalesOrderItem title="Paid Invoice" color={primaryBrand}>
            {data.invoiceCode || "#N/A"}
          </SalesOrderItem>
        </div>

        <div style={{ borderTop: "0.25px solid #8A8886", margin: "4px 0 20px" }}></div>

        <div className="d-flex flex-row flex-wrap">
          <SalesOrderItem title="Nama Pembeli" color={primaryBrand}>
            {data.buyer ? data.buyer.user.fullName : "#N/A"}
          </SalesOrderItem>
          <SalesOrderItem title="Nomor Kontak">
            {data.buyer ? data.buyer.user.phoneNumber : "#N/A"}
          </SalesOrderItem>
          <SalesOrderItem title="Alamat" color={gray500}>
            {data.buyer ? data.buyer.user.address : "#N/A"}
          </SalesOrderItem>
        </div>
      </div>
    </Panel>
  );
};

const SalesOrderItem = ({
  children,
  color = gray800,
  title,
}: {
  children: React.ReactNode;
  color?: string;
  title: string;
}) => {
  return (
    <div className="sales-order-info">
      <SubjectTitle color={secondary}>{title}</SubjectTitle>
      <HeaderText className="mt-1" color={color}>
        {children}
      </HeaderText>
    </div>
  );
};
