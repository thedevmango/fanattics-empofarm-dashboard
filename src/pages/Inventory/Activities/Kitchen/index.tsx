/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { BackButton, SubjectTitle } from "../../../../components";
import { API_URL } from "../../../../constants";
import RincianPenjualan from "./RincianPenjualan";
import RincianPenjualanItem from "./RincianPenjualanItem";
import SalesOrder from "./SalesOrder";
import TransactionKitchen from "./TransactionKitchen";

export default (props: any) => {
  const parentData = props.location.state.data;

  const [trxDetails, setTrxDetails] = useState<any[]>([]);
  const [selectedItem, setSelectedItem] = useState<{ id?: number } | undefined>(undefined);

  const toggleItem = (item: any) => {
    if (selectedItem?.id === item.id) return setSelectedItem(undefined);
    setSelectedItem(item);
  };

  async function fetchSaleTrxDetails(id: number) {
    try {
      const { data } = await Axios.get(`${API_URL}/sales/sales-details?salesTransactionId=${id}`);
      setTrxDetails(data.result);
    } catch (err) {
      console.log(err.response);
    }
  }

  useEffect(() => {
    fetchSaleTrxDetails(parentData.id);
  }, [parentData.id]);

  // useEffect(() => {
  //   /** ONLY FOR TESTING PANEL KICTHEN PURPOSES */

  //   if (!trxDetails) return;
  //   setSelectedItem(trxDetails[0]);
  // }, [trxDetails]);

  console.log("selectedItem", selectedItem);
  if (!trxDetails)
    return (
      <div className="inventory inventory-kitchen">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="inventory inventory-kitchen">
      <Helmet>
        <title>Transaction Kitchen</title>
      </Helmet>
      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="row mb-4">
        <div className="col">
          <SalesOrder data={parentData} className="content-sales-order" />
        </div>
      </div>

      <div className="row">
        <RincianPenjualan className="content-rincian-penjualan">
          {trxDetails.map((detail, i) => {
            return (
              <RincianPenjualanItem
                border={detail.id === selectedItem?.id}
                onClick={() => toggleItem(detail)}
                item={detail}
                key={detail.id}
              />
            );
          })}
        </RincianPenjualan>

        <TransactionKitchen data={selectedItem} />
      </div>
    </div>
  );
};
