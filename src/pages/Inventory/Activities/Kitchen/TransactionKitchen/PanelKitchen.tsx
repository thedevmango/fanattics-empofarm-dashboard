/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { BodyText, Button, PaneHeader, Panel } from "../../../../../components";
import { gray800, primary } from "../../../../../constants/Colors";

export default ({
  children,
  className,
  handleSimpan,
  isEditing,
  isVerified,
  isQtyMatched,
  item,
  toggleEditing,
}: {
  children: React.ReactNode;
  className: string;
  handleSimpan: () => any;
  isEditing: boolean;
  isVerified: boolean,
  isQtyMatched: boolean,
  item: any;
  toggleEditing: any;
}) => {
  useEffect(() => {
    if (isVerified === false) return toggleEditing(true);
  }, [isVerified]);

  return (
    <Panel className={className}>
      <div className="header" style={{ borderBottom: "0.25px solid #E1E1E1", marginBottom: 32 }}>
        <div className="d-flex justify-content-between mb-4">
          <PaneHeader bold color={gray800}>
            Transaction Kitchen
          </PaneHeader>
        </div>
        <div className="d-flex flex-column mb-4">
          <PaneHeader color={primary}>
            {`${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`}
          </PaneHeader>
          <BodyText>SKU:&nbsp;&nbsp;{`${item.product_sku.sku}`}</BodyText>
        </div>
      </div>

      <div className="content">{children}</div>

      <div className="button-wrapper">
        {isVerified && isEditing ? (
          <Button
            containerStyle={{ width: 130 }}
            onClick={() => toggleEditing(false)}
            theme="secondary"
          >
            Batalkan
          </Button>
        ) : null}

        {isEditing ? (
          <Button
            disabled={!isQtyMatched}
            containerStyle={{ width: 130 }}
            onClick={handleSimpan}
            theme="primary"
          >
            Simpan
          </Button>
        ) : (
          <Button
            containerStyle={{ width: 130 }}
            onClick={() => toggleEditing(true)}
            theme="primary"
          >
            Edit
          </Button>
        )}
      </div>
    </Panel>
  );
};
