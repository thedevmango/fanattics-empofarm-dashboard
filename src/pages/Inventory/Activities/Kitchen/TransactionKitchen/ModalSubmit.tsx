import React from "react";
import { Button, Modal, ModalBody, SubjectTitle } from "../../../../../components";

interface Props {
  children?: React.ReactNode;
  handleSubmit?: () => any;
  isOpen: boolean;
  toggle: () => any;
}

export default ({ children, handleSubmit, isOpen, toggle }: Props) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalBody>
        <SubjectTitle color="#000">Simpan Komposisi Item?</SubjectTitle>

        {children}

        <div className="d-flex justify-content-end mt-5">
          <Button
            onClick={toggle}
            containerStyle={{ marginRight: 12, width: 100 }}
            theme="secondary"
          >
            Kembali
          </Button>
          <Button
            onClick={handleSubmit}
            containerStyle={{ marginLeft: 12, width: 100 }}
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
