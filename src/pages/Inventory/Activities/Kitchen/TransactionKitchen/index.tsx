/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { MdErrorOutline } from "react-icons/md";
import {
  BodyText,
  Form,
  GreetingTitle,
  Input,
  MetadataText,
  SubjectTitle,
} from "../../../../../components";
import { formatquantity } from "../../../../../configs/formatnumber";
import { gray500 } from "../../../../../constants/Colors";
import ModalSubmit from "./ModalSubmit";
import PanelKitchen from "./PanelKitchen";

export default ({ data }: { data: any }) => {
  const [isSubmiting, setIsSubmiting] = useState(false);
  const [isVerified, setIsVerified] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const [qtyTotal, setQtyTotal] = useState(0);
  const [qtyReal, setQtyReal] = useState(0);
  const [qtyKonsinyasi, setQtyKonsinyasi] = useState(0);
  const isQtyMatched = qtyReal + qtyKonsinyasi === qtyTotal;

  const toggleEditing = (state: boolean) => setIsEditing(state);
  const toggleSubmiting = () => setIsSubmiting(!isSubmiting)

  const handleChange = (e: any) => {
    if (!data) return;

    let name = e.target.name;
    let value = e.target.value ? parseInt(e.target.value) : 0;

    if (name === "qtyReal" && value <= data.product_sku.draftStock) {
      setQtyReal(value);
    }
    if (name === "qtyKonsinyasi" && value <= data.product_sku.consignmentStock) {
      setQtyKonsinyasi(value);
    }
  };

  const openModalSimpan = () => {
    if (!isQtyMatched) return
    setIsSubmiting(true)
  };
  const handleSimpan = () => {
    setIsSubmiting(false)
  }

  useEffect(() => {
    if (!data) return;
    setQtyTotal(data.quantity);

    if (!data.product_composition) return;
    setIsVerified(data.product_composition.realStock > 0);
  }, [data]);

  if (!data) return null;
  return (
    <div className="col-6">
      <ModalSubmit isOpen={isSubmiting} toggle={toggleSubmiting} handleSubmit={handleSimpan}>
        <div className="mt-5 w-50 d-flex justify-content-between">
          <BodyText>Real Stock</BodyText>
          <BodyText bold>{`${formatquantity(qtyReal)} Kg`}</BodyText>
        </div>
        <div className="mt-3 w-50 d-flex justify-content-between">
          <BodyText>Konsinyasi</BodyText>
          <BodyText bold>{`${formatquantity(qtyKonsinyasi)} Kg`}</BodyText>
        </div>
      </ModalSubmit>
    
      <PanelKitchen
        className="mol-trx-kitchen"
        handleSimpan={openModalSimpan}
        isEditing={isEditing}
        isVerified={isVerified}
        item={data}
        toggleEditing={toggleEditing}
        isQtyMatched={isQtyMatched}
      >
        <SubjectTitle className="mb-4" bold>
          Inventory
        </SubjectTitle>

        <FormInputGroup
          title="Real Stock"
          qty={data.product_sku.draftStock}
          onChange={handleChange}
          name="qtyReal"
          placeholder="Input Real Stock"
        />
        <FormInputGroup
          title="Konsinyasi"
          qty={data.product_sku.consignmentStock}
          onChange={handleChange}
          name="qtyKonsinyasi"
          placeholder="Input Konsinyasi Stock"
        />

        <div className="d-flex flex-column mt-3 mb-3">
          <SubjectTitle bold>Total Selected</SubjectTitle>
          <GreetingTitle bold accent>
            {formatquantity(qtyReal + qtyKonsinyasi)} Kg / {formatquantity(qtyTotal)} Kg
          </GreetingTitle>
        </div>

        <div className="d-flex">
          <MdErrorOutline color={isQtyMatched ? "transparent" : "#D92C2C"} />
          &nbsp;&nbsp;
          <MetadataText color={isQtyMatched ? "transparent" : "#D92C2C"}>
            Kuantitas belum cocok
          </MetadataText>
        </div>
      </PanelKitchen>
    </div>
  );
};

interface IProps {
  title: string;
  qty: number;
  name: string;
  onChange: any;
  placeholder: string;
}

const FormInputGroup = ({ title, qty, name, onChange, placeholder }: IProps) => {
  return (
    <>
      <div className="d-flex justify-content-between mb-1">
        <BodyText>{title}</BodyText>
        <BodyText color={gray500} bold>
          {`Tersedia ${formatquantity(qty)} Kg`}
        </BodyText>
      </div>
      <Form className="inventory-input-qty">
        <Input
          prepend="Kuantitas"
          append="Kg"
          max={qty}
          onChange={onChange}
          name={name}
          placeholder={placeholder}
          type="number"
        />
      </Form>
    </>
  );
};
