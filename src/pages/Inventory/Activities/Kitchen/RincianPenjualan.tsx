import React from "react";
import { PaneHeader, Panel } from "../../../../components";
import { gray800 } from "../../../../constants/Colors";

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  className?: string;
}

export default ({ children, className }: Props) => {
  return (
    <div className="col-6">
      <Panel className={`mol-rincian-penjualan ${className}`}>
        <div className="header">
          <PaneHeader bold color={gray800}>
            Rincian Penjualan
          </PaneHeader>
        </div>
        <div style={{ borderTop: "0.25px solid #E1E1E1", marginTop: 16 }}></div>

        <div className="content">{children}</div>
      </Panel>
    </div>
  );
};
