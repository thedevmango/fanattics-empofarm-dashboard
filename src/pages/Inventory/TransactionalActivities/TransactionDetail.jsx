/* eslint-disable no-unused-vars */
import React from "react";
import DummyImage from "../../assets/images/madu-murni.png";
import InputRange from "react-input-range";
import { useState } from "react";
import Button from "../../../components/General/Button";
import { Panel, PanelHead, PanelTitle, PanelBody } from "../../../components/Panel";

// Refactor all of these components into separate files
// preferably in typescript
const CardListItem = ({ status }) => {
  return (
    <div className="transaction-detail-card-list-item pb-3">
      <div className="row">
        <div className="col-3">
          <img src={DummyImage} style={{ width: "100%" }} alt="" />
        </div>
        <div className="col-9">
          <div className="sub-title">Madu Murni</div>
          <div className="row mt-3">
            <div className="col">
              <div className="label">SKU</div>
              <div className="content">081920192 0912 09120</div>
            </div>
            <div className="col">
              <div className="label">Kuantitas</div>
              <div className="content">100 Botol</div>
            </div>
          </div>
          <div className="row mt-1">
            <div className="col">
              <div className="label">Pilihan Packaging</div>
              <div className="content">Karung</div>
            </div>
            <div className="col">
              <div className="label">Farmer</div>
              <div className="content">Joko Tole</div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col">
              <div className={`indicator-${status}`}>Terverifikasi</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ProductCompositionItem = (props) => {
  return (
    <div className="product-composition-item mt-2">
      <div className="row">
        <div className="col-4">
          <div className="border-right">
            <div className="label">Kategori Stock</div>
            <div className="content">Stock Konsinyasi</div>
            <div className="label mt-4">Stock Tersedia</div>
            <div className="content">200 Botol</div>
          </div>
        </div>
        <div className="col d-flex flex-column justify-content-center">
          <div className="input-stock d-flex flex-row">
            Jumlah
            <input placeholder="50" type="text" />
          </div>
          <div className="d-flex flex-row align-items-center mt-2">
            <div className="label">Sisa Stock</div>
            <div className="content mt-0 ml-3">150</div>
          </div>
        </div>
      </div>
    </div>
  );
};

const TransactionKitchen = (props) => {
  return (
    <div className="transaction-detail-card transaction-kitchen">
      <div className="title-medium border-bottom pb-3 ">Transaction Kitchen</div>
      <div className="row mt-2">
        <div className="col-6">
          <div className="sub-title">Madu Murni</div>
          <div className="label">SKU 081920192 0912 09120</div>
        </div>
        <div className="col-6">
          <div className="label">Packaging</div>
          <div className="content font-weight-bold">Botol (5 Liter)</div>
        </div>
      </div>
      <div className="row mt-2 pb-3">
        <div className="col-6">
          <div className="label">Farmer</div>
          <div className="content font-weight-bold">Joko Tole</div>
        </div>
        <div className="col-6">
          <div className="label">Quantity Total</div>
          <div className="content font-weight-bold">100 Botol</div>
        </div>
      </div>
      <div className="border-bottom" />
      <div className="product-composition mt-3">
        <div className="title">Inventory</div>
        <ProductCompositionItem />
        <ProductCompositionItem />
        <div className="d-flex flex-row align-items-center justify-content-between mt-5">
          <div>
            <div className="label">Total Selected</div>
            <div className="total">100 Botol / 100 Botol</div>
          </div>
          <button className="save-button">Simpan</button>
        </div>
      </div>
    </div>
  );
};

const UploadDocument = (props) => {
  return <div></div>;
};

const transactionDetails = {
  transactionCode: "ALEPHOENTR975",
  performaInvoice: "INV-10-10010",
  createdAt: "Selasa, 26/04/2020",
  paidInvoice: "INV-10-10010",
  buyer: {
    fullName: "Aji Solihin",
    contactNumber: "+62 21 7222006",
    address: `Jl. Wolter Mongonsidi No. 82 (Petogogan, Kebayoran Baru) Jakarta Selatan , Jakarta`,
  },
};

// const transactionStatus = [
//   "Pesanan Masuk",
//   "TPC  Diproses",
//   "TPC dalam Out-Bound QC",
//   "TPC Selesai"
// ]

export default (props) => {
  return (
    <div className="container transaction-detail mt-4 pb-4">
      <div className="row">
        <div className="col-12">
          <div className="transaction-detail-card">
            <div className="title-large">Detail Transaksi</div>
            <div className="row mt-4 border-bottom pb-3">
              <div className="content-container col">
                <div className="label">Kode Transaksi</div>
                <div className="content">{transactionDetails.transactionCode}</div>
              </div>
              <div className="content-container col">
                <div className="label">Performa Invoice</div>
                <div className="content highlight">{transactionDetails.performaInvoice}</div>
              </div>
              <div className="content-container col">
                <div className="label">Tanggal Permintaan</div>
                <div className="content">{transactionDetails.createdAt}</div>
              </div>
              <div className="content-container col">
                <div className="label">Paid Invoice</div>
                <div className="content highlight">{transactionDetails.paidInvoice}</div>
              </div>
            </div>
            <div className="row mt-3">
              <div className="content-container col-3">
                <div className="label">Nama Pembeli</div>
                <div className="content highlight">{transactionDetails.buyer.fullName}</div>
              </div>
              <div className="content-container col-3">
                <div className="label">Nomor Kontak</div>
                <div className="content">{transactionDetails.buyer.contactNumber}</div>
              </div>
              <div className="content-container col-6">
                <div className="label">Alamat</div>
                <div className="content">{transactionDetails.buyer.address}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row mt-4">
        <div className="col-6">
          <div className="transaction-detail-card">
            <div className="title-medium border-bottom pb-3">Rincian Pembelian</div>
            <CardListItem status="success" />
            <CardListItem status="warning" />
          </div>
        </div>
        <div className="col-6">
          <Panel>
            <PanelHead bordered>
              <PanelTitle bold>Kelengkapan Dokumen</PanelTitle>
            </PanelHead>
            <PanelBody>
              <div className="component-card">test</div>
            </PanelBody>
          </Panel>
        </div>
      </div>
    </div>
  );
};
