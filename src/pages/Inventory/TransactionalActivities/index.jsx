/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import {
  Table,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell,
} from "../../../components/Table";
import Tabs from "../../../components/Tabs";
import { FiSearch } from "react-icons/fi";
import { BsChevronRight } from "react-icons/bs";
import { Link } from "react-router-dom";
import { Pagination, usePagination } from "../../../components/Pagination";

const dummyDataTable = [
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
  {
    date: "20/08/2020",
    transactionCode: "LKJ-12139- 2209",
    fullName: "Budi Doremi Redo",
    invoiceCode: "Jagung Pipil Grade 1",
    notes: "-",
  },
];

export default (props) => {
  const [tableData, setTableData] = useState(dummyDataTable);
  const [activeTab, setActiveTab] = useState("");
  const [searchText, setSearchText] = useState("");

  const renderTableData = () => {
    return tableData.map((val, idx) => {
      return (
        <TableRow key={idx}>
          <TableCell>{val.date}</TableCell>
          <TableCell>{val.transactionCode}</TableCell>
          <TableCell>{val.fullName}</TableCell>
          <TableCell>{val.invoiceCode}</TableCell>
          <TableCell>{val.notes}</TableCell>
          <TableCell>
            <Link
              to={`transactions/detail/${idx}`}
              style={{ textDecoration: "none", color: "inherit" }}
            >
              <div
                style={{
                  borderLeft: "1px solid #E1E1E1",
                  padding: "0px 16px",
                }}
              >
                <BsChevronRight size={12} />
              </div>
            </Link>
          </TableCell>
        </TableRow>
      );
    });
  };

  const inputHandler = (e) => {
    setSearchText(e.target.value);
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: tableData,
    itemsPerPage: 7,
  });

  return (
    <div className="inventory inventory-trx-activities">
      <div className="row mt-5 mb-4">
        <div className="col-12">
          <h1>Outbound Inventory</h1>
        </div>
      </div>
      <div className="row mb-4 d-flex flex-row align-items-center">
        <div className="col-8 d-flex flex-row align-items-center">
          <Tabs
            options={["Pesanan Masuk", "TPC Diproses", "TPC dalam Out-Bound QC", "TPC Selesai"]}
            activeTab={activeTab}
            setActiveTab={setActiveTab}
          />
        </div>
        <div className="col-4">
          <div className="search">
            <FiSearch />
            <input type="text" onChange={inputHandler} placeholder="Cari nama pemesan" />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12 overflow-auto">
          <Table bordered>
            <TableHead>
              <TableRow>
                <TableHeadCell>Tanggal Permintaan</TableHeadCell>
                <TableHeadCell>Kode Transaksi</TableHeadCell>
                <TableHeadCell>Nama Pemesan</TableHeadCell>
                <TableHeadCell>Kode Invoice</TableHeadCell>
                <TableHeadCell>Keterangan</TableHeadCell>
                <TableHeadCell></TableHeadCell>
              </TableRow>
            </TableHead>
            <TableBody>{renderTableData()}</TableBody>
          </Table>
        </div>
      </div>
      <div className="pagination-wrapper my-3">
        <Pagination
          next={next}
          prev={prev}
          jump={jump}
          pages={pages}
          currentPage={currentPage}
          maxPage={maxPage}
        />
      </div>
    </div>
  );
};
