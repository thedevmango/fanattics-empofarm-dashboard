/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";

import Commodity from "./Commodity";
// import TransactionalActivities from "./TransactionalActivities";
import TransactionalActivities from "./Activities";
import TransactionKitchen from "./Activities/Kitchen";
import TransactionDetails from "./Activities/Details";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/commodity`} component={Commodity} />
      <Route
        exact
        path={`${props.match.path}/transactional-activities`}
        component={TransactionalActivities}
      />
      <Route
        path={`${props.match.path}/transactional-activities/sales-order`}
        component={TransactionKitchen}
      />
      <Route
        path={`${props.match.path}/transactional-activities/details`}
        component={TransactionDetails}
      />
    </>
  );
};
