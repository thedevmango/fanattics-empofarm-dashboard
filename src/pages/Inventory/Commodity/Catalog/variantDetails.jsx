import React from "react";
import { useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import { Colors } from "../../../../constants";
import { formatnumber } from "../../../../configs/formatnumber";
import {
  TableHead,
  TableRow,
  TableHeadCell,
  Table,
  TableBody,
  TableCell,
  TableFooter,
} from "../../../../components/Table";
import { PaneHeader } from "../../../../components/Typography";
import { Button, ButtonIcon } from "../../../../components/Button";
import { Pagination, usePagination } from "../../../../components/Pagination";
import Panel from "../../../../components/Panel/Panel";
import { BsThreeDotsVertical } from "react-icons/bs";
import { Card } from "../../../../components/Card";

export default (props) => {
  const data = props.location.state.data;
  const dataSubcategory = props.location.state.subcategory;
  const history = useHistory();

  const RenderBodyTable = ({ data }) => {
    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => (
      <TableRow key={idx}>
        <TableCell>{item.farmerName}</TableCell>
        <TableCell>{item.stockCategory}</TableCell>
        <TableCell>{item.location}</TableCell>
        <TableCell>{formatnumber(item.quantity)}</TableCell>
        <TableCell>{item.description}</TableCell>
        <TableCell style={{ width: 50, borderLeft: "1px solid #E1E1E1" }}>
          <Button theme="action-table">
            <BsThreeDotsVertical size={18} />
          </Button>
        </TableCell>
      </TableRow>
    ));
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data.skuList,
    itemsPerPage: 10,
  });

  return (
    <div className="content content-variant-details">
      <Button onClick={history.goBack} theme="text" containerStyle={{ padding: "6px 0" }}>
        <ButtonIcon type="prepend">
          <FiArrowLeft color={Colors.primaryBrand} />
        </ButtonIcon>
        Kembali ke Commodity Catalog
      </Button>

      <Panel className="mt-3">
        <PaneHeader bold className="mb-3">
          {data.variantName}
        </PaneHeader>

        <div className="content-header">
          <div className="content-header-card">
            <CardComponent value={data.totalStock} unit={dataSubcategory.unit}>
              Total Stok
            </CardComponent>
            <CardComponent value={data.realStock} unit={dataSubcategory.unit}>
              Real Stock
            </CardComponent>
            <CardComponent value={data.draftStock} unit={dataSubcategory.unit}>
              Stok Konsinyasi
            </CardComponent>
          </div>

          <div className="content-header-button">
            <Button theme="primary">Mutasikan Stok</Button>
          </div>
        </div>

        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Nama Farmer</TableHeadCell>
              <TableHeadCell>Kategori Stok</TableHeadCell>
              <TableHeadCell>Lokasi</TableHeadCell>
              <TableHeadCell>Kuantitas (Kg)</TableHeadCell>
              <TableHeadCell>Keterangan</TableHeadCell>
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <TableCell colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </TableCell>
    </TableRow>
  );
};

const CardComponent = ({ children, value, unit }) => {
  return (
    <Card className="content-header-card-item">
      <div className="title">{children}</div>
      <div className="value">
        {formatnumber(value)}&nbsp;{unit}
      </div>
    </Card>
  );
};
