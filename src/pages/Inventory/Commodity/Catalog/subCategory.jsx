/* eslint-disable no-unused-vars */
import React from "react";
import { useHistory } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import { Colors } from "../../../../constants";
import { formatnumber } from "../../../../configs/formatnumber";
import {
  Table,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell,
  TableFooter,
} from "../../../../components/Table";
import { PaneHeader } from "../../../../components/Typography";
import { Button, ButtonIcon } from "../../../../components/Button";
import { Pagination, usePagination } from "../../../../components/Pagination";
import { Panel } from "../../../../components/Panel";
import { BsChevronRight } from "react-icons/bs";

export default (props) => {
  const data = props.location.state.data;
  const history = useHistory();

  const RenderBodyTable = ({ data }) => {
    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => {
      const link = `${props.match.url}/${item.variantName.split(" ").join("-").toLowerCase()}`;
      const subcategory = props.location.state.data;
      const nextClick = () => history.push(link, { data: item, subcategory });

      return (
        <TableRow key={idx} onClick={nextClick}>
          <TableCell>{item.variantName}</TableCell>
          <TableCell>{formatnumber(item.draftStock)}</TableCell>
          <TableCell>{formatnumber(item.realStock)}</TableCell>
          <TableCell>{formatnumber(item.totalStock)}</TableCell>
          <TableCell style={{ width: 50, borderLeft: "1px solid #E1E1E1" }}>
            <Button theme="action-table">
              <BsChevronRight size={12} />
            </Button>
          </TableCell>
        </TableRow>
      );
    });
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data.variants,
    itemsPerPage: 10,
  });

  return (
    <div className="content content-subcategory">
      <Button onClick={history.goBack} theme="text" containerStyle={{ padding: "6px 0" }}>
        <ButtonIcon type="prepend">
          <FiArrowLeft color={Colors.primaryBrand} />
        </ButtonIcon>
        Kembali ke Commodity Catalog
      </Button>

      <Panel className="mt-3">
        <PaneHeader bold className="mb-3">
          {data.subCategory}
        </PaneHeader>

        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Nama Varian</TableHeadCell>
              <TableHeadCell>{`Stok Konsinyasi (${data.unit})`}</TableHeadCell>
              <TableHeadCell>{`Real Stock (${data.unit})`}</TableHeadCell>
              <TableHeadCell colSpan={2}>{`Total Stock (${data.unit})`}</TableHeadCell>
              {/* <TableHeadCell>&nbsp;</TableHeadCell> */}
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <TableCell colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </TableCell>
    </TableRow>
  );
};
