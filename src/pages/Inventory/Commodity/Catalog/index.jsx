/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import Img from "react-cool-img";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import { formatnumber } from "../../../../configs/formatnumber";
import { SubjectTitle, MetadataText, BodyText } from "../../../../components/Typography";
import { Button } from "../../../../components/Button";
import { Panel } from "../../../../components/Panel";
import { Card } from "../../../../components/Card";
import Tabs from "../../../../components/Tabs";
import { SegmentedTitle } from "../../../../components/Molecules";

export default (props) => {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isActive, setIsActive] = useState("Hard");

  const toggleTitle = ({ value }) => history.push(`/inventory/commodity/${value}`);

  useEffect(() => {
    if (data) {
      setFilteredData(data.filter((val) => val.category === isActive));
    }
  }, [data, isActive]);

  useEffect(() => {
    setTimeout(() => setData(dummyData), 250);
  }, []);

  return (
    <div className="content content-catalog">
      <Helmet>
        <title>Commodity Catalog | Inventory</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        onClick={toggleTitle}
        titles={["Commodity Catalog", "Commodity Flow History"]}
        slug={["catalog", "flow-history"]}
      />

      <Panel>
        <div className="d-flex align-items-end justify-content-between">
          <Tabs
            options={["Hard", "Soft", "Liquid", "Equipment"]}
            activeTab={isActive}
            setActiveTab={setIsActive}
            className="tab-catalog mr-auto"
          />
          <div className="col-2 d-flex justify-content-end pr-0">
            <Button theme="primary">Internal Logging</Button>
          </div>
        </div>

        <div className="border-bottom my-3"></div>

        <SubjectTitle className="w-100" bold>
          Sub Kategori
        </SubjectTitle>

        <div className="card-wrapper mt-3">
          {!filteredData.length
            ? null
            : filteredData.map((item, idx) => (
                <Card
                  key={idx}
                  onClick={() =>
                    history.push(
                      `${props.match.path}/${item.subCategory.replace(" ", "-").toLowerCase()}`,
                      { data: item }
                    )
                  }
                  elevationDepth="4"
                  className="card-item"
                >
                  <Img
                    style={{
                      backgroundColor: "grey",
                      width: 100,
                      minHeight: 178,
                      objectFit: "cover",
                      alignSelf: "center",
                    }}
                    src={item.image}
                    alt={item.subCategory}
                  />

                  <div className="info d-flex flex-column justify-content-between">
                    <SubjectTitle>{item.subCategory}</SubjectTitle>

                    <div className="d-flex justify-content-between">
                      <div className="d-flex flex-column">
                        <MetadataText color="#919191">Draft Stock</MetadataText>
                        <BodyText>{`${formatnumber(item.draftStock)} Kilogram`}</BodyText>
                      </div>
                      <div className="d-flex flex-column">
                        <MetadataText color="#919191">Real Stock</MetadataText>
                        <BodyText>{`${formatnumber(item.realStock)} Kilogram`}</BodyText>
                      </div>
                    </div>

                    <div className="d-flex flex-column align-items-end">
                      <MetadataText color="#919191">Real Stock</MetadataText>
                      <SubjectTitle bold>{formatnumber(item.totalStock) + "(Kg)"}</SubjectTitle>
                    </div>
                  </div>
                </Card>
              ))}
        </div>
      </Panel>
    </div>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const dummySkuList = [...Array(20)].map((item, idx) => {
  return {
    id: idx + 1,
    farmerName: "Joko Tole",
    sku: "081920192 0912 09120",
    quantity: 2000,
    location: "Tempat Lokasi",
    stockCategory: randomizeData(["Konsinyasi", "Real Stock"]),
    description: randomizeData(["Sudah Dibayar", "Status Pembayaran"]),
  };
});
const dummyDatum = [
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Beras Putih", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Gabah", "Hard", "Kg", require("../../../../assets/images/gabah2.png")],
  ["Kelapa", "Hard", "Kg", require("../../../../assets/images/kelapa.png")],
  ["Kopi", "Hard", "Kg", require("../../../../assets/images/kopi.png")],
  ["Kacang Almond", "Hard", "Kg", require("../../../../assets/images/kacang-almond.png")],
  ["Beras Ketan", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Beras Putih", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Gabah", "Hard", "Kg", require("../../../../assets/images/gabah2.png")],
  ["Kelapa", "Hard", "Kg", require("../../../../assets/images/kelapa.png")],
  ["Kopi", "Hard", "Kg", require("../../../../assets/images/kopi.png")],
  ["Kacang Almond", "Hard", "Kg", require("../../../../assets/images/kacang-almond.png")],
  ["Beras Ketan", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Beras Putih", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Gabah", "Hard", "Kg", require("../../../../assets/images/gabah2.png")],
  ["Kelapa", "Hard", "Kg", require("../../../../assets/images/kelapa.png")],
  ["Kopi", "Hard", "Kg", require("../../../../assets/images/kopi.png")],
  ["Kacang Almond", "Hard", "Kg", require("../../../../assets/images/kacang-almond.png")],
  ["Beras Ketan", "Hard", "Kg", require("../../../../assets/images/beras.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Hard", "Kg", require("../../../../assets/images/jagung-super.png")],

  ["Jagung", "Soft", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Beras Putih", "Soft", "Kg", require("../../../../assets/images/beras.png")],
  ["Gabah", "Soft", "Kg", require("../../../../assets/images/gabah2.png")],
  ["Kelapa", "Soft", "Kg", require("../../../../assets/images/kelapa.png")],
  ["Kopi", "Soft", "Kg", require("../../../../assets/images/kopi.png")],
  ["Kacang Almond", "Soft", "Kg", require("../../../../assets/images/kacang-almond.png")],
  ["Beras Ketan", "Soft", "Kg", require("../../../../assets/images/beras.png")],
  ["Jagung", "Soft", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Soft", "Kg", require("../../../../assets/images/jagung-super.png")],

  ["Jagung", "Liquid", "Kg", require("../../../../assets/images/jagung-super.png")],
  ["Beras Putih", "Liquid", "Kg", require("../../../../assets/images/beras.png")],
  ["Gabah", "Liquid", "Kg", require("../../../../assets/images/gabah2.png")],
  ["Kelapa", "Liquid", "Kg", require("../../../../assets/images/kelapa.png")],
  ["Kopi", "Equipment", "Kg", require("../../../../assets/images/kopi.png")],
  ["Kacang Almond", "Equipment", "Unit", require("../../../../assets/images/kacang-almond.png")],
  ["Beras Ketan", "Equipment", "Unit", require("../../../../assets/images/beras.png")],
  ["Jagung", "Equipment", "Unit", require("../../../../assets/images/jagung-super.png")],
  ["Jagung", "Equipment", "Unit", require("../../../../assets/images/jagung-super.png")],
];
const dummyGrade = ["Grade 1", "Grade 2", "Grade 3", "Grade 4"];
const dummyData = dummyDatum.map((item, idx) => {
  return {
    id: idx + 1,
    category: item[1],
    subCategory: item[0],
    image: item[3],
    draftStock: 20000,
    realStock: 7500,
    totalStock: 20000,
    unit: item[2],
    variants: dummyGrade.map((variant, idx) => {
      return {
        id: idx + 1,
        variantId: "081920192 0912 09120",
        variantName: `${item[0]} ${variant}`,
        realStock: 7500,
        totalStock: 20000,
        draftStock: 20000,
        location: "Gudang Apa",
        skuList: dummySkuList,
      };
    }),
  };
});
