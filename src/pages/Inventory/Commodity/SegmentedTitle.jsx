import React from "react";
import { PaneHeader } from "../../../components/Typography";

export default ({ className = "", titles, slug, location, toggleTitle }) => {
  return (
    <div className={`component-segmented-title ${className}`}>
      {titles.map((title, idx) => {
        return (
          <PaneHeader
            key={idx}
            onClick={() => toggleTitle(slug[idx])}
            bold={location.includes(slug[idx])}
            accent={location.includes(slug[idx])}
            className="title-item"
          >
            {title}
          </PaneHeader>
        );
      })}
    </div>
  );
};
