import React from "react";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import { SegmentedTitle } from "../../../../components/Molecules";

export default (props) => {
  const history = useHistory();
  const toggleTitle = ({ value }) => history.push(`/inventory/commodity/${value}`);

  return (
    <div className="content content-flow-history">
      <Helmet>
        <title>Commodity Flow History | Inventory</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        onClick={toggleTitle}
        titles={["Commodity Catalog", "Commodity Flow History"]}
        slug={["catalog", "flow-history"]}
      />

      <h1>FLOW HISTORY</h1>
    </div>
  );
};
