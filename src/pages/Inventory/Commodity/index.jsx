import React from "react";
import { Route } from "react-router-dom";
import Catalog from "./Catalog";
import SubCategory from "./Catalog/subCategory";
import VariantDetails from "./Catalog/variantDetails";
import FlowHistory from "./FlowHistory";

export default (props) => {
  return (
    <div className="inventory inventory-commodity">
      <Route exact path={`${props.match.path}/catalog`} component={Catalog} />
      <Route exact path={`${props.match.path}/catalog/:subcategoryName`} component={SubCategory} />
      <Route
        path={`${props.match.path}/catalog/:subcategoryName/:variantName`}
        component={VariantDetails}
      />

      <Route path={`${props.match.path}/flow-history`} component={FlowHistory} />
    </div>
  );
};
