import React from "react";
import { Route } from "react-router-dom";
import ProductCatalog from "./ProductCatalog";
import subCategory from "./ProductCatalog/subCategory";
import variantDetails from "./ProductCatalog/variantDetails";
import TransactionalActivities from "./Activities";
import TransactionalActivitiesDetails from "./Activities/Details";
import PriceSettings from "./PriceSettings";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/product-catalog`} exact component={ProductCatalog} />
      <Route
        path={`${props.match.path}/product-catalog/subcategory/:subcategoryName`}
        exact
        component={subCategory}
      />
      <Route
        path={`${props.match.path}/product-catalog/subcategory/:subcategoryName/variant/:variantName`}
        exact
        component={variantDetails}
      />
      <Route
        exact
        path={`${props.match.path}/transactional-activities`}
        component={TransactionalActivities}
      />
      <Route
        path={`${props.match.path}/transactional-activities/details`}
        component={TransactionalActivitiesDetails}
      />
      <Route path={`${props.match.path}/commodity-price-setting`} component={PriceSettings} />
    </>
  );
};
