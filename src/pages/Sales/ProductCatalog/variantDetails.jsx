import React from "react";
import { Helmet } from "react-helmet";
import { useHistory } from "react-router-dom";
import { formatnumber } from "../../../configs/formatnumber";
import { BsThreeDotsVertical } from "react-icons/bs";
import { FiArrowLeft } from "react-icons/fi";
import { Colors } from "../../../constants";
import {
  TableHead,
  TableRow,
  TableHeadCell,
  Table,
  TableBody,
  TableCell,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { Button, ButtonIcon } from "../../../components/Button";
import { Input, Form } from "../../../components/Form";
import { Panel } from "../../../components/Panel";
import { Card } from "../../../components/Card";

export default (props) => {
  const data = props.location.state.data;
  const dataSubcategory = props.location.state.subcategory;
  const history = useHistory();

  const RenderBodyTable = ({ data }) => {
    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => {
      return (
        <TableRow key={idx}>
          <TableCell> {item.sku} </TableCell>
          <TableCell> {item.farmerName} </TableCell>
          <TableCell> {item.quantity} </TableCell>
          <TableCell> {item.stockCategory} </TableCell>
          <TableCell> {item.description} </TableCell>
          <TableCell style={{ width: 50, borderLeft: "1px solid #E1E1E1" }}>
            <Button theme="action-table">
              <BsThreeDotsVertical size={18} />
            </Button>
          </TableCell>
        </TableRow>
      );
    });
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data.skuList,
    itemsPerPage: 10,
  });

  return (
    <div className="sales sales-variant-details">
      <Helmet>
        <title>{`${data.variantName} | Sales`}</title>
      </Helmet>

      <Button onClick={history.goBack} theme="text" containerStyle={{ padding: "6px 0" }}>
        <ButtonIcon type="prepend">
          <FiArrowLeft color={Colors.primaryBrand} />
        </ButtonIcon>
        Kembali ke Sub Kategori
      </Button>

      <div className="header">{data.variantName}</div>

      <Panel style={{ borderRadius: 4 }} className="content">
        <div className="content-header">
          <div className="content-header-button">
            <Form className="form-siap-dijual">
              <Input prepend="Stok Siap Dijual" type="number" placeholder="Masukkan Jumlah" />
            </Form>
            <Button theme="primary">SIMPAN</Button>
          </div>

          <div className="content-header-card">
            <CardComponent value={data.realStock} unit={dataSubcategory.unit}>
              Real Stok
            </CardComponent>
            <CardComponent value={data.consignmentStock} unit={dataSubcategory.unit}>
              Stok Konsinyasi
            </CardComponent>
            <CardComponent value={dataSubcategory.settleStock} unit={dataSubcategory.unit}>
              Stok Mengendap
            </CardComponent>
            <CardComponent value={data.totalStock} unit={dataSubcategory.unit}>
              Total Stok
            </CardComponent>
          </div>
        </div>

        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell> SKU </TableHeadCell>
              <TableHeadCell> Nama Farmer </TableHeadCell>
              <TableHeadCell> {`Kuantitas (${dataSubcategory.unit})`} </TableHeadCell>
              <TableHeadCell> Kategori Stok </TableHeadCell>
              <TableHeadCell> Keterangan </TableHeadCell>
              <TableHeadCell> &nbsp; </TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable data={currentData()} />
          </TableBody>
        </Table>

        <div className="pagination-wrapper my-3">
          <Pagination
            next={next}
            prev={prev}
            jump={jump}
            pages={pages}
            currentPage={currentPage}
            maxPage={maxPage}
          />
        </div>
      </Panel>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <td colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </td>
    </TableRow>
  );
};

const CardComponent = ({ children, value, unit }) => {
  return (
    <Card className="content-header-card-item right">
      <div className="title">{children}</div>
      <div className="value">
        {formatnumber(value)}&nbsp;{unit}
      </div>
    </Card>
  );
};
