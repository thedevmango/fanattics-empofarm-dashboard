/* eslint-disable no-unused-vars */
import React from "react";
import { useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Colors } from "../../../constants";
import { FiArrowLeft } from "react-icons/fi";
import { BsThreeDotsVertical } from "react-icons/bs";
import { formatnumber } from "../../../configs/formatnumber";
import {
  Table,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { Button, ButtonIcon } from "../../../components/Button";
import { Panel } from "../../../components/Panel";
import { Card } from "../../../components/Card";

export default (props) => {
  const data = props.location.state.data;
  const history = useHistory();

  const RenderBodyTable = ({ data }) => {
    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => {
      const link = `${props.match.url}/variant/${item.variantName
        .toLowerCase()
        .split(" ")
        .join("-")}`;
      const subcategory = props.location.state.data;
      const nextClick = () => history.push(link, { data: item, subcategory });

      return (
        <TableRow key={idx}>
          <TableCell onClick={nextClick} style={{ minWidth: 300 }}>
            {item.variantName}
          </TableCell>
          <TableCell onClick={nextClick}>{formatnumber(item.realStock)}</TableCell>
          <TableCell onClick={nextClick}>{formatnumber(item.consignmentStock)}</TableCell>
          <TableCell onClick={nextClick}>{formatnumber(item.totalStock)}</TableCell>
          <TableCell style={{ width: 50, borderLeft: "1px solid #E1E1E1" }}>
            <Button theme="action-table">
              <BsThreeDotsVertical size={18} />
            </Button>
          </TableCell>
        </TableRow>
      );
    });
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data.variants,
    itemsPerPage: 10,
  });

  return (
    <div className="sales sales-subcategory">
      <Helmet>
        <title>{`${data.subCategory} | Sales`}</title>
      </Helmet>

      <Button onClick={history.goBack} theme="text" containerStyle={{ padding: "6px 0" }}>
        <ButtonIcon type="prepend">
          <FiArrowLeft color={Colors.primaryBrand} />
        </ButtonIcon>
        Kembali ke Product Catalog
      </Button>

      <div className="header">Sub Kategori - {data.subCategory}</div>

      <Panel style={{ borderRadius: 4 }} className="content">
        <div className="content-header">
          <div className="content-header-card">
            <CardComponent value={data.totalStock} unit={data.unit}>
              Total Stok
            </CardComponent>
            <CardComponent value={data.empofarmStock} unit={data.unit}>
              Empofarm Stockeeping
            </CardComponent>
            <CardComponent value={data.settleStock} unit={data.unit}>
              Stok Mengendap
            </CardComponent>
            <CardComponent value={data.readyStock} unit={data.unit}>
              Stok Siap Dijual
            </CardComponent>
          </div>

          <div className="content-header-button">
            <Button theme="primary">TAMBAH VARIAN PRODUK</Button>
          </div>
        </div>

        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Nama Varian</TableHeadCell>
              <TableHeadCell>{`Real Stock (${data.unit})`}</TableHeadCell>
              <TableHeadCell>{`Stok Konsinyasi (${data.unit})`}</TableHeadCell>
              <TableHeadCell>{`Total Stock (${data.unit})`}</TableHeadCell>
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable data={currentData()} />
          </TableBody>
        </Table>

        <div className="pagination-wrapper my-3">
          <Pagination
            next={next}
            prev={prev}
            jump={jump}
            pages={pages}
            currentPage={currentPage}
            maxPage={maxPage}
          />
        </div>
      </Panel>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <td colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </td>
    </TableRow>
  );
};

const CardComponent = ({ children, value, unit }) => {
  return (
    <Card className="content-header-card-item">
      <div className="title">{children}</div>
      <div className="value">
        {formatnumber(value)}&nbsp;{unit}
      </div>
    </Card>
  );
};
