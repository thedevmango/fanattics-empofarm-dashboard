/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Tabs from "../../../components/Tabs";
import { Panel } from "../../../components/Panel";
import { SubjectTitle } from "../../../components/Typography";
import { Card } from "../../../components/Card";
import { formatnumber } from "../../../configs/formatnumber";
import { Button, ButtonIcon } from "../../../components/Button";
import { Helmet } from "react-helmet";
import { FiPlus } from "react-icons/fi";

export default (props) => {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [isActive, setIsActive] = useState("Hard");

  const fetchData = (tab) => {
    if (tab === "Hard") {
      return setData(dummyData.filter((val) => val.category === "Hard"));
    }
    if (tab === "Soft") {
      return setData(dummyData.filter((val) => val.category === "Soft"));
    }
    if (tab === "Liquid") {
      return setData(dummyData.filter((val) => val.category === "Liquid"));
    }
    if (tab === "Equipment") {
      return setData(dummyData.filter((val) => val.category === "Equipment"));
    }
    return setData(dummyData);
  };

  useEffect(() => {
    fetchData(isActive);
  }, [isActive]);

  return (
    <div className="sales sales-product-catalog">
      <Helmet>
        <title>Product Catalog | Sales</title>
      </Helmet>

      <Panel style={{ borderRadius: 4 }}>
        <div className="header mb-4 d-flex justify-content-between align-items-center">
          <div className="header-title">Product Catalog</div>
          <div className="d-flex">
            <Button theme="primary">Tambah Sub Kategori</Button>
            <Button theme="secondary">Edit Kategori Produk</Button>
          </div>
        </div>

        <div className="d-flex mx-3 mb-4">
          <Tabs
            options={["Hard", "Soft"]}
            activeTab={isActive}
            setActiveTab={setIsActive}
            className="tab-product-catalog"
          />
          <div className="d-flex align-items-start">
            <Button theme="text" containerStyle={{ padding: 0, color: "#605E5C" }}>
              <ButtonIcon type="prepend">
                <FiPlus />
              </ButtonIcon>
              Tambah Kategori Produk
            </Button>
          </div>
        </div>

        <div className="body">
          {data.map((item, idx) => {
            const link = `${props.match.path}/subcategory/${item.subCategory
              .split(" ")
              .join("-")
              .toLowerCase()}`;

            return (
              <div key={idx} className="subcategory-card">
                <Card
                  key={idx}
                  style={{ cursor: "pointer" }}
                  onClick={() => history.push(link, { data: item })}
                  elevationDepth="4"
                  className="image-card"
                >
                  <img src={item.image} alt={item.subCategory} />
                </Card>

                <div className="info">
                  <div className="info-title">{item.subCategory}</div>
                  <SubjectTitle color="#605E5C">
                    {`Total Stok: ${formatnumber(item.totalStock)} ${item.unit}`}
                  </SubjectTitle>
                </div>
              </div>
            );
          })}
        </div>
      </Panel>
    </div>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const dummySkuList = [...Array(20)].map((item, idx) => {
  return {
    id: idx + 1,
    farmerName: "Farmer's Name",
    sku: "081920192 0912 09120",
    quantity: 200,
    stockCategory: randomizeData(["Konsinyasi", "Real Stock", "Mengendap"]),
    description: randomizeData(["Sudah Dibayar", "Status Pembayaran"]),
  };
});
const dummyDatum = [
  ["Hard", "Jagung Super", 1000, "Liter", require("../../../assets/images/jagung-super.png")],
  ["Hard", "Beras", 125000, "Kg", require("../../../assets/images/beras.png")],
  ["Hard", "Beras Ketan", 1000, "Liter", require("../../../assets/images/gabah2.png")],
  ["Hard", "Gabah", 1500, "Liter", require("../../../assets/images/gabah2.png")],
  ["Hard", "Jagung Super", 1000, "Liter", require("../../../assets/images/jagung-super.png")],
  ["Hard", "Beras", 125000, "Kg", require("../../../assets/images/beras.png")],
  ["Hard", "Beras Ketan", 1000, "Liter", require("../../../assets/images/gabah2.png")],
  ["Hard", "Gabah", 1500, "Liter", require("../../../assets/images/gabah2.png")],
  ["Hard", "Jagung Super", 1000, "Liter", require("../../../assets/images/jagung-super.png")],
  ["Hard", "Beras", 125000, "Kg", require("../../../assets/images/beras.png")],
  ["Hard", "Beras Ketan", 1000, "Liter", require("../../../assets/images/gabah2.png")],
  ["Hard", "Gabah", 1500, "Liter", require("../../../assets/images/gabah2.png")],

  ["Soft", "Kelapa", 75000, "Kg", require("../../../assets/images/kelapa.png")],
  ["Soft", "Jagung Pipil", 2000, "Liter", require("../../../assets/images/jagung-pipil.png")],
  ["Soft", "Kopi", 10000, "Kg", require("../../../assets/images/kopi.png")],
  ["Soft", "Kacang", 2000, "Kg", require("../../../assets/images/kacang-almond.png")],
  ["Soft", "Kelapa", 75000, "Kg", require("../../../assets/images/kelapa.png")],
  ["Soft", "Jagung Pipil", 2000, "Liter", require("../../../assets/images/jagung-pipil.png")],
  ["Soft", "Kopi", 10000, "Kg", require("../../../assets/images/kopi.png")],
  ["Soft", "Kacang", 2000, "Kg", require("../../../assets/images/kacang-almond.png")],
  ["Soft", "Kelapa", 75000, "Kg", require("../../../assets/images/kelapa.png")],
  ["Soft", "Jagung Pipil", 2000, "Liter", require("../../../assets/images/jagung-pipil.png")],
  ["Soft", "Kopi", 10000, "Kg", require("../../../assets/images/kopi.png")],
  ["Soft", "Kacang", 2000, "Kg", require("../../../assets/images/kacang-almond.png")],
];
const dummyGrade = ["Grade 1", "Grade 2", "Grade 3", "Grade 4"];
const dummyData = dummyDatum.map((item, idx) => {
  return {
    id: idx + 1,
    category: item[0],
    subCategory: item[1],
    image: item[4],
    empofarmStock: item[2] * 0.3,
    settleStock: item[2] * 0.2,
    readyStock: item[2] * 0.5,
    totalStock: item[2],
    unit: item[3],
    variants: dummyGrade.map((variant, idx) => {
      return {
        id: idx + 1,
        variantName: `${item[1]} ${variant}`,
        totalStock: item[2] * 0.25,
        empofarmStock: item[2] * 0.25 * 0.7,
        farmerStock: item[2] * 0.25 * 0.3,
        consignmentStock: item[2] * 0.25 * 0.8,
        realStock: item[2] * 0.25 * 0.2,
        skuList: dummySkuList,
      };
    }),
  };
});
