/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import Tabs from "../../../components/Tabs";
import { Button } from "../../../components/Button";
import { PaneHeader, PageTitle } from "../../../components/Typography";
import ComponentPanel from "./ComponentPanel";

export default (props) => {
  const [data, setData] = useState([]);
  const [isActive, setIsActive] = useState("Hard");

  const fetchData = (tab) => {
    if (tab === "Hard") {
      return setData(dummyData.filter((val) => val.category === "Hard"));
    }
    if (tab === "Soft") {
      return setData(dummyData.filter((val) => val.category === "Soft"));
    }
    if (tab === "Liquid") {
      return setData(dummyData.filter((val) => val.category === "Liquid"));
    }
    if (tab === "Equipment") {
      return setData(dummyData.filter((val) => val.category === "Equipment"));
    }
    return setData(dummyData);
  };

  useEffect(() => {
    fetchData(isActive);
  }, [isActive]);

  return (
    <div className="sales sales-product-catalog">
      <Helmet>
        <title>Commodity Price Setting | Sales</title>
      </Helmet>

      <PageTitle bold className="mb-4">
        Pengaturan Harga Jual Empofarm
      </PageTitle>

      <div className="d-flex mx-3 mb-4">
        <Tabs
          options={["Hard", "Soft", "Liquid", "Equipment"]}
          activeTab={isActive}
          setActiveTab={setIsActive}
          className="tab-product-catalog"
        />
      </div>

      <div>
        {data.map((variants, idx) => {
          return <ComponentPanel key={idx} data={variants} />;
        })}
      </div>
    </div>
  );
};

const dummyDatum = [
  [
    "Hard",
    "Jagung Super",
    1000,
    "Liter",
    require("../../../assets/images/jagung-super.png"),
  ],
  ["Hard", "Beras", 125000, "Kg", require("../../../assets/images/beras.png")],
  [
    "Hard",
    "Beras Ketan",
    1000,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],
  [
    "Hard",
    "Gabah",
    1500,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],
  [
    "Soft",
    "Jagung Pipil",
    1000,
    "Liter",
    require("../../../assets/images/jagung-super.png"),
  ],
  ["Soft", "Beras", 125000, "Kg", require("../../../assets/images/beras.png")],
  [
    "Soft",
    "Beras Merah",
    1000,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],
  [
    "Soft",
    "Gabah Hitam",
    1500,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],
  [
    "Soft",
    "Jagung Super",
    1000,
    "Liter",
    require("../../../assets/images/jagung-super.png"),
  ],
  [
    "Liquid",
    "Beras",
    125000,
    "Kg",
    require("../../../assets/images/beras.png"),
  ],
  [
    "Liquid",
    "Beras Ketan",
    1000,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],
  [
    "Liquid",
    "Gabah",
    1500,
    "Liter",
    require("../../../assets/images/gabah2.png"),
  ],

  [
    "Equipment",
    "Kelapa",
    75000,
    "Kg",
    require("../../../assets/images/kelapa.png"),
  ],
  [
    "Equipment",
    "Jagung Pipil",
    2000,
    "Liter",
    require("../../../assets/images/jagung-pipil.png"),
  ],
  [
    "Equipment",
    "Kopi",
    10000,
    "Kg",
    require("../../../assets/images/kopi.png"),
  ],
  [
    "Equipment",
    "Kacang",
    2000,
    "Kg",
    require("../../../assets/images/kacang-almond.png"),
  ],
  [
    "Equipment",
    "Kelapa",
    75000,
    "Kg",
    require("../../../assets/images/kelapa.png"),
  ],
  [
    "Equipment",
    "Jagung Pipil",
    2000,
    "Liter",
    require("../../../assets/images/jagung-pipil.png"),
  ],
  [
    "Equipment",
    "Kopi",
    10000,
    "Kg",
    require("../../../assets/images/kopi.png"),
  ],
  [
    "Equipment",
    "Kacang",
    2000,
    "Kg",
    require("../../../assets/images/kacang-almond.png"),
  ],
];
const dummyGrade = ["Grade 1", "Grade 2", "Grade 3", "Grade 4"];
const dummyData = dummyDatum.map((item, idx) => {
  return {
    id: idx + 1,
    category: item[0],
    subCategory: item[1],
    image: item[4],
    empofarmPrice: item[2] * 0.3,
    govPrice: item[2] * 0.2,
    readyStock: item[2] * 0.5,
    totalStock: item[2],
    unit: item[3],
    variants: dummyGrade.map((variant, idx) => {
      return {
        id: idx + 1,
        variantName: `${item[1]} ${variant}`,
        govPrice: item[2] * 0.25,
        empofarmPrice: item[2] * 0.25 * 0.7,
        farmerStock: item[2] * 0.25 * 0.3,
        consignmentStock: item[2] * 0.25 * 0.8,
        realStock: item[2] * 0.25 * 0.2,
      };
    }),
  };
});
