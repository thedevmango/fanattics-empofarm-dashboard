/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableFooter,
  TableRow,
  TableHeadCell,
} from "../../../components/Table";
import { BsThreeDotsVertical } from "react-icons/bs";
import { Button } from "../../../components/Button";
import { PaneHeader, MetadataText } from "../../../components/Typography";
import { Panel } from "../../../components/Panel";
import { formatnumber } from "../../../configs/formatnumber";
import { Colors } from "../../../constants";

export default ({ data }) => {
  const RenderBodyTable = ({ variants }) => {
    if (!variants) return <EmptyDataRow colSpan={7} />;
    return variants.map((variant, idx) => (
      <TableRow key={idx}>
        <TableCell>{`${variant.variantName}`}</TableCell>
        <TableCell>{`Rp ${formatnumber(variant.govPrice)}/Kg`}</TableCell>
        <TableCell>{`Rp ${formatnumber(variant.empofarmPrice)}/Kg`}</TableCell>
        <TableCell>
          <Button theme="action-table">
            <BsThreeDotsVertical size={12} />
          </Button>
        </TableCell>
      </TableRow>
    ));
  };

  return (
    <Panel style={{ borderRadius: 4 }} className="content mt-3">
      <div className="d-flex  justify-content-between">
        <PaneHeader bold className="mb-3">
          {data.subCategory}
        </PaneHeader>

        <div
          className="d-flex flex-column col-2"
          style={{ textAlign: "right" }}
        >
          <MetadataText color={Colors.gray400}>Update Terakhir</MetadataText>
          <MetadataText color={Colors.gray400}>13/09/2020</MetadataText>
        </div>
      </div>

      <Table>
        <TableHead>
          <TableRow>
            <TableHeadCell> Nama Varian </TableHeadCell>
            <TableHeadCell> Harga Acuan Pemerintah </TableHeadCell>
            <TableHeadCell> Harga Jual Empofarm </TableHeadCell>
            <TableHeadCell> &nbsp; </TableHeadCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <RenderBodyTable variants={data ? data.variants : []} />
        </TableBody>
        <TableFooter>
          <TableRow>
            <td colSpan={5}>
              <div style={{ fontStyle: "italic" }} className="ml-1">
                *Price constantly updated
              </div>
            </td>
          </TableRow>
        </TableFooter>
      </Table>
    </Panel>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <td colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </td>
    </TableRow>
  );
};
