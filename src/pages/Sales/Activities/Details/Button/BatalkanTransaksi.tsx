/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { BodyText, Button } from "../../../../../components";

export default ({ onClick }: { onClick: any }) => {
  return (
    <Button
      onClick={onClick}
      containerStyle={{ width: 200, textDecorationColor: "#D92C2C" }}
      theme="link"
      className="ml-2"
    >
      <BodyText color="#D92C2C">BATALKAN TRANSAKSI</BodyText>
    </Button>
  );
};
