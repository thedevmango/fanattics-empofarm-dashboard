import Axios from "axios";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { PopupCancellation } from "../../../../components/Molecules/Popup";
import { API_URL } from "../../../../constants";
import { getSalesTransaction } from "../../../../redux/actions/sales";

export default ({ data, isOpen, toggle }: { data: any; isOpen: boolean; toggle: any }) => {
  const dispatch = useDispatch();
  const [loadingCancellation, setLoadingCancellation] = useState(false);

  const handleSubmitCancellation = async (val: string) => {
    setLoadingCancellation(true);
    const resCancel = await Axios.post(`${API_URL}/cancellation-cases`, {
      saleTransactionId: data.id,
      requestedByAdminId: 1, //TODO SHOULD BE CHANGED BY CURRENT ADMIN ID
      remarks: val,
    });
    console.log(resCancel);
    toggle();
    dispatch(getSalesTransaction({ id: data.id }));
    setLoadingCancellation(false);
  };

  return (
    <PopupCancellation
      isOpen={isOpen}
      loading={loadingCancellation}
      onSubmit={handleSubmitCancellation}
      toggle={toggle}
    />
  );
};
