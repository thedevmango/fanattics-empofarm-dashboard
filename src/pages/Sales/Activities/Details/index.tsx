/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { BackButton, BodyText, HeaderText, PageTitle } from "../../../../components";
import { useDispatch, useSelector } from "react-redux";
import { BsDownload } from "react-icons/bs";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  ReportQC,
  ReportQCItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { OutboundStatus } from "../../../../constants";
import { BatalkanTransaksi, DownloadHalaman } from "./Button";
import { getShipmentLogs } from "../../../../redux/actions/logs";
import { getSalesTransaction, getSaleTrxDetails } from "../../../../redux/actions/sales";
import RincianPenjualan from "./RincianPenjualan";
import RincianPenjualanItem from "./RincianPenjualanItem";
import PopupCancellation from "./PopupCancellation";
import { SALES_LIST_DETAILS, SALES_TRX_DETAILS } from "../../../../redux/types";

//TODO: ENBALE PAID INVOICE DOWNLOAD BUTTON

export default (props: any) => {
  const dispatch = useDispatch();
  const parentData = props.location.state.data;
  const tab = props.location.state.tab;
  const { FinanceIncoming, FinanceOrder, FinanceFinished } = OutboundStatus;

  //* GLOBAL STATE
  const data = useSelector(({ sales }: any) => sales.salesListDetails);
  const sale_transaction_details = useSelector(({ sales }: any) => sales.sale_transaction_details);
  const shipmentLogs = useSelector(({ logs }: any) => logs.shipmentLogs);

  //* LOCAL STATE
  const [openCancellation, setOpenCancellation] = useState(false);

  //* FUNCTION
  const toggleCancellation = () => setOpenCancellation(!openCancellation);
  const handleDownloadReport = () => {
    return alert("downloaded");
  };

  useEffect(() => {
    if (!data) return;
    dispatch(getShipmentLogs({ data: data, type: "outbound" }));
  }, [dispatch, data]);

  useEffect(() => {
    dispatch(getSalesTransaction({ id: parentData.id }));
    dispatch(getSaleTrxDetails({ id: parentData.id }));
  }, [dispatch, parentData.id]);

  console.log("salesListDetails", parentData.id, data);
  if (!data || !sale_transaction_details)
    return (
      <div className="sales sales-transactional-activities">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="sales sales-transactional-activities">
      <Helmet>
        <title>Details | Sales</title>
      </Helmet>

      <BackButton
        onClick={() => {
          dispatch({ type: SALES_LIST_DETAILS, payload: [] });
          dispatch({ type: SALES_TRX_DETAILS, payload: [] });
          props.history.goBack();
        }}
      >
        Kembali ke Transactional Activities
      </BackButton>

      <PopupCancellation data={data} isOpen={openCancellation} toggle={toggleCancellation} />

      <div className="header d-flex justify-content-between mb-4">
        <PageTitle>Detail Transaksi Pemesanan</PageTitle>

        <div className="button-wrapper d-flex">
          {tab !== FinanceFinished && !data.isCancelRequest ? (
            <BatalkanTransaksi onClick={toggleCancellation} />
          ) : null}

          <DownloadHalaman {...props} id={parentData.id} />
        </div>
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(parentData.updatedAt).format("DD/MM/YYYY H:mm")}>
            <HeaderText>{parentData.status}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Buyer"
            name={parentData.buyer.user.fullName}
            contact={parentData.buyer.user.phoneNumber}
            address={parentData.buyer.user.address}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {parentData.transactionCode || "-"}
            </DataTransaksiItem>
            <DataTransaksiItem title="Performa Invoice">
              {!parentData.performaInvoice ? (
                "-"
              ) : (
                <BodyText className="d-flex" accent>
                  {parentData.performaInvoice}
                  <BsDownload
                    size={12}
                    style={{
                      cursor: "pointer",
                      borderRadius: 4,
                      border: "0.5px solid #87AD70",
                      padding: 5,
                      marginLeft: 10,
                      boxSizing: "content-box",
                    }}
                  />
                </BodyText>
              )}
            </DataTransaksiItem>
            <DataTransaksiItem title="Metode Pembayaran">
              {parentData.paymentMethod}
            </DataTransaksiItem>
            <DataTransaksiItem title="Cara Pembayaran">{"-"}</DataTransaksiItem>
          </DataTransaksi>

          {FinanceFinished.includes(parentData.status) ? (
            <ReportQC
              type="Outbound"
              officerName=""
              qcPassed
              handleDownloadReport={handleDownloadReport}
            >
              <ReportQCItem
                verified
                title="Quantity"
                signedBy="Name"
                timestamp={moment().format("DD/MM/YYYY H:mm")}
              />
              <ReportQCItem
                verified
                title="Quality"
                signedBy="Name"
                timestamp={moment().format("DD/MM/YYYY H:mm")}
              />
              <ReportQCItem
                verified
                title="Packaging"
                signedBy="Name"
                timestamp={moment().format("DD/MM/YYYY H:mm")}
              />
              <ReportQCItem
                verified
                title="Document"
                signedBy="Name"
                timestamp={moment().format("DD/MM/YYYY H:mm")}
              />
            </ReportQC>
          ) : null}
        </div>

        <div className="col-8">
          <RincianPenjualan parentData={parentData}>
            {!sale_transaction_details
              ? null
              : sale_transaction_details.map((item: any, i: number) => {
                  return <RincianPenjualanItem key={i} item={item} />;
                })}
          </RincianPenjualan>

          {FinanceIncoming.includes(data?.status) || !shipmentLogs ? null : (
            <RiwayatEkspedisi data={shipmentLogs} />
          )}
        </div>
      </div>
    </div>
  );
};
