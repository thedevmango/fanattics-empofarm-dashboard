import React from "react";
import {
  BodyText,
  GreetingTitle,
  HeaderText,
  PaneHeader,
  Panel,
  SubjectTitle,
} from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";

interface Props {
  children: React.ReactNode;
  parentData?: any;
}

export default ({ children, parentData }: Props) => {
  return (
    <Panel padding="24" className="mol-rincian-penjualan">
      <div className="header">
        <HeaderText color="#6E6E6E" bold>
          Rincian Penjualan
        </HeaderText>
      </div>

      <div className="content">{children}</div>

      <div className="footer">
        <div className="biaya-tambahan">
          <div className="title">
            <HeaderText color="#000000">Biaya Tambahan</HeaderText>
          </div>
          <div className="value">
            <div className="value-item">
              <BodyText color="#605E5C">Biaya Packaging</BodyText>
              <SubjectTitle color="#323130">
                {"-"}
                {/* {!packagingCost ? <>&ndash;</> : `Rp ${formatquantity(packagingCost)}`} */}
              </SubjectTitle>
            </div>
            <div className="value-item">
              <BodyText color="#605E5C">Asuransi</BodyText>
              <SubjectTitle color="#323130">
                {"-"}
                {/* {!insuranceFee ? <>&ndash;</> : `Rp ${formatquantity(insuranceFee)}`} */}
              </SubjectTitle>
            </div>
            <div className="value-item">
              <BodyText color="#605E5C">Biaya Pengiriman</BodyText>
              <SubjectTitle color="#323130">
                {"-"}
                {/* {!shippingCost ? <>&ndash;</> : `Rp ${formatquantity(shippingCost)}`} */}
              </SubjectTitle>
            </div>
          </div>
        </div>

        <div className="total-nominal-transaksi">
          <div className="title">
            <PaneHeader color="#605E5C">Total Nominal Penjualan</PaneHeader>
          </div>
          <div className="value">
            <GreetingTitle bold accent>
              {!parentData.totalPrice ? (
                <>&ndash;</>
              ) : (
                `Rp ${formatquantity(parentData.totalPrice)}`
              )}
            </GreetingTitle>
          </div>
        </div>
      </div>
    </Panel>
  );
};
