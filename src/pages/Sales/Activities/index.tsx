/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { BsChevronRight } from "react-icons/bs";
import {
  BodyText,
  Button,
  Pagination,
  Panel,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  usePagination,
} from "../../../components";
import { formatnumber } from "../../../configs/formatnumber";
import { OutboundStatus } from "../../../constants";
import { useDispatch, useSelector } from "react-redux";
import { SALES_SET_TAB } from "../../../redux/types";
import { getSalesTransaction } from "../../../redux/actions/sales";

export default (props: any) => {
  const dispatch = useDispatch();
  const { FinanceIncoming, FinanceOrder, FinanceFinished } = OutboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.sales.tab);
  const data = useSelector((state: any) => state.sales.salesList);

  //* LOCAL STATE
  const [title, setTitle] = useState("");

  //* FUNCTION
  const toggleTab = (selectedTab: string[]) => {
    if (selectedTab === tab) return;
    dispatch({ type: SALES_SET_TAB, payload: selectedTab });
  };

  useEffect(() => {
    if (tab === FinanceIncoming) setTitle("Pesanan Masuk");
    if (tab === FinanceOrder) setTitle("Sales Order");
    if (tab === FinanceFinished) setTitle("Transaksi Selesai");
    setPage(1);
    dispatch(getSalesTransaction({ status: tab }));
  }, [tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  console.log("salesList", data);
  if (!data)
    return (
      <div className="sales sales-transactional-activities">
        <Helmet>
          <title>loading...</title>
        </Helmet>
      </div>
    );
  return (
    <div className="sales sales-transactional-activities">
      <Helmet>
        <title>{title} | Sales</title>
      </Helmet>

      <div className="mb-4">
        <Tabs
          options={["Pesanan Masuk", "Sales Order", "Transaksi Selesai"]}
          slug={[FinanceIncoming, FinanceOrder, FinanceFinished]}
          activeTab={tab}
          setActiveTab={toggleTab}
        />
      </div>

      <Panel className="content">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Tanggal Pesanan</TableHeadCell>
              <TableHeadCell>Nama Pembeli</TableHeadCell>
              <TableHeadCell>Performa Invoice ID</TableHeadCell>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell style={{ textAlign: "right" }}>Nominal Transaksi</TableHeadCell>

              <TableHeadCell style={{ textAlign: "right" }}>
                {tab === FinanceIncoming ? "Status Pembayaran" : "Status"}
              </TableHeadCell>

              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ history, match, tab, data }) => {
  const { SalesIncoming, SalesUnconfirmed } = OutboundStatus;
  return !data
    ? null
    : data.map((item: any, idx: number) => {
        const link = `${match.url}/details`;
        const nextClick = () => history.push(link, { data: item, tab: tab });
        return (
          <TableRow key={idx} onClick={nextClick}>
            <TableCell>
              <BodyText>{moment(item.createdAt).format("DD/MM/YYYY")}</BodyText>
            </TableCell>
            <TableCell>
              <BodyText>{item.buyer.user.fullName}</BodyText>
            </TableCell>
            <TableCell>
              <BodyText>{item.invoiceCode || "-"}</BodyText>
            </TableCell>
            <TableCell>
              <BodyText>{item.transactionCode || "-"}</BodyText>
            </TableCell>
            <TableCell style={{ textAlign: "right" }}>
              <BodyText>{`Rp ${formatnumber(item.totalPrice)}`}</BodyText>
            </TableCell>

            {tab === SalesIncoming ? (
              <TableCell style={{ textAlign: "right" }}>
                {item.status === SalesIncoming ? (
                  <BodyText color="#D92C2C">Belum Dibayar</BodyText>
                ) : item.status === SalesUnconfirmed ? (
                  <BodyText color="#2F9F28">Sudah Dibayar</BodyText>
                ) : null}
              </TableCell>
            ) : (
              <TableCell style={{ textAlign: "right" }}>{item.status}</TableCell>
            )}

            <TableCell>
              <Button containerStyle={{ backgroundColor: "transparent" }} theme="action-table">
                <BsChevronRight />
              </Button>
            </TableCell>
          </TableRow>
        );
      });
};
