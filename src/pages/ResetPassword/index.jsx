/* eslint-disable no-unused-vars */
import React from "react";
import Illustration from "../../assets/images/bg-reset-pass.png";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Spinner } from "reactstrap";

import { Form, Input } from "../../components/Form";
import { Button } from "../../components/Button";
import { RESET_PASS_ONCHANGE } from "../../redux/types/resetPass";
import { submitResetPassword } from "../../redux/actions/resetPass";
import { Modal, ModalHeader, ModalBody } from "../../components/Modal";

export default (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  // const resetPass = useSelector((state) => state.resetPass);
  const resetPass = {
    loading: false,
    success: false,
    error: false,
  };

  function valueOnChange(e) {
    let name = e.target.name;
    let value = e.target.value;
    dispatch({ type: RESET_PASS_ONCHANGE, payload: { name, value } });
  }
  function submitResetPass() {
    dispatch(submitResetPassword());
  }

  return (
    <div className="reset-pass">
      <img src={Illustration} alt="reset-pass" />
      {resetPass.loading ? (
        <LoadingComponent />
      ) : resetPass.success ? (
        <SuccessComponent onLogin={() => history.push("/login")} />
      ) : resetPass.error ? (
        <ErrorComponent onResubmit={submitResetPass} />
      ) : (
        <ContentComponent
          onSubmit={submitResetPass}
          onChange={valueOnChange}
          loading={resetPass.loading}
        />
      )}
    </div>
  );
};

const LoadingComponent = () => {
  return (
    <div className="content-wrapper loading">
      <Spinner className="reset-pass-spinner" />
      <div className="harap-tunggu">Harap Tunggu Sebentar...</div>
    </div>
  );
};
const SuccessComponent = ({ onLogin }) => {
  return (
    <div className="content-wrapper success">
      <div className="title">Password kamu berhasil diperbarui!</div>
      <div className="subtitle">Silahkan melakukan login kembali dengan password baru kamu</div>
      <Button
        bold
        width={310}
        theme="primary"
        onClick={onLogin}
        containerStyle={{ height: 55, marginTop: 60 }}
      >
        Login
      </Button>
    </div>
  );
};
const ErrorComponent = ({ onResubmit }) => {
  return (
    <div className="content-wrapper success">
      <div className="title">Password kamu gagal diperbarui!</div>
      <Button
        bold
        width={310}
        theme="primary"
        onClick={onResubmit}
        containerStyle={{ height: 55, marginTop: 60 }}
      >
        Ulangi
      </Button>
    </div>
  );
};

const ContentComponent = ({ onSubmit, onChange, loading }) => {
  return (
    <Modal backdrop={false} fade={false} isOpen={true}>
      <ModalHeader>Password Baru</ModalHeader>
      <ModalBody className="reset-password-body">
        <div className="subtitle">Harap Masukkan Password Baru</div>

        <Form className="form-reset-password">
          <Input
            label="Password Baru"
            name="newPassword"
            type="password"
            placeholder="Masukkan Password Baru"
            onChange={onChange}
          />
          <Input
            label="Konfirmasi Password Baru"
            name="newPassword2"
            type="password"
            placeholder="Konfirmasi Password Baru"
            onChange={onChange}
          />
        </Form>

        <div className="d-flex justify-content-center mt-5">
          <Button containerStyle={{ width: 130 }} bold theme="text" onClick={onSubmit}>
            Batalkan
          </Button>
          <Button containerStyle={{ width: 130 }} bold theme="primary" onClick={onSubmit}>
            {loading ? <Spinner size="sm" /> : "Konfirmasi"}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
