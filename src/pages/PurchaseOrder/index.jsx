/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import {
  BodyText,
  MetadataText,
  HeaderText,
  SubjectTitle,
  PaneHeader,
  HeroTitle,
  PageTitle,
} from "../../components/Typography";
import { Panel, PanelBody, PanelHead } from "../../components/Panel";
import { Colors } from "../../constants";
import { Card } from "../../components/Card";
import { BsDownload, BsChevronRight } from "react-icons/bs";
import QCReportListItem from "./QCReportListItem";
import { Button } from "../../components/Button";
import Jagung from "../../assets/images/jagung-pipil.png";
import TransactionStatusHistoryItem from "./TransactionStatusHistoryItem";

// TODO: Add sample data
export default ({ ...props }) => {
  return (
    <div className="mt-3 pb-4 sales">
      <div className="d-flex flex-row justify-content-between mb-3">
        <PageTitle className="mb-3">Purchasing Order Detail</PageTitle>
        <Button theme="primary" style={{ padding: "6px 20px" }}>
          DOWNLOAD HALAMAN INI
        </Button>
      </div>
      <div className="row">
        <div className="col-4">
          <Panel padding="16">
            <div className="row">
              <div className="col-8">
                <BodyText color="#919191">Update Status Transaksi</BodyText>
                <HeaderText className="mt-1">Memproses Invoice oleh Finance</HeaderText>
              </div>
              <div className="col">
                <MetadataText color={Colors.gray500} className="d-block text-right">
                  15:15
                </MetadataText>
                <MetadataText color={Colors.gray500} className="d-block text-right">
                  06/06/2020
                </MetadataText>
                <div className="mt-5 pl-4 pr-0">
                  <MetadataText color="#20C4F4">Lihat Detail</MetadataText>
                </div>
              </div>
            </div>
          </Panel>
          <Panel className="mt-2">
            <PanelHead bordered>
              <HeaderText color="#6e6e6e" bold>
                Data Farmer
              </HeaderText>
            </PanelHead>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Nama Farmer
              </MetadataText>
              <BodyText className="d-block" accent>
                Pak Aji Sohiji
              </BodyText>
            </PanelBody>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Nomor Kontak
              </MetadataText>
              <BodyText className="d-block">+62 21 7222006</BodyText>
            </PanelBody>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Alamat farm
              </MetadataText>
              <BodyText className="d-block">
                Jl. Wolter Mongonsidi No. 82 (Petogogan, Kebayoran Baru) Jakarta Selatan , Jakarta
              </BodyText>
            </PanelBody>
          </Panel>
          <Panel className="mt-2">
            <PanelHead bordered>
              <HeaderText color="#6e6e6e" bold>
                Data Tender
              </HeaderText>
            </PanelHead>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Tanggal Pengajuan
              </MetadataText>
              <BodyText className="d-block">Selasa, 26/04/2020</BodyText>
            </PanelBody>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Kode Transaki
              </MetadataText>
              <BodyText className="d-block">ALEPHOENTR975</BodyText>
            </PanelBody>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Bentuk Transaksi
              </MetadataText>
              <BodyText className="d-block">Direct Selling</BodyText>
            </PanelBody>
            <PanelBody>
              <MetadataText color={Colors.secondary} bold>
                Paid Invoice
              </MetadataText>
              <BodyText className="d-block" accent>
                INV-10-10010{" "}
                <BsDownload
                  className="ml-2"
                  style={{
                    borderRadius: "4px",
                    border: "1px solid #87AD70",
                    padding: "5px",
                    boxSizing: "content-box",
                  }}
                />
              </BodyText>
            </PanelBody>
          </Panel>
          <Panel className="mt-2">
            <PanelHead bordered>
              <div className="col">
                <HeaderText color="#6e6e6e" bold className="d-block text-left">
                  Inbound QC Report
                </HeaderText>
                <MetadataText color={Colors.gray400}>Petugas QC: Supirman</MetadataText>
                <div className="d-block text-left mt-4">
                  <SubjectTitle bold>Item Name Goes Here</SubjectTitle>
                  <MetadataText
                    style={{
                      borderRadius: "4px",
                      border: "1px solid #C3D6B7",
                      padding: "5px",
                      boxSizing: "content-box",
                    }}
                    className="ml-4"
                  >
                    QC Passed
                  </MetadataText>
                  <BsChevronRight className="ml-4" size={12} />
                </div>
              </div>
            </PanelHead>
            <Card
              style={{
                backgroundColor: Colors.gray25,
                padding: "16px",
                marginTop: "10px",
              }}
            >
              {/* Refactor this to a separate component */}
              <QCReportListItem verified />
              <QCReportListItem margin />
              <QCReportListItem margin />
              <QCReportListItem verified />
              <div className="text-center mt-3">
                <a href="#">
                  <MetadataText accent> Download QC Report</MetadataText>
                </a>
              </div>
            </Card>
            <div className="d-flex flex-row justify-content-end">
              <Button className="mt-2" theme="primary">
                Pilih Petugas QC
              </Button>
            </div>
          </Panel>
        </div>
        <div className="col-8">
          <Panel padding="24">
            <PanelHead bordered>
              <HeaderText bold color={Colors.gray500}>
                Rincian Tender
              </HeaderText>
            </PanelHead>
            <div className="row mt-3">
              <div className="col-2">
                <img style={{ width: "100%", objectFit: "cover" }} src={Jagung} alt="" />
              </div>
              <div className="col-10">
                <div className="border-bottom pb-3">
                  <PaneHeader color={Colors.gray600}>Jagung Pipil Grade 1</PaneHeader>
                </div>
                <div className="row py-3 mt-4 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Category
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Hard</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Sub Category
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Jagung</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Harga Acuan Empofarm
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Rp5.000/ Kg</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Harga Penawaran farmer
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Rp5.000/ Kg</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Harga Setelah Negosiasi
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Rp4.500/ Kg</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 border-bottom justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Kuantitas Barang
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>500 Kg</PaneHeader>
                  </div>
                </div>
                <div className="row py-3 justify-content-center">
                  <div className="col-4">
                    <BodyText color={Colors.gray600} bold>
                      Deskripsi Barang
                    </BodyText>
                  </div>
                  <div className="col-8">
                    <PaneHeader color={Colors.gray600}>Deskripsi barang disini</PaneHeader>
                  </div>
                </div>
              </div>
            </div>
            <div className="row border-top pb-5">
              <div className="col offset-2 mt-4">
                <PaneHeader color={Colors.secondary} className="d-block">
                  Harga Tender Final
                </PaneHeader>
                <HeroTitle accent>Rp2,250,000</HeroTitle>
              </div>
            </div>
          </Panel>
          <Panel padding="24" className="mt-2">
            <PaneHeader bold color={Colors.gray500} className="d-block">
              Riwayat Status Transaksi
            </PaneHeader>
            <SubjectTitle color={Colors.gray400} className="mt-4">
              Petugas Kurir: Heo Joon Jae
            </SubjectTitle>
            <TransactionStatusHistoryItem />
            <TransactionStatusHistoryItem />
            <TransactionStatusHistoryItem />
            <TransactionStatusHistoryItem />
          </Panel>
        </div>
      </div>
    </div>
  );
};
