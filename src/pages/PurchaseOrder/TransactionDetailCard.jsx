import React, { useState } from "react";
import { BodyText, HeaderText } from "../../components/Typography";
import { Colors } from "../../constants";
import { Card } from "../../components/Card";
import ProfilePic from "../../assets/images/profpict.jpg";
import { IoIosArrowDown } from "react-icons/io";
import { Collapse } from "reactstrap";

export default ({ ...props }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div
      style={{
        border: "1px solid #D2E0C9",
        borderRadius: "8px",
        padding: "16px",
        margin: "8px 0px",
      }}
    >
      <Card className="d-flex flex-row">
        <div>
          <img src={ProfilePic} className="profile-pic" alt="" />
        </div>
        <div className="ml-2">
          <HeaderText bold accent>
            Madu Murni
          </HeaderText>
          <div className="d-flex flex-row flex-wrap row mt-2">
            <div className="d-flex flex-column col-3">
              <BodyText color={Colors.gray500}>SKU</BodyText>
              <BodyText color={Colors.gray500} bold>
                081920192 0912 09120
              </BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText color={Colors.gray500}>Farmer</BodyText>
              <BodyText color={Colors.gray500} bold>
                Joko Tole
              </BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText color={Colors.gray500}>Pilihan Packaging</BodyText>
              <BodyText color={Colors.gray500} bold>
                Botol
              </BodyText>
            </div>
            <div className="d-flex flex-column col-3">
              <BodyText color={Colors.gray500}>Total Pembelian</BodyText>
              <BodyText color={Colors.gray500} bold>
                500 Botol
              </BodyText>
            </div>
          </div>
          <div className="row">
            <div className="d-flex flex-column mr-3 my-2 col">
              <BodyText color={Colors.gray500}>Harga per Botol</BodyText>
              <BodyText color={Colors.gray500} bold>
                Rp. 15.000
              </BodyText>
            </div>
            <div className="d-flex flex-column mr-3 my-2 col">
              <BodyText color={Colors.gray500}>Harga Terjual</BodyText>
              <BodyText color={Colors.gray500} bold>
                Rp. 75.000
              </BodyText>
            </div>
          </div>
        </div>
      </Card>
      {/* TODO: Animate chevron arrow */}
      <div
        className="collapse-toggle d-flex flex-row justify-content-between"
        onClick={() => setIsOpen(!isOpen)}
      >
        <BodyText color={Colors.gray500}>Laporan Inventory Manager</BodyText>
        <IoIosArrowDown />
      </div>
      <Collapse isOpen={isOpen}>
        <Card
          className="d-flex flex-row"
          style={{
            backgroundColor: Colors.gray25,
            padding: "37.5px 40px",
          }}
        >
          <Card
            className="d-flex flex-row align-items-center mx-2"
            style={{ flex: 1 }}
          >
            <div className="primary-circle">
              <BodyText bold>75%</BodyText>
            </div>
            <div className="ml-4">
              <BodyText color={Colors.gray500}>Stock Real</BodyText>
              <HeaderText className="d-block" color={Colors.gray800}>
                400 Botol
              </HeaderText>
            </div>
          </Card>
          <Card
            className="d-flex flex-row align-items-center mx-2"
            style={{ flex: 1 }}
          >
            <div className="primary-circle">
              <BodyText bold>75%</BodyText>
            </div>
            <div className="ml-4">
              <BodyText color={Colors.gray500}>Stock Real</BodyText>
              <HeaderText className="d-block" color={Colors.gray800}>
                400 Botol
              </HeaderText>
            </div>
          </Card>
        </Card>
      </Collapse>
    </div>
  );
};
