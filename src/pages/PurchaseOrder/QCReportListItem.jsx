import React from "react";
import { MetadataText } from "../../components/Typography";
import { Colors } from "../../constants";
import { GoVerified } from "react-icons/go";

export default ({ margin, verified, ...props }) => {
  return (
    <div
      className={`d-flex flex-row justify-content-between ${
        margin ? "my-4" : ""
      }`}
    >
      <div className="d-flex flex-column">
        <MetadataText color={Colors.gray500} bold>
          Quality
          {verified ? (
            <GoVerified className="ml-2" color={Colors.teal} />
          ) : null}
        </MetadataText>
        <MetadataText color={Colors.gray800}>
          Signed by: Employee Name
        </MetadataText>
      </div>
      <div className="d-flex flex-column text-right">
        <MetadataText color={Colors.gray500}>15:15</MetadataText>
        <MetadataText color={Colors.gray500}>06/07/2020</MetadataText>
      </div>
    </div>
  );
};
