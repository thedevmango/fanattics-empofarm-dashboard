import React from "react";
import { BodyText, MetadataText } from "../../components/Typography";
import { Colors } from "../../constants";
import { BsCircleFill } from "react-icons/bs";

export default ({ ...props }) => {
  return (
    <div className="transaction-status-history-item d-flex flex-row mt-3">
      <div>
        <BsCircleFill color={Colors.primaryBrand} />
      </div>
      <div className="ml-2">
        <BodyText bold>Kurir Dalam Perjalanan Pick Up</BodyText> - {"  "}
        <MetadataText> Farm Pak Rizal</MetadataText>
        <MetadataText className="d-block">15 Juli 2020, 08:00 WIB</MetadataText>
      </div>
    </div>
  );
};
