/* eslint-disable no-unused-vars */
import React from "react";
import { formatnumber } from "../../../configs/formatnumber";

export const ProductCard = ({ data, margin, history }) => {
  return (
    <div
      onClick={() => history.push(`/variant/${data.subcategory.toLowerCase()}`, { data })}
      style={{ margin, cursor: "pointer" }}
      className="product">
      <div className="product-image">
        <img src={data.image} alt="product" />
      </div>

      <div className="product-info">
        <div className="product-info-title">{data.subcategory}</div>
        <div className="product-info-stock">
          <span>{`Total Stock: ${formatnumber(data.stock)} ${data.unit}`}</span>
        </div>
      </div>
    </div>
  );
};
