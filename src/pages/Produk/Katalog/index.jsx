/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import ImgGabah from "../../../assets/images/gabah.png";
import ImgJagungPipil from "../../../assets/images/jagung-pipil.png";
import ImgJagungSuper from "../../../assets/images/jagung-super.png";
import ImgMadu from "../../../assets/images/madu-murni.png";
import ImgNanas from "../../../assets/images/nanas.png";
import Pagination from "../../../components/Pagination";
import { ProductCard } from "./ProductCard";
import { PRODUCT_FETCH_SUCCESS } from "../../../redux/types/product";
import usePagination from "../../../components/Pagination/usePagination";

export default function KatalogProduk(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const page = useSelector((state) => state.product.page);
  const productList = useSelector((state) => state.product.productList);

  function ShowProducts({ products }) {
    let result = null;
    if (products.length > 0) {
      result = products.map((product, index) => {
        return <ProductCard history={history} key={index} data={product} />;
      });
    }
    return result;
  }
  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination(
    productList,
    10
  );

  useEffect(() => {
    dispatch({ type: PRODUCT_FETCH_SUCCESS, payload: dummySubCategories });
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [page]);

  if (productList.length < 1) {
    return <div className="is-loading">loading...</div>;
  } else
    return (
      <div className="katalog-container">
        <div className="katalog-header">
          <div className="katalog-title">Katalog Produk</div>
        </div>

        <div className="katalog-body">
          <div
            className="product-container"
            style={{
              justifyContent: currentData().length < 10 ? "flex-start" : "space-between",
              // height: rowsPerPage.length < 6 ? 300 : 560,
            }}>
            <ShowProducts products={currentData()} />
          </div>
        </div>

        <div className="katalog-footer">
          <Pagination
            next={next}
            prev={prev}
            jump={jump}
            pages={pages}
            currentPage={currentPage}
            maxPage={maxPage}
          />
        </div>
      </div>
    );
}
const dummySubCategoriesList = [
  ["Nanas Muda", 123000, "Kg", ImgNanas, "Soft"],
  ["Gabah", 123000, "Kg", ImgGabah, "Hard"],
  ["Jagung Pipil", 1000, "Liter", ImgJagungPipil, "Hard"],
  ["Jagung Super", 1000, "Liter", ImgJagungSuper, "Hard"],
  ["Madu Murni", 1000, "Liter", ImgMadu, "Liquid"],
  ["Nanas Muda", 123000, "Kg", ImgNanas, "Soft"],
  ["Gabah", 123000, "Kg", ImgGabah, "Hard"],
  ["Jagung Pipil", 1000, "Liter", ImgJagungPipil, "Hard"],
  ["Jagung Super", 1000, "Liter", ImgJagungSuper, "Hard"],
  ["Madu Murni", 1000, "Liter", ImgMadu, "Liquid"],
  ["Nanas Muda", 123000, "Kg", ImgNanas, "Soft"],
  ["Gabah", 123000, "Kg", ImgGabah, "Hard"],
  ["Jagung Pipil", 1000, "Liter", ImgJagungPipil, "Hard"],
  ["Jagung Super", 1000, "Liter", ImgJagungSuper, "Hard"],
  ["Madu Murni", 1000, "Liter", ImgMadu, "Liquid"],
];
const dummyGrade = ["Grade 1", "Grade 2", "Grade 3"];
const dummySubCategories = dummySubCategoriesList.map((subcategory, idx) => {
  return {
    id: idx + 1,
    subcategory: subcategory[0],
    stock: subcategory[1],
    unit: subcategory[2],
    image: subcategory[3],
    category: subcategory[4],
    variants: dummyGrade.map((variant, idx) => {
      return {
        id: idx + 1,
        variantId: "081920192 0912 09120",
        variantName: `${subcategory[0]} ${variant}`,
        stockEmpofarm: 17500,
        stockFarmer: 2500,
        stockTotal: 20000,
        skuList: [
          {
            id: 1,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 2,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Farmer",
            storage: "Farmer",
            sellingStatus: true,
          },
          {
            id: 3,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Farmer",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 4,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 5,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Farmer",
            sellingStatus: false,
          },
          {
            id: 6,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 7,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Farmer",
            storage: "Farmer",
            sellingStatus: false,
          },
          {
            id: 8,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Farmer",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 9,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Empofarm",
            sellingStatus: false,
          },
          {
            id: 10,
            farmerName: "Joko Tole",
            stock: 20000,
            ownership: "Empofarm",
            storage: "Farmer",
            sellingStatus: false,
          },
        ],
      };
    }),
  };
});
