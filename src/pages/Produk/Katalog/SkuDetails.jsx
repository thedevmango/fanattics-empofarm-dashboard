/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Modal, ModalBody } from "reactstrap";
import { formatnumber } from "../../../configs/formatnumber";

import Button from "../../../components/General/Button";
import { FiArrowLeft } from "react-icons/fi";
import { AiOutlineCheckSquare, AiOutlineBorder } from "react-icons/ai";

export default function SkuDetails(props) {
  const data = props.location.state.data;
  const history = useHistory();

  const [isActive, setIsActive] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const toggleModal = () => setModalOpen(!modalOpen);

  function handleStatus(checked) {
    setIsActive(checked);
    return setModalOpen(true);
    // return alert("TES " + checked);
  }

  console.log(data);
  return (
    <div className="product-details">
      <Modal
        modalClassName="modal-activating-product"
        centered
        isOpen={modalOpen}
        toggle={toggleModal}
        size="sm">
        <ModalBody>
          <div className="title">
            {isActive
              ? "Yakin Ingin Non-aktifkan Produk Untuk Dijual?"
              : "Yakin Ingin Mengaktifkan Produk Untuk Dijual?"}
          </div>
          {isActive ? (
            <div className="button-wrapper">
              <Button bold className="is-primary">
                Batalkan
              </Button>
              <Button bold className="is-danger">
                Non-Aktifkan Produk
              </Button>
            </div>
          ) : (
            <div className="button-wrapper">
              <Button bold className="is-danger">
                Batalkan
              </Button>
              <Button bold className="is-primary">
                Aktifkan Produk
              </Button>
            </div>
          )}
        </ModalBody>
      </Modal>

      <div className="header">
        <Button
          icon={<FiArrowLeft />}
          onClick={history.goBack}
          iconStyle={{ color: "#69984C" }}
          textStyle={{ color: "#323130" }}
          theme="link">
          Kembali ke Daftar Variant
        </Button>
      </div>

      <div className="content">
        <div className="content-header">
          <div className="header-title">{data.variantName}</div>
        </div>

        <div className="content-body">
          <table>
            <thead>
              <tr>
                <th>Nama Farmer</th>
                <th>Stock</th>
                <th>Tipe Kepemilikan</th>
                <th>Tipe Penyimpanan</th>
                <th>Status Aktivasi Penjualan Produk</th>
              </tr>
            </thead>

            <tbody>
              {data.skuList.map((item, idx) => {
                return (
                  <tr style={{ cursor: "default" }} key={idx}>
                    <td>
                      <b>{item.farmerName}</b>
                    </td>
                    <td>{formatnumber(item.stock) + " Kg"}</td>
                    <td>{item.ownership}</td>
                    <td>{item.storage + " Stock Keeping"}</td>
                    <td>
                      <CheckboxComp onClick={handleStatus} checked={item.sellingStatus} />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

const CheckboxComp = ({ checked, onClick }) => {
  return (
    <Button
      onClick={() => onClick(checked)}
      theme="link"
      iconStyle={{ color: "#69984C" }}
      icon={checked ? <AiOutlineCheckSquare size={20} /> : <AiOutlineBorder size={20} />}
    />
  );
};
