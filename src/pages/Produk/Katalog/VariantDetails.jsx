import React from "react";
import { useHistory } from "react-router-dom";

import Button from "../../../components/General/Button";
import { FiArrowLeft } from "react-icons/fi";
import { formatnumber } from "../../../configs/formatnumber";

export default function VariantDetails(props) {
  const data = props.location.state.data;
  const history = useHistory();

  return (
    <div className="product-details">
      <div className="header">
        <Button
          icon={<FiArrowLeft />}
          onClick={history.goBack}
          iconStyle={{ color: "#69984C" }}
          textStyle={{ color: "#323130" }}
          theme="link">
          Kembali ke Katalog Produk
        </Button>
      </div>

      <div className="content">
        <div className="content-header">
          <div className="header-title">{data.subcategory}</div>
        </div>

        <div className="content-body">
          <table>
            <thead>
              <tr>
                <th>Varian</th>
                <th>Varian</th>
                <th>Total Stock (Kg)</th>
                <th>Empofarm Stock Keeping (Kg)</th>
                <th>Farmer Stock Keeping (Kg)</th>
              </tr>
            </thead>

            <tbody>
              {data.variants.map((item, idx) => {
                return (
                  <tr
                    key={idx}
                    onClick={() =>
                      history.push(
                        `/sku-list/${item.variantName.toLowerCase().replace(" ", "-")}`,
                        { data: item }
                      )
                    }>
                    <td>{item.variantId}</td>
                    <td>{item.variantName}</td>
                    <td>{formatnumber(item.stockTotal)}</td>
                    <td>{formatnumber(item.stockEmpofarm)}</td>
                    <td>{formatnumber(item.stockFarmer)}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
