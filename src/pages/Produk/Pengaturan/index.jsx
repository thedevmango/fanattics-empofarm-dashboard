/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FiPlus } from "react-icons/fi";

import Button from "../../../components/General/Button";
import ContentListKategori from "./ContentListKategori";
import ContentListProduk from "./ContentListProduk";
import ContentListSubproduk from "./ContentListSubproduk";

export default function PengaturanProduk(props) {
  const dispatch = useDispatch();
  const [content, setContent] = useState("produk");

  return (
    <div className="pengaturan-produk-container">
      <div className="pengaturan-produk-header">
        <div className="pengaturan-produk-title">Pengaturan Produk</div>

        <div className="button-pivot">
          <Button
            active={content === "produk"}
            onClick={() => setContent("produk")}
            theme="pivot-stack">
            Pengaturan Produk Berdasarkan Kategori
          </Button>
          <Button
            active={content === "harga"}
            onClick={() => setContent("harga")}
            theme="pivot-stack">
            Pengaturan Harga Produk
          </Button>
        </div>

        <div className="pengaturan-produk-body">
          <ContentListKategori title="nama-kategori" />
          <ContentListProduk title="nama-produk" />
          <ContentListSubproduk title="nama-subproduk" />
          {/* <ContentItemProduk title="nama-produk">
            <AddButton>Tambah Produk Baru</AddButton>
          </ContentItemProduk>
          <ContentItemSubproduk title="nama-subproduk">
            <AddButton>Tambah Subproduk Baru</AddButton>
          </ContentItemSubproduk> */}
          {content === "harga" ? <PengaturanHarga /> : null}
        </div>
      </div>
    </div>
  );
}
const AddButton = ({ children }) => <Button icon={<FiPlus />}>{children}</Button>;
const PengaturanHarga = () => {
  return (
    <div className="content-container harga">
      <div className="body">
        <div className="title">Harga</div>
      </div>
    </div>
  );
};
