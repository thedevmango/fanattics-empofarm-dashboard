/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FiPlus } from "react-icons/fi";
import { BsThreeDotsVertical } from "react-icons/bs";
import { UncontrolledPopover } from "reactstrap";

import {
  PENGATURAN_PRODUK_SET_PRODUK,
  PENGATURAN_PRODUK_TOGGLE_INPUT,
  PENGATURAN_PRODUK_CHANGE_PRODUK,
} from "../../../redux/types/pengaturanProduk";
import Button from "../../../components/General/Button";

export default function ContentItemProduk({ title }) {
  const dispatch = useDispatch();
  const selectedKategori = useSelector((state) => state.pengaturanProduk.selectedKategori);
  const selectedProduk = useSelector((state) => state.pengaturanProduk.selectedProduk);
  const dataProduk = useSelector((state) => state.pengaturanProduk.dataProduk);
  const isAddingProduk = useSelector((state) => state.pengaturanProduk.isAddingProduk);
  const inputProduk = useSelector((state) => state.pengaturanProduk.inputProduk);

  const [data, setData] = useState([]);

  function toggleInput() {
    dispatch({ type: PENGATURAN_PRODUK_TOGGLE_INPUT, payload: "isAddingProduk" });
  }
  function toggleInputString(e) {
    dispatch({ type: PENGATURAN_PRODUK_CHANGE_PRODUK, payload: e.target.value });
  }
  function onSubmitInput(e) {
    let payload = {
      id: 4,
      kategoriId: selectedKategori.id,
      name: inputProduk,
    };
    if (e.key === "Enter") {
      dispatch({ type: "PENGATURAN_PRODUK_ADD_PRODUK", payload });
    }
  }

  useEffect(() => {
    if (selectedKategori) {
      setData(dataProduk.filter((item) => item.kategoriId === selectedKategori.id));
    } else {
      setData([]);
    }
  }, [selectedKategori, dataProduk]);

  const ItemListComponent = ({ itemList }) => {
    function onClick(index) {
      dispatch({
        type: PENGATURAN_PRODUK_SET_PRODUK,
        payload: dataProduk.filter((val) => val.id === index)[0],
      });
    }

    console.log(dataProduk.length);
    return itemList.map((val, index) => (
      <div key={index} className="item-list-component">
        <Button
          active={selectedProduk ? val.id === selectedProduk.id : false}
          onClick={() => onClick(val.id)}
          className={`item-${title}`}>
          {val.name}
        </Button>
        <Button id={`dots-${val.id}`} theme="link" icon={<BsThreeDotsVertical />} />
        <UncontrolledPopover
          hideArrow
          popperClassName="popover3dots"
          trigger="legacy"
          placement="left"
          target={`dots-${val.id}`}>
          <Button onClick={() => null} width={75} theme="link">
            Edit
          </Button>
          <Button onClick={() => null} width={75} theme="link">
            Hapus
          </Button>
        </UncontrolledPopover>
      </div>
    ));
  };

  return (
    <div className={"content-container " + title}>
      <div className="body">
        <div className="title">{title.replace("-", " ")}</div>
        <div className="item-list">
          <ItemListComponent itemList={data} />
        </div>
        {isAddingProduk ? (
          <div className="input-new">
            <input
              onKeyDown={onSubmitInput}
              onChange={toggleInputString}
              type="text"
              placeholder="Tulis Nama Produk"
            />
          </div>
        ) : null}
      </div>

      <div className="footer">
        {selectedKategori ? (
          <Button onClick={toggleInput} icon={<FiPlus />}>
            Tambah Kategori Baru
          </Button>
        ) : null}
      </div>
    </div>
  );
}
