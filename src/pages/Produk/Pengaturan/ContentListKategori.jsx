import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FiPlus } from "react-icons/fi";
import { BsThreeDotsVertical } from "react-icons/bs";
import { UncontrolledPopover } from "reactstrap";

import {
  PENGATURAN_PRODUK_SET_KATEGORI,
  PENGATURAN_PRODUK_TOGGLE_INPUT,
  PENGATURAN_PRODUK_CHANGE_KATEGORI,
} from "../../../redux/types/pengaturanProduk";
import Button from "../../../components/General/Button";

export default function ContentItemKategori({ title }) {
  const dispatch = useDispatch();
  const selectedKategori = useSelector((state) => state.pengaturanProduk.selectedKategori);
  const dataKategori = useSelector((state) => state.pengaturanProduk.dataKategori);
  const isAddingKategori = useSelector((state) => state.pengaturanProduk.isAddingKategori);
  const inputKategori = useSelector((state) => state.pengaturanProduk.inputKategori);

  const [data, setData] = useState([]);

  function toggleInput() {
    dispatch({ type: PENGATURAN_PRODUK_TOGGLE_INPUT, payload: "isAddingKategori" });
  }
  function toggleInputString(e) {
    dispatch({ type: PENGATURAN_PRODUK_CHANGE_KATEGORI, payload: e.target.value });
  }
  function onSubmitInput(e) {
    let payload = {
      id: 4,
      name: inputKategori,
    };
    if (e.key === "Enter") {
      dispatch({ type: "PENGATURAN_PRODUK_ADD_KATEGORI", payload });
    }
  }

  useEffect(() => {
    if (dataKategori) {
      setData(dataKategori);
    }
  }, [dataKategori]);

  const ItemListComponent = ({ itemList }) => {
    function onClick(index) {
      dispatch({
        type: PENGATURAN_PRODUK_SET_KATEGORI,
        payload: dataKategori.filter((val) => val.id === index)[0],
      });
    }

    return itemList.map((val, index) => (
      <div key={index} className="item-list-component">
        <Button
          active={selectedKategori ? val.id === selectedKategori.id : false}
          onClick={() => onClick(val.id)}
          className={`item-${title}`}>
          {val.name}
        </Button>
        <Button id={`dots-${val.id}`} theme="link" icon={<BsThreeDotsVertical />} />
        <UncontrolledPopover
          hideArrow
          popperClassName="popover3dots"
          trigger="legacy"
          placement="left"
          target={`dots-${val.id}`}>
          <Button onClick={() => null} width={75} theme="link">
            Edit
          </Button>
          <Button onClick={() => null} width={75} theme="link">
            Hapus
          </Button>
        </UncontrolledPopover>
      </div>
    ));
  };

  console.log(inputKategori);
  return (
    <div className={"content-container " + title}>
      <div className="body">
        <div className="title">{title.replace("-", " ")}</div>
        <div className="item-list">
          <ItemListComponent itemList={data} />
        </div>
        {isAddingKategori ? (
          <div className="input-new">
            <input
              onChange={toggleInputString}
              onKeyDown={onSubmitInput}
              type="text"
              placeholder="Tulis Nama Kategori"
            />
          </div>
        ) : null}
      </div>

      <div className="footer">
        <Button onClick={toggleInput} icon={<FiPlus />}>
          Tambah Kategori Baru
        </Button>
      </div>
    </div>
  );
}
