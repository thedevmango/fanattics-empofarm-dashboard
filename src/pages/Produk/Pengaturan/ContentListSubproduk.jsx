/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FiPlus } from "react-icons/fi";
import { BsThreeDotsVertical } from "react-icons/bs";
import { UncontrolledPopover } from "reactstrap";

import {
  PENGATURAN_PRODUK_TOGGLE_INPUT,
  PENGATURAN_PRODUK_CHANGE_SUBPRODUK,
} from "../../../redux/types/pengaturanProduk";
import Button from "../../../components/General/Button";

export default function ContentItemSubroduk({ title }) {
  const dispatch = useDispatch();
  const selectedKategori = useSelector((state) => state.pengaturanProduk.selectedKategori);
  const selectedProduk = useSelector((state) => state.pengaturanProduk.selectedProduk);
  const dataSubproduk = useSelector((state) => state.pengaturanProduk.dataSubproduk);
  const isAddingSubproduk = useSelector((state) => state.pengaturanProduk.isAddingSubproduk);
  const inputSubproduk = useSelector((state) => state.pengaturanProduk.inputSubproduk);

  const [data, setData] = useState([]);

  function toggleInput() {
    dispatch({ type: PENGATURAN_PRODUK_TOGGLE_INPUT, payload: "isAddingSubproduk" });
  }
  function toggleInputString(e) {
    dispatch({ type: PENGATURAN_PRODUK_CHANGE_SUBPRODUK, payload: e.target.value });
  }
  function onSubmitInput(e) {
    let payload = {
      id: 7,
      kategoriId: selectedKategori.id,
      produkId: selectedProduk.id,
      name: inputSubproduk,
    };
    if (e.key === "Enter") {
      dispatch({
        type: "PENGATURAN_PRODUK_ADD_SUBPRODUKK",
        payload,
      });
    }
  }

  useEffect(() => {
    if (selectedProduk) {
      setData(dataSubproduk.filter((item) => item.kategoriId === selectedProduk.id));
    } else {
      setData([]);
    }
  }, [selectedProduk, dataSubproduk]);

  const ItemListComponent = ({ itemList }) => {
    return itemList.map((val, index) => (
      <div key={index} className="item-list-component">
        <Button disabled className={`item-${title}`}>
          {val.name}
        </Button>
        <Button id={`dots-${val.id}`} theme="link" icon={<BsThreeDotsVertical />} />
        <UncontrolledPopover
          hideArrow
          popperClassName="popover3dots"
          trigger="legacy"
          placement="left"
          target={`dots-${val.id}`}>
          <Button onClick={() => null} width={75} theme="link">
            Edit
          </Button>
          <Button onClick={() => null} width={75} theme="link">
            Hapus
          </Button>
        </UncontrolledPopover>
      </div>
    ));
  };

  return (
    <div className={"content-container " + title}>
      <div className="body">
        <div className="title">{title.replace("-", " ")}</div>
        <div className="item-list">
          <ItemListComponent itemList={data} />
        </div>
        {isAddingSubproduk ? (
          <div className="input-new">
            <input
              onKeyDown={onSubmitInput}
              onChange={toggleInputString}
              type="text"
              placeholder="Tulis Nama Subproduk"
            />
          </div>
        ) : null}
      </div>

      <div className="footer">
        {selectedProduk ? (
          <Button onClick={toggleInput} icon={<FiPlus />}>
            Tambah Kategori Baru
          </Button>
        ) : null}
      </div>
    </div>
  );
}
