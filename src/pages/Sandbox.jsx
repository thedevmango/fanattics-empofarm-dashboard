/* eslint-disable no-unused-vars */
import React from "react";
import { Panel, PanelTitle, PanelHead, PanelBody } from "../components/Panel";
import {
  BodyText,
  GreetingTitle,
  HeaderText,
  HeroTitle,
  MetadataText,
  PageTitle,
  PaneHeader,
  SubjectTitle,
  XLNumerics,
} from "../components/Typography";
import { Button, ButtonIcon } from "../components/Button";
import { AiOutlineInfoCircle } from "react-icons/ai";
import Card from "../components/Card/Card";
import {Select} from '../components/Select'

// This page/component is used to test components and other things

const TypographySection = () => {
  return (
    <div>
      <h1>Typography</h1>
      <div>
        <BodyText>TESTING 123</BodyText>
      </div>
      <div>
        <GreetingTitle>Greeting Title</GreetingTitle>
      </div>
      <div>
        <HeaderText>Header Text</HeaderText>
      </div>
      <div>
        <HeroTitle>Hero Title</HeroTitle>
      </div>
      <div>
        <MetadataText>Metadata Text</MetadataText>
      </div>
      <div>
        <PageTitle>Page Title</PageTitle>
      </div>
      <div>
        <PaneHeader>Pane Header</PaneHeader>
      </div>
      <div>
        <SubjectTitle>Subject Title</SubjectTitle>
      </div>
      <div>
        <XLNumerics>XL Numerics</XLNumerics>
      </div>
      {/* Bold Typography */}
      <h1>Bold Typography</h1>
      <div>
        <BodyText bold>TESTING 123</BodyText>
      </div>
      <div>
        <GreetingTitle bold>Greeting Title</GreetingTitle>
      </div>
      <div>
        <HeaderText bold>Header Text</HeaderText>
      </div>
      <div>
        <HeroTitle bold>Hero Title</HeroTitle>
      </div>
      <div>
        <MetadataText bold>Metadata Text</MetadataText>
      </div>
      <div>
        <PageTitle bold>Page Title</PageTitle>
      </div>
      <div>
        <PaneHeader bold>Pane Header</PaneHeader>
      </div>
      <div>
        <SubjectTitle bold>Subject Title</SubjectTitle>
      </div>
      <div>
        <XLNumerics bold>XL Numerics</XLNumerics>
      </div>
    </div>
  );
};

export default (props) => {
  return (
    <div className="container">
      <h1>SANDBOX</h1>
      <div className="row">
        <div className="col-12">
          <Card borderColor="primary" className="mb-5">
            Hellooo
          </Card>
          <Panel>
            <PanelHead bordered>
              <PanelTitle bold size="medium">
                WADIDAWA
              </PanelTitle>
            </PanelHead>
            <PanelBody>
              <div>TESTING 123</div>
            </PanelBody>
          </Panel>
          {/* <TypographySection /> */}
          <Select>
            <option>transactional activity</option>
            <option>List</option>
          </Select>
          <Button  >Testing 123</Button>
          <Button theme="primary">Primary</Button>
          <Button theme="secondary">Secondary</Button>
          <Button theme="text" disabled>
            Text
          </Button>

          <Button theme="primary">
            Primary Icon
            <ButtonIcon type="append">
              <AiOutlineInfoCircle />
            </ButtonIcon>
          </Button>

          <Button split theme="primary">
            Primary Icon
            <ButtonIcon type="split">
              <AiOutlineInfoCircle />
            </ButtonIcon>
          </Button>
        </div>
      </div>
    </div>
  );
};
