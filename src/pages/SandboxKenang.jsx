/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { useDropzone } from "react-dropzone";
import "../assets/styles/sandbox.scss";
import "../../node_modules/bootstrap/scss/bootstrap.scss";
import { Panel, PanelHead } from "../components/Panel";
import {
  BodyText,
  SubjectTitle,
  HeaderText,
  MetadataText,
  PaneHeader,
} from "../components/Typography";
import moment from "moment";
import { Button } from "../components/Button";
import { Link } from "react-router-dom";
import dummyImg from "../assets/images/jagung-super.png";
import { formatquantity } from "../configs/formatnumber";
import {
  DataUser,
  DataTender,
  RincianTender,
  RincianTenderItem,
  DataTenderItem,
  UpdateStatusTrx,
  ReportQC,
  ReportQCItem,
  RincianPenjualan,
} from "../components/Molecules/PanelDetail";
import { gray300, gray500, gray800 } from "../constants/Colors";
import { GoVerified } from "react-icons/go";
import Collapse from "../components/Collapse";
import { BsChevronDown, BsChevronRight } from "react-icons/bs";
import { ResponsiveSankey } from "@nivo/sankey";
import { Card } from "../components/Card";
import { FiChevronDown, FiChevronRight } from "react-icons/fi";

// This page/component is used to test components and other things

export default (props) => {
  return (
    <div className="sandbox-ken">
      <Helmet>
        <title>Sandbox Kenang</title>
      </Helmet>

      <div className="row">
        <div className="col-8"></div>
      </div>

      <div className="col-8" style={{ height: 400 }}>
        {/* <ResponsiveSankey
            data={dummySankey}
            margin={{ top: 40, right: 160, bottom: 40, left: 50 }}
            align="justify"
            colors={["#3296ED", "#116985", "#27AAB0", "#51D2BB", "#94E7A8"]}
            nodeOpacity={1}
            nodeThickness={18}
            nodeInnerPadding={3}
            nodeSpacing={24}
            nodeBorderWidth={0}
            nodeBorderColor={{ from: "color", modifiers: [["darker", 0.8]] }}
            linkOpacity={0.5}
            linkHoverOthersOpacity={0.1}
            enableLinkGradient={true}
            labelPosition="outside"
            labelOrientation="vertical"
            labelPadding={16}
            labelTextColor={{ from: "color", modifiers: [["darker", 1]] }}
            animate={true}
            motionStiffness={140}
            motionDamping={13}
            legends={[
              {
                anchor: "bottom-right",
                direction: "column",
                translateX: 130,
                itemWidth: 100,
                itemHeight: 14,
                itemDirection: "right-to-left",
                itemsSpacing: 2,
                itemTextColor: "#999",
                symbolSize: 14,
                effects: [
                  {
                    on: "hover",
                    style: {
                      itemTextColor: "#000",
                    },
                  },
                ],
              },
            ]}
          /> */}
      </div>
    </div>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const createNode = (title, color) => ({ id: title, color });
const createLinks = (source, target, value) => ({ source, target, value });
const dummyNodes = [
  "Hard",
  "Soft",
  "Liquid",
  "Equipment",
  "Other",
  "Area A",
  "Area B",
  "RDC 1",
  "RDC 2",
  "RDC 3",
  "RDC 4",
  "RDC 5",
];

const dummySankey = {
  nodes: [
    createNode("Hard", "#3296ED"),
    createNode("Soft", "#3296ED"),
    createNode("Liquid", "#3296ED"),
    createNode("Equipment", "#3296ED"),
    createNode("Other", "#3296ED"),

    createNode("RDC 1", "#C7F296"),
    createNode("RDC 2", "#C7F296"),
    createNode("RDC 3", "#C7F296"),
    createNode("RDC 4", "#C7F296"),
    createNode("RDC 5", "#C7F296"),
  ],
  links: [
    createLinks("Liquid", "RDC 1", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Liquid", "RDC 2", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Liquid", "RDC 3", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Liquid", "RDC 4", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Liquid", "RDC 5", randomizeData([750, 600, 575, 800, 675])),

    createLinks("Soft", "RDC 1", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Soft", "RDC 2", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Soft", "RDC 3", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Soft", "RDC 4", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Soft", "RDC 5", randomizeData([750, 600, 575, 800, 675])),

    createLinks("Hard", "RDC 1", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Hard", "RDC 2", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Hard", "RDC 3", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Hard", "RDC 4", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Hard", "RDC 5", randomizeData([750, 600, 575, 800, 675])),

    createLinks("Equipment", "RDC 1", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Equipment", "RDC 3", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Equipment", "RDC 4", randomizeData([750, 600, 575, 800, 675])),

    createLinks("Other", "RDC 3", randomizeData([750, 600, 575, 800, 675])),
    createLinks("Other", "RDC 4", randomizeData([750, 600, 575, 800, 675])),
  ],
};

/**



      //* UpdateStatusCard
      <div className="d-flex flex-column col-4 p-0">
        <UpdateStatusTrx timestamp="10/25/2020 15:20" link="/sandbox-ken">
          Menunggu pembayaran oleh Buyer
        </UpdateStatusTrx>
        <UpdateStatusTrx className="mt-3" timestamp="10/25/2020 15:20" link="/sandbox-ken">
          Dibayar oleh Buyer
        </UpdateStatusTrx>
        <UpdateStatusTrx className="mt-3" timestamp="10/25/2020 15:20" link="/sandbox-ken">
          Menunggu approval oleh Finance
        </UpdateStatusTrx>
      </div>
 */
