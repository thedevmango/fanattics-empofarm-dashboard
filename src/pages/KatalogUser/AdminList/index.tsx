/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { BsChevronDown, BsCircleFill } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContextMenu,
  Form,
  Input,
  MetadataText,
  PageTitle,
  Pagination,
  Panel,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  useContextMenu,
  usePagination,
} from "../../../components";
import { AdminRoles } from "../../../constants";
import { getAdminList } from "../../../redux/actions/admin";
import { ADMIN_TAB } from "../../../redux/types";
import PopupAddAdmin from "./PopupAddAdmin";

//TODO: ENABLE DEPARTMENT
//TODO: ENABLE FILTER BY TIPE AKUN
//TODO: INTEGRATE POST & UPDATE ADMIN

export default (props: any) => {
  const dispatch = useDispatch();
  const { roleAdmin, roleStaff } = AdminRoles;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.admin.tab);
  const data = useSelector((state: any) => state.admin.adminList);
  const dataAll = useSelector((state: any) => state.admin.adminListAll);

  //* LOCAL STATE
  const [filteredData, setFilteredData] = useState<any>([]);
  const [accountType, setAccountType] = useState("");
  const [searchQuery, setSearchQuery] = useState("");

  const [selectedRow, setSelectedRow] = useState<any>(null);
  const [selectedData, setSelectedData] = useState<any>(undefined);
  const [openPopup, setOpenPopup] = useState(false);

  //* FUNCTION
  const { xPos, yPos, showMenu, contextData } = useContextMenu(selectedRow);

  const toggleAccount = (e: any) => setAccountType(e.target.value);
  const toggleTab = (selectedTab: string[]) => dispatch({ type: ADMIN_TAB, payload: selectedTab });
  const toggleSelectedRow = (val: any) => setSelectedRow(val);
  const togglePopup = (val?: any) => {
    if (openPopup) {
      setOpenPopup(false);
      setSelectedData(undefined);
    } else {
      setOpenPopup(true);
      setSelectedData(val);
    }
  };

  const handleSubmit = (data: any) => {
    console.log(data);
    return null;
  };

  //* FETCH DATA
  useEffect(() => {
    if (searchQuery) {
      const results = dataAll.filter((item: any) => {
        return item.fullName.toLowerCase().includes(searchQuery.toLowerCase());
      });
      return setFilteredData(results);
    } else {
      return setFilteredData(data);
    }
  }, [searchQuery, data]);

  useEffect(() => {
    dispatch(getAdminList(tab));
  }, [tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  if (!data)
    return (
      <div className="katalog-user katalog-user-admin-list">
        <Helmet>
          <title>loading...</title>
        </Helmet>
      </div>
    );
  return (
    <div className="katalog-user katalog-user-admin-list">
      <Helmet>
        <title>Admin | Katalog User</title>
      </Helmet>
      <PageTitle bold>Daftar Admin</PageTitle>

      <ContextMenu xPos={xPos} yPos={yPos} showMenu={showMenu}>
        <div className="d-flex flex-column">
          <Button theme="text" onClick={() => togglePopup(contextData)}>
            <MetadataText>Edit Admin</MetadataText>
          </Button>
          <Button theme="text">
            <MetadataText>Delete Admin</MetadataText>
          </Button>
        </div>
      </ContextMenu>

      <PopupAddAdmin
        data={selectedData}
        isOpen={openPopup}
        onCancel={togglePopup}
        onSubmit={handleSubmit}
      />

      <div className="d-flex mb-4 align-items-end justify-content-between">
        <Tabs
          options={["Web Admin", "Staff"]}
          slug={[roleAdmin, roleStaff]}
          activeTab={tab}
          setActiveTab={toggleTab}
        />

        <div className="d-flex col-4 align-items-end">
          <Searchbar
            className="col-6 mr-3"
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
            placeholder="Cari nama admin"
          />
          <Form className="col-6 pr-0">
            <Input
              containerStyle={{ marginBottom: 0 }}
              label="Tipe Akun"
              type="select"
              onChange={toggleAccount}
              value={accountType}
            >
              <option value="">Semua Tipe Akun</option>
              {["inventory", "logistic", "iqc", "iqc-manager", "oqc", "oqc-manager"].map(
                (item, idx) => (
                  <option key={idx} value={item}>
                    {item}
                  </option>
                )
              )}
            </Input>
          </Form>
        </div>
      </div>

      <Panel className="content">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Nama</TableHeadCell>
              <TableHeadCell>User ID</TableHeadCell>
              <TableHeadCell>Tipe Akun</TableHeadCell>
              <TableHeadCell>Access Rights</TableHeadCell>
              <TableHeadCell>Status</TableHeadCell>
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable onMouseOver={toggleSelectedRow} data={currentData()} />
          </TableBody>
        </Table>

        <div className="row pagination-wrapper pt-4">
          <div className="col-2"></div>
          <div className="col-8">
            <Pagination
              next={next}
              prev={prev}
              jump={jump}
              pages={pages}
              currentPage={currentPage}
              maxPage={maxPage}
            />
          </div>
          <div className="col-2 d-flex justify-content-end">
            <Button onClick={togglePopup} theme="primary">
              Tambah Member
            </Button>
          </div>
        </div>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, onMouseOver }) => {
  return !data
    ? null
    : data.map((item: any, i: number) => (
        <TableRow key={i} onMouseOver={() => onMouseOver(item)}>
          <TableCell>
            <b>{item.fullName}</b>
          </TableCell>
          <TableCell>{item.userId || "-"}</TableCell>
          <TableCell>{item.role || "-"}</TableCell>
          <TableCell>{item.accessRights || "-"}</TableCell>
          <TableCell>
            <BsCircleFill color="#5EBB72" size={8} className="mr-2" />
            Active
          </TableCell>
          <TableCell style={{ width: 50 }}>
            <Button theme="action-table">
              <BsChevronDown size={12} />
            </Button>
          </TableCell>
        </TableRow>
      ));
};
