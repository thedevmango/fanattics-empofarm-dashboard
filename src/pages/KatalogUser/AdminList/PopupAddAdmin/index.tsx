import React, { useEffect, useReducer } from "react";
import { VscKey } from "react-icons/vsc";
import { ModalBody } from "reactstrap";
import { Button, Form, HeaderText, Input, Modal, SubjectTitle } from "../../../../components";

interface Props {
  data?: any;
  isOpen: boolean;
  onCancel?: any;
  onSubmit?: any;
}

export default ({ data, isOpen, onCancel, onSubmit }: Props) => {
  const init_state = {
    fullName: "",
    email: "",
    role: "",
    password: "",
    password2: "",
  };

  const reducer = (state: any = init_state, { type, payload }: { type: string; payload?: any }) => {
    switch (type) {
      case "change":
        return { ...state, [payload.name]: payload.value };
      case "set_default":
        return { ...state, ...payload };
      case "reset":
        return init_state;
      default:
        return state;
    }
  };
  const [state, dispatch] = useReducer(reducer, init_state);
  const { fullName, email, role, password, password2 } = state;

  const handleChange = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    dispatch({ type: "change", payload: { name: name, value: value } });
  };

  const handleSubmit = () => {
    if (!data && (!password || !password2 || password !== password2))
      return alert("password doesn't match");
    const username = fullName.split(" ").join("_").toLowerCase();
    onSubmit({ ...state, username: username });
  };

  const handleCancel = () => {
    dispatch({ type: "reset" });
    onCancel();
  };

  useEffect(() => {
    if (!data) return;
    dispatch({ type: "set_default", payload: data });
  }, [data]);

  return (
    <Modal size="lg" isOpen={isOpen} toggle={handleCancel}>
      <ModalBody className="popup-add-new-admin">
        <div className="popup-header">
          <HeaderText bold>Pembuatan Admin Baru</HeaderText>
        </div>

        <PopupContent title="Informasi Akun">
          <Form className="input-new-admin row flex-column">
            <div className="col-6">
              <Input
                label="Nama Admin"
                name="fullName"
                value={fullName}
                onChange={handleChange}
                placeholder="Masukkan Nama Admin"
              />
            </div>
            <div className="col-6">
              <Input
                label="Email"
                name="email"
                value={email}
                onChange={handleChange}
                placeholder="Masukkan Email Admin"
              />
            </div>
          </Form>

          <Form className="input-new-admin row">
            <div className="col-6">
              <Input label="Area" type="select" disabled>
                <option value="">Pilih Region</option>
              </Input>
            </div>
            <div className="col-6">
              <Input label="Distribution Center" type="select" disabled>
                <option>Lokasi Distribution Center</option>
              </Input>
            </div>
          </Form>
        </PopupContent>

        <PopupContent title="Role Akun">
          <Form className="input-new-admin row">
            <div className="col-6">
              <Input label="Departemen" type="select" disabled>
                <option value="">Pilih Departemen</option>
              </Input>
            </div>
            <div className="col-6">
              <Input
                label="Jabatan"
                defaultValue={role}
                type="select"
                name="role"
                onChange={handleChange}
              >
                <option value="">Pilih Jabatan</option>
                <option value="super">Super Admin</option>
                <option value="inventory">Inventory Manager Admin</option>
                <option value="logistic">Logistic Admin</option>
                <option value="iqc-manager">Inbound Control Manager</option>
                <option value="iqc">Inbound Control Staff</option>
                <option value="oqc-manager">Outbound Control Manager</option>
                <option value="oqc">Outbound Control Staff</option>
              </Input>
            </div>
          </Form>
        </PopupContent>

        <PopupContent title="Pasword">
          <Form className="input-new-admin row">
            <div className="col-6">
              <Input
                prepend={<VscKey />}
                name="password"
                value={password}
                onChange={handleChange}
                label="Password"
                placeholder="Password"
              />
            </div>
            <div className="col-6">
              <Input
                prepend={<VscKey />}
                name="password2"
                value={password2 ? password2 : password}
                onChange={handleChange}
                label="Ulangi Password"
                placeholder="Ulangi Password"
              />
            </div>
          </Form>
        </PopupContent>

        <div className="button-wrap d-flex">
          <Button onClick={handleCancel} theme="secondary" containerStyle={{ marginLeft: "auto" }}>
            BATALKAN
          </Button>
          <Button onClick={handleSubmit} theme="primary" containerStyle={{ marginLeft: 8 }}>
            SIMPAN
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

const PopupContent = ({ children, title }) => {
  return (
    <div className="popup-content">
      <SubjectTitle className="mb-1" bold>
        {title}
      </SubjectTitle>

      {children}
    </div>
  );
};
