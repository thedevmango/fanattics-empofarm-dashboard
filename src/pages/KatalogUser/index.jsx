/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";

import AdminList from "./AdminList";
import UserCatalog from "./UserCatalog";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/admin-list`} component={AdminList} />
      <Route path={`${props.match.path}/user-catalog`} component={UserCatalog} />
    </>
  );
};
