/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Pagination, usePagination } from "../../../components/Pagination";
import { PageTitle } from "../../../components/Typography";
import { Form, Input } from "../../../components/Form";
import Searchbar from "../../../components/Searchbar";
import { Panel } from "../../../components/Panel";
import Tabs from "../../../components/Tabs";
import ViewList from "./ViewList";
import ViewGrid from "./ViewGrid";

export default (props) => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isActive, setIsActive] = useState("Semua Member");
  const [accountType, setAccountType] = useState("");
  const [viewOption, setViewOption] = useState("list");
  const [searchQuery, setSearchQuery] = useState("");

  const filterData = (status) => dummyData.filter((val) => val.accountStatus === status);
  const filterType = (type) => data.filter((val) => val.accountType === type);
  const toggleAccount = (e) => setAccountType(e.target.value);
  const toggleViewOption = (e) => setViewOption(e.target.value);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  useEffect(() => {
    if (isActive === "Semua Member") {
      setData(dummyData);
      setFilteredData(dummyData);
    }
    if (isActive === "Member Terverifikasi") {
      setData(filterData("verified"));
      setFilteredData(filterData("verified"));
    }
    if (isActive === "Menunggu Approval") {
      setData(filterData("unverified"));
      setFilteredData(filterData("unverified"));
    }
    setPage(1);
    setAccountType("");
  }, [isActive]);

  useEffect(() => {
    if (accountType === "Farmer") {
      setFilteredData(filterType("Farmer"));
    } else if (accountType === "Buyer") {
      setFilteredData(filterType("Buyer"));
    } else if (accountType === "Seller") {
      setFilteredData(filterType("Seller"));
    } else {
      if (data.length) setFilteredData(data);
    }
    setPage(1);
  }, [accountType]);

  return (
    <div className="katalog-user katalog-user-user-catalog">
      <Helmet>
        <title>Katalog Member Pihak Ketiga | Katalog User</title>
      </Helmet>

      <PageTitle bold>Katalog Member Pihak Ketiga</PageTitle>

      <div className="d-flex mb-4 align-items-end justify-content-between">
        <Tabs
          className="mr-auto"
          options={["Semua Member", "Member Terverifikasi", "Menunggu Approval"]}
          activeTab={isActive}
          setActiveTab={setIsActive}
        />

        <div className="d-flex col-6 pr-0 align-items-end justify-content-end">
          <Form className="col-5">
            <Searchbar
              searchQuery={searchQuery}
              setSearchQuery={setSearchQuery}
              placeholder="Cari nama pembeli"
            />
          </Form>

          <Form className="col-4">
            <Input
              containerStyle={{ marginBottom: 0 }}
              label="Tipe Akun"
              onChange={toggleAccount}
              value={accountType}
              type="select"
            >
              <option value="">Semua Tipe Akun</option>
              {dummyType.map((item, idx) => (
                <option key={idx} value={item}>
                  {item}
                </option>
              ))}
            </Input>
          </Form>

          <Form className="col-2 pr-0">
            <Input
              containerStyle={{ marginBottom: 0 }}
              label="View Option"
              onChange={toggleViewOption}
              value={viewOption}
              type="select"
            >
              <option value="list">List</option>
              <option value="grid">Grid</option>
            </Input>
          </Form>
        </div>
      </div>

      {viewOption === "list" ? (
        <Panel className="content">
          <ViewList usePagination={usePagination} data={currentData()}>
            <Pagination
              next={next}
              prev={prev}
              jump={jump}
              pages={pages}
              currentPage={currentPage}
              maxPage={maxPage}
            />
          </ViewList>
        </Panel>
      ) : (
        <div className="grid-view-catalog">
          <ViewGrid data={filteredData} />
        </div>
      )}
    </div>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const dummyName = [
  "Joko Tole",
  "Bessie Cooper",
  "Cameron Williamson",
  "Kristin Watson",
  "Cody Fisher",
  "Marvin McKinney",
];
const dummyType = ["Farmer", "Buyer", "Seller"];
const dummyBuyerType = ["Individu", "Perusahaan"];
const dummyCompany = ["fanAttics", "Fresh Green", "Total Fresh"];
const dummyImage = [
  require("../../../assets/images/profpict.jpg"),
  require("../../../assets/images/person1.jpg"),
  require("../../../assets/images/person2.jpg"),
  require("../../../assets/images/person3.jpg"),
  require("../../../assets/images/person4.jpg"),
  require("../../../assets/images/person5.jpg"),
  require("../../../assets/images/person6.jpg"),
  require("../../../assets/images/person7.jpg"),
  require("../../../assets/images/person8.jpg"),
];
const dummyAddress = [
  [
    "Jalan Wolter Mongonsidi No. 82 Petogogan",
    "Kebayoran Baru",
    "Jakarta Selatan",
    "DKI Jakarta",
    "Indonesia",
  ],
  [
    "Jalan Raya Narogong No. 43 Atrium Cileungsi Blok R-9",
    "Cileungsi",
    "Bogor",
    "Jawa Barat",
    "Indonesia",
  ],
  ["Jalan Raya Abadi No. 368", "", "Manado", "Sulawesi", "Indonesia"],
];

const dummyData = [...Array(33)].map((_, idx) => {
  return {
    id: idx + 1,
    fullname: randomizeData(dummyName),
    email: `${randomizeData(dummyName).toLowerCase().replace(" ", "")}@mail.com`,
    userId: `IDJKT${Math.floor(10000 + Math.random() * 90000)}`,
    accountType: randomizeData(dummyType),
    buyerType: randomizeData(dummyBuyerType),
    accountStatus: randomizeData(["verified", "verified", "verified", "unverified"]),
    company: randomizeData(dummyCompany),
    phoneNumber: `0892${Math.floor(10000000 + Math.random() * 90000000)}`,
    image: randomizeData(dummyImage),
    street: randomizeData(dummyAddress)[0],
    city: randomizeData(dummyAddress)[1],
    province: randomizeData(dummyAddress)[2],
    state: randomizeData(dummyAddress)[3],
  };
});
