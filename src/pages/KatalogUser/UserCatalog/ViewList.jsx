import React from "react";
import {
  Table,
  TableHead,
  TableBody,
  TableFooter,
  TableRow,
  TableHeadCell,
  TableCell,
} from "../../../components/Table";
import { Button } from "../../../components/Button";
import { BsChevronRight } from "react-icons/bs";

export default ({ children, data }) => {
  return (
    <Table bordered>
      <TableHead>
        <TableRow>
          <TableHeadCell>Nama</TableHeadCell>
          <TableHeadCell>User ID</TableHeadCell>
          <TableHeadCell>Tipe Akun</TableHeadCell>
          <TableHeadCell>Nama Perusahaan/Farm</TableHeadCell>
          <TableHeadCell>Nomor Telepon</TableHeadCell>
          <TableHeadCell>&nbsp;</TableHeadCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <RenderBodyTable data={data} />
      </TableBody>
      <TableFooter>
        <TableRow>
          <TableCell colSpan={10}>{children}</TableCell>
        </TableRow>
      </TableFooter>
    </Table>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <TableCell colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </TableCell>
    </TableRow>
  );
};

const RenderBodyTable = ({ data }) => {
  if (!data) return <EmptyDataRow colSpan={7} />;
  return data.map((item, idx) => (
    <TableRow key={idx}>
      <TableCell>{item.fullname}</TableCell>
      <TableCell>{item.userId}</TableCell>

      {item.accountType === "Buyer" ? (
        <TableCell>{`${item.accountType} ${item.buyerType}`}</TableCell>
      ) : (
        <TableCell>{item.accountType}</TableCell>
      )}

      <TableCell>{item.company}</TableCell>
      <TableCell>{item.phoneNumber}</TableCell>
      <TableCell style={{ width: 50, borderLeft: "1px solid #E1E1E1" }}>
        <Button theme="action-table">
          <BsChevronRight size={12} />
        </Button>
      </TableCell>
    </TableRow>
  ));
};
