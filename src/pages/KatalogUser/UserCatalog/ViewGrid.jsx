/* eslint-disable no-unused-vars */
import React from "react";
import Img from "react-cool-img";
import LazyLoad from "react-lazyload";
import { Card } from "../../../components/Card";
import { HeaderText, SubjectTitle } from "../../../components/Typography";

export default ({ data }) => {
  return data.map((item, idx) => (
    <Card key={idx} elevationDepth="4" className="member-card">
      <div className="member-img-wrapper">
        <Img
          style={{
            backgroundColor: "grey",
            width: "100%",
            height: 200,
            objectFit: "cover",
            borderRadius: 8,
            alignSelf: "center",
          }}
          src={item.image}
          alt="member"
          className="member-img"
        />
      </div>
      <div className="info">
        <div className="info-fullname">
          <HeaderText bold color="#323130">
            {item.fullname}
          </HeaderText>
        </div>
        <div className="info-type">
          <SubjectTitle color="#605E5C">{item.accountType}</SubjectTitle>
        </div>
        <div className="info-location">
          <SubjectTitle color="#605E5C">{`${item.province}, ${item.state}`}</SubjectTitle>
        </div>
      </div>
    </Card>
  ));
};
