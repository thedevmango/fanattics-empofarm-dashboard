import React, { useState } from "react";
import { Button } from "../../../components/Button";
import "../../../../node_modules/bootstrap/scss/bootstrap.scss";
import { Modal, ModalBody } from "../../../components/Modal";
import { GreetingTitle, HeaderText } from "../../../components/Typography";
import { BsFiles } from "react-icons/bs";

export default (props) => {
  const [isOpen, setIsOpen] = useState(true);
  const toggleModal = () => setIsOpen(!isOpen);

  return (
    <div>
      <Modal toggle={toggleModal} isOpen={isOpen}>
        <ModalBody>
          <GreetingTitle className="w-100 text-center" bold color="#69984C">
            File Terupload!
          </GreetingTitle>
          <div className="justify-content-center d-flex mt-5">
            <BsFiles color="#69984C" size={100} />
          </div>

          <div className="d-flex justify-content-center mt-3">
            <HeaderText color="#6E6E6E">fakturAttics.csv</HeaderText>
          </div>

          <div className="d-flex justify-content-center mt-5 mb-2">
            <Button
              theme="secondary"
              className="mr-2"
              color="#69984C"
              containerStyle={{ borderRadius: "4px" }}
            >
              Hapus File
            </Button>
            <Button theme="primary" containerStyle={{ borderRadius: "4px" }}>
              Simpan
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
};
