import React, { useState } from "react";
import { Button } from "../../../components/Button";
import "../../../../node_modules/bootstrap/scss/bootstrap.scss";
import { Modal, ModalBody } from "../../../components/Modal";
import { PageTitle, BodyText } from "../../../components/Typography";

export default (props) => {
  const [isOpen, setIsOpen] = useState(true);
  const toggleModal = () => setIsOpen(!isOpen);

  return (
    <div>
      <Modal toggle={toggleModal} isOpen={isOpen}>
        <ModalBody>
          <BodyText className="w-100 text-center" color="#6E6E6E">
            Upload faktur penjualan untuk melanjutkan proses
          </BodyText>

          <PageTitle className="w-100 text-center mt-3" color="#292929">
            Drag and Drop Files <br /> or{" "}
          </PageTitle>

          <div className="justify-content-center d-flex mt-2">
            <Button
              className="w-75 mt-2 py-3 text-center"
              theme="primary"
              containerStyle={{ borderRadius: "8px" }}
            >
              <PageTitle color="#fff">Browse</PageTitle>
            </Button>
          </div>

          <div className="d-flex justify-content-center mt-3">
            <BodyText color="#ACACAC" bold>
              PDF, CSV, Excel Document, etc
            </BodyText>
          </div>

          <div className="d-flex justify-content-center mt-5 mb-2">
            <Button
              theme="secondary"
              className="mr-2"
              color="#69984C"
              containerStyle={{ borderRadius: "4px" }}
            >
              Batalkan
            </Button>
            <Button theme="primary" containerStyle={{ borderRadius: "4px" }}>
              Proses Pembayaran
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
};
