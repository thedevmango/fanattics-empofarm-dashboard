import React, { useState } from "react";
import { Button } from "../../../components/Button";
import "../../../../node_modules/bootstrap/scss/bootstrap.scss";
import { Modal, ModalBody } from "../../../components/Modal";
import { PaneHeader, MetadataText } from "../../../components/Typography";
import { FiTruck, FiShoppingBag } from "react-icons/fi";

export default (props) => {
  const [isOpen, setIsOpen] = useState(true);
  const toggleModal = () => setIsOpen(!isOpen);

  return (
    <div>
      <Modal toggle={toggleModal} isOpen={isOpen} size="lg">
        <ModalBody>
          <PaneHeader
            className="d-flex justify-content-center"
            color="#000"
            bold
          >
            Pilih Metode Pengambilan Barang
          </PaneHeader>
          <div className="d-flex justify-content-center mt-5 mb-5">
            <Button
              theme="secondary"
              className="mr-5 flex-column py-4 w-50"
              color="#A19F9D"
              containerStyle={{ borderRadius: "8px" }}
            >
              <FiTruck
                size={80}
                style={{ strokeWidth: "1px", marginBottom: "10px" }}
              />
              <MetadataText bold color="#000">
                Ambil Barang Dengan Kurir Empofarm
              </MetadataText>
            </Button>

            <Button
              theme="secondary"
              className="flex-column w-50"
              color="#A19F9D"
              containerStyle={{ borderRadius: "8px" }}
            >
              <FiShoppingBag
                size={80}
                style={{ marginBottom: "10px", strokeWidth: "1px" }}
              />
              <MetadataText bold color="#000">
                Farmer Mengantar Barang Secara Independen
              </MetadataText>
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
};
