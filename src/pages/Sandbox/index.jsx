/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import ModalUpload from "./Modal/modalUpload";
import ModalFinish from "./Modal/modalFinish";
import ModalShipment from "./Modal/modalShipment";

export default (props) => {
  return (
    <div>
      <ModalUpload />
      <ModalShipment />
      <ModalFinish />
    </div>
  );
};
