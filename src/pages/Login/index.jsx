/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Redirect } from "react-router-dom";
import { handleLogin, handleForgotPassword } from "../../redux/actions/auth";
import { AUTH_MODAL, AUTH_ONCHANGE } from "../../redux/types/auth";
import { Spinner } from "reactstrap";

import Illustration from "../../assets/images/bg-login.png";
import LogoEmpofarm from "../../assets/images/logo-empofarm-light.png";
import { Button } from "../../components/Button";
import { Form, Input } from "../../components/Form";
import { Modal, ModalBody } from "../../components/Modal";
import ModalForgetPassword from "./Modal/ForgotPasswordModal";
import { SIDEBAR_SELECT_CHILD } from "../../redux/types/sidebar";

export default (props) => {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const loading = useSelector((state) => state.auth.loadingLogin);
  const modalForgot = useSelector((state) => state.auth.modalForgot);
  const emailForgot = useSelector((state) => state.auth.emailForgot);
  const toastForgot = useSelector((state) => state.auth.toastForgot);

  const valueOnChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    dispatch({ type: AUTH_ONCHANGE, payload: { name, value } });
  };
  const toggleForgot = () => {
    dispatch({ type: AUTH_ONCHANGE, payload: { name: "modalForgot", value: !modalForgot } });
  };
  const openForgotmodal = () => dispatch({ type: AUTH_MODAL, payload: true });
  const submitForgot = () => dispatch(handleForgotPassword());
  const submitLogin = () => {
    const payload = {
      id: 1,
      profilePicture: require("../../assets/images/profpict.jpg"),
      name: "Syafruddin",
      role: "Admin",
      email: "syafruddin@mail.com",
    };
    dispatch(handleLogin({ payload }));
  };

  if (auth.login) {
    let selectedMenu = "inventory";
    let menu = "commodity/catalog";
    return <Redirect to={`/${selectedMenu}/${menu}`} />;
  }
  return (
    <div className="login-page">
      <ModalForgetPassword
        isOpen={modalForgot}
        toggle={toggleForgot}
        onValueChange={valueOnChange}
        onSubmit={submitForgot}
      />
      <Modal backdrop={false} isOpen={toastForgot}>
        <ModalBody>
          <center>
            Email untuk restart password berhasil dikirim ke Email untuk restart password berhasil
            dikirim ke <b>{emailForgot}</b>
          </center>
        </ModalBody>
      </Modal>

      <div className="image-wrapper">
        <img className="img-illustration" src={Illustration} alt="empofarm-bg" />
      </div>
      <div className="content-wrapper">
        <div className="logo-wrapper">
          <img className="img-logo" src={LogoEmpofarm} alt="empofarm-logo" />
        </div>

        <div className="subtitle">Selamat Datang di Empofarm Dashboard</div>
        <div className="title">Login Ke Akun Anda</div>

        <div className="form-wrapper">
          <Form>
            <Input
              label="Email"
              onChange={valueOnChange}
              name="email"
              placeholder="Masukkan Email"
              type="text"
            />

            <Input
              label="Password"
              onChange={valueOnChange}
              name="password"
              placeholder="Masukkan Password"
              type="password"
            />
          </Form>

          <div className="row-info-login d-flex flex-row justify-content-between">
            <div className="form-check">
              <input className="form-check-input" type="checkbox" value="" id="ingat-login" />
              <label className="form-check-label" htmlFor="ingat-login">
                Ingat Saya
              </label>
            </div>

            <a href="#" onClick={openForgotmodal}>
              Forgot Password?
            </a>
          </div>

          <Button
            bold
            block
            width="100%"
            theme="primary"
            onClick={submitLogin}
            containerStyle={{ height: 50, width: "100%", borderRadius: 5 }}
          >
            {loading ? <Spinner size="sm" /> : "Login Sekarang"}
          </Button>
        </div>
      </div>
    </div>
  );
};
