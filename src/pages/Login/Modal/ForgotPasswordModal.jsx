/* eslint-disable no-unused-vars */
import React from "react";
import { Spinner } from "reactstrap";
import { useSelector } from "react-redux";
import { Modal, ModalHeader, ModalBody } from "../../../components/Modal";
import { Form, Input } from "../../../components/Form";
import { Button } from "../../../components/Button";

export default ({ isOpen, toggle, onSubmit, onValueChange }) => {
  const loading = useSelector((state) => state.auth.loadingForgot);

  return (
    <Modal className="forget-password-modal" toggle={toggle} isOpen={isOpen}>
      <ModalHeader>Forgot Password</ModalHeader>
      <ModalBody className="forget-password-body">
        <div className="subtitle">
          Harap Masukan Alamat Email Untuk Dapat Melakukan Reset Password
        </div>

        <Form className="forget-password-form">
          <Input
            label="Email"
            onChange={onValueChange}
            name="emailForgot"
            placeholder="Masukkan Alamat Email"
            type="text"
          />
        </Form>

        <div className="button-container">
          <Button containerStyle={{ width: 128 }} onClick={toggle} theme="text">
            Batalkan
          </Button>
          <Button containerStyle={{ width: 128 }} onClick={onSubmit} theme="primary">
            {loading ? <Spinner size="sm" /> : "Tambahkan"}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
