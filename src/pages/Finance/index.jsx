/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";

import Dashboard from "./Dashboard";
import Purchasing from "./Activities/Purchasing";
import PurchasingDetails from "./Activities/Purchasing/PurchasingDetails";
import Sales from "./Activities/Sales";
import SalesDetails from "./Activities/Sales/SalesDetails";
import CapitalFlow from "./CapitalFlow";
import Cancellation from "./Cancellation";
import CancellationDetails from "./Cancellation/Details";

export default (props) => {
  return (
    <>
      <Route path={`${props.match.path}/dashboard`} component={Dashboard} />
      <Route path={`${props.match.path}/sales-transaction`} exact component={Sales} />
      <Route path={`${props.match.path}/sales-transaction/details`} component={SalesDetails} />
      <Route path={`${props.match.path}/purchasing-transaction`} exact component={Purchasing} />
      <Route
        path={`${props.match.path}/purchasing-transaction/details`}
        component={PurchasingDetails}
      />
      <Route path={`${props.match.path}/capital-flow-history`} component={CapitalFlow} />
      <Route path={`${props.match.path}/cancellation`} exact component={Cancellation} />
      <Route path={`${props.match.path}/cancellation/details`} component={CancellationDetails} />
    </>
  );
};