/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { BsChevronRight } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { getSalesTransaction } from "../../../../redux/actions/sales";
import { FINANCE_S_TAB } from "../../../../redux/types/finance";
import { formatquantity } from "../../../../configs/formatnumber";
import { OutboundStatus } from "../../../../constants";
import {
  BodyText,
  Button,
  Pagination,
  Panel,
  SegmentedTitle,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  usePagination,
} from "../../../../components";

export default (props: any) => {
  const dispatch = useDispatch();
  const { FinanceIncoming, FinanceOrder, FinanceFinished } = OutboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.finance.s_tab);
  const data = useSelector((state: any) => state.sales.salesList);

  //* FUNCTION
  const toggleTitle = ({ value }) => props.history.push(`/finance/${value}`);

  const toggleTab = (selectedTab: any) => dispatch({ type: FINANCE_S_TAB, payload: selectedTab });

  //* FETCH DATA
  useEffect(() => {
    setPage(1);
    dispatch(getSalesTransaction({ status: tab }));
  }, [tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  console.log("home", data);
  if (!data)
    return (
      <div className="finance finance-activities">
        <Helmet>
          <title>loading...</title>
        </Helmet>
      </div>
    );
  return (
    <div className="finance finance-activities">
      <Helmet>
        <title>Sales | Finance</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        titles={["Sales Transaction", "Purchasing Transaction"]}
        slug={["sales-transaction", "purchasing-transaction"]}
        onClick={toggleTitle}
      />

      <Tabs
        options={["Incoming Transaction", "Sales Order", "Finished Transaction"]}
        slug={[FinanceIncoming, FinanceOrder, FinanceFinished]}
        activeTab={tab}
        setActiveTab={toggleTab}
      />

      <Panel className="mt-4 content content-purchasing">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Tanggal Transaksi</TableHeadCell>
              <TableHeadCell>Nama Pembeli</TableHeadCell>

              {tab !== FinanceOrder ? (
                <TableHeadCell>Performa Invoice ID</TableHeadCell>
              ) : (
                <TableHeadCell>Paid Invoice ID</TableHeadCell>
              )}

              {tab === FinanceIncoming ? (
                <TableHeadCell style={{ textAlign: "left" }}>Status Invoice</TableHeadCell>
              ) : null}

              <TableHeadCell style={{ textAlign: "right" }}>Nominal Transaksi</TableHeadCell>
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} data={currentData()} tab={tab} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, history, tab, match }) => {
  const { FinanceIncoming, FinanceOrder, FinanceFinished } = OutboundStatus;
  return !data
    ? null
    : data.map((item: any, i: number) => {
        const link = `${match.path}/details`;
        const nextClick = () => history.push(link, { data: item, tab: tab });
        return (
          <TableRow key={i} onClick={nextClick}>
            <TableCell>{moment(item.createdAt).format("DD/MM/YYYY")}</TableCell>
            <TableCell>{item.buyer.user.fullName}</TableCell>
            <TableCell>{item.invoiceCode || "-"}</TableCell>

            {tab === FinanceIncoming ? (
              <TableCell style={{ textAlign: "left" }}>
                {item.status}
                {/* {item.performaInvoiceStatus === "Refunded" ? (
                  <BodyText color="#605E5C">{item.performaInvoiceStatus}</BodyText>
                ) : item.performaInvoiceStatus === "Overdue" ? (
                  <BodyText color="#EF443D">{item.performaInvoiceStatus}</BodyText>
                ) : (
                  <BodyText color="#5EBB72">{item.performaInvoiceStatus}</BodyText>
                )} */}
              </TableCell>
            ) : null}

            <TableCell style={{ textAlign: "right" }}>
              Rp {formatquantity(item.totalPrice)}
            </TableCell>
            <TableCell style={{ width: 50, borderLeftColor: "transparent" }}>
              <Button theme="action-table">
                <BsChevronRight />
              </Button>
            </TableCell>
          </TableRow>
        );
      });
};
