/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { useDropzone } from "react-dropzone";
import { useDispatch, useSelector } from "react-redux";
import { BackButton } from "../../../../../components/Molecules";
import {
  BodyText,
  HeaderText,
  PageTitle,
  PaneHeader,
  SubjectTitle,
} from "../../../../../components/Typography";
import { IoMdRadioButtonOff } from "react-icons/io";
import { Button, ButtonIcon } from "../../../../../components/Button";
import {
  DataTransaksi,
  DataTransaksiItem,
  DataUser,
  ReportQC,
  ReportQCItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../../components/Molecules/PanelDetail";
import { Panel } from "../../../../../components/Panel";
import {
  PopupConfirmUploadedFile,
  PopupLoading,
  PopupUploadFile,
} from "../../../../../components/Molecules/Popup";
import { gray300, gray800 } from "../../../../../constants/Colors";
import { getSalesTransaction, getSaleTrxDetails } from "../../../../../redux/actions/sales";
import RincianPenjualan from "./RincianPenjualan";
import RincianPenjualanItem from "./RincianPenjualanItem";
import { API_URL, OutboundStatus } from "../../../../../constants";
import { BatalkanTransaksi, DownloadHalaman, Spinner } from "./Button";
import Axios from "axios";
import PopupCancellation from "./PopupCancellation";
import { SALES_LIST_DETAILS, SALES_TRX_DETAILS } from "../../../../../redux/types";
import { getShipmentLogs } from "../../../../../redux/actions/logs";

export default (props: any) => {
  const dispatch = useDispatch();
  const { FinanceIncoming, FinanceOrder, FinanceFinished } = OutboundStatus;

  const parentData = props.location.state.data;
  const tab = props.location.state.tab;

  //* GLOBAL STATE
  const data = useSelector(({ sales }: any) => sales.salesListDetails);
  const sale_transaction_details = useSelector(({ sales }: any) => sales.sale_transaction_details);
  const shipmentLogs = useSelector(({ logs }: any) => logs.shipmentLogs);

  //* LOCAL STATE
  const [dataQC, setDataQC] = useState<any>(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const [openUpload, setOpenUpload] = useState(false);
  const [openLoading, setOpenLoading] = useState(false);
  const [openConfirmUploadedFile, setOpenConfirmUploadedFile] = useState(false);
  const [selectedFile, setSelectedFile] = useState<any>(undefined);
  const [loadingHapusFile, setLoadingHapusFile] = useState(false);
  const [openCancellation, setOpenCancellation] = useState(false);

  //* FUNCTION
  const toggleCancellation = () => setOpenCancellation(!openCancellation);

  const handleUploadInvoice = () => setOpenUpload(true);

  const closeUploadInvoice = () => {
    setSelectedFile(undefined);
    setOpenUpload(false);
  };

  const handleProsesPembayaran = async () => {
    setOpenUpload(false);
    setOpenLoading(true);

    //TODO put POST Request here
    const body = {
      status: "sales-confirmed",
      paidDate: new Date(),
    };

    const formData = new FormData();
    formData.append("data", JSON.stringify(body));
    formData.append("paidInvoice", selectedFile);

    try {
      const uploadFile = await Axios.put(`${API_URL}/sales/${parentData.id}`, formData);
      dispatch(getSalesTransaction({ id: parentData.id }));
      setOpenLoading(false);
      setOpenConfirmUploadedFile(true);
      // const fetchNew = Axios.get()
      // setTimeout(() => {
      // }, 3000);
    } catch (e) {
      setOpenLoading(false);
      console.log(e);
    }
  };

  const handleHapusFile = async () => {
    setLoadingHapusFile(true);

    //TODO put DELETE Request here
    try {
      const uploadFile = Axios.put(`${API_URL}/sales/${parentData.id}`, {
        paidDate: null,
        paidInvoice: null,
        status: "sales-unconfirmed",
      });
      // const fetchNew = Axios.get()
      // setTimeout(() => {
      setLoadingHapusFile(false);
      setSelectedFile(undefined);
      setOpenConfirmUploadedFile(false);
      setOpenUpload(true);
      // }, 1000);
    } catch (e) {
      setLoadingHapusFile(true);
      console.log(e);
    }
  };

  const handleSimpanFile = async () => {
    setOpenConfirmUploadedFile(false);
    setSelectedFile(undefined);
  };

  const { isDragActive, getRootProps, getInputProps, open } = useDropzone({
    multiple: false,
    accept: ".pdf, .csv, .xls, .xlsx",
    noClick: true,
    noKeyboard: true,
    onDrop: React.useCallback((acceptedFiles) => {
      setSelectedFile(acceptedFiles[0]);
    }, []),
  });

  //* FETCH DATA
  useEffect(() => {
    if (!data) return;
    dispatch(getShipmentLogs({ data: data, type: "outbound" }));
  }, [dispatch, data]);

  useEffect(() => {
    dispatch(getSalesTransaction({ id: parentData.id }));
    dispatch(getSaleTrxDetails({ id: parentData.id }));
  }, []);

  console.log(data);
  if (!data || !sale_transaction_details)
    return (
      <div className="finance">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="finance finance-details">
      <Helmet>
        <title>Sales Details | Finance</title>
      </Helmet>

      <PopupConfirmUploadedFile
        isOpen={openConfirmUploadedFile}
        isLoading={loadingHapusFile}
        onDelete={handleHapusFile}
        onDeleteText="Hapus File"
        onSave={handleSimpanFile}
        selectedFile={selectedFile}
        title="File Terupload!"
      />

      <PopupLoading isOpen={openLoading} />

      <PopupUploadFile
        isOpen={openUpload}
        onClose={closeUploadInvoice}
        onSubmit={handleProsesPembayaran}
        onSubmitText="Proses Pembayaran"
        selectedFile={selectedFile}
        title="Upload faktur penjualan untuk melanjutkan proses"
      >
        <div {...getRootProps({ className: "dropzone w-100" })}>
          <div
            style={{
              border: "2px dashed",
              borderColor: isDragActive ? " #e1e1e1" : "transparent",
            }}
            className="d-flex py-2 flex-column align-items-center"
          >
            <input {...getInputProps()} />
            <PaneHeader color={gray800}>Drag and Drop File</PaneHeader>
            <PaneHeader color={gray800}>or</PaneHeader>
            <Button className="mt-2" onClick={open} theme="secondary">
              Browse File
            </Button>
            <BodyText className="mt-3" color={gray300}>
              PDF, CSV, Excel Document, etc
            </BodyText>
          </div>
        </div>
      </PopupUploadFile>

      <PopupCancellation data={data} isOpen={openCancellation} toggle={toggleCancellation} />

      <BackButton
        onClick={() => {
          props.history.goBack();
          dispatch({ type: SALES_LIST_DETAILS, payload: [] });
          dispatch({ type: SALES_TRX_DETAILS, payload: [] });
        }}
      >
        Kembali ke Transactional Activities
      </BackButton>

      <div className="header d-flex justify-content-between mb-4">
        <PageTitle>Detail Transaksi Penjualan</PageTitle>

        <div className="button-wrapper d-flex">
          {tab !== FinanceFinished && !data.isCancelRequest ? (
            <BatalkanTransaksi onClick={toggleCancellation} />
          ) : null}

          {FinanceIncoming.includes(data.status) ? (
            <Button
              onClick={handleUploadInvoice}
              containerStyle={{ minWidth: 220 }}
              theme="primary"
            >
              UPLOAD PAID INVOICE
              <ButtonIcon type="append">
                {isLoading ? <Spinner /> : <IoMdRadioButtonOff />}
              </ButtonIcon>
            </Button>
          ) : (
            <DownloadHalaman {...props} id={parentData.id} />
          )}
        </div>
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment().format("DD/MM/YYY H:mm")} link={false}>
            <HeaderText>{data.status}</HeaderText>
            &nbsp;&mdash;&nbsp;
            {data.performaInvoiceStatus === "Refunded" ? (
              <HeaderText color="#605E5C">{data.performaInvoiceStatus}</HeaderText>
            ) : data.performaInvoiceStatus === "Overdue" ? (
              <HeaderText color="#EF443D">{data.performaInvoiceStatus}</HeaderText>
            ) : (
              <HeaderText color="#5EBB72">{data.performaInvoiceStatus}</HeaderText>
            )}
          </UpdateStatusTrx>

          <DataUser
            type="Buyer"
            name={data ? data?.buyer?.user?.fullName : "-"}
            contact={data ? data?.buyer?.user?.phoneNumber : "-"}
            address={data ? data?.buyer?.user?.address : "-"}
          />

          <DataTransaksi>
            <DataTransaksiItem title="Tanggal Permintaan">
              {moment(data.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTransaksiItem>
            <DataTransaksiItem title="Kode Transaksi">
              {data.transactionCode || "-"}
            </DataTransaksiItem>
            <DataTransaksiItem accent url={data.performaInvoiceFile} title="Performa Invoice">
              {data.invoiceCode || "-"}
            </DataTransaksiItem>

            {tab !== FinanceIncoming || data.paidInvoice ? (
              <DataTransaksiItem accent url={data.paidInvoice} title="Paid Invoice">
                {data.invoiceCode || "-"}
              </DataTransaksiItem>
            ) : null}
          </DataTransaksi>

          {tab === FinanceFinished ? (
            <ReportQC type="Outbound" officerName={dataQC ? dataQC.qcOfficer : undefined}>
              <ReportQCItem
                title="Quality"
                verified={dataQC ? dataQC.quality.verified : undefined}
                signedBy={dataQC ? dataQC.quality.signedBy : undefined}
                timestamp={
                  dataQC ? moment(dataQC.quality.date).format("DD/MM/YYYY H:mm") : ""
                }
              />
              <ReportQCItem
                title="Quantity"
                verified={dataQC ? dataQC.quantity.verified : undefined}
                signedBy={dataQC ? dataQC.quantity.signedBy : undefined}
                timestamp={
                  dataQC ? moment(dataQC.quantity.date).format("DD/MM/YYYY H:mm") : ""
                }
              />
            </ReportQC>
          ) : null}
        </div>

        <div className="col-8">
          <RincianPenjualan parentData={parentData}>
            {!sale_transaction_details
              ? null
              : sale_transaction_details.map((item: any, i: number) => {
                  return <RincianPenjualanItem key={i} item={item} />;
                })}
          </RincianPenjualan>

          {tab !== FinanceIncoming ? (
            <>
              {/* //TODO this should be a molecules */}
              <Panel className="mb-3">
                <div className="header pb-3 border-bottom">
                  <PaneHeader>Kelengkapan Dokumen</PaneHeader>
                </div>
                <div className="mb-3"></div>

                <div className="body">
                  <div className="content-item d-flex flex-column">
                    <SubjectTitle color="#605E5C">Shipmen List</SubjectTitle>
                    <HeaderText color="#107C10">Daftar Pengiriman.pdf</HeaderText>
                  </div>
                </div>
              </Panel>

              <RiwayatEkspedisi data={shipmentLogs} />
            </>
          ) : null}
        </div>
      </div>
    </div>
  );
};
