/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { BodyText, Card, HeaderText } from "../../../../../components";
import LaporanInventoryManager from "../../../../../components/Molecules/PanelDetail/LaporanInventoryManager";
import { formatquantity } from "../../../../../configs/formatnumber";

export default ({ item }: { item: any }) => {
  return (
    <Card className="rincian-penjualan-item">
      <div className="body">
        <div className="image-wrapper">
          <img
            style={{ width: 84, height: 84, objectFit: "cover" }}
            src={item.product_sku.product_variant.imageUrl}
            alt="product"
          />
        </div>

        <div className="product-info">
          <div className="product-info-title">
            <HeaderText accent bold>
              {`${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`}
            </HeaderText>
          </div>
          <div className="product-info-details">
            <DetailsItem title="SKU">{item.product_sku.sku}</DetailsItem>
            <DetailsItem title="Farmer">{item.product_sku.farmer.user.fullName}</DetailsItem>
            <DetailsItem title="Pilihan Packaging">{item.packagingUnit}</DetailsItem>
            <DetailsItem title="Total Pembelian">{formatquantity(item.quantity)} Kg</DetailsItem>
          </div>

          <div className="product-info-details">
            <DetailsItem title="Harga per Satuan">
              Rp {formatquantity(item.product_sku.price)}/Kg
            </DetailsItem>
            <DetailsItem title="Harga Total">
              Rp {formatquantity(item.quantity * item.product_sku.price)}
            </DetailsItem>
            <DetailsItem title="Harga Packaging">
              Rp {formatquantity(item.packagingCost)}/Kg
            </DetailsItem>
          </div>
        </div>
      </div>

      <div className="accordion">{/* <LaporanInventoryManager data={item} /> */}</div>
    </Card>
  );
};

const DetailsItem = ({ children, title }) => {
  return (
    <div className="details-item">
      <BodyText color="#605E5C">{title}</BodyText>
      <BodyText color="#323130" bold>
        {children}
      </BodyText>
    </div>
  );
};
