/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../components/Table";
import { pricetag } from "../../../configs/formatnumber";
import { Pagination, usePagination } from "../../../components/Pagination";
import Tabs from "../../../components/Tabs";

export default (props) => {
  const [data, setData] = useState([]);
  const [isActive, setIsActive] = useState("Incoming Transaction");

  const fetchData = (status) => {
    if (status === "Incoming Transaction") {
      return setData(dummyDataTable.filter((val) => val.trxStatus === "proccessed"));
    }
    if (status === "Sales Order (SO)") {
      return setData(dummyDataTable.filter((val) => val.trxStatus === "ordered"));
    }
    if (status === "Finished Transaction") {
      return setData(dummyDataTable.filter((val) => val.trxStatus === "finished"));
    }
    return setData(dummyDataTable);
  };

  useEffect(() => {
    fetchData(isActive);
  }, [isActive]);

  const RenderBodyTable = ({ data }) => {
    const RenderStatus = ({ children }) => {
      if (children === "Paid") return <TableCell style={{ color: "#69984C" }}>Paid</TableCell>;
      if (children === "Overdue")
        return <TableCell style={{ color: "#8D1F1F" }}>Overdue</TableCell>;
      if (children === "Refunded") return <TableCell>Refunded</TableCell>;
      if (children === "Confirmed") return <TableCell>Confirmed</TableCell>;
      if (children === "Rejected") return <TableCell>Rejected</TableCell>;
      if (children === "processed") return <TableCell>Processed</TableCell>;
      return <TableCell>&nbsp;</TableCell>;
    };

    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => (
      <TableRow key={idx}>
        <TableCell>{item.entryDate}</TableCell>
        <TableCell>{item.buyerName}</TableCell>
        <TableCell>{item.performarInvoiceCode}</TableCell>
        <RenderStatus>{item.invoiceStatus}</RenderStatus>
        <TableCell>{pricetag(item.totalPrice)}</TableCell>
      </TableRow>
    ));
  };

  const { next, prev, jump, currentData, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 7,
  });

  return (
    <div className="content-child">
      <Helmet>
        <title>Inflow | Transactional Activities</title>
      </Helmet>

      <Tabs
        options={["Incoming Transaction", "Sales Order (SO)", "Finished Transaction"]}
        activeTab={isActive}
        setActiveTab={setIsActive}
      />

      <Table className="trx-activities">
        <TableHead>
          <TableRow>
            <TableHeadCell> Tanggal Transaksi </TableHeadCell>
            <TableHeadCell> Nama Pembeli </TableHeadCell>
            <TableHeadCell> Performa Invoice ID </TableHeadCell>
            <TableHeadCell> Status Transaksi </TableHeadCell>
            <TableHeadCell> Nominal Transaksi </TableHeadCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <RenderBodyTable data={currentData()} />
        </TableBody>
      </Table>

      <div className="pagination-wrapper my-3">
        <Pagination
          next={next}
          prev={prev}
          jump={jump}
          pages={pages}
          currentPage={currentPage}
          maxPage={maxPage}
        />
      </div>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <td colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </td>
    </TableRow>
  );
};

const dummyDataTable = [
  ["CV. Burhan", "proccessed", "Paid"],
  ["Farm Cikole", "proccessed", "Paid"],
  ["CV. Burhan", "proccessed", "Paid"],
  ["RPA Sierad", "proccessed", "Paid"],
  ["Moms and Pops", "proccessed", "Overdue"],
  ["RPA Sierad", "proccessed", "Overdue"],
  ["Moms and Pops", "proccessed", "Overdue"],
  ["RPA Sierad", "proccessed", "Overdue"],
  ["CV. Burhan", "proccessed", "Refunded"],
  ["CV. Burhan", "proccessed", "Refunded"],
  ["CV. Burhan", "proccessed", "Refunded"],
  ["CV. Burhan", "proccessed", "Refunded"],
  ["Moms and Pops", "ordered", "Paid"],
  ["RPA Sierad", "ordered", "Paid"],
  ["Moms and Pops", "ordered", "Paid"],
  ["Farm Cikole", "ordered", "Paid"],
  ["Moms and Pops", "ordered", "Paid"],
  ["CV. Burhan", "ordered", "Overdue"],
  ["CV. Burhan", "ordered", "Overdue"],
  ["Farm Cikole", "ordered", "Overdue"],
  ["CV. Burhan", "ordered", "Overdue"],
  ["CV. Burhan", "ordered", "Overdue"],
  ["Farm Cikole", "ordered", "Refunded"],
  ["Farm Cikole", "ordered", "Refunded"],
  ["Moms and Pops", "ordered", "Refunded"],
  ["CV. Burhan", "ordered", "Refunded"],
  ["CV. Burhan", "ordered", "Refunded"],
  ["CV. Burhan", "finished", "Confirmed"],
  ["CV. Burhan", "finished", "Confirmed"],
  ["Moms and Pops", "finished", "Confirmed"],
  ["CV. Burhan", "finished", "Confirmed"],
  ["Farm Cikole", "finished", "Confirmed"],
  ["CV. Burhan", "finished", "Refunded"],
  ["CV. Burhan", "finished", "Refunded"],
  ["Moms and Pops", "finished", "Refunded"],
  ["Moms and Pops", "finished", "Refunded"],
  ["CV. Burhan", "finished", "Refunded"],
  ["CV. Burhan", "finished", "Rejected"],
  ["CV. Burhan", "finished", "Rejected"],
  ["Farm Cikole", "finished", "Rejected"],
  ["CV. Burhan", "finished", "Rejected"],
  ["CV. Burhan", "finished", "Rejected"],
].map((item, idx) => {
  return {
    id: idx + 1,
    buyerName: item[0],
    trxStatus: item[1],
    invoiceStatus: item[2],
    transactionCode: "ALEPHOENTR975",
    paidInvoiceCode: "INV-482392382",
    performarInvoiceCode: "INV-482392382",
    address: "Jl. Wolter Mongonsidi No. 82 (Petogogan, Kebayoran Baru) Jakarta Selatan, Jakarta",
    phoneNumber: "+62 21 7222006",
    entryDate: "13/09/2020",
    totalPrice: 150000000,
  };
});
