/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from "react";
import Axios from "axios";
import moment from "moment";
import { Helmet } from "react-helmet";
import { useDropzone } from "react-dropzone";
import { gray300, gray800 } from "../../../../../constants/Colors";
import { API_URL, InboundStatus } from "../../../../../constants";
import { DownloadHalaman, RequestPembayaran, SetujuiTender, TolakTender } from "./Button";
import RincianTender from "./RincianTender";
import { enumTenderStatus, enumType, enumTypeCategory } from "../../../../../configs/enums";
import {
  BackButton,
  BodyText,
  Button,
  HeaderText,
  Input,
  PageTitle,
  PaneHeader,
} from "../../../../../components";
import {
  PopupConfirmUploadedFile,
  PopupLoading,
  // PopupUploadFile,
} from "../../../../../components/Molecules/Popup";
import {
  DataTender,
  DataTenderItem,
  ReportQC,
  ReportQCItem,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../../components/Molecules/PanelDetail";
import PopupCancellation from "./PopupCancellation";
import { useDispatch, useSelector } from "react-redux";
import { getShipmentLogs } from "../../../../../redux/actions/logs";
import { getIQCReports } from "../../../../../redux/actions/i_qc";
import { getPurchasingTransactionDetails } from "../../../../../redux/actions/purchasing";
import DataFarmer from "./DataFarmer";
import PopupRequestPembayaran from "./PopupRequestPembayaran";
import PopupUploadFile from "./PopupUploadFile";

//todo FIX CANCEL BUTTON STATE SHOULD AUTO HIDE AFTER SUCCESS POST  👌🏻
//todo RENAME TOLAK PENAWARAN => TENDER  👌🏻

export default (props: any) => {
  const dispatch = useDispatch();
  const parentData = props.location.state.data;
  const { FinancePre, FinanceOrder, FinancePayment } = InboundStatus;

  //* GLOBAL STATE
  const data = useSelector((state: any) => state.purchasing.purchaseTrxListDetails);
  const iqc_reports = useSelector((state: any) => state.i_qc.iqc_reports);
  const shipmentLogs = useSelector((state: any) => state.logs.shipmentLogs);

  //* LOCAL STATE
  const [inputVoucher, setInputVoucher] = useState(false);
  const [dueDate, setDueDate] = useState("");

  const [openUpload, setOpenUpload] = useState(false);
  const [openLoading, setOpenLoading] = useState(false);
  const [openConfirmUploadedFile, setOpenConfirmUploadedFile] = useState(false);
  const [selectedFile, setSelectedFile] = useState<any>(undefined);
  const [loadingHapusFile, setLoadingHapusFile] = useState(false);
  const [openCancellation, setOpenCancellation] = useState(false);

  //* FUNCTION
  const toggleCancellation = () => setOpenCancellation(!openCancellation);

  //* SECTION FOR UPLOAD VOUCHER PEMBAYARAN
  const handleChangeDueDate = (val: string) => setDueDate(val);
  const openInputVoucher = () => setInputVoucher(true);
  const closeInputVoucher = () => {
    setSelectedFile(undefined);
    setInputVoucher(false);
  };
  const handleUploadVoucher = async (val: any) => {
    setInputVoucher(false);
    setOpenLoading(true);

    // const formData = new FormData();
    // formData.append("paymentVoucher", selectedFile);

    try {
      // const link = `${API_URL}/purchases/${parentData.id}/payment-voucher`;
      // const uploadFile = await Axios.put(link, formData);
      const link = `${API_URL}/purchases/${parentData.id}`;
      const uploadFile = await Axios.put(link, {
        status: "payment-request",
        dueDate: val.dueDate,
      });
      console.log(uploadFile.data.message);
      dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
      setOpenLoading(false);
      // setOpenConfirmUploadedFile(true);
    } catch (err) {
      console.log(err);
    }
  };

  const handleHapusFileVoucher = async () => {
    setLoadingHapusFile(true);
    try {
      const body = { dueDate: null, paymentVoucher: null, status: "finance-approval" };
      const link = `${API_URL}/purchases/${parentData.id}/payment-voucher`;
      const deleteFile = await Axios.put(link, body);
      console.log(deleteFile.data.message);
      dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
      setLoadingHapusFile(false);
      setSelectedFile(undefined);
      setOpenConfirmUploadedFile(false);
      setInputVoucher(true);
    } catch (e) {
      console.log(e);
    }
  };

  //* SECTION FOR UPLOAD PAID INVOICE
  const openSetujuiTender = () => setOpenUpload(true);
  const closeSetujuiTender = () => {
    setSelectedFile(undefined);
    setOpenUpload(false);
  };
  const handleProsesPembayaran = async (files: any[]) => {
    setOpenUpload(false);
    setOpenLoading(true);

    const formData = new FormData();
    const body = { status: "item-stored", paidDate: new Date() };
    formData.append("data", JSON.stringify(body));
    files.forEach((file) => formData.append("paidInvoice", file));
    // formData.append("paidInvoice", selectedFile);
    // console.log(files);

    try {
      const link = `${API_URL}/purchases/${parentData.id}`;
      const uploadFile = await Axios.put(link, formData);
      console.log(uploadFile.data.message);
      dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
      setOpenLoading(false);
      setOpenConfirmUploadedFile(true);
    } catch (e) {
      console.log(e);
    }
  };;

  const handleHapusFile = async () => {
    setLoadingHapusFile(true);
    try {
      const body = { paidDate: null, paidInvoice: null, status: "finance-approval" };
      const link = `${API_URL}/purchases/${parentData.id}`;
      const deleteFile = await Axios.put(link, body);
      console.log(deleteFile.data.message);
      dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
      setLoadingHapusFile(false);
      setSelectedFile(undefined);
      setOpenConfirmUploadedFile(false);
      setOpenUpload(true);
    } catch (e) {
      console.log(e);
    }
  };

  const handleSimpanFile = async () => {
    setOpenConfirmUploadedFile(false);
    setSelectedFile(undefined);
  };

  const { isDragActive, getRootProps, getInputProps, open } = useDropzone({
    multiple: false,
    accept: ".pdf, .csv, .xls, .xlsx",
    noClick: true,
    noKeyboard: true,
    onDrop: useCallback((acceptedFiles) => {
      setSelectedFile(acceptedFiles[0]);
    }, []),
  });

  useEffect(() => {
    if (!data) return;
    dispatch(getIQCReports({ data: data }));
    dispatch(getShipmentLogs({ data: data, type: "inbound" }));
  }, [data]);

  useEffect(() => {
    dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
  }, []);

  console.log(dueDate ? moment(dueDate).format("DD MMMM YYYY") : null, data);
  if (!data)
    return (
      <div className="finance finance-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="finance finance-details">
      <Helmet>
        <title>{`${parentData.product_variant.subcategory.name} ${parentData.product_variant.productName} | Purchasing Details`}</title>
      </Helmet>

      <PopupRequestPembayaran
        data={data}
        isOpen={inputVoucher}
        onClose={closeInputVoucher}
        onSubmit={handleUploadVoucher}
        title="Voucher Request Pembayaran"
      />

      <PopupConfirmUploadedFile
        isOpen={openConfirmUploadedFile}
        isLoading={loadingHapusFile}
        onDelete={openUpload ? handleHapusFile : handleHapusFileVoucher}
        onDeleteText="Hapus File"
        onSave={handleSimpanFile}
        selectedFile={selectedFile}
        title="File Terupload!"
      />

      <PopupLoading isOpen={openLoading} />

      <PopupUploadFile
        isOpen={openUpload}
        onClose={closeSetujuiTender}
        onSubmit={handleProsesPembayaran}
      >
        {/* <div {...getRootProps({ className: "dropzone w-100" })}>
          <div
            style={{
              border: "2px dashed",
              borderColor: isDragActive ? " #e1e1e1" : "transparent",
            }}
            className="d-flex py-2 flex-column align-items-center"
          >
            <input {...getInputProps()} />
            <PaneHeader color={gray800}>Drag and Drop File</PaneHeader>
            <PaneHeader color={gray800}>or</PaneHeader>
            <Button className="mt-2" onClick={open} theme="secondary">
              Browse File
            </Button>
            <BodyText className="mt-3" color={gray300}>
              PDF, CSV, Excel Document, etc
            </BodyText>
          </div>
        </div> */}
      </PopupUploadFile>

      <PopupCancellation data={data} isOpen={openCancellation} toggle={toggleCancellation} />

      <BackButton>Kembali ke Transactional Activities</BackButton>

      <div className="header d-flex justify-content-between mb-4">
        <PageTitle>Detail Pesanan</PageTitle>

        <div className="button-wrapper d-flex">
          {FinancePre.includes(data.status) && !data.isCancelRequest ? (
            <TolakTender onClick={toggleCancellation} />
          ) : FinanceOrder.includes(data.status) && !data.isCancelRequest ? (
            <TolakTender onClick={toggleCancellation}>BATALKAN TRANSAKSI</TolakTender>
          ) : FinancePayment.includes(data.status) && !data.isCancelRequest ? (
            <TolakTender onClick={toggleCancellation} />
          ) : null}

          {FinanceOrder.includes(data.status) ? (
            <RequestPembayaran onClick={openInputVoucher} />
          ) : FinancePayment.includes(data.status) ? (
            <SetujuiTender onClick={openSetujuiTender} />
          ) : (
            <DownloadHalaman {...props} />
          )}
        </div>
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(data.updatedAt).format("DD/MM/YYY H:mm")} link={false}>
            <HeaderText>{enumTenderStatus(data.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataFarmer
            name={parentData.farmer.user.fullName}
            contact={parentData.farmer.user.phoneNumber}
            address={parentData.farmer.user.address}
            invoiceCode={data.invoiceCode}
            paidInvoice={data.paidInvoice}
            paymentVoucher={data.paymentVoucher}
            transactionCode={data.transactionCode}
            paidDate={data.paidDate}
          />

          <DataTender>
            <DataTenderItem title="Tanggal Pengajuan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTenderItem>
            <DataTenderItem title="Bentuk Transaksi">
              {enumTypeCategory(parentData.type)} - {enumType(parentData.type)}
            </DataTenderItem>
          </DataTender>

          {iqc_reports ? (
            <ReportQC
              type="Inbound"
              itemName={`${data.product_variant.subcategory.name} ${data.product_variant.productName}`}
              officerName={!iqc_reports ? "" : iqc_reports.admin.fullName}
              qcPassed={
                !iqc_reports
                  ? false
                  : iqc_reports.reports
                  ? iqc_reports.reports.status !== "reject"
                  : false
              }
            >
              {iqc_reports ? (
                <>
                  <ReportQCItem
                    title="Quantity"
                    signedBy={iqc_reports.admin.fullName}
                    timestamp={moment(iqc_reports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                    verified={iqc_reports.reports.status !== "reject"}
                    valueOffer={`${data.quantity} Kg`}
                    valueActual={`${iqc_reports.reports.quantity} Kg`}
                  />
                  <ReportQCItem
                    title="Quality"
                    signedBy={iqc_reports.admin.fullName}
                    timestamp={moment(iqc_reports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                    verified={iqc_reports.reports.status !== "reject"}
                    valueOffer={data.product_variant.productName}
                    valueActual={iqc_reports.reports.quality}
                  />
                </>
              ) : null}
            </ReportQC>
          ) : null}
        </div>

        <div className="col-8">
          <RincianTender data={data} />

          {shipmentLogs ? <RiwayatEkspedisi data={shipmentLogs} /> : null}
        </div>
      </div>
    </div>
  );
};;;;
