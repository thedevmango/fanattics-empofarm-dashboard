/* eslint-disable no-unused-vars */
import React from "react";
import { RiwayatEkspedisi } from "../../../../../components/Molecules/PanelDetail";

export default (props) => {
  const [data, setData] = React.useState([]);

  return <RiwayatEkspedisi data={data} courrierOfficer="Supirman" />;
};
