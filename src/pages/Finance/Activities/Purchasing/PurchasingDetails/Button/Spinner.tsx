import React from "react";

export default () => (
  <div className="spinner-border spinner-border-sm">
    <span className="sr-only"></span>
  </div>
);
