import React from "react";
import { BodyText, Button } from "../../../../../../components";

export default ({ children, onClick }: { children?: React.ReactNode; onClick: any }) => {
  return (
    <Button
      onClick={onClick}
      containerStyle={{ padding: "6px 20px", textDecorationColor: "#D92C2C" }}
      theme="link"
      className="ml-2"
    >
      <BodyText color="#D92C2C">{children || "TOLAK TENDER"}</BodyText>
    </Button>
  );
};
