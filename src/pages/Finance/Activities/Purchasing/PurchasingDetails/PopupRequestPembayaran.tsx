/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { AiOutlineInfoCircle, AiOutlinePrinter } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";
import {
  BodyText,
  Button,
  ButtonIcon,
  HeaderText,
  Input,
  MetadataText,
  Modal,
  ModalBody,
  PaneHeader,
  SubjectTitle,
} from "../../../../../components";
import { FileDownload } from "../../../../../configs/FileDownload";
import { formatquantity } from "../../../../../configs/formatnumber";
import { Colors } from "../../../../../constants";

interface Props {
  className?: string;
  data?: any;
  isOpen: boolean;
  onClose: any;
  onSubmit: any;
  title?: string;
}

export default ({ className, data, isOpen, onClose, onSubmit, title = "Upload file" }: Props) => {
  const [dueDate, setDueDate] = useState("");
  const [maxCount, setMaxCount] = useState(400);
  const [value, setValue] = useState("");

  const handleChangeDueDate = (val: string) => setDueDate(val);

  function handleChange(e: any) {
    setValue(e.target.value);
  }
  function textColor(val: number) {
    if (val <= maxCount) return "#C8C8C8";
    return "#CB3B0E";
  }
  function stringCounter(val: number) {
    if (!value) return 0;
    if (val <= maxCount) return val;
    return maxCount - val;
  }

  function onCancel() {
    setValue("");
    onClose();
  }

  function handleSubmit() {
    const body = {
      dueDate: dueDate,
      remarks: value,
    };
    onSubmit(body);
  }

  const download = () => {
    const filename = (data.transactionCode || "") + ".pdf";
    const dummy =
      "https://empofarm-api.fanattics.tech/uploads/payment-voucher/PAYMENT_VOUCHER-1603707783445.pdf";
    FileDownload(data.paidInvoice ? data.paidInvoice : dummy, filename);
  };

  useEffect(() => {
    setDueDate(moment().format("YYYY-MM-DD"));
  }, []);

  return (
    <Modal className="request-pembayaran" isOpen={isOpen} size="lg">
      <ModalBody className={`mol-popup-req-voucher ${className}`}>
        <div className="header">
          <div className="header-title">
            <div className="icon">
              <AiOutlineInfoCircle size={21} color={Colors.gray500} />
            </div>

            <div className="title-info">
              <PaneHeader color={Colors.gray500} bold>
                {title}
              </PaneHeader>
              <MetadataText color={Colors.gray500}>Kode Voucher Pembayaran</MetadataText>
              <SubjectTitle color={Colors.gray800} bold>
                {data.voucherCode || "123898909"}
              </SubjectTitle>
            </div>
          </div>

          <div className="button-wrapper">
            <Button onClick={onClose} className="button-close">
              <ButtonIcon>
                <IoMdClose size={16} />
              </ButtonIcon>
            </Button>
          </div>
        </div>

        <div className="content row">
          <div className="col-7 content-left">
            <RequestInfoItem title="ID Transaksi">
              <Button onClick={onCancel} theme="link">
                <SubjectTitle color="#0078D4">{data.transactionCode}</SubjectTitle>
              </Button>
            </RequestInfoItem>

            <RequestInfoItem title="Farmer" value={data.farmer.user.fullName} />
            <RequestInfoItem title="Bank Tujuan" value={data.farmer.bankName} />
            <RequestInfoItem title="Rekening Tujuan" value={data.farmer.bankAccountNumber} />
            <RequestInfoItem
              title="Nama Pemilik Rekening"
              value={data.farmer.bankAccountOwnerName}
            />
            <RequestInfoItem title="Informasi Item">
              <SubjectTitle color={Colors.gray600}>
                {`${data.product_variant.subcategory.name} ${data.product_variant.productName}`}
              </SubjectTitle>
              <SubjectTitle color={Colors.gray500}>{formatquantity(data.quantity)} Kg</SubjectTitle>
            </RequestInfoItem>
          </div>

          <div className="col-5 content-right">
            <Input
              label="Due Date"
              containerStyle={{ width: 250 }}
              type="date"
              defaultValue={moment().format("YYYY-MM-DD")}
              onChange={(e: any) => handleChangeDueDate(e.target.value)}
            />

            <div className="nominal-transfer">
              <BodyText color={Colors.gray400}>Nominal Transfer</BodyText>
              <HeaderText bold color={Colors.gray600}>
                Rp {formatquantity(14650000)}
              </HeaderText>
            </div>
          </div>

          <div className="col-12 remarks">
            <SubjectTitle color={Colors.gray800} bold>
              Remarks
            </SubjectTitle>

            <FormTextArea
              handleChange={handleChange}
              maxCount={maxCount}
              stringCounter={stringCounter}
              textColor={textColor}
              value={value}
            />
          </div>
        </div>

        <div className="footer">
          <Button onClick={download} theme="action">
            <ButtonIcon type="prepend">
              <AiOutlinePrinter size={16} />
            </ButtonIcon>
            <BodyText color={Colors.gray700} bold>
              Download Voucher
            </BodyText>
          </Button>

          <div className="btn-submit-wrapper">
            <Button onClick={onCancel} theme="secondary" bold>
              Cancel
            </Button>
            <Button
              onClick={handleSubmit}
              disabled={value.length > maxCount || !dueDate}
              theme="primary"
            >
              Ajukan Permintaan
            </Button>
          </div>
        </div>
      </ModalBody>
    </Modal>
  );
};

const FormTextArea = ({ handleChange, maxCount, stringCounter, textColor, value }) => {
  return (
    <div className="form-textarea">
      <textarea
        onChange={handleChange}
        name="remarks"
        id="remarks-cancel"
        rows={5}
        placeholder=""
      />

      <div className="text-counter">
        <BodyText color={textColor(value.length)}>
          {`${stringCounter(value.length)}/${maxCount} Characters`}
        </BodyText>
      </div>
    </div>
  );
};

const RequestInfoItem = ({
  children,
  title,
  value,
}: {
  children?: React.ReactNode;
  title: string;
  value?: string | number;
}) => {
  return (
    <div className="req-info-item">
      <div className="item-title">
        <BodyText color={Colors.gray800} bold>
          {title}
        </BodyText>
      </div>
      <div className="item-value">{children ? children : <SubjectTitle>{value}</SubjectTitle>}</div>
    </div>
  );
};
