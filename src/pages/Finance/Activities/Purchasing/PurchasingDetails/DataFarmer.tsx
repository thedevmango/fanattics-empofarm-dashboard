import React from "react";
import { BsDownload } from "react-icons/bs";
import {
  BodyText,
  Button,
  HeaderText,
  MetadataText,
  Panel,
  SubjectTitle,
} from "../../../../../components";
import { FileDownload } from "../../../../../configs/FileDownload";

interface IDataUser {
  address: string;
  className?: string;
  contact: string;
  invoiceCode?: string;
  name: string;
  paidDate: string;
  paidInvoice: any;
  paymentVoucher: any;
  transactionCode: string;
}

export default ({
  address,
  className,
  contact,
  invoiceCode,
  name,
  paidDate,
  paidInvoice,
  paymentVoucher,
  transactionCode,
}: IDataUser) => {
  const filename = (type: "paid" | "voucher") => {
    if (type === "paid") return (invoiceCode || "") + ".pdf";
    if (type === "voucher") return paymentVoucher.split("/").pop().split(".")[0];
  };

  const download = (type: "paid" | "voucher") => {
    FileDownload(paidInvoice ? paidInvoice : paymentVoucher, filename(type));
  };

  return (
    <Panel padding="24" className={`mol-data-user ${className}`}>
      <div className="header">
        <HeaderText color="#404040" bold>
          Data Transaksi
        </HeaderText>
      </div>

      <div className="content">
        <ContentItem title="Nama Farmer">
          <SubjectTitle accent>{name}</SubjectTitle>
        </ContentItem>
        <ContentItem title="Nomor Kontak">
          <SubjectTitle color="#323130">{contact}</SubjectTitle>
        </ContentItem>
        <ContentItem title="Alamat Farm">
          <SubjectTitle color="#323130">{address}</SubjectTitle>
        </ContentItem>

        <ContentItem title="Kode Transaksi">
          <SubjectTitle color="#323130">{transactionCode}</SubjectTitle>
        </ContentItem>

        <ContentItem title="Bukti Pembayaran">
          {paidInvoice ? (
            <div className="d-flex align-items-center">
              <BodyText accent>{filename("paid")}</BodyText>
              <Button
                onClick={() => download("paid")}
                containerStyle={{ padding: 5 }}
                theme="action"
              >
                <BsDownload size={12} />
              </Button>
            </div>
          ) : "-"}
        </ContentItem>

        <ContentItem title="Kode Voucher">
          {paymentVoucher ? (
              <div className="d-flex align-items-center">
                <BodyText accent>{filename("voucher")}</BodyText>
                <Button
                  onClick={() => download("voucher")}
                  containerStyle={{ padding: 5 }}
                  theme="action"
                >
                  <BsDownload size={12} />
                </Button>
              </div>
          ) : "-"}
        </ContentItem>

        <ContentItem title="Tanggal Pembayaran">
          {paidDate || "-"}
        </ContentItem>
      </div>
    </Panel>
  );
};;

const ContentItem = ({ children, title }) => {
  return (
    <>
      <div className="info-title">
        <MetadataText bold>{title}</MetadataText>
      </div>
      <div className="info-value">{children}</div>
    </>
  );
};
