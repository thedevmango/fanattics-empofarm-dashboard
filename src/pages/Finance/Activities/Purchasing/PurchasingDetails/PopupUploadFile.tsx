/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from "react";
import { IoMdClose } from "react-icons/io";
import { Colors } from "../../../../../constants";
import {
  BodyText,
  Button,
  ButtonIcon,
  Input,
  Modal,
  ModalBody,
  PaneHeader,
} from "../../../../../components";
import { gray300 } from "../../../../../constants/Colors";

interface IUploadFile {
  children?: React.ReactNode;
  className?: string;
  isOpen: boolean;
  onClose: any;
  onSubmit: any;
  toggle?: any;
}

export default ({ children, className, isOpen, onClose, onSubmit, toggle }: IUploadFile) => {
  const [file1, setFile1] = useState<any>(undefined);
  const [file2, setFile2] = useState<any>(undefined);
  const [file3, setFile3] = useState<any>(undefined);

  const handleSubmit = () => {
    let files: any = [];
    if (file1) files.push(file1);
    if (file2) files.push(file2);
    if (file3) files.push(file3);
    return onSubmit(files);
  };

  return (
    <Modal toggle={toggle} isOpen={isOpen} size="lg">
      <ModalBody className={`mol-popup-upload-bukti-transfer ${className}`}>
        {toggle ? null : (
          <div className="button-close-wrapper">
            <Button onClick={onClose} className="button-close">
              <ButtonIcon>
                <IoMdClose size={16} />
              </ButtonIcon>
            </Button>
          </div>
        )}

        <PaneHeader color={Colors.gray500}>
          Upload bukti transfer untuk melanjutkan proses
        </PaneHeader>

        <div className="input-wrapper">
          <div className="input-item">
            <input value={file1 ? file1.name : ""} type="text" disabled />
            <input
              onChange={(e: any) => setFile1(e.target.files[0])}
              id="upload-file-1"
              type="file"
              accept=".pdf"
            />
            <label htmlFor="upload-file-1" className="component-btn component-btn-secondary">
              Browse File
            </label>
          </div>

          <div className="input-item">
            <input value={file2 ? file2.name : ""} type="text" disabled />
            <input
              onChange={(e: any) =>
                e.target.files ? setFile2(e.target.files[0]) : setFile2(undefined)
              }
              id="upload-file-2"
              type="file"
              accept=".pdf"
            />
            <label htmlFor="upload-file-2" className="component-btn component-btn-secondary">
              Browse File
            </label>
          </div>

          <div className="input-item">
            <input value={file3 ? file3.name : ""} type="text" disabled />
            <input
              onChange={(e: any) => setFile3(e.target.files[0])}
              id="upload-file-3"
              type="file"
              accept=".pdf"
            />
            <label htmlFor="upload-file-3" className="component-btn component-btn-secondary">
              Browse File
            </label>
          </div>
        </div>

        <div className="description">
          <BodyText color={gray300}>
            File dapat berupa faktur pembelian, tanda terima barang, bukti transfer, atau lampiran
            persetujuan pembayaran
          </BodyText>
        </div>

        <div className="my-3">
          <Button onClick={handleSubmit} theme="primary">
            Proses Pembayaran
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
