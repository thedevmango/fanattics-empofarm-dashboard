/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { BsChevronRight } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { FINANCE_P_TAB } from "../../../../redux/types/finance";
import { getPurchasingTransaction } from "../../../../redux/actions/purchasing";
import { formatquantity } from "../../../../configs/formatnumber";
import { InboundStatus } from "../../../../constants";
import { enumTenderStatus } from "../../../../configs/enums";
import {
  BodyText,
  Button,
  MetadataText,
  Pagination,
  Panel,
  SegmentedTitle,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  usePagination,
} from "../../../../components";
import { gray500, gray800 } from "../../../../constants/Colors";

export default (props: any) => {
  const dispatch = useDispatch();
  const { FinancePre, FinanceOrder, FinancePayment, FinanceSuccess, FinanceFailed } = InboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.finance.p_tab);
  const data = useSelector((state: any) => state.purchasing.purchaseTrxList);

  //* FUNCTION
  const toggleTitle = ({ value }) => {
    props.history.push(`/finance/${value}`);
  };

  const toggleTab = (selectedTab: string[]) => {
    if (selectedTab === tab) return;
    dispatch({ type: FINANCE_P_TAB, payload: selectedTab });
  };

  //* FETCH DATA
  useEffect(() => {
    setPage(1);
    dispatch(
      getPurchasingTransaction({
        status: tab,
        include: ["farmer", "product_sku", "product_variant"],
        isFailed: tab === FinanceFailed ? true : false,
      })
    );
  }, [tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  // console.log(tab, data); //TODO delete soon
  return (
    <div className="finance finance-activities">
      <Helmet>
        <title>Purchasing | Finance</title>
      </Helmet>

      <SegmentedTitle
        {...props}
        titles={["Sales Transaction", "Purchasing Transaction"]}
        slug={["sales-transaction", "purchasing-transaction"]}
        onClick={toggleTitle}
      />

      <Tabs
        options={[
          "Pre-Purchase Order",
          "Purchase Order",
          "Payment",
          "Transaksi Berhasil",
          "Transaksi Gagal",
        ]}
        slug={[FinancePre, FinanceOrder, FinancePayment, FinanceSuccess, FinanceFailed]}
        activeTab={tab}
        setActiveTab={toggleTab}
      />

      <Panel className="mt-4 content content-purchasing">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>ID Transaksi</TableHeadCell>
              <TableHeadCell>Nama</TableHeadCell>

              {tab === FinancePayment ? (
                <TableHeadCell>Item</TableHeadCell>
              ) : (
                <>
                  <TableHeadCell>Nama Item</TableHeadCell>
                  <TableHeadCell>Kuantitas</TableHeadCell>
                </>
              )}

              {tab === FinancePayment ? (
                <TableHeadCell style={{ textAlign: "right" }}>Nominal Bayar</TableHeadCell>
              ) : (
                <TableHeadCell style={{ textAlign: "right" }}>Nominal Pengajuan</TableHeadCell>
              )}

              {tab === FinanceSuccess || tab === FinanceFailed ? null : (
                <TableHeadCell>Status Transaksi</TableHeadCell>
              )}

              {tab === FinancePayment ? (
                <TableHeadCell style={{ textAlign: "right" }}>Status Pembelian</TableHeadCell>
              ) : tab === FinanceSuccess ? (
                <>
                  <TableHeadCell>Bukti Transfer</TableHeadCell>
                  <TableHeadCell>Paid Date</TableHeadCell>
                </>
              ) : tab === FinanceFailed ? (
                <TableHeadCell>Tanggal Ditolak</TableHeadCell>
              ) : null}

              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} data={currentData()} tab={tab} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ data, history, match, tab }) => {
  const { FinancePre, FinanceOrder, FinancePayment, FinanceSuccess, FinanceFailed } = InboundStatus;
  return !data
    ? null
    : data.map((item: any, i: number) => {
        const link = `${match.path}/details`;
        const nextClick = () => history.push(link, { data: item, tab: tab });
        return (
          <TableRow key={i} onClick={nextClick}>
            <TableCell>{item.transactionCode}</TableCell>
            <TableCell>
              <div className="d-flex flex-column">
                <BodyText color={gray800}>{item.farmer.user.fullName}</BodyText>
                <MetadataText color={gray500}>Farmer</MetadataText>
              </div>
            </TableCell>

            {tab === FinancePayment ? (
              <TableCell>
                <div className="d-flex flex-column">
                  <BodyText color={gray800}>
                    {item.product_variant.subcategory.name}&nbsp;{item.product_variant.productName}
                  </BodyText>
                  <MetadataText color={gray500}>
                    {formatquantity(item.quantity)}&nbsp;Kg
                  </MetadataText>
                </div>
              </TableCell>
            ) : (
              <>
                <TableCell>
                  {item.product_variant.subcategory.name}&nbsp;{item.product_variant.productName}
                </TableCell>
                <TableCell>{formatquantity(item.quantity)}&nbsp;Kg</TableCell>
              </>
            )}

            {tab === FinancePayment ? (
              <TableCell style={{ textAlign: "right" }}>
                Rp&nbsp;{formatquantity(item.totalPrice)}
              </TableCell>
            ) : (
              <TableCell style={{ textAlign: "right" }}>
                {item.salePricePerUnit
                  ? `Rp ${formatquantity(item.salePricePerUnit * item.quantity)}`
                  : "-"}
              </TableCell>
            )}

            {tab === FinanceSuccess || tab === FinanceFailed ? null : (
              <TableCell>
                {item.type === "DS" || item.type === "CSE" ? "New Purchase" : "Product Acquisition"}
              </TableCell>
            )}

            {tab === FinancePayment ? (
              <TableCell style={{ textAlign: "right" }}>
                <div className="d-flex flex-column">
                  <BodyText color={gray800}>Pending Payment</BodyText>
                  <MetadataText color={gray500}>
                    DUE: {item.dueDate ? moment(item.dueDate).format("DD/MM/YYYY") : "-"}
                  </MetadataText>
                </div>
              </TableCell>
            ) : tab === FinanceSuccess ? (
              <>
                <TableCell>{item.invoiceCode}</TableCell>
                <TableCell>{moment(item.paidDate).format("DD/MM/YYYY")}</TableCell>
              </>
            ) : tab === FinanceFailed ? (
              <TableCell>{moment(item.updatedAt).format("DD/MM/YYYY")}</TableCell>
            ) : null}

            {/* {tab === InboundStatus.FinanceOrder ? (
              <TableCell style={{ textAlign: "right" }}>
                {item.type === "DS" || item.type === "CSE" ? "New Purchase" : "Product Acquisition"}
              </TableCell>
            ) : tab === InboundStatus.FinancePre ? (
              <TableCell style={{ textAlign: "right" }}>{enumTenderStatus(item.status)}</TableCell>
            ) : tab === InboundStatus.FinanceSuccess ? (
              <>
                <TableCell style={{ textAlign: "right" }}>{item.invoiceCode}</TableCell>
                <TableCell style={{ textAlign: "right" }}>
                  {moment(item.paidDate).format("DD/MM/YYYY")}
                </TableCell>
              </>
            ) : (
              <TableCell style={{ textAlign: "right" }}>
                {moment(item.rejectDate).format("DD/MM/YYYY")}
              </TableCell>
            )} */}

            <TableCell style={{ width: 50, borderLeftColor: "transparent" }}>
              <Button theme="action-table">
                <BsChevronRight />
              </Button>
            </TableCell>
          </TableRow>
        );
      });
};
