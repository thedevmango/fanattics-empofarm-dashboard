/* eslint-disable no-unused-vars */
import React from "react";
import { Helmet } from "react-helmet";

export default (props) => {
  return (
    <div className="capital-flow-history">
      <Helmet>
        <title>Capital Flow History</title>
      </Helmet>

      <h1>CAPITAL FLOW HISTORY</h1>
    </div>
  );
};
