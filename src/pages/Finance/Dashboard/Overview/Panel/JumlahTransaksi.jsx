/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FINANCE_CHANGE_VALUE } from "../../../../../redux/types/finance";
import { ResponsiveContainer, AreaChart, XAxis, Area, Tooltip } from "recharts";
import { dummyAreaDaily, dummyAreaMonthly, dummyAreaWeekly } from "../../data";
import { WidgetPanel } from "../../../../../components/Molecules";
import { sumByKey } from "../../../../../configs/sumByKey";

export default ({ className = "" }) => {
  const dispatch = useDispatch();
  const { timeframe, dropdownOpen } = useSelector(({ dashboardFinance }) => {
    return {
      timeframe: dashboardFinance.timeframeJumlahTransaksi,
      dropdownOpen: dashboardFinance.dropdownJumlahTransaksi,
    };
  });

  const [data, setData] = useState([]);
  const [value, setValue] = useState(0);
  const [valueBefore, setValueBefore] = useState(0);
  const [percentage, setPercentage] = useState(0);

  const dropdownToggle = ({ payload }) => {
    let name = "dropdownJumlahTransaksi";
    let value = !dropdownOpen;
    dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    if (payload) {
      let name = "timeframeJumlahTransaksi";
      let value = payload;
      dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    }
  };

  useEffect(() => {
    if (timeframe === "Hari Ini") setData(dummyAreaDaily);
    if (timeframe === "1 Minggu") setData(dummyAreaWeekly);
    if (timeframe === "1 Bulan") setData(dummyAreaMonthly);
  }, [timeframe]);

  useEffect(() => {
    setValue(sumByKey(data, "Transaksi"));
    setValueBefore(sumByKey(data, "TransaksiBefore"));
  }, [data]);

  useEffect(() => {
    setPercentage(Math.round(((value - valueBefore) / valueBefore) * 100));
  }, [value, valueBefore]);

  return (
    <div className={className}>
      <WidgetPanel
        dropdownItems={["Hari Ini", "1 Minggu", "1 Bulan"]}
        dropdownOpen={dropdownOpen}
        dropdownToggle={dropdownToggle}
        size="sm"
        timeframe={timeframe}
        title="Jumlah Transaksi"
        trendPercentage={percentage}
        totalValue={value}
        type="quantity"
      >
        <ResponsiveContainer width="100%" height={50}>
          <AreaChart data={data} margin={{ top: 10, right: 0, left: 0, bottom: 0 }}>
            <defs>
              <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#D2B5FA" stopOpacity={1} />
                <stop offset="95%" stopColor="#F3E6FF" stopOpacity={0.3} />
              </linearGradient>
            </defs>
            {/* <Tooltip /> */}
            <XAxis dataKey="time" hide />
            <Area
              type="basis"
              dataKey="Transaksi"
              strokeWidth={2}
              stroke="#975FE4"
              fillOpacity={1}
              fill="url(#color)"
            />
          </AreaChart>
        </ResponsiveContainer>
      </WidgetPanel>
    </div>
  );
};
