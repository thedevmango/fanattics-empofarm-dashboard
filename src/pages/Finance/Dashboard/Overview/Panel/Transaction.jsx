/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { VscCalendar } from "react-icons/vsc";
import { Panel } from "../../../../../components/Panel";
import { Form, Input } from "../../../../../components/Form";
import { HeaderText } from "../../../../../components/Typography";
import { WidgetCard } from "../../../../../components/Molecules";
import {
  ResponsiveContainer,
  LineChart,
  Tooltip,
  XAxis,
  Line,
  YAxis,
  CartesianGrid,
  Legend,
} from "recharts";
import { dummyRevenue, dummyProfit, dummyCost, dummyTrx } from "../../data";
import { sumByKey } from "../../../../../configs/sumByKey";
import { formatnumber } from "../../../../../configs/formatnumber";

export default ({ className = "" }) => {
  const [isActive, setIsActive] = useState("Revenue");
  const [data, setData] = useState([]);

  const [valueRevenue, setValueRevenue] = useState([]);
  const [valueProfit, setValueProfit] = useState([]);
  const [valueCost, setValueCost] = useState([]);
  const [valueTrx, setValueTrx] = useState([]);

  useEffect(() => {
    setValueRevenue([
      sumByKey(dummyRevenue(), "Periode Ini"),
      sumByKey(dummyRevenue(), "Periode Sebelumnya"),
    ]);
    setValueProfit([
      sumByKey(dummyProfit(), "Periode Ini"),
      sumByKey(dummyProfit(), "Periode Sebelumnya"),
    ]);
    setValueCost([
      sumByKey(dummyCost(), "Periode Ini"),
      sumByKey(dummyCost(), "Periode Sebelumnya"),
    ]);
    setValueTrx([sumByKey(dummyTrx(), "Periode Ini"), sumByKey(dummyTrx(), "Periode Sebelumnya")]);
  }, []);

  useEffect(() => {
    if (isActive === "Revenue") setData(dummyRevenue);
    if (isActive === "Profit") setData(dummyProfit);
    if (isActive === "Production Cost") setData(dummyCost);
    if (isActive === "Transaction") setData(dummyTrx);
  }, [isActive]);

  return (
    <div className={className}>
      <Panel className="mol-widget mol-widget-panel h-100">
        <div className="widget-header mb-3">
          <div className="widget-header-title">
            <HeaderText>Transaction</HeaderText>
          </div>
          <Form className="widget-header-form col-3 pr-0">
            <Input
              prepend={<VscCalendar size={18} />}
              defaultValue={moment().format("MMMM")}
              type="select"
            >
              <option value="May">May</option>
              <option value="June">June</option>
              <option value="July">July</option>
              <option value="August">August</option>
              <option value="September">September</option>
            </Input>
          </Form>
        </div>

        <div className="d-flex">
          <WidgetCard
            isActive={isActive}
            onClick={setIsActive}
            className="clickable mr-3"
            title="Revenue"
            trendIcon="arrow"
            trendPercentage={Math.round(
              ((valueRevenue[0] - valueRevenue[1]) / valueRevenue[1]) * 100
            )}
            totalValue={valueRevenue[0]}
            type="priceunit"
          />
          <WidgetCard
            isActive={isActive}
            onClick={setIsActive}
            className="clickable mr-3"
            title="Profit"
            trendIcon="arrow"
            trendPercentage={Math.round(((valueProfit[0] - valueProfit[1]) / valueProfit[1]) * 100)}
            totalValue={valueProfit[0]}
            type="priceunit"
          />
          <WidgetCard
            isActive={isActive}
            onClick={setIsActive}
            className="clickable mr-3"
            title="Production Cost"
            trendIcon="arrow"
            trendPercentage={Math.round(((valueCost[0] - valueCost[1]) / valueCost[1]) * 100)}
            totalValue={valueCost[0]}
            type="priceunit"
          />
          <WidgetCard
            isActive={isActive}
            onClick={setIsActive}
            className="clickable "
            title="Transaction"
            trendIcon="arrow"
            trendPercentage={Math.round(((valueTrx[0] - valueTrx[1]) / valueTrx[1]) * 100)}
            totalValue={valueTrx[0]}
            type="quantity"
          />
        </div>
        <div style={{ borderBottom: "1px solid #ACACAC" }} className="my-4" />

        <div className="body">
          <ResponsiveContainer width="100%" height={250}>
            <LineChart
              style={{ backgroundColor: "#F8F8F8" }}
              data={data}
              margin={{ top: 10, right: 10, left: 10, bottom: 0 }}
            >
              <Tooltip content={<CustomTooltip />} />
              <Legend iconType="plainline" align="left" verticalAlign="top" height={36} />
              <CartesianGrid strokeOpacity={0.5} color="#F0F0F0" strokeDasharray="3 3" />
              <XAxis dataKey="time" />
              <YAxis
                scale="linear"
                tickFormatter={(number) => {
                  if (number > 1000000) {
                    return (number / 1000000).toString() + " juta";
                  } else if (number > 1000) {
                    return (number / 1000).toString() + " ribu";
                  } else {
                    return number.toString();
                  }
                }}
                domain={[(dataMin) => 0 - Math.abs(dataMin), (dataMax) => dataMax * 2]}
                orientation="right"
              />
              <Line
                type="linear"
                dataKey="Periode Ini"
                dot={false}
                strokeWidth={2}
                stroke="#4C84FF"
              />
              <Line
                type="linear"
                dataKey="Periode Sebelumnya"
                dot={false}
                strokeWidth={2}
                stroke="#979797"
                strokeDasharray="2 2"
              />
            </LineChart>
          </ResponsiveContainer>
        </div>
      </Panel>
    </div>
  );
};

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    return (
      <div className="widget-custom-tooltip">
        <p className="label">{`Periode Ini: ${formatnumber(payload[0].value)}`}</p>
        <p className="label">{`Periode Sebelumnya: ${formatnumber(payload[1].value)}`}</p>
      </div>
    );
  }
  return null;
};
