/* eslint-disable no-unused-vars */
import React from "react";
import { WidgetPanel } from "../../../../../components/Molecules";

export default ({ className }) => {
  return (
    <div className={className}>
      <WidgetPanel className="h-100" size="md" title="Last Transaction"></WidgetPanel>
    </div>
  );
};
