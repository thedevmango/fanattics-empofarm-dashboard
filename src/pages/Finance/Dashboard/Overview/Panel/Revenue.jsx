/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FINANCE_CHANGE_VALUE } from "../../../../../redux/types/finance";
import { ResponsiveContainer, XAxis, Tooltip, LineChart, Line } from "recharts";
import { dummyLinearDaily, dummyLinearMonthly, dummyLinearWeekly } from "../../data";
import { WidgetPanel } from "../../../../../components/Molecules";
import { sumByKey } from "../../../../../configs/sumByKey";

export default ({ className = "" }) => {
  const dispatch = useDispatch();
  const { timeframe, dropdownOpen } = useSelector(({ dashboardFinance }) => {
    return {
      timeframe: dashboardFinance.timeframeRevenue,
      dropdownOpen: dashboardFinance.dropdownRevenue,
    };
  });

  const [data, setData] = useState([]);
  const [value, setValue] = useState(0);
  const [valueBefore, setValueBefore] = useState(0);
  const [percentage, setPercentage] = useState(0);

  const dataKey = (time) => {
    if (time === "1 Minggu") return ["Minggu Ini", "Minggu Lalu"];
    if (time === "1 Bulan") return ["Bulan Ini", "Bulan Lalu"];
    return ["Hari Ini", "Kemarin"];
  };

  const dropdownToggle = ({ payload }) => {
    let name = "dropdownRevenue";
    let value = !dropdownOpen;
    dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    if (payload) {
      let name = "timeframeRevenue";
      let value = payload;
      dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    }
  };

  useEffect(() => {
    if (timeframe === "Hari Ini") setData(dummyLinearDaily);
    if (timeframe === "1 Minggu") setData(dummyLinearWeekly);
    if (timeframe === "1 Bulan") setData(dummyLinearMonthly);
  }, [timeframe]);

  useEffect(() => {
    setValue(sumByKey(data, dataKey(timeframe)[0]));
    setValueBefore(sumByKey(data, dataKey(timeframe)[1]));
  }, [data, timeframe]);

  useEffect(() => {
    setPercentage(Math.round(((value - valueBefore) / valueBefore) * 100));
  }, [value, valueBefore]);

  return (
    <div className={`col ${className}`}>
      <WidgetPanel
        dropdownItems={["Hari Ini", "1 Minggu", "1 Bulan"]}
        dropdownOpen={dropdownOpen}
        dropdownToggle={dropdownToggle}
        size="sm"
        timeframe={timeframe}
        title="Revenue"
        trendPercentage={percentage}
        totalValue={value}
        type="priceunit"
      >
        <ResponsiveContainer width="100%" height={50}>
          <LineChart data={data} margin={{ top: 10, right: 0, left: 0, bottom: 0 }}>
            {/* <Tooltip /> */}
            <XAxis dataKey="time" hide />
            <Line
              type="monotone"
              dataKey={dataKey(timeframe)[0]}
              dot={false}
              strokeWidth={2}
              stroke="#4C84FF"
            />
            <Line
              type="monotone"
              dataKey={dataKey(timeframe)[1]}
              dot={false}
              strokeWidth={2}
              stroke="#979797"
              strokeDasharray="2 2"
            />
          </LineChart>
        </ResponsiveContainer>
      </WidgetPanel>
    </div>
  );
};
