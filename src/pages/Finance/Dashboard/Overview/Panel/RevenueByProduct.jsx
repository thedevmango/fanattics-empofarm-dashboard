/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { WidgetPanel } from "../../../../../components/Molecules";
import { FINANCE_CHANGE_VALUE } from "../../../../../redux/types/finance";
import { dummyRevenueDaily, dummyRevenueWeekly, dummyRevenueMonthly } from "../../data";
import { BodyText } from "../../../../../components/Typography";
import { formatquantity } from "../../../../../configs/formatnumber";
import Percentage from "../../../../../components/Molecules/Widget/Percentage";

export default ({ className }) => {
  const dispatch = useDispatch();
  const { timeframe, dropdownOpen } = useSelector(({ dashboardFinance }) => {
    return {
      timeframe: dashboardFinance.timeframeRevenueByProduct,
      dropdownOpen: dashboardFinance.dropdownRevenueByProduct,
    };
  });

  const [data, setData] = useState([]);

  const dropdownToggle = ({ payload }) => {
    let name = "dropdownRevenueByProduct";
    let value = !dropdownOpen;
    dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    if (payload) {
      let name = "timeframeRevenueByProduct";
      let value = payload;
      dispatch({ type: FINANCE_CHANGE_VALUE, payload: { name, value } });
    }
  };

  useEffect(() => {
    if (timeframe === "Hari Ini") setData(dummyRevenueDaily);
    if (timeframe === "Minggu Ini") setData(dummyRevenueWeekly);
    if (timeframe === "Bulan Ini") setData(dummyRevenueMonthly);
  }, [timeframe]);

  return (
    <div className={className}>
      <WidgetPanel
        dropdownItems={["Hari Ini", "Minggu Ini", "Bulan Ini"]}
        dropdownOpen={dropdownOpen}
        dropdownToggle={dropdownToggle}
        size="md"
        timeframe={timeframe}
        title="Revenue By Product"
      >
        <div className="widget-data panel-by-product mt-3">
          {data.map((item, idx) => (
            <DataItem key={idx} data={item} />
          ))}
        </div>
      </WidgetPanel>
    </div>
  );
};

const DataItem = ({ data }) => {
  const { name, value, valueBefore } = data;
  const percentage = Math.round(((value - valueBefore) / valueBefore) * 100);
  return (
    <div className="widget-data-item">
      <BodyText className="">{name}</BodyText>
      <BodyText>{`${formatquantity(value)}K`}</BodyText>
      <Percentage icon="arrow" value={percentage} />
    </div>
  );
};
