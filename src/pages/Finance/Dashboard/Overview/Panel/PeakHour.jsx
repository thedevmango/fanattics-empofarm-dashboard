/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { ResponsiveContainer, Tooltip, BarChart, XAxis, YAxis, CartesianGrid, Bar } from "recharts";
import { PageTitle, MetadataText } from "../../../../../components/Typography";
import Title from "../../../../../components/Molecules/Widget/Title";
import { Panel } from "../../../../../components/Panel";
import { dummyPeakHour } from "../../data";

export default ({ className = "" }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(dummyPeakHour);
  }, []);

  return (
    <div className={className}>
      <Panel padding="16" className="mol-widget mol-widget-panel pb-0">
        <div className="widget-header align-items-start">
          <Title size="sm">Peak Hour</Title>

          <div className="d-flex flex-column justify-content-end">
            <div className="d-flex align-items-end mr-1">
              <PageTitle color="#333333">{moment().format("H:mm")}</PageTitle>
              <MetadataText color="#333333">WIB</MetadataText>
            </div>
            <MetadataText color="#AEAEAE">Range Sep 9 ~ 27</MetadataText>
          </div>
        </div>

        <div className="body">
          <ResponsiveContainer width="100%" height={90}>
            <BarChart data={data} margin={{ top: 10, right: 0, left: -25, bottom: -10 }}>
              <defs>
                <linearGradient
                  id="colorPeak"
                  x1="0"
                  y1="0"
                  x2="0"
                  y2="100%"
                  spreadMethod="reflect"
                >
                  <stop offset="0" stopColor="#F57BB7" />
                  <stop offset="1" stopColor="#EFB486" />
                </linearGradient>
              </defs>
              <Tooltip />
              <CartesianGrid strokeOpacity={0.25} color="#F0F0F0" />
              <XAxis
                dataKey="time"
                tickFormatter={(cat) => cat[0] + cat[1]}
                domain={["dataMin", "dataMax"]}
              />
              <YAxis
                scale="linear"
                tickFormatter={(number) => {
                  if (number >= 1000) {
                    return (number / 1000).toString() + "k";
                  } else {
                    return number.toString();
                  }
                }}
                orientation="left"
              />
              <Bar dataKey="value" fill="url(#colorPeak)" />
            </BarChart>
          </ResponsiveContainer>
        </div>
      </Panel>
    </div>
  );
};
