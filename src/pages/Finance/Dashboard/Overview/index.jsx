/* eslint-disable no-unused-vars */
import React from "react";
import {
  PanelJumlahTransaksi,
  PanelGrossMargin,
  PanelRevenue,
  PanelTransaction,
  PanelLastTransaction,
  PanelRevenueByProduct,
  PanelCostByProduct,
  PanelPeakHour,
} from "./Panel";

export default (props) => {
  return (
    <div className="row content content-overview">
      <div className="row col-12 pr-0 mb-3">
        <PanelJumlahTransaksi className="col" />
        <PanelGrossMargin className="col" />
        <PanelRevenue className="col" />
        <PanelPeakHour className="col" />
      </div>

      <div className="row col-12 pr-0 mb-3">
        <PanelTransaction className="col" />
      </div>

      <div className="row col-4">
        <PanelRevenueByProduct className="col-12 mb-3" />
        <PanelCostByProduct className="col-12" />
      </div>
      <div className="row col-5">
        <PanelLastTransaction className="col" />
      </div>
    </div>
  );
};
