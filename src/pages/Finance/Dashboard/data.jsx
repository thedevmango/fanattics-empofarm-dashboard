/* eslint-disable no-unused-vars */
const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const qty = [1892, 2200, 2010, 1780, 2156];
const number = [4000, 3000, 2000, 2780, 1890, 2390, 3490];
const number2 = [9200, 8200, 7400, 6900, 6100];
const hari = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];

//* DATA AREA
export const dummyAreaDaily = () =>
  [...Array(24)].map((_, idx) => {
    return {
      time: `${idx + 1}.00`,
      Transaksi: randomizeData(qty),
      TransaksiBefore: randomizeData(qty),
    };
  });
export const dummyAreaWeekly = () =>
  [...Array(7)].map((_, idx) => {
    return {
      time: hari[idx],
      Transaksi: randomizeData(qty) * 7,
      TransaksiBefore: randomizeData(qty) * 7,
    };
  });
export const dummyAreaMonthly = () =>
  [...Array(30)].map((_, idx) => {
    return {
      time: idx + 1,
      Transaksi: randomizeData(qty) * randomizeData([31, 30, 29, 28]),
      timeBefore: idx + 1,
      TransaksiBefore: randomizeData(qty) * randomizeData([31, 30, 29, 28]),
    };
  });

//* DATA LINEAR
export const dummyLinearDaily = () =>
  [...Array(24)].map((_, idx) => {
    return {
      time: `${idx + 1}.00`,
      "Hari Ini": randomizeData(number) * randomizeData([300, 500]),
      Kemarin: randomizeData(number) * randomizeData([300, 500]),
    };
  });
export const dummyLinearWeekly = () =>
  [...Array(7)].map((_, idx) => {
    return {
      time: hari[idx],
      "Minggu Ini": randomizeData(number) * randomizeData([600, 800, 900]),
      "Minggu Lalu": randomizeData(number) * randomizeData([600, 800, 900]),
    };
  });
export const dummyLinearMonthly = () =>
  [...Array(30)].map((_, idx) => {
    return {
      time: idx + 1,
      "Bulan Ini": randomizeData(number) * randomizeData([1100, 1300, 1500]),
      "Bulan Lalu": randomizeData(number) * randomizeData([1100, 1300, 1500]),
    };
  });

//
//* DATA TRANSACTION
export const dummyRevenue = () =>
  [...Array(randomizeData([30, 25, 20]))].map((_, idx) => {
    return {
      time: idx + 1,
      "Periode Ini": randomizeData(number) * randomizeData([600, 800, 900]),
      "Periode Sebelumnya": randomizeData(number) * randomizeData([600, 800, 900]),
    };
  });
export const dummyProfit = () =>
  [...Array(randomizeData([30, 25, 20]))].map((_, idx) => {
    return {
      time: idx + 1,
      "Periode Ini": randomizeData(number) * randomizeData([600, 800, 900]),
      "Periode Sebelumnya": randomizeData(number) * randomizeData([600, 800, 900]),
    };
  });
export const dummyCost = () =>
  [...Array(randomizeData([30, 25, 20]))].map((_, idx) => {
    return {
      time: idx + 1,
      "Periode Ini": randomizeData(number) * randomizeData([600, 800, 900]),
      "Periode Sebelumnya": randomizeData(number) * randomizeData([600, 800, 900]),
    };
  });
export const dummyTrx = () =>
  [...Array(randomizeData([30, 25, 20]))].map((_, idx) => {
    return {
      time: idx + 1,
      "Periode Ini": randomizeData(number),
      "Periode Sebelumnya": randomizeData(number),
    };
  });

//
//* DATA REVENUE BY PRODUCT
const ArrProduct = [
  "Jagung Pipil",
  "Gabah",
  "Kelapa",
  "Beras",
  "Susu Sapi",
  "Kacang Tanah",
  "Madu Murni",
];
export const dummyRevenueDaily = ArrProduct.map((item, idx) => {
  return {
    id: idx + 1,
    name: item,
    value: randomizeData(number2),
    valueBefore: randomizeData(number2),
  };
});
export const dummyRevenueWeekly = ArrProduct.map((item, idx) => {
  return {
    id: idx + 1,
    name: item,
    value: randomizeData(number2) * 7,
    valueBefore: randomizeData(number2) * 7,
  };
});
export const dummyRevenueMonthly = ArrProduct.map((item, idx) => {
  return {
    id: idx + 1,
    name: item,
    value: randomizeData(number2) * 30,
    valueBefore: randomizeData(number2) * 30,
  };
});

//
//* DATA PEAK HOUR
export const dummyPeakHour = () =>
  [
    [16, "10.00"],
    [14, "10.30"],
    [17, "11.00"],
    [23, "11.30"],
    [20, "12.00"],
    [21, "12.30"],
    [17, "13.00"],
    [9, "13.30"],
    [27, "14.00"],
    [29, "14.30"],
    [23, "15.00"],
    [42, "15.30"],
    [35, "16.00"],
    [49, "16.30"],
    [55, "17.00"],
    [44, "17.30"],
    [31, "18.00"],
    [17, "18.30"],
    [22, "19.00"],
  ].map((item, idx) => {
    return {
      id: idx + 1,
      time: item[1],
      value: item[0] * 250,
      valueBefore: item[0] * randomizeData([250, 251, 249]),
    };
  });
