/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { useHistory, Route } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { FINANCE_RESET_DROPDOWN } from "../../../redux/types/finance";

import { PageTitle } from "../../../components/Typography";
import Tabs from "../../../components/Tabs";
import BusinessOverview from "./Overview";

export default (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const {
    dropdownCostByProduct,
    dropdownGrossMargin,
    dropdownJumlahTransaksi,
    dropdownPeakHour,
    dropdownRevenue,
    dropdownRevenueByProduct,
  } = useSelector(({ dashboardFinance }) => {
    return {
      dropdownCostByProduct: dashboardFinance.dropdownCostByProduct,
      dropdownGrossMargin: dashboardFinance.dropdownGrossMargin,
      dropdownJumlahTransaksi: dashboardFinance.dropdownJumlahTransaksi,
      dropdownPeakHour: dashboardFinance.dropdownPeakHour,
      dropdownRevenue: dashboardFinance.dropdownRevenue,
      dropdownRevenueByProduct: dashboardFinance.dropdownRevenueByProduct,
    };
  });

  const [isActive, setIsActive] = useState("business-overview");
  const resetDropdown = () => {
    if (
      dropdownCostByProduct ||
      dropdownGrossMargin ||
      dropdownJumlahTransaksi ||
      dropdownPeakHour ||
      dropdownRevenue ||
      dropdownRevenueByProduct
    ) {
      return dispatch({ type: FINANCE_RESET_DROPDOWN });
    }
    return null;
  };

  useEffect(() => {
    history.push(`${props.match.path}/${isActive}`);
  }, [isActive]);

  return (
    <div onClick={resetDropdown} className="finance finance-dashboard">
      <Helmet>
        <title>Dashboard | Finance</title>
      </Helmet>

      <PageTitle bold>Finance Dashboard</PageTitle>

      <div className="d-flex mb-5 justify-content-between align-items-end">
        <Tabs
          className="col-10 pl-0"
          options={["Business Overview", "Report Detail", "Forecasting"]}
          slug={["business-overview", "report-detail", "forecasting"]}
          activeTab={isActive}
          setActiveTab={setIsActive}
        />

        <div className="d-flex flex-column align-items-end col-2">
          <div>Last Update</div>
          <div>{moment().format("D/MMM/YYYY H:mm:ss")}</div>
        </div>
      </div>

      <Route path={`${props.match.path}/business-overview`} component={BusinessOverview} />
    </div>
  );
};
