import React from "react";
import { Collapse, TableCell, TableRow } from "../../../components";

interface Props {
  children: React.ReactNode;
  isOpen: boolean;
}

export default ({ children, isOpen }: Props) => {
  return (
    <TableRow>
      <TableCell colSpan={20} style={{ padding: 0 }}>
        <Collapse className="table-collapse" isOpen={isOpen}>
          <div className="table-collapse-content">{children}</div>
        </Collapse>
      </TableCell>
    </TableRow>
  );
};
