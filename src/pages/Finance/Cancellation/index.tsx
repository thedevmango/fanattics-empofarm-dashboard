/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import {
  BodyText,
  MetadataText,
  Pagination,
  PaneHeader,
  Panel,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  usePagination,
} from "../../../components";
import { getCancellationList } from "../../../redux/actions/cancellation";
import { CANCELLATION_TAB } from "../../../redux/types";
import RenderBodyTable from "./RenderBodyTable";

export default (props: any) => {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.cancellation.tab);
  const data = useSelector((state: any) => state.cancellation.list);

  //* LOCAL STATE
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [checkbox, setCheckbox] = useState({ purchases: true, sales: true });

  //* FUNCTION
  const toggleCheckbox = (name: string) => {
    if (checkbox[name]) return setCheckbox({ ...checkbox, [name]: false });
    setCheckbox({ ...checkbox, [name]: true });
  };

  const toggleTab = (selectedTab: any) => {
    dispatch({ type: CANCELLATION_TAB, payload: selectedTab });
  };

  useEffect(() => {
    if (searchQuery) {
      const results = data.filter((item: any) => {
        return item.transaction.transactionCode.toLowerCase().includes(searchQuery.toLowerCase());
      });
      return setFilteredData(results);
    } else if (checkbox.purchases && !checkbox.sales) {
      return setFilteredData(
        data.filter((item: any) => {
          return item.purchasingTransactionId !== null;
        })
      );
    } else if (!checkbox.purchases && checkbox.sales) {
      return setFilteredData(
        data.filter((item: any) => {
          return item.saleTransactionId !== null;
        })
      );
    } else if (!checkbox.purchases && !checkbox.sales) {
      return setFilteredData([]);
    } else {
      return setFilteredData(data);
    }
  }, [searchQuery, data, checkbox]);

  useEffect(() => {
    dispatch(getCancellationList({ isRejected: tab }));
  }, [dispatch, tab]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  // console.log("cancellation", data);
  return (
    <div className="finance finance-cancellation">
      <Helmet>
        <title>Pembatalan | Finance</title>
      </Helmet>
      <PaneHeader bold className="d-block">
        Pembatalan Transaksi
      </PaneHeader>
      <BodyText className="d-block mt-3 mb-4">
        Daftar permintaan pembatalan transaksi Sales dan Purchasing
      </BodyText>

      <div className="d-flex">
        <Tabs
          options={["Permintaan Pembatalan", "Transaksi Dibatalkan"]}
          slug={[false, true]}
          activeTab={tab}
          setActiveTab={toggleTab}
        />

        <div className="d-flex">
          <Checkbox
            id="cancel-purchases"
            name="purchases"
            value={checkbox.purchases}
            onChange={toggleCheckbox}
          >
            Purchases
          </Checkbox>

          <Checkbox id="cancel-sales" name="sales" value={checkbox.sales} onChange={toggleCheckbox}>
            Sales
          </Checkbox>

          <Searchbar
            placeholder="Cari Transaksi"
            className="ml-3"
            containerStyle={{ width: 250 }}
            searchQuery={searchQuery}
            setSearchQuery={setSearchQuery}
          />
        </div>
      </div>

      <Panel className="mt-3 content content-cancellation">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>&nbsp;</TableHeadCell>
              <TableHeadCell>Tanggal Permintaan</TableHeadCell>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Transaksi</TableHeadCell>
              <TableHeadCell>Status Transaksi</TableHeadCell>
              {!tab ? <TableHeadCell>Pembuat</TableHeadCell> : null}
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} data={currentData()} tab={tab} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const Checkbox = ({ children, id, name, onChange, value }) => {
  return (
    <div className="ml-3 form-check d-flex align-items-center">
      <input
        className="form-check-input"
        onChange={() => onChange(name)}
        type="checkbox"
        checked={value}
        value={value}
        name={name}
        id={id}
      />
      <label className="form-check-label" style={{ userSelect: "none" }} htmlFor={id}>
        <MetadataText>{children}</MetadataText>
      </label>
    </div>
  );
};
