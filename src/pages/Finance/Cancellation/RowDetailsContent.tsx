import React from "react";
import moment from "moment";
import { BodyText, Button, Table, TableBody, TableCell, TableRow } from "../../../components";
import { gray800 } from "../../../constants/Colors";
import { useHistory, useRouteMatch } from "react-router-dom";

interface Props {
  data: any;
  tab: boolean;
}

export default ({ data, tab }: Props) => {
  const history = useHistory();
  const match = useRouteMatch();

  const isSales = data.saleTransactionId !== null;;
  const customer = isSales
    ? `${data.transaction.buyer.user.fullName} (Buyer)`
    : `${data.transaction.farmer.user.fullName} (Farmer)`;
  const tanggal = moment(data.transaction.updatedAt).format("DD/MM/YYYY H:mm");
  const status = data.transaction.status;
  const requestAdmin = data.requestedByAdmin.fullName;
  const cancellationCaseCode = data.cancellationCaseCode;
  const remarks = data.remarks;
  const trxCreatedAt = moment(data.transaction.createdAt).format("DD MMMM YYYY");

  let itemInformation: string[] = [];
  if (isSales) {
    data.transaction.sale_transaction_details.map((val: any) =>
      itemInformation.push(
        `${val.product_sku.product_variant.subcategory.name} ${val.product_sku.product_variant.productName}`
      )
    );
  } else {
    itemInformation.push(
      `${data.transaction.product_variant.subcategory.name} ${data.transaction.product_variant.productName}`
    );
  }

  return (
    <Table>
      <TableBody>
        <TableRow style={{ verticalAlign: "top" }}>
          <TableCellEmpty />
          <TableCell style={{ width: "17.5%" }}>Customer</TableCell>
          <TableCell style={{ width: "25%" }}>{customer}</TableCell>
          <TableCell style={{ width: "15%" }}>Status Terakhir</TableCell>
          <TableCell style={{ width: "20%" }}>
            <div className="d-flex flex-column">
              <BodyText color={gray800}>{tanggal}</BodyText>
              <BodyText color={gray800}>{status}</BodyText>
            </div>
          </TableCell>
          <TableCell style={{ width: "30%" }}>
            <Button
              onClick={() => history.push(`${match.path}/details`, { data: data, tab: tab })}
              containerStyle={{ textDecorationColor: "#0078D4" }}
              className="ml-0"
              theme="link"
            >
              <BodyText color="#0078D4">Proses Transaksi</BodyText>
            </Button>
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCellEmpty />
          <TableCell style={{ width: "17.5%" }}>Informasi Item</TableCell>
          <TableCell style={{ width: "25%" }}>
            <div className="d-flex flex-column">
              {itemInformation.map((val, i) => (
                <BodyText key={i} color={gray800}>
                  {val}
                </BodyText>
              ))}
            </div>
          </TableCell>
          <TableCell style={{ width: "15%" }}>Open Case ID</TableCell>
          <TableCell colSpan={2}>
            <div className="d-flex flex-column">
              <BodyText color="#00B294">{cancellationCaseCode}</BodyText>
              <BodyText color={gray800}>{`oleh ${requestAdmin}`}</BodyText>
            </div>
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCellEmpty />
          <TableCell style={{ width: "17.5%" }}>Alasan Pembatalan</TableCell>
          <TableCell style={{ width: "25%" }}>{remarks}</TableCell>
          <TableCell style={{ width: "15%" }}>Tanggal Pembuatan Transaksi</TableCell>
          <TableCell colSpan={2}>{trxCreatedAt}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
};

const TableCellEmpty = () => (
  <TableCell style={{ width: "2.5%" }}>
    <div style={{ width: 16 }} />
  </TableCell>
);
