import React, { Fragment, useState } from "react";
import moment from "moment";
import { BodyText, Button, MetadataText, TableCell, TableRow } from "../../../components";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";
import { gray500, gray800 } from "../../../constants/Colors";
import RowDetails from "./RowDetails";
import RowDetailsContent from "./RowDetailsContent";
import { useHistory, useRouteMatch } from "react-router-dom";

interface Props {
  data: any;
  tab: boolean;
}

export default ({ data, tab }: Props) => {
  const history = useHistory();
  const match = useRouteMatch();

  const [isOpen, setIsOpen] = useState<any>({});
  const toggleOpen = (idx: number) => {
    if (isOpen[idx]) return setIsOpen({ ...isOpen, [idx]: false });
    setIsOpen({ ...isOpen, [idx]: true });
  };

  return !data
    ? null
    : data.map((item: any, i: number) => {
        const tanggal = moment(item.createdAt).format("DD/MM/YYYY");
        const jam = moment(item.createdAt).format("H:mm");
        const nextClick = () => history.push(`${match.path}/details`, { data: item, tab: tab });
        
        return (
          <Fragment key={i}>
            <TableRow>
              <TableCell
                onClick={() => toggleOpen(item.id)}
                style={{ width: "2.5%", borderLeftColor: "transparent" }}
              >
                <Button theme="action-table">
                  {isOpen[item.id] ? <BsChevronUp /> : <BsChevronDown />}
                </Button>
              </TableCell>

              <TableCell onClick={nextClick} style={{ width: "17.5%" }}>
                <div className="d-flex flex-column">
                  <BodyText color={gray800}>{tanggal}</BodyText>
                  <MetadataText color={gray500}>{jam}</MetadataText>
                </div>
              </TableCell>

              <TableCell onClick={nextClick} style={{ width: "25%" }}>
                {item.transaction.transactionCode}
              </TableCell>

              <TableCell onClick={nextClick} style={{ width: "15%" }}>
                {item.saleTransactionId ? "Sales" : "Purchases"}
              </TableCell>

              <TableCell onClick={nextClick} style={{ width: "20%" }}>
                {item.transaction.status}
              </TableCell>

              {!tab ? (
                <TableCell onClick={nextClick} style={{ width: "30%" }}>
                  <div className="d-flex flex-column">
                    <BodyText color={gray800}>{item.requestedByAdmin.fullName}</BodyText>
                    <BodyText color={gray800}>{`(${item.requestedByAdmin.role})`}</BodyText>
                  </div>
                </TableCell>
              ) : null}
            </TableRow>

            <RowDetails isOpen={isOpen[item.id]}>
              <RowDetailsContent tab={tab} data={item} />
            </RowDetails>
          </Fragment>
        );
      });
};
