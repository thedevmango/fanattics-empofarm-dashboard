/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { BodyText } from "../../../../components";
import { Events, Link, scrollSpy } from "react-scroll";
import Content from "./Content";
import { formatDate } from "../../../../configs/formatdate";

export default (props: any) => {
  const tab = props.location.state.tab;
  const parentData = props.location.state.data;
  const isSales = parentData.saleTransactionId !== null;
  const isIQCReports = parentData.transaction.iqc_reports;

  useEffect(() => {
    Events.scrollEvent.register("begin", (to, element) => console.log("begin", to, element));
    Events.scrollEvent.register("end", (to, element) => console.log("end", to, element));
    scrollSpy.update();
    return () => {
      Events.scrollEvent.remove("begin");
      Events.scrollEvent.remove("end");
    };
  });

  useEffect(() => window.scrollTo(0, 0), []);

  console.log(parentData);
  return (
    <div className="finance finance-cancellation-details">
      <Helmet>
        <title>Cancellation Details</title>
      </Helmet>

      <div className="body">
        <div className="content content-left">
          <Link
            className="menu-list-item"
            activeClass="active"
            to="ringkasan"
            spy={true}
            smooth={true}
            offset={-60}
          >
            Ringkasan Transaksi
          </Link>

          <Link
            className="menu-list-item"
            activeClass="active"
            to="alasan"
            spy={true}
            smooth={true}
            offset={-60}
          >
            Alasan Pembatalan
          </Link>

          {isSales ? (
            <Link
              className="menu-list-item"
              activeClass="active"
              to="pembayaran"
              spy={true}
              smooth={true}
              offset={-60}
            >
              Informasi Pembayaran
            </Link>
          ) : null}

          <Link
            className="menu-list-item"
            activeClass="active"
            to="pembelian"
            spy={true}
            smooth={true}
            offset={-60}
          >
            Informasi Pembelian
          </Link>

          {!isSales ? (
            <Link
              className="menu-list-item"
              activeClass="active"
              to="tender"
              spy={true}
              smooth={true}
              offset={-60}
            >
              Informasi Produk Tender
            </Link>
          ) : null}

          {!isSales && isIQCReports ? (
            <Link
              className="menu-list-item"
              activeClass="active"
              to="quality-control"
              spy={true}
              smooth={true}
              offset={-60}
            >
              Informasi Quality Control
            </Link>
          ) : null}
        </div>

        <Content parentData={parentData} tab={tab} />

        <div className="content content-right">
          <div className="header">
            <BodyText bold>Tanggal Pembuatan Transaksi</BodyText>
            <BodyText>{formatDate(parentData.transaction.createdAt, "DD MMMM YYYY")}</BodyText>
          </div>
        </div>
      </div>
    </div>
  );
};
