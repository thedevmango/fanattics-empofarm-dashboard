import React from "react";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { SubjectTitle } from "../../../../../components";
import { gray600 } from "../../../../../constants/Colors";

export default ({ children }) => {
  return (
    <section id="alasan" className="d-flex flex-column border-bottom border-black p-4">
      <SubjectTitle color={gray600} className="mb-3 d-flex align-items-center" bold>
        <AiOutlineInfoCircle size={20} className="mr-2" />
        Alasan Pembatalan
      </SubjectTitle>

      <SubjectTitle>{children}</SubjectTitle>
    </section>
  );
};
