import React from "react";
import { BodyText, MetadataText, SubjectTitle } from "../../../../../components";
import { formatquantity } from "../../../../../configs/formatnumber";
import { gray600, gray800 } from "../../../../../constants/Colors";

export default ({ parentData, tab }) => {
  const isSales = parentData.saleTransactionId !== null;

  return (
    <div>
      <SubjectTitle color={gray600} className="mb-3" bold>
        Ringkasan Pembatalan
      </SubjectTitle>

      <ItemList title="Open Case ID">
        <BodyText color="#00B294">{parentData.cancellationCaseCode}</BodyText>
        <BodyText color={gray800}>Oleh {parentData.requestedByAdmin.fullName}</BodyText>
      </ItemList>

      <ItemList title="Nama Pembeli">
        <BodyText color={gray800}>
          {isSales
            ? parentData.transaction.buyer.user.fullName
            : parentData.transaction.farmer.user.fullName}
        </BodyText>
      </ItemList>

      <ItemList title="Lokasi Pembeli">
        <BodyText color={gray800}>
          {isSales
            ? parentData.transaction.buyer.user.address
            : parentData.transaction.farmer.user.address}
        </BodyText>
      </ItemList>

      <ItemList title="Nominal Transaksi">
        <BodyText color={gray800}>Rp {formatquantity(parentData.transaction.totalPrice)}</BodyText>
      </ItemList>

      <ItemList title="Jumlah SKU">
        <BodyText color={gray800}>
          {isSales ? `${parentData.transaction.sale_transaction_details.length} SKU` : "1 SKU"}
        </BodyText>
      </ItemList>
    </div>
  );
};

const ItemList = ({ title, children }) => {
  return (
    <div className="row mb-3">
      <div className="col-3">
        <MetadataText color={gray800} bold>
          {title}
        </MetadataText>
      </div>
      <div className="col-9 d-flex flex-column">{children}</div>
    </div>
  );
};
