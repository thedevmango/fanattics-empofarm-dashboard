import Axios from "axios";
import React, { useEffect, useState } from "react";
import { BodyText, MetadataText, SubjectTitle } from "../../../../../components";
import { formatquantity } from "../../../../../configs/formatnumber";
import { API_URL } from "../../../../../constants";
import { gray500, gray800 } from "../../../../../constants/Colors";

export default ({ parentData }) => {
  const productName = `${parentData.transaction.product_variant.subcategory.name} ${parentData.transaction.product_variant.productName}`;
  const iqc_reports =
    parentData.transaction.iqc_reports[parentData.transaction.iqc_reports.length - 1];
  const offerQuantity = parentData.transaction.quantity;
  const offerQuality = parentData.transaction.product_variant.productName;

  const [iqcOfficer, setIqcOfficer] = useState<any>(undefined);
  const getQCOfficer = (id: number) => {
    Axios.get(`${API_URL}/users/admins?id=${id}`)
      .then((res) => setIqcOfficer(res.data.result[0]))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getQCOfficer(parentData.transaction.iqcOfficerId);
  }, [parentData.transaction.iqcOfficerId]);

  if (!iqcOfficer) return null;
  return (
    <section id="quality-control" className="border-bottom border-black p-4">
      <SubjectTitle className="mb-4" bold>
        Informasi Quality Control
      </SubjectTitle>

      <div className="d-flex mb-3">
        <SubjectTitle color={gray800} bold>
          {productName}
        </SubjectTitle>

        {iqc_reports.status === "accept" ? (
          <div className="badge-qc">QC Diterima</div>
        ) : (
          <div className="badge-qc-reject">QC Ditolak</div>
        )}
      </div>

      <div className="row">
        <QCReport
          type="Quantity"
          iqcOfficer={iqcOfficer.fullName}
          offer={`${formatquantity(offerQuantity)} Kg`}
          actual={`${formatquantity(iqc_reports.quantity)} Kg`}
        />

        <QCReport
          type="Quality"
          iqcOfficer={iqcOfficer.fullName}
          offer={offerQuality}
          actual={iqc_reports.quality}
        />
      </div>
    </section>
  );
};

const QCReport = ({
  actual,
  iqcOfficer,
  offer,
  type,
}: {
  actual: string;
  iqcOfficer: string;
  offer: string;
  type: "Quality" | "Quantity";
}) => {
  return (
    <div className="col-6 row">
      <div className="col-12">
        <BodyText color={gray500} bold>
          {type}
        </BodyText>
      </div>
      <div className="col-12">
        <MetadataText color={gray800}>Signed by {iqcOfficer}</MetadataText>
      </div>
      <div className="col-6">
        <BodyText color={gray500} bold>
          Offer
        </BodyText>
      </div>
      <div className="col-6">
        <BodyText color={gray800}>{offer}</BodyText>
      </div>
      <div className="col-6">
        <BodyText color={gray500} bold>
          Actual
        </BodyText>
      </div>
      <div className="col-6">
        <BodyText color={gray800}>{actual}</BodyText>
      </div>
    </div>
  );
};
