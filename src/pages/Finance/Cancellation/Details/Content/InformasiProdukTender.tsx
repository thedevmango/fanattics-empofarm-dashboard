import Axios from "axios";
import React, { useEffect, useState } from "react";
import { BodyText, Button, SubjectTitle } from "../../../../../components";
import { formatquantity } from "../../../../../configs/formatnumber";
import { API_URL } from "../../../../../constants";
import { gray500, gray600, gray800 } from "../../../../../constants/Colors";

export default ({ parentData }) => {
  const productName = `${parentData.transaction.product_variant.subcategory.name} ${parentData.transaction.product_variant.productName}`;
  const quantity = `${formatquantity(parentData.transaction.quantity)} Kg`;

  const [productSku, setProductSku] = useState<any>(undefined);
  const fetchSKU = (id: number) => {
    Axios.get(`${API_URL}/products/skus?id=${id}`)
      .then((res) => setProductSku(res.data.result[0]))
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetchSKU(parentData.transaction.productSkuId);
  }, [parentData.transaction.productSkuId]);

  if (!productSku) return null;
  return (
    <section id="tender" className="border-bottom border-black p-4">
      <div className="d-flex mb-4 align-items-center justify-content-between">
        <SubjectTitle color={gray600} bold>
          Informasi Produk Tender
        </SubjectTitle>

        <div>
          <Button containerStyle={{ textDecorationColor: "#0078D4" }} theme="link">
            <BodyText color="#0078D4">Lihat Detail</BodyText>
          </Button>
        </div>
      </div>

      <SubjectTitle className="mb-3" color={gray800} bold>
        {productName}
      </SubjectTitle>

      <ProductItemList title="SKU">{productSku.sku}</ProductItemList>
      <ProductItemList title="Farmer">
        {parentData.transaction.farmer.user.fullName}
      </ProductItemList>
      <ProductItemList title="Kuantitas">{quantity}</ProductItemList>
    </section>
  );
};

const ProductItemList = ({ title, children }) => {
  return (
    <div className="row mb-2">
      <div className="col-3">
        <BodyText color={gray500} bold>
          {title}
        </BodyText>
      </div>
      <div className="col-9 d-flex flex-column">
        <BodyText color={gray600}>{children}</BodyText>
      </div>
    </div>
  );
};
