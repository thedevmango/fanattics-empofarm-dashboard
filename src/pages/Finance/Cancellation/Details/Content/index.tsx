/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from "react";
import { BackButton, Button, HeaderText, SubjectTitle } from "../../../../../components";
import PopupKonfirmasi from "../PopupKonfirmasi";
import PopupTolak from "../PopupTolak";
import AlasanPembatalan from "./AlasanPembatalan";
import InformasiPembayaran from "./InformasiPembayaran";
import InformasiPembelian from "./InformasiPembelian";
import InformasiProdukTender from "./InformasiProdukTender";
import InformasQC from "./InformasQC";
import RingkasanPembatalan from "./RingkasanPembatalan";

//TODO: FIX STYLING BADGE QC DITOLAK

export default ({ parentData, tab }: { parentData: any; tab: boolean }) => {
  const isSales = parentData.saleTransactionId !== null;
  const isIQCReports = parentData.transaction.iqc_reports;

  const [popupTolak, setPopupTolak] = useState(false);
  const [popupOpen, setPopupOpen] = useState(false);

  const toggleTolak = () => setPopupTolak(!popupOpen);
  const togglePopup = () => setPopupOpen(!popupOpen);

  return (
    <div className="content content-main">
      <PopupKonfirmasi onCancel={togglePopup} data={parentData} isOpen={popupOpen} />
      <PopupTolak onCancel={toggleTolak} data={parentData} isOpen={popupTolak} />

      <section id="ringkasan" className="border-bottom border-black p-4">
        <div className="header">
          <div className="d-flex flex-column">
            <BackButton className="pl-0 d-flex mr-auto">Kembali ke Pembatalan Transaksi</BackButton>

            <div>
              <HeaderText className="mt-2">
                {isSales ? "Pembatalan Sales Transaction" : "Pembatalan Purchasing Transaction"}
              </HeaderText>
            </div>
          </div>

          {!tab ? (
            <div className="button-wrapper">
              <Button theme="secondary" onClick={toggleTolak} bold>
                Tolak
              </Button>
              <Button theme="primary" onClick={togglePopup}>
                Setujui
              </Button>
            </div>
          ) : (
            <Button className="d-flex mt-auto" theme="secondary" bold>
              Download Laporan
            </Button>
          )}
        </div>

        <RingkasanPembatalan parentData={parentData} tab={tab} />
      </section>

      <AlasanPembatalan>{parentData.remarks}</AlasanPembatalan>

      {isSales ? <InformasiPembayaran parentData={parentData} /> : null}

      <InformasiPembelian parentData={parentData} />

      {!isSales ? <InformasiProdukTender parentData={parentData} /> : null}

      {isIQCReports ? <InformasQC parentData={parentData} /> : null}
    </div>
  );
};
