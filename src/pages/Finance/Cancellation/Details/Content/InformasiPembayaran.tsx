import React from "react";
import { IconMoney } from "../../../../../assets/Icons";
import { BodyText, Button, MetadataText, SubjectTitle } from "../../../../../components";
import { formatDate } from "../../../../../configs/formatdate";
import { gray600, gray800 } from "../../../../../constants/Colors";

export default ({ parentData }) => {
  return (
    <section id="pembayaran" className="border-bottom border-black p-4">
      <div className="d-flex align-items-center justify-content-between">
        <SubjectTitle color={gray600} className="mb-3" bold>
          <IconMoney size={24} />
          Informasi Pembayaran
        </SubjectTitle>

        <div>
          <Button containerStyle={{ textDecorationColor: "#0078D4" }} theme="link">
            <BodyText color="#0078D4">Lihat Detail</BodyText>
          </Button>
        </div>
      </div>

      <ItemList title="Metode Pembayaran">
        <BodyText color={gray800}>{parentData.transaction.paymentMethod}</BodyText>
      </ItemList>

      <ItemList title="Batas Waktu Pembayaran">
        <BodyText color={gray800}>
          {parentData.transaction.dueDate
            ? formatDate(parentData.transaction.dueDate, "DD/MM/YYYY H:mm")
            : "#N/A"}
        </BodyText>
      </ItemList>
    </section>
  );
};

const ItemList = ({ title, children }) => {
  return (
    <div className="row mb-1">
      <div className="col-3">
        <MetadataText color={gray800} bold>
          {title}
        </MetadataText>
      </div>
      <div className="col-9 d-flex flex-column">{children}</div>
    </div>
  );
};
