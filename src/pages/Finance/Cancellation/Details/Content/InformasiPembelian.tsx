import React from "react";
import { BodyText, MetadataText, SubjectTitle } from "../../../../../components";
import { formatquantity } from "../../../../../configs/formatnumber";
import { gray500, gray600, gray800 } from "../../../../../constants/Colors";

export default ({ parentData }) => {
  const isSales = parentData.saleTransactionId !== null;

  return (
    <section id="pembelian" className="border-bottom border-black p-4">
      <div className={`list-item${isSales ? "-sales" : ""}`}>
        <SubjectTitle color={gray600} bold>
          Informasi Pembelian
        </SubjectTitle>
      </div>

      <div className={`product-wrapper${isSales ? "-sales" : ""}`}>
        {isSales ? (
          parentData.transaction.sale_transaction_details.map((item: any, i: number) => {
            return <ProductItem item={item} key={i} />;
          })
        ) : (
          <InfoPurchasing item={parentData} />
        )}
      </div>
    </section>
  );
};

const ProductItem = ({ item }) => {
  const productName = `${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`;
  const packaging = `${item.packagingUnit} (${item.packagingQuantity}Kg)`;
  return (
    <div className="list-item-sales">
      <SubjectTitle className="mb-3" color={gray800} bold>
        {productName}
      </SubjectTitle>

      <ProductItemList title="SKU">{item.product_sku.sku}</ProductItemList>
      <ProductItemList title="Farmer">{item.product_sku.farmer.user.fullName}</ProductItemList>
      <ProductItemList title="Packaging">{packaging}</ProductItemList>
      <ProductItemList title="Kuantitas Pembelian">
        {formatquantity(item.quantity)} Kilogram
      </ProductItemList>
      <ProductItemList title="Nominal Pembelian">
        Rp {formatquantity(item.salePrice * item.quantity)}
      </ProductItemList>
      <ProductItemList title="Biaya Packaging">&ndash;</ProductItemList>
    </div>
  );
};

const ProductItemList = ({ title, children }) => {
  return (
    <div className="row mb-2">
      <div className="col-3">
        <BodyText color={gray500} bold>
          {title}
        </BodyText>
      </div>
      <div className="col-9 d-flex flex-column">
        <BodyText color={gray600}>{children}</BodyText>
      </div>
    </div>
  );
};

const InfoPurchasing = ({ item }) => {
  const trxType = item.transaction.type === "DS" ? "Direct Selling" : "Konsinyasi";
  const hargaTenderAwal = `Rp ${formatquantity(item.transaction.totalPrice)}`;
  const totalSalePrice = item.transaction.salePricePerUnit * item.transaction.quantity;
  const hargaSetelahNego = totalSalePrice ? `Rp ${formatquantity(totalSalePrice)}` : "-";
  return (
    <>
      <ItemList title="Bentuk Transaksi">{trxType}</ItemList>
      <ItemList title="Harga Tender Awal">{hargaTenderAwal}</ItemList>
      <ItemList title="Harga Setelah Negosiasi">{hargaSetelahNego}</ItemList>
    </>
  );
};

const ItemList = ({ title, children }) => {
  return (
    <div className="row mb-3">
      <div className="col-3">
        <MetadataText color={gray800} bold>
          {title}
        </MetadataText>
      </div>
      <div className="col-9 d-flex flex-column">
        <BodyText color={gray600}>{children}</BodyText>
      </div>
    </div>
  );
};
