import React from "react";
import { AiOutlineInfoCircle } from "react-icons/ai";
import { IoMdClose } from "react-icons/io";
import { Modal, ModalBody, HeaderText, Button, BodyText } from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";
import { gray500, gray600, gray800 } from "../../../../constants/Colors";

interface Props {
  data: any;
  isOpen: boolean;
  onCancel?: () => any;
  onSubmit?: () => any;
}

export default ({ data, isOpen, onCancel, onSubmit }: Props) => {
  const isSales = data.saleTransactionId !== null;
  const purchasingItemName = () => {
    return `${data.transaction.product_variant.subcategory.name} ${data.transaction.product_variant.productName}`;
  };
  const salesItemName = (item: any) => {
    return `${item.product_sku.product_variant.subcategory.name} ${item.product_sku.product_variant.productName}`;
  };

  return (
    <Modal isOpen={isOpen} size="lg" toggle={onCancel}>
      <ModalBody className="konfirmasi-pembatalan">
        <div className="header">
          <HeaderText color={gray600} className="d-flex flex-grow-1 align-items-center" bold>
            <AiOutlineInfoCircle color="#E05454" size={20} className="mr-2" />
            {isSales ? "Konfirmasi Pembatalan Sales" : "Konfirmasi Pembatalan Purchasing"}
          </HeaderText>

          <Button onClick={onCancel} containerStyle={{ padding: 5 }} theme="icon-only">
            <IoMdClose size={14} />
          </Button>
        </div>

        <div className="body">
          <ItemList title="Kode Transaksi">
            <BodyText color={gray800}>{data.transaction.transactionCode}</BodyText>
          </ItemList>
          <ItemList title="Open Case ID">
            <BodyText color="#00B294">{data.cancellationCaseCode}</BodyText>
            <BodyText color={gray800}>Oleh {data.requestedByAdmin.fullName}</BodyText>
          </ItemList>
          <ItemList title="Nama Pembeli">
            <BodyText color={gray800}>
              {isSales
                ? data.transaction.buyer.user.fullName
                : data.transaction.farmer.user.fullName}
            </BodyText>
          </ItemList>
          <ItemList title="Informasi Item">
            {isSales ? (
              data.transaction.sale_transaction_details.map((item: any, i: number) => (
                <ProductItem title={salesItemName(item)} key={i}>
                  {formatquantity(item.quantity)} Kg
                </ProductItem>
              ))
            ) : (
              <ProductItem title={purchasingItemName()}>
                {formatquantity(data.transaction.quantity)} Kg
              </ProductItem>
            )}
          </ItemList>
          <ItemList title="Nilai Transaksi">
            <BodyText color={gray800}>Rp {formatquantity(data.transaction.totalPrice)}</BodyText>
          </ItemList>
        </div>

        <div className="button-wrapper">
          <Button theme="danger">Setujui Permintaan</Button>
          <Button onClick={onCancel} theme="secondary" bold>
            Kembali
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

const ProductItem = ({ children, title }) => {
  return (
    <div className="d-flex align-items-center">
      <BodyText color={gray600} style={{ width: 200 }}>
        {title}
      </BodyText>
      <BodyText color={gray500} style={{ marginLeft: 5 }}>
        {children}
      </BodyText>
    </div>
  );
};

const ItemList = ({ title, children }) => {
  return (
    <div className="row mb-3">
      <div className="col-3">
        <BodyText color={gray800} bold>
          {title}
        </BodyText>
      </div>
      <div className="col-9 d-flex flex-column justify-content-center">{children}</div>
    </div>
  );
};
