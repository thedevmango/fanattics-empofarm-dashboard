import React from "react";
import { Button, Modal, ModalBody, SubjectTitle } from "../../../../components";

interface Props {
  data: any;
  isOpen: boolean;
  onCancel: () => any;
}

export default function PopupTolak({ data, isOpen, onCancel }: Props) {
  const handleSubmit = () => {
    return null;
  };

  return (
    <Modal isOpen={isOpen} toggle={onCancel}>
      <ModalBody className="konfirmasi-tolak-pembatalan">
        <SubjectTitle bold>Yakin tolak pembatalan transaksi?</SubjectTitle>

        <div className="button-wrapper d-flex align-items-center justify-content-around mt-4">
          <Button onClick={handleSubmit} theme="danger">
            Ya
          </Button>
          <Button onClick={onCancel} theme="secondary">
            Tidak
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
}
