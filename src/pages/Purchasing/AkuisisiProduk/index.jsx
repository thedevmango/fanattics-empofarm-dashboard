/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { SegmentedTitle } from "../../../components/Molecules";
import AcquisitionRequest from "./AcquisitionRequest";
import Mutasi from "./MutasiKonsinyasi";

export default (props) => {
  const [activeTitle, setActiveTitle] = useState("Product Acquisition Request (PAR)");
  const toggleTitle = ({ value }) => setActiveTitle(value);

  return (
    <div className="purchasing purchasing-product-acquisition-request">
      <SegmentedTitle
        {...props}
        titles={["Product Acquisition Request (PAR)", "Mutasi Konsinyasi"]}
        activeTitle={activeTitle}
        onClick={toggleTitle}
      />

      {activeTitle === "Product Acquisition Request (PAR)" ? (
        <AcquisitionRequest {...props} />
      ) : (
        <Mutasi {...props} />
      )}
    </div>
  );
};
