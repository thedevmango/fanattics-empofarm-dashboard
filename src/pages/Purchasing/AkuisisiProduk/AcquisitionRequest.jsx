/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { VscCalendar } from "react-icons/vsc";
import { formatnumber } from "../../../configs/formatnumber";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../components/Table";
import { Pagination, usePagination } from "../../../components/Pagination";
import { BodyText, MetadataText } from "../../../components/Typography";
import { Form, Input } from "../../../components/Form";
import Searchbar from "../../../components/Searchbar";
import { Panel } from "../../../components/Panel";
import Tabs from "../../../components/Tabs";

export default (props) => {
  const [dummyData, setDummyData] = useState([]);
  const [data, setData] = useState([]);
  const [isActive, setIsActive] = useState("Acquisition Request");
  const [searchQuery, setSearchQuery] = useState("");
  const [date, setDate] = useState(moment().format("D MMMM YYYY"));
  const [inputDateType, setInputDateType] = useState("text");

  const toggleDate = (date) => {
    setDate(date);
    setTimeout(() => document.activeElement.blur());
  };

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: data,
    itemsPerPage: 10,
  });

  useEffect(() => {
    if (!dummyData.length) return;
    if (isActive === "Acquisition Request") {
      setData(dummyData.filter((val) => val.tenderStatus === "onRequest"));
    }
    if (isActive === "On Process") {
      setData(dummyData.filter((val) => val.tenderStatus === "onProcess"));
    }
    if (isActive === "Completed") {
      setData(dummyData.filter((val) => val.tenderStatus === "done"));
    }
    setPage(1);
  }, [isActive, dummyData]);

  useEffect(() => {
    setTimeout(() => setDummyData(dummyDataTable), 1000);
  }, [isActive]);

  const RenderBodyTable = ({ data }) => {
    if (!data) return <EmptyDataRow colSpan={7} />;
    return data.map((item, idx) => {
      return (
        <TableRow key={idx}>
          <TableCell>
            <BodyText>{item.transactionCode}</BodyText>
          </TableCell>
          <TableCell>
            <BodyText>{item.productName}</BodyText>
            <br />
            <MetadataText color="#6E6E6E">{item.category}</MetadataText>
          </TableCell>
          <TableCell>
            <BodyText>{item.farmerName}</BodyText>
          </TableCell>
          <TableCell>
            <BodyText>{item.storageLocation}</BodyText>
            <br />
            <MetadataText color="#6E6E6E">{item.storageArea}</MetadataText>
          </TableCell>
          <TableCell>
            <BodyText>{`${formatnumber(item.quantity)} ${item.unit}`}</BodyText>
          </TableCell>

          {isActive === "Acquisition Request" ? null : (
            <>
              <TableCell>
                <BodyText>{formatnumber(item.negoPrice)}</BodyText>
              </TableCell>
              {isActive === "On Process" ? (
                <TableCell>
                  <BodyText>{item.negoStatus}</BodyText>
                </TableCell>
              ) : (
                <TableCell>
                  <BodyText color={item.productStatus === "Berhasil" ? "#2F9F28" : "#D92C2C"}>
                    {item.productStatus}
                  </BodyText>
                </TableCell>
              )}
            </>
          )}
        </TableRow>
      );
    });
  };

  return (
    <div>
      <Helmet>
        <title>Product Acquisition Request | Purchasing</title>
      </Helmet>

      <div className="mb-3">
        <Tabs
          options={["Acquisition Request", "On Process", "Completed"]}
          counter={[10, 15, 5]}
          activeTab={isActive}
          setActiveTab={setIsActive}
        />
      </div>

      <div className="d-flex mb-3 justify-content-between">
        <Form className="col-3 pl-0 input-date">
          <Input
            value={date}
            onChange={(e) => toggleDate(moment(e.target.value).format("D MMMM YYYY"))}
            prepend={<VscCalendar size={18} />}
            type={inputDateType === "date" ? "date" : "text"}
            onFocus={() => setInputDateType("date")}
            onBlur={() => setInputDateType("text")}
          />
        </Form>

        <Searchbar
          placeholder="Pencarian"
          className="col-2"
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />
      </div>

      <Panel className="content content-product-acquisition-request">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Nama Produk</TableHeadCell>
              <TableHeadCell>Nama Farmer</TableHeadCell>
              <TableHeadCell>Lokasi Penyimpanan</TableHeadCell>
              <TableHeadCell>Kuantitas</TableHeadCell>
              {isActive === "Acquisition Request" ? null : (
                <>
                  <TableHeadCell>Harga Negosiasi (Rp)</TableHeadCell>
                  {isActive === "On Process" ? (
                    <TableHeadCell>Status Negosiasi</TableHeadCell>
                  ) : (
                    <TableHeadCell>Keterangan</TableHeadCell>
                  )}
                </>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable data={currentData()} />
          </TableBody>
        </Table>

        <div className="pagination-wrapper my-3">
          <Pagination
            next={next}
            prev={prev}
            jump={jump}
            pages={pages}
            currentPage={currentPage}
            maxPage={maxPage}
          />
        </div>
      </Panel>
    </div>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <td colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </td>
    </TableRow>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const storageLoc = ["RDC A", "RDC B", "RDC C", "RDC 1", "RDC 2", "RDC 3", "RDC 4", "RDC 5"];
const dummyQuantity = [2000, 200, 10000, 150000, 1500, 20000];
const dummyPrice = [75000, 3000, 120000, 135000, 10000];
const farmerName = [
  "Joko Tole",
  "Bessie Cooper",
  "Cameron Williamson",
  "Kristin Watson",
  "Cody Fisher",
  "Marvin McKinney",
];
const dummyDataTable = [
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 1", "Liquid", "Liter"],
  ["Madu Hutan Grade 2", "Liquid", "Liter"],
  ["Madu Hutan Grade 2", "Liquid", "Liter"],
  ["Madu Hutan Grade 2", "Liquid", "Liter"],
  ["Madu Hutan Grade 3", "Liquid", "Liter"],
  ["Madu Hutan Grade 3", "Liquid", "Liter"],
  ["Madu Hutan Grade 3", "Liquid", "Liter"],
  ["Madu Hutan Grade 3", "Liquid", "Liter"],
  ["Madu Hutan Grade 4", "Liquid", "Liter"],
  ["Madu Hutan Grade 4", "Liquid", "Liter"],
  ["Madu Hutan Grade 4", "Liquid", "Liter"],
  ["Madu Hutan Grade 4", "Liquid", "Liter"],
  ["Madu Hutan Grade 4", "Liquid", "Liter"],

  ["Beras Putih Grade 1", "Hard", "Kilogram"],
  ["Beras Putih Grade 1", "Hard", "Kilogram"],
  ["Beras Putih Grade 1", "Hard", "Kilogram"],
  ["Beras Putih Grade 1", "Hard", "Kilogram"],
  ["Beras Putih Grade 1", "Hard", "Kilogram"],
  ["Beras Putih Grade 2", "Hard", "Kilogram"],
  ["Beras Putih Grade 2", "Hard", "Kilogram"],
  ["Beras Putih Grade 2", "Hard", "Kilogram"],
  ["Beras Putih Grade 2", "Hard", "Kilogram"],
  ["Beras Putih Grade 2", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],
  ["Beras Putih Grade 3", "Hard", "Kilogram"],

  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 1", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 2", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
  ["Jagung Pipil Grade 3", "Hard", "Kilogram"],
].map((item, idx) => {
  return {
    id: idx + 1,
    entryDate: "26/08/2020",
    tenderStatus: randomizeData(["onRequest", "onProcess", "done"]),
    negoStatus: randomizeData(["Status Negosiasi"]),
    productStatus: randomizeData(["Berhasil", "Gagal"]),
    transactionCode: Math.floor(10000000 + Math.random() * 90000000),
    farmerName: randomizeData(farmerName),
    productName: item[0],
    category: item[1],
    quantity: randomizeData(dummyQuantity),
    unit: item[2],
    storageLocation: randomizeData(storageLoc),
    storageArea: randomizeData(["Area A", "Area B"]),
    negoPrice: randomizeData(dummyPrice),
    totalPrice: 200000000,
  };
});
