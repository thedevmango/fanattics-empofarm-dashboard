/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableFooter,
  TableRow,
  TableHeadCell,
} from "../../../components/Table";
import { BsThreeDotsVertical } from "react-icons/bs";
import { Button } from "../../../components/Button";
import { PaneHeader, MetadataText } from "../../../components/Typography";
import { Panel } from "../../../components/Panel";
import { formatnumber } from "../../../configs/formatnumber";
import { Colors } from "../../../constants";

export default ({ data }) => {
  return (
    <Panel style={{ borderRadius: 4 }} className="content mt-3">
      <div className="d-flex  justify-content-between">
        <PaneHeader bold className="mb-3">
          {data.subCategory}
        </PaneHeader>

        <div className="d-flex flex-column col-2" style={{ textAlign: "right" }}>
          <MetadataText color={Colors.gray400}>Update Terakhir</MetadataText>
          <MetadataText color={Colors.gray400}>{data.date}</MetadataText>
        </div>
      </div>

      <Table>
        <TableHead>
          <TableRow>
            <TableHeadCell>SKU</TableHeadCell>
            <TableHeadCell>Nama Varian</TableHeadCell>
            <TableHeadCell>Harga Acuan Pemerintah</TableHeadCell>
            <TableHeadCell>Harga Beli Empofarm</TableHeadCell>
            <TableHeadCell>Harga Acuan Pemerintah</TableHeadCell>
            <TableHeadCell>&nbsp;</TableHeadCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <RenderBodyTable variants={data ? data.variants : []} />
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell style={{ color: Colors.gray400 }} colSpan={10}>
              <i>*Price constantly updated</i>
            </TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </Panel>
  );
};

const EmptyDataRow = ({ colSpan = 7 }) => {
  return (
    <TableRow>
      <TableCell colSpan={colSpan} style={{ textAlign: "center" }}>
        No Data
      </TableCell>
    </TableRow>
  );
};

const RenderBodyTable = ({ variants }) => {
  if (!variants) return <EmptyDataRow colSpan={6} />;
  return variants.map((variant, idx) => (
    <TableRow key={idx}>
      <TableCell>{`${variant.skuNumber}`}</TableCell>
      <TableCell>{`${variant.variantName}`}</TableCell>
      <TableCell>{`${formatnumber(variant.refPriceGov)}`}</TableCell>
      <TableCell>{`${formatnumber(variant.refPriceEmpo)}`}</TableCell>
      <TableCell>{`${formatnumber(variant.refPriceGov)}`}</TableCell>
      <TableCell>
        <Button theme="action-table">
          <BsThreeDotsVertical size={12} />
        </Button>
      </TableCell>
    </TableRow>
  ));
};
