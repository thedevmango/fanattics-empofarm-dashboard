/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import Tabs from "../../../components/Tabs";
import { Button } from "../../../components/Button";
import { PaneHeader, PageTitle } from "../../../components/Typography";
import ComponentPanel from "./ComponentPanel";

export default (props) => {
  const [data, setData] = useState([]);
  const [isActive, setIsActive] = useState("Hard");

  const filterData = (status) => dummyData.filter((val) => val.category === status);

  useEffect(() => {
    if (isActive === "Hard") return setData(filterData("Hard"));
    if (isActive === "Soft") return setData(filterData("Soft"));
    if (isActive === "Liquid") return setData(filterData("Liquid"));
    if (isActive === "Equipment") return setData(filterData("Equipment"));
  }, [isActive]);

  return (
    <div className="purchasing purchasing-price-setting">
      <Helmet>
        <title>Commodity Price Setting | Purchasing</title>
      </Helmet>

      <PageTitle bold className="mb-4">
        Pengaturan Harga Beli Empofarm
      </PageTitle>

      <div className="d-flex mx-3 mb-4">
        <Tabs
          options={["Hard", "Soft", "Liquid", "Equipment"]}
          activeTab={isActive}
          setActiveTab={setIsActive}
          className="tab-product-catalog"
        />
      </div>

      <div>
        {data.map((val, idx) => {
          return <ComponentPanel key={idx} data={val} />;
        })}
      </div>
    </div>
  );
};

const randomizeData = (obj) => obj[Math.floor(Math.random() * obj.length)];
const dummyDate = [
  [moment().format("DD/MM/YYYY"), moment().subtract(1, "days").format("DD/MM/YYYY")],
  [
    moment().subtract(2, "days").format("DD/MM/YYYY"),
    moment().subtract(3, "days").format("DD/MM/YYYY"),
  ],
];
const dummyGrade = ["Grade 1", "Grade 2", "Grade 3"];
const dummyData = [
  ["Hard", "Jagung Super"],
  ["Hard", "Beras"],
  ["Hard", "Beras Ketan"],
  ["Hard", "Gabah"],

  ["Soft", "Jagung Pipil"],
  ["Soft", "Beras"],
  ["Soft", "Gabah Hitam"],
  ["Soft", "Jagung Super"],

  ["Liquid", "Beras"],
  ["Liquid", "Beras Ketan"],
  ["Liquid", "Gabah"],

  ["Equipment", "Kelapa"],
  ["Equipment", "Jagung Pipil"],
  ["Equipment", "Kopi"],
].map((item, idx) => {
  return {
    id: idx + 1,
    date: moment().format("DD/MM/YYYY"),
    category: item[0],
    subCategory: item[1],
    variants: dummyGrade.map((variant, idx) => {
      return {
        id: idx + 1,
        skuNumber: `081920192 0912 ${Math.floor(100000 + Math.random() * 900000)}`,
        variantName: `${item[1]} ${variant}`,
        date: randomizeData(dummyDate)[0],
        refPriceGov: randomizeData([10000, 11000, 12000]),
        refPriceEmpo: randomizeData([10000, 11000, 12000]),
        dateBefore: randomizeData(dummyDate)[1],
        refPriceGovBefore: randomizeData([10000, 11000, 12000]),
        refPriceEmpoBefore: randomizeData([10000, 11000, 12000]),
      };
    }),
  };
});
