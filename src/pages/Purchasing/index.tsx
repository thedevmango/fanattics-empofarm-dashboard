/* eslint-disable no-unused-vars */
import React from "react";
import { Route } from "react-router-dom";

import Tender from "./Tender";
import TenderDetails from "./Tender/TenderDetails";
import AkuisisiProduk from "./PAR";
import AkuisisiProdukDetail from "./PAR/Details";
import PriceSetting from "./PriceSetting";

export default (props: any) => {
  return (
    <>
      <Route path={`${props.match.path}/tender`} exact component={Tender} />
      <Route path={`${props.match.path}/tender/details`} component={TenderDetails} />
      <Route exact path={`${props.match.path}/akuisisi-produk`} component={AkuisisiProduk} />
      <Route
        path={`${props.match.path}/akuisisi-produk/details`}
        component={AkuisisiProdukDetail}
      />
      <Route exact path={`${props.match.path}/commodity-price-setting`} component={PriceSetting} />
    </>
  );
};
