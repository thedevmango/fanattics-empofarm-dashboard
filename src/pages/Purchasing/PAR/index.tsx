/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import {
  BodyText,
  Form,
  Input,
  MetadataText,
  Pagination,
  Panel,
  SegmentedTitle,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  usePagination,
} from "../../../components";
import Tabs from "../../../components/Tabs";
import { InboundStatus } from "../../../constants";
import { PURCHASING_SET_TAB } from "../../../redux/types";
import { getPurchasingTransaction } from "../../../redux/actions/purchasing";
import { VscCalendar } from "react-icons/vsc";
import Searchbar from "../../../components/Searchbar";
import { formatquantity } from "../../../configs/formatnumber";
import { gray500, gray800 } from "../../../constants/Colors";

export default (props: any) => {
  const dispatch = useDispatch();
  const { TenderMasuk, TenderPembayaran, TenderSelesai } = InboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.purchasing.tab);
  const data = useSelector((state: any) => state.purchasing.purchaseTrxList);

  //* LOCAL STATE
  const [title, setTitle] = useState("Tender Masuk");

  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [date, setDate] = useState(moment().format("D MMMM YYYY"));
  const [inputDateType, setInputDateType] = useState("text");

  //* FUNCTION
  const toggleTitle = ({ value }) => {
    if (value === "akuisisi-produk") props.history.push(`/purchasing/akuisisi-produk`);
    if (value === "mutasi-konsinyasi") return null;
  };

  const toggleTab = (selectedTab: any) => {
    if (selectedTab === tab) return;
    dispatch({ type: PURCHASING_SET_TAB, payload: selectedTab });
  };

  const toggleDate = (date: any) => {
    setDate(date);
    if (document.activeElement instanceof HTMLElement) {
      document.activeElement.blur();
    }
  };

  //* FETCH DATA
  useEffect(() => {
    if (tab === TenderMasuk) setTitle("Acquisition Request");
    if (tab === TenderPembayaran) setTitle("In Process");
    if (tab === TenderSelesai) setTitle("Completed");
    dispatch(
      getPurchasingTransaction({
        status: tab,
        type: "DS", //TODO SHOULD BE CHANGE TO "CSE"
      })
    );
  }, [tab]);

  useEffect(() => {
    if (!data.length) return setFilteredData([]);
    if (searchQuery) {
      const results = data.filter((item: any) => {
        return item.farmer.user.fullName.toLowerCase().includes(searchQuery.toLowerCase());
      });
      return setFilteredData(results);
    } else {
      return setFilteredData(data);
    }
  }, [searchQuery, data]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  console.log(data);
  if (!data)
    return (
      <div className="purchasing purchasing-par">
        <Helmet>
          <title>...loading</title>
        </Helmet>
      </div>
    );
  return (
    <div className="purchasing purchasing-par">
      <Helmet>
        <title>{title} | PAR</title>
      </Helmet>
      <SegmentedTitle
        {...props}
        titles={["Product Acquisition Request (PAR)", "Mutasi Konsinyasi"]}
        slug={["akuisisi-produk", "mutasi-konsinyasi"]}
        onClick={toggleTitle}
      />

      <div className="d-flex mb-3">
        <Tabs
          options={["Acquisition Request", "In Process", "Completed"]}
          slug={[TenderMasuk, TenderPembayaran, TenderSelesai]}
          activeTab={tab}
          setActiveTab={toggleTab}
        />
      </div>

      <div className="d-flex mb-4 justify-content-between">
        <Form className="col-3 pl-0 input-date">
          <Input
            value={date}
            // onChange={(e: any) => toggleDate(moment(e.target.value).format("D MMMM YYYY"))}
            prepend={<VscCalendar size={18} />}
            type={inputDateType === "date" ? "date" : "text"}
            onFocus={() => setInputDateType("date")}
            onBlur={() => setInputDateType("text")}
          />
        </Form>

        <Searchbar
          placeholder="Pencarian"
          className="col-2"
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />
      </div>

      <Panel className="content content-par">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Nama Produk</TableHeadCell>
              <TableHeadCell>Nama Farmer</TableHeadCell>
              <TableHeadCell>Lokasi Penyimpanan</TableHeadCell>
              <TableHeadCell style={{ textAlign: "right" }}>Kuantitas</TableHeadCell>

              {tab === TenderPembayaran ? (
                <TableHeadCell style={{ textAlign: "right" }}>Harga Negosiasi (Rp)</TableHeadCell>
              ) : null}

              {tab === TenderPembayaran ? (
                <TableHeadCell style={{ textAlign: "right" }}>Status Negosiasi</TableHeadCell>
              ) : tab === TenderSelesai ? (
                <TableHeadCell style={{ textAlign: "right" }}>Keterangan</TableHeadCell>
              ) : null}
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={10}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

interface TProps {
  data: any;
  history: any;
  match: any;
  tab: string[];
}

const RenderBodyTable = ({ data, history, match, tab }: TProps) => {
  const { TenderPembayaran, TenderSelesai } = InboundStatus;
  return !data
    ? null
    : data.map((item: any, i: number) => {
        const link = `${match.url}/details`;
        const nextClick = () => history.push(link, { data: item, tab: tab });

        return (
          <TableRow key={i} onClick={nextClick}>
            <TableCell>{item.transactionCode}</TableCell>
            <TableCell>
              <div className="d-flex flex-column">
                <BodyText>
                  {item.product_variant.subcategory.name}&nbsp;{item.product_variant.productName}
                </BodyText>
                <MetadataText color={gray500}>
                  {item.product_variant.subcategory.category.categoryName}
                </MetadataText>
              </div>
            </TableCell>
            <TableCell>{item.farmer.user.fullName}</TableCell>
            <TableCell>
              <div className="d-flex flex-column">
                <BodyText color={gray800}>{item.rdc || "-"}</BodyText>
                <MetadataText color={gray500}>{item.area || "-"}</MetadataText>
              </div>
            </TableCell>
            <TableCell style={{ textAlign: "right" }}>{formatquantity(item.quantity)} Kg</TableCell>

            {tab === TenderPembayaran ? (
              <TableCell style={{ textAlign: "right" }}>
                {formatquantity(item.salePricePerUnit) || "-"}
              </TableCell>
            ) : null}

            {tab === TenderPembayaran ? (
              <TableCell style={{ textAlign: "right" }}>{item.status}</TableCell>
            ) : tab === TenderSelesai ? (
              <TableCell style={{ textAlign: "right" }}>
                {item.isSuccessful ? (
                  <BodyText color="#2F9F28">Berhasil</BodyText>
                ) : (
                  <BodyText color="#D92C2C">Gagal</BodyText>
                )}
              </TableCell>
            ) : null}
          </TableRow>
        );
      });
};
