/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { SubjectTitle, BodyText } from "../../../../components/Typography";
import { Modal, ModalBody } from "../../../../components/Modal";
import { Button } from "../../../../components/Button";
import { Form } from "../../../../components/Form";

export default ({
  toggle,
  isOpen,
  onSubmit,
  loading,
}: {
  toggle: () => any;
  isOpen: boolean;
  onSubmit: any;
  loading: boolean;
}) => {
  const [value, setValue] = useState("");

  const formatCurrency = (input: string | number) => {
    if (input !== "") {
      return input
        .toLocaleString("id")
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    return "";
  };

  const handleInput = (e: any) => {
    setValue("Rp " + formatCurrency(e.target.value));
  };

  const handleCancel = () => {
    setValue("");
    toggle();
  };

  return (
    <Modal toggle={toggle} isOpen={isOpen} size="lg">
      <ModalBody className="d-flex flex-column w-100">
        <div className="d-flex align-items-end justify-content-between w-100">
          <div className="d-flex flex-column">
            <SubjectTitle bold>Catatan:</SubjectTitle>
            <SubjectTitle bold>
              Pastikan harga pembelian sudah dikonfirmasi oleh farmer
            </SubjectTitle>
          </div>

          <Button theme="secondary">Hubungi Farmer</Button>
        </div>

        <div className="mt-5 d-flex flex-column align-items-center w-100">
          <BodyText>Masukkan harga akhir setelah negosiasi</BodyText>
          <Form className="custom-harga-akhir-wrapper mt-3">
            <input
              value={value}
              onChange={(e) => handleInput(e)}
              className="custom-harga-akhir-input"
              type="text"
            />
          </Form>
        </div>

        <div className="mt-5 mx-auto">
          <Button className="mr-1" onClick={handleCancel} theme="secondary">
            Batalkan
          </Button>
          <Button
            containerStyle={{ width: 168 }}
            onClick={() => onSubmit({ value })}
            theme="primary"
          >
            {!loading ? (
              "Proses Pembayaran"
            ) : (
              <div className="h-100">
                <div className="spinner-border spinner-border-sm">
                  <span className="sr-only"></span>
                </div>
              </div>
            )}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};
