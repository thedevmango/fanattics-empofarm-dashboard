import React from "react";
import { GreetingTitle, HeaderText } from "../../../../components/Typography";
import { Modal, ModalBody } from "../../../../components/Modal";
import { primaryBrand } from "../../../../constants/Colors";
import { Button } from "../../../../components/Button";

export default ({
  isOpen,
  children,
  onClick,
}: {
  isOpen: boolean;
  children: React.ReactNode;
  onClick: () => any;
}) => {
  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalBody className="d-flex flex-column align-items-center pt-5">
        <GreetingTitle bold>Pembayaran Diproses!</GreetingTitle>

        <div className="mt-3 mb-5">
          <HeaderText className="text-center">
            Pembayaran dengan ID Transaksi&nbsp;
            <span style={{ color: primaryBrand }}>{children}</span>
            &nbsp;akan dilanjutkan oleh divisi Finance
          </HeaderText>
        </div>

        <Button onClick={onClick} theme="link">
          Tutup
        </Button>
      </ModalBody>
    </Modal>
  );
};
