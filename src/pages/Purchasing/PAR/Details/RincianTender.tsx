import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RincianTenderItem } from "../../../../components/Molecules/PanelDetail";
import { formatquantity } from "../../../../configs/formatnumber";
import { InboundStatus } from "../../../../constants";
import { getSKUInfo } from "../../../../redux/actions/purchasing";
import RincianTenderPanel from "./RincianTenderPanel";

export default ({ data }: { data: any }) => {
  const dispatch = useDispatch();
  const { TenderMasuk } = InboundStatus;

  const productSKU = useSelector((state: any) => state.purchasing.skuInfo);

  useEffect(() => {
    dispatch(getSKUInfo(data.productSkuId));
  }, [dispatch, data.productSkuId]);

  if (!data) return null;
  return (
    <RincianTenderPanel
      cover={data.product_variant.imageUrl}
      productName={`${data.product_variant.subcategory.name} ${data.product_variant.productName}`}
      totalNominal={TenderMasuk.includes(data.status) ? data.totalPrice : data.salePricePerUnit}
    >
      <RincianTenderItem title="Category">
        {data.product_variant.subcategory.category.categoryName}
      </RincianTenderItem>
      <RincianTenderItem title="Sub Category">
        {data.product_variant.subcategory.name}
      </RincianTenderItem>
      <RincianTenderItem title="SKU Varian">{data.product_variant.productName}</RincianTenderItem>
      <RincianTenderItem title="Harga Acuan Empofarm">
        {productSKU ? `Rp ${formatquantity(productSKU.price)}/Kg` : <>&mdash;</>}
      </RincianTenderItem>
      <RincianTenderItem title="Harga Penawaran Farmer">
        {`Rp ${formatquantity(data.pricePerUnit)}/Kg`}
      </RincianTenderItem>
      <RincianTenderItem title="Total Harga Penawaran">
        {data.salePricePerUnit ? (
          <>{`Rp ${formatquantity(data.salePricePerUnit)}`}</>
        ) : (
          <>&mdash;</>
        )}
      </RincianTenderItem>
      <RincianTenderItem title="Kuantitas Barang">
        {formatquantity(data.quantity)}&nbsp;Kg
      </RincianTenderItem>
      <RincianTenderItem title="Deskripsi Barang">
        {data.description ? data.description : <>&mdash;</>}
      </RincianTenderItem>
    </RincianTenderPanel>
  );
};
