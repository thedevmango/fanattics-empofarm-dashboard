/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { BackButton, Button, HeaderText, PageTitle } from "../../../../components";
import { useDispatch, useSelector } from "react-redux";
import { API_URL, InboundStatus } from "../../../../constants";
import { getPurchasingTransactionDetails } from "../../../../redux/actions/purchasing";
import {
  DataTender,
  DataTenderItem,
  DataUser,
  RiwayatEkspedisi,
  UpdateStatusTrx,
} from "../../../../components/Molecules/PanelDetail";
import { enumTenderStatus, enumType } from "../../../../configs/enums";
import RincianTender from "./RincianTender";
import PopupInputHarga from "./PopupInputHarga";
import PopupNotif from "./PopupNotif";
import Axios from "axios";
import { getShipmentLogs } from "../../../../redux/actions/logs";

export default (props: any) => {
  const dispatch = useDispatch();
  const { TenderMasuk, TenderPembayaran, TenderSelesai } = InboundStatus;
  const parentData = props.location.state.data;
  const tab = props.location.state.tab;

  //* GLOBAL STATE
  const data = useSelector((state: any) => state.purchasing.purchaseTrxListDetails);
  const shipmentLogs = useSelector((state: any) => state.logs.shipmentLogs);

  //* LOCAL STATE
  const [loadingInputHarga, setLoadingInputHarga] = useState<boolean>(false);
  const [openNotif, setOpenNotif] = useState<boolean>(false);
  const [openInputHarga, setOpenInputHarga] = useState<boolean>(false);

  //* FETCHING DATA
  useEffect(() => {
    dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
  }, []);

  useEffect(() => {
    data && dispatch(getShipmentLogs({ data: data, type: "inbound" }));
  }, [data]);

  //* FUNCTION
  const toggleInputHarga = () => setOpenInputHarga(!openInputHarga);

  const handleSubmitHarga = async ({ value }) => {
    return alert(`value ${value}`);

    // let price = value.split("Rp ")[1].split(".").join("");
    // setLoadingInputHarga(true);
    // if (!value) {
    //   setLoadingInputHarga(false);
    //   alert("Input tidak boleh kosong");
    // } else {
    //   try {
    //     const resSubmit = await Axios.put(`${API_URL}/purchases/${parentData.id}`, {
    //       salePricePerUnit: parseInt(price) / data.quantity,
    //       status: "finance-approval",
    //     });
    //     dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
    //     setLoadingInputHarga(false);
    //     setOpenInputHarga(false);
    //     setOpenNotif(true);
    //     console.log(resSubmit.data.message);
    //   } catch (err) {
    //     console.log(err);
    //   }
    // }
  };

  const handleClose = () => {
    setOpenNotif(false);
  };

  console.log("details", data);
  if (!data)
    return (
      <div className="purchasing purchasing-par-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="purchasing purchasing-par-details">
      <Helmet>
        <title>{`${parentData.product_variant.subcategory.name} ${parentData.product_variant.productName} | PAR`}</title>
      </Helmet>
      <BackButton>Kembali ke Transactional Activities</BackButton>

      <PopupInputHarga
        onSubmit={handleSubmitHarga}
        loading={loadingInputHarga}
        toggle={toggleInputHarga}
        isOpen={openInputHarga}
      />

      <PopupNotif isOpen={openNotif} onClick={handleClose}>
        {data.transactionCode}
      </PopupNotif>

      <div className="header d-flex justify-content-between mb-4">
        <PageTitle>Purchasing Order Detail</PageTitle>

        {TenderMasuk.includes(data.status) ? (
          <Button onClick={toggleInputHarga} theme="primary">
            INPUT HARGA SETELAH NEGOSIASI
          </Button>
        ) : null}
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(data.updatedAt).format("DD/MM/YYYY H:mm")}>
            <HeaderText>{enumTenderStatus(data.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Farmer"
            name={parentData.farmer.user.fullName}
            contact={parentData.farmer.user.phoneNumber}
            address={parentData.farmer.farmAddress}
          />

          <DataTender>
            <DataTenderItem title="Tanggal Pengajuan">
              {moment(parentData.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTenderItem>
            <DataTenderItem title="Kode Transaksi">{parentData.transactionCode}</DataTenderItem>
            <DataTenderItem title="Bentuk Transaksi">{enumType(parentData.type)}</DataTenderItem>
          </DataTender>
        </div>

        <div className="col-8">
          <RincianTender data={data} />

          <RiwayatEkspedisi data={shipmentLogs} />
        </div>
      </div>
    </div>
  );
};
