import React from "react";
import { GreetingTitle, PaneHeader, Panel } from "../../../../components";
import { formatquantity } from "../../../../configs/formatnumber";

interface IRincianTender {
  children: React.ReactNode;
  className?: string;
  cover: any;
  productName: string;
  totalNominal?: number;
}

export default ({ children, className = "", cover, totalNominal, productName }: IRincianTender) => {
  return (
    <Panel padding="24" className={`mol-rincian-tender ${className}`}>
      <PaneHeader className="mb-4" color="#404040" bold>
        Rincian Tender
      </PaneHeader>

      <div className="content">
        <div className="image-wrapper">
          <img src={cover} alt="product" />
        </div>

        <div className="tender-info">
          <PaneHeader className="col pl-0 pb-3" color="#404040">
            {productName}
          </PaneHeader>
          {children}
        </div>
      </div>

      {totalNominal ? (
        <div className="total-nominal-tender">
          <PaneHeader color="#605E5C">Total Harga Setelah Negosiasi</PaneHeader>
          <GreetingTitle accent>{`Rp ${formatquantity(totalNominal)}`}</GreetingTitle>
        </div>
      ) : null}
    </Panel>
  );
};
