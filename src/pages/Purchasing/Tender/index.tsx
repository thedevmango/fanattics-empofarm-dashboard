/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import { Helmet } from "react-helmet";
import { VscCalendar } from "react-icons/vsc";
import { BsChevronRight } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { formatnumber } from "../../../configs/formatnumber";
import { gray500, gray800 } from "../../../constants/Colors";
import {
  BodyText,
  Button,
  Form,
  Input,
  MetadataText,
  PageTitle,
  Pagination,
  Panel,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  Tabs,
  usePagination,
} from "../../../components";
import { PURCHASING_SET_TAB } from "../../../redux/types/purchasing";
import { getPurchasingTransaction } from "../../../redux/actions/purchasing";
import { enumTenderStatus, enumType } from "../../../configs/enums";
import { InboundStatus } from "../../../constants";

//TODO: ENBALE PAID INVOICE DOWNLOAD BUTTON

export default (props: any) => {
  const dispatch = useDispatch();
  const { TenderMasuk, TenderDiproses, TenderPembayaran, TenderSelesai } = InboundStatus;

  //* GLOBAL STATE
  const tab = useSelector((state: any) => state.purchasing.tab);
  const data = useSelector((state: any) => state.purchasing.purchaseTrxList);

  //* LOCAL STATE
  const [title, setTitle] = useState("Tender Masuk");

  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);
  const [date, setDate] = useState(moment().format("D MMMM YYYY"));
  const [inputDateType, setInputDateType] = useState("text");

  //* FUNCTION
  const toggleDate = (date: any) => {
    setDate(date);
    if (document.activeElement instanceof HTMLElement) {
      document.activeElement.blur();
    }
  };

  const toggleTab = (selectedTab: string[]) => {
    if (selectedTab === tab) return;
    dispatch({ type: PURCHASING_SET_TAB, payload: selectedTab });
  };

  //* FETCH DATA
  useEffect(() => {
    if (tab === TenderMasuk) setTitle("Tender Masuk");
    if (tab === TenderDiproses) setTitle("Tender Diproses");
    if (tab === TenderPembayaran) setTitle("Pembayaran Tender");
    if (tab === TenderSelesai) setTitle("Tender Selesai");
    dispatch(getPurchasingTransaction({ status: tab }));
  }, [tab]);

  useEffect(() => {
    if (!data.length) return setFilteredData([]);
    if (searchQuery) {
      const results = data.filter((item: any) => {
        return item.farmer.user.fullName.toLowerCase().includes(searchQuery.toLowerCase());
      });
      return setFilteredData(results);
    } else {
      return setFilteredData(data);
    }
  }, [searchQuery, data]);

  const { next, prev, jump, currentData, setPage, pages, currentPage, maxPage } = usePagination({
    data: filteredData,
    itemsPerPage: 10,
  });

  console.log(data); //TODO delete soon
  return (
    <div className="purchasing purchasing-tender">
      <Helmet>
        <title>{title} | Purchasing</title>
      </Helmet>

      <PageTitle className="mb-4" bold>
        Purchasing - Tender
      </PageTitle>

      <div className="d-flex mb-3">
        <Tabs
          options={["Tender Masuk", "Tender Diproses", "Pembayaran Tender", "Tender Selesai"]}
          slug={[TenderMasuk, TenderDiproses, TenderPembayaran, TenderSelesai]}
          activeTab={tab}
          setActiveTab={toggleTab}
        />
        <div>
          <Button containerStyle={{ width: 155 }} theme="primary">
            Buat Tender Baru
          </Button>
        </div>
      </div>

      <div className="d-flex mb-4 justify-content-between">
        <Form className="col-3 pl-0 input-date">
          <Input
            value={date}
            onChange={(e) => toggleDate(moment(e.target.value).format("D MMMM YYYY"))}
            prepend={<VscCalendar size={18} />}
            type={inputDateType === "date" ? "date" : "text"}
            onFocus={() => setInputDateType("date")}
            onBlur={() => setInputDateType("text")}
          />
        </Form>

        <Searchbar
          placeholder="Cari nama Farmer"
          className="col-2"
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />
      </div>

      <Panel className="content content-tender">
        <Table bordered>
          <TableHead>
            <TableRow>
              <TableHeadCell>Kode Transaksi</TableHeadCell>
              <TableHeadCell>Nama Farmer</TableHeadCell>
              <TableHeadCell>Nama Barang</TableHeadCell>
              {tab !== TenderMasuk ? null : <TableHeadCell>Bentuk Kerjasama</TableHeadCell>}
              <TableHeadCell style={{ textAlign: "right" }}>Kuantitas</TableHeadCell>
              {tab !== TenderDiproses ? null : (
                <TableHeadCell style={{ textAlign: "right" }}>Status</TableHeadCell>
              )}
              {tab === TenderPembayaran || tab === TenderSelesai ? (
                <>
                  <TableHeadCell style={{ textAlign: "right" }}>Harga Negosiasi</TableHeadCell>
                  <TableHeadCell style={{ textAlign: "right" }}>Status</TableHeadCell>
                </>
              ) : null}
              <TableHeadCell>&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <RenderBodyTable {...props} tab={tab} data={currentData()} />
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={7}>
                <Pagination
                  next={next}
                  prev={prev}
                  jump={jump}
                  pages={pages}
                  currentPage={currentPage}
                  maxPage={maxPage}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Panel>
    </div>
  );
};

const RenderBodyTable = ({ match, tab, history, data }) => {
  const { TenderMasuk, TenderDiproses, TenderPembayaran, TenderSelesai } = InboundStatus;
  return !data
    ? null
    : data.map((item: any, idx: number) => {
        const link = `${match.url}/details`;
        const nextClick = () => history.push(link, { data: item, tab: tab });
        return (
          <TableRow key={idx} onClick={nextClick}>
            <TableCell>{item.transactionCode}</TableCell>
            <TableCell>{item.farmer.user.fullName}</TableCell>
            <TableCell>
              <BodyText color={gray800}>
                {item.product_variant.subcategory.name}&nbsp;{item.product_variant.productName}
              </BodyText>
              <br />
              <MetadataText color={gray500}>
                {item.product_variant.subcategory.category.categoryName}
              </MetadataText>
            </TableCell>

            {tab === TenderMasuk ? <TableCell>{enumType(item.type)}</TableCell> : null}

            <TableCell style={{ textAlign: "right" }}>
              {formatnumber(item.quantity)}&nbsp;Kg
            </TableCell>

            {tab === TenderDiproses ? (
              <TableCell style={{ textAlign: "right" }}>{enumTenderStatus(item.status)}</TableCell>
            ) : null}

            {tab === TenderPembayaran ? (
              <>
                <TableCell style={{ textAlign: "right" }}>
                  <BodyText color={gray800}>
                    {item.salePricePerUnit
                      ? `Rp ${formatnumber(item.salePricePerUnit * item.quantity)}`
                      : "-"}
                  </BodyText>
                  <br />
                  <MetadataText color={gray500}>
                    {item.salePricePerUnit ? `Rp ${formatnumber(item.salePricePerUnit)}/Kg` : "-"}
                  </MetadataText>
                </TableCell>
                <TableCell style={{ textAlign: "right", maxWidth: 175 }}>
                  {enumTenderStatus(item.status)}
                </TableCell>
              </>
            ) : null}

            {tab === TenderSelesai ? (
              <>
                <TableCell style={{ textAlign: "right" }}>
                  <BodyText color={gray800}>Rp&nbsp;{formatnumber(item.totalPrice)}</BodyText>
                  <br />
                  <MetadataText color={gray500}>
                    Rp&nbsp;{formatnumber(item.salePricePerUnit)}/Kg
                  </MetadataText>
                </TableCell>
                <TableCell style={{ textAlign: "right" }}>
                  {enumTenderStatus(item.status) === "Berhasil"
                    ? enumTenderStatus(item.status)
                    : enumTenderStatus(item.status)}
                </TableCell>
              </>
            ) : null}

            <TableCell>
              <Button theme="action-table">
                <BsChevronRight />
              </Button>
            </TableCell>
          </TableRow>
        );
      });
};
