/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import moment from "moment";
import Axios from "axios";
import { Helmet } from "react-helmet";
import { useDispatch, useSelector } from "react-redux";
import {
  UpdateStatusTrx,
  DataUser,
  DataTender,
  DataTenderItem,
  RiwayatEkspedisi,
  ReportQC,
  ReportQCItem,
} from "../../../../components/Molecules/PanelDetail";
import {
  PopupInputHargaNegosiasi,
  PopupPembayaranDiproses,
} from "../../../../components/Molecules/Popup";
import {
  BatalkanTender,
  BatalkanTransaksi,
  DownloadHalaman,
  InputHargaTender,
  SetujuiTender,
  TolakTender,
} from "./Button";
import { BackButton } from "../../../../components/Molecules";
import { PageTitle, HeaderText } from "../../../../components/Typography";
import { getPurchasingTransactionDetails } from "../../../../redux/actions/purchasing";
import { getShipmentLogs } from "../../../../redux/actions/logs";
import { getIQCReports } from "../../../../redux/actions/i_qc";
import { enumTenderStatus, enumType } from "../../../../configs/enums";
import { API_URL, InboundStatus } from "../../../../constants";
import RincianTender from "./RincianTender";
import PopupCancellation from "./PopupCancellation";

export default (props: any) => {
  const dispatch = useDispatch();
  const { TenderMasuk, TenderDiproses, TenderPembayaran, TenderSelesai } = InboundStatus;
  const parentData = props.location.state.data;

  //* GLOBAL STATE
  const data = useSelector((state: any) => state.purchasing.purchaseTrxListDetails);
  const iqc_reports = useSelector((state: any) => state.i_qc.iqc_reports);
  const shipmentLogs = useSelector((state: any) => state.logs.shipmentLogs);

  //* LOCAL STATE
  const [loadingInputHarga, setLoadingInputHarga] = useState(false);
  const [openNotif, setOpenNotif] = useState(false);
  const [openInputHarga, setOpenInputHarga] = useState(false);
  const [openCancellation, setOpenCancellation] = useState(false);

  //* FUNCTION
  const toggleInputHarga = () => setOpenInputHarga(!openInputHarga);
  const toggleCancellation = () => setOpenCancellation(!openCancellation);

  const handleSubmit = async ({ value }) => {
    let price = parseInt(value.split("Rp ")[1].split(".").join(""));
    setLoadingInputHarga(true);
    if (!value) {
      setLoadingInputHarga(false);
      alert("Input tidak boleh kosong");
    } else {
      try {
        const resSubmit = await Axios.put(`${API_URL}/purchases/${parentData.id}`, {
          //TODO ADD NEW FIELD TO STORE TOTAL PRICE AFTER NEGOTIATION
          salePricePerUnit: Math.round(price / data.quantity),
          status: "finance-approval",
        });
        dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
        setLoadingInputHarga(false);
        setOpenInputHarga(false);
        setOpenNotif(true);
        console.log(resSubmit.data.message);
      } catch (err) {
        setTimeout(() => setLoadingInputHarga(false), 500);
        console.log(err);
      }
    }
  };

  const handleClose = () => setOpenNotif(false);

  //* FETCHING DATA
  useEffect(() => {
    if (!data) return;
    dispatch(getIQCReports({ data: data }));
    dispatch(getShipmentLogs({ data: data, type: "inbound" }));
  }, [data]);

  useEffect(() => {
    dispatch(getPurchasingTransactionDetails({ id: parentData.id }));
  }, []);

  // console.log("tenderdetails", iqc_reports);
  if (!data || (data.iqcOfficerId && !iqc_reports))
    return (
      <div className="purchasing purchasing-tender-details">
        <Helmet>
          <title>loading...</title>
        </Helmet>
        <BackButton>Kembali ke Transactional Activities</BackButton>
      </div>
    );
  return (
    <div className="purchasing purchasing-tender-details">
      <Helmet>
        <title>{`${data.product_variant.subcategory.name} ${data.product_variant.productName} | Sales`}</title>
      </Helmet>

      <PopupInputHargaNegosiasi
        onSubmit={handleSubmit}
        loading={loadingInputHarga}
        toggle={toggleInputHarga}
        isOpen={openInputHarga}
      />

      <PopupPembayaranDiproses isOpen={openNotif} onClick={handleClose}>
        {data.transactionCode}
      </PopupPembayaranDiproses>

      <PopupCancellation data={data} isOpen={openCancellation} toggle={toggleCancellation} />

      <BackButton
        onClick={() => {
          dispatch({ type: "G_PURCHASING_TRX_DETAILS_RESET" });
          props.history.goBack();
        }}
      >
        Kembali ke Transactional Activities
      </BackButton>

      <div className="header d-flex justify-content-between mb-4">
        <PageTitle>Purchasing Order Detail</PageTitle>

        <div className="button-wrapper d-flex">
          {TenderMasuk.includes(parentData.status) && !data.isCancelRequest ? (
            <>
              <TolakTender {...props} id={parentData.id} onClick={toggleCancellation} />
              <SetujuiTender {...props} id={parentData.id} />
            </>
          ) : TenderDiproses.includes(parentData.status) && !data.isCancelRequest ? (
            <>
              <BatalkanTransaksi {...props} id={parentData.id} onClick={toggleCancellation} />
              <DownloadHalaman {...props} id={parentData.id} />
            </>
          ) : TenderPembayaran.includes(parentData.status) && !data.isCancelRequest ? (
            <>
              <BatalkanTender {...props} id={parentData.id} onClick={toggleCancellation} />
              {!data.salePricePerUnit ? (
                <InputHargaTender onClick={toggleInputHarga} />
              ) : (
                <DownloadHalaman {...props} id={parentData.id} />
              )}
            </>
          ) : (
            <DownloadHalaman {...props} id={parentData.id} />
          )}
        </div>
      </div>

      <div className="content row">
        <div className="col-4">
          <UpdateStatusTrx timestamp={moment(data.updatedAt).format("DD/MM/YYYY H:mm")}>
            <HeaderText>{enumTenderStatus(data.status)}</HeaderText>
          </UpdateStatusTrx>

          <DataUser
            type="Farmer"
            name={data.farmer.user.fullName}
            contact={data.farmer.user.phoneNumber}
            address={data.farmer.farmAddress}
          />

          <DataTender>
            <DataTenderItem title="Tanggal Pengajuan">
              {moment(data.createdAt).format("dddd, DD/MM/YYYY")}
            </DataTenderItem>
            <DataTenderItem title="Kode Transaksi">{data.transactionCode}</DataTenderItem>
            <DataTenderItem title="Bentuk Transaksi">{enumType(data.type)}</DataTenderItem>
          </DataTender>

          {TenderPembayaran.includes(data.status) || TenderSelesai.includes(data.status) ? (
            <ReportQC
              type="Inbound"
              itemName={data.product_variant.productName}
              officerName={
                !data.iqcOfficerId || !iqc_reports ? undefined : iqc_reports.admin.fullName
              }
              qcPassed={
                !iqc_reports
                  ? false
                  : iqc_reports.reports
                  ? iqc_reports.reports.status !== "reject"
                  : false
              }
            >
              {(TenderPembayaran.includes(data.status) && iqc_reports) ||
              TenderSelesai.includes(data.status) ? (
                <>
                  <ReportQCItem
                    title="Quantity"
                    signedBy={iqc_reports.admin.fullName}
                    timestamp={moment(iqc_reports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                    verified={iqc_reports.reports.status !== "reject"}
                    valueOffer={`${data.quantity} Kg`}
                    valueActual={`${iqc_reports.reports.quantity} Kg`}
                  />
                  <ReportQCItem
                    title="Quality"
                    signedBy={iqc_reports.admin.fullName}
                    timestamp={moment(iqc_reports.reports.createdAt).format("DD/MM/YYYY H:mm")}
                    verified={iqc_reports.reports.status !== "reject"}
                    valueOffer={data.product_variant.productName}
                    valueActual={iqc_reports.reports.quality}
                  />
                </>
              ) : null}
            </ReportQC>
          ) : null}
        </div>

        <div className="col-8">
          <RincianTender data={data} />

          <RiwayatEkspedisi data={shipmentLogs} />
        </div>
      </div>
    </div>
  );
};
