/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { IoIosRadioButtonOff } from "react-icons/io";
import { BodyText, Button, ButtonIcon } from "../../../../../components";

interface Props {
  onClick: () => void;
}

export default ({ onClick }: Props) => {
  return (
    <Button onClick={onClick} containerStyle={{ width: 220 }} theme="primary" className="ml-2">
      <ButtonIcon type="prepend">
        <IoIosRadioButtonOff />
      </ButtonIcon>
      <BodyText color="#fff" bold>
        INPUT HARGA TENDER
      </BodyText>
    </Button>
  );
};
