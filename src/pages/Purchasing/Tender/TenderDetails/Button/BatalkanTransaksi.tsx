/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import React, { useState } from "react";
import { BodyText, Button } from "../../../../../components";
import { API_URL, InboundStatus } from "../../../../../constants";

interface Props {
  history: any;
  id: number;
  onClick: any;
}

export default ({ history, id, onClick }: Props) => {
  const [loading, setLoading] = useState(false);

  // const onClick = async () => {
  //   console.log("ID =>", id);
  //   setLoading(true);
  //   try {
  //     const { data } = await Axios.put(`${API_URL}/purchases/${id}`, {
  //       status: InboundStatus.onPurchasing,
  //     });
  //     console.log(data.message);

  //     history.goBack();
  //     setLoading(false);
  //   } catch (err) {
  //     setLoading(false);
  //     console.log(err);
  //   }
  // };

  return (
    <Button
      onClick={onClick}
      containerStyle={{ width: 200, textDecorationColor: "#D92C2C" }}
      theme="link"
      className="ml-2"
    >
      <BodyText color="#D92C2C">BATALKAN TRANSAKSI</BodyText>
    </Button>
  );
};
