/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import React, { useState } from "react";
import { IoIosRadioButtonOff } from "react-icons/io";
import { BodyText, Button, ButtonIcon } from "../../../../../components";
import { API_URL, InboundStatus } from "../../../../../constants";
import Spinner from "./Spinner";

interface Props {
  history: any;
  id: number;
}

export default ({ history, id }: Props) => {
  const [loading, setLoading] = useState(false);

  const onClick = async () => {
    console.log("ID =>", id);
    setLoading(true);
    try {
      const { data } = await Axios.put(`${API_URL}/purchases/${id}`, {
        status: InboundStatus.logisticInboundHold,
      });
      console.log(data.message);

      setTimeout(() => {
        history.goBack();
        setLoading(false);
      }, 1000);
    } catch (err) {
      setLoading(false);
      console.log(err);
    }
  };

  return (
    <Button onClick={onClick} containerStyle={{ width: 200 }} theme="primary" className="ml-2">
      <ButtonIcon type="prepend">{loading ? <Spinner /> : <IoIosRadioButtonOff />}</ButtonIcon>
      <BodyText color="#fff" bold>
        SETUJUI TENDER
      </BodyText>
    </Button>
  );
};
