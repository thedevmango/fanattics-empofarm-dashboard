import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RincianTender, RincianTenderItem } from "../../../../components/Molecules/PanelDetail";
import { formatquantity } from "../../../../configs/formatnumber";
import { getSKUInfo } from "../../../../redux/actions/purchasing";

export default ({ data }) => {
  const dispatch = useDispatch();

  const productSKU = useSelector((state: any) => state.purchasing.skuInfo);

  useEffect(() => {
    dispatch(getSKUInfo(data.productSkuId));
  }, [dispatch, data.productSkuId]);

  console.log("RINCIAN TENDER", data);
  return !data ? null : (
    <RincianTender
      cover={data.product_variant.imageUrl}
      productName={`${data.product_variant.subcategory.name} ${data.product_variant.productName}`}
      totalNominal={data.salePricePerUnit * data.quantity}
    >
      <RincianTenderItem title="Category">
        {data.product_variant.subcategory.category.categoryName}
      </RincianTenderItem>
      <RincianTenderItem title="Sub Category">
        {data.product_variant.subcategory.name}
      </RincianTenderItem>
      <RincianTenderItem title="SKU Varian">{data.product_variant.productName}</RincianTenderItem>
      <RincianTenderItem title="Harga Acuan Empofarm">
        {productSKU ? `Rp ${formatquantity(productSKU.price)}/Kg` : <>&mdash;</>}
      </RincianTenderItem>
      <RincianTenderItem title="Harga Penawaran Farmer">
        {`Rp ${formatquantity(data.pricePerUnit)}/Kg`}
      </RincianTenderItem>
      <RincianTenderItem title="Total Harga Penawaran">
        Rp {formatquantity(data.totalPrice)}
      </RincianTenderItem>
      <RincianTenderItem title="Kuantitas Barang">
        {formatquantity(data.quantity)}&nbsp;Kg
      </RincianTenderItem>
      <RincianTenderItem title="Deskripsi Barang">
        {data.description ? data.description : <>&mdash;</>}
      </RincianTenderItem>
    </RincianTender>
  );
};
