import { InboundStatus } from "../../constants";
import {
  PURCHASING_SET_TAB,
  G_PURCHASING_TRX_START,
  G_PURCHASING_TRX_FAILED,
  G_PURCHASING_TRX_DONE,
  G_PURCHASING_TRX_DETAILS_START,
  G_PURCHASING_TRX_DETAILS_FAILED,
  G_PURCHASING_TRX_DETAILS_DONE,
  G_PRODUCT_SKU,
} from "../types";

const init_state = {
  tab: InboundStatus.TenderMasuk,
  purchaseTrxList: [],
  purchaseTrxListDetails: undefined,

  skuInfo: undefined,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case PURCHASING_SET_TAB:
      return { ...state, tab: payload };

    case G_PURCHASING_TRX_START:
      return { ...state, error: "", loading: true, purchaseTrxList: [] };
    case G_PURCHASING_TRX_FAILED:
      return { ...state, error: payload, loading: false };
    case G_PURCHASING_TRX_DONE:
      return { ...state, error: "", loading: false, purchaseTrxList: payload };

    case G_PURCHASING_TRX_DETAILS_START:
      return { ...state, error: "", loading: true, purchaseTrxListDetails: undefined };
    case G_PURCHASING_TRX_DETAILS_FAILED:
      return { ...state, error: payload, loading: false, purchaseTrxListDetails: undefined };
    case G_PURCHASING_TRX_DETAILS_DONE:
      return { ...state, error: "", loading: false, purchaseTrxListDetails: payload };
    case "G_PURCHASING_TRX_DETAILS_RESET":
      return { ...state, purchaseTrxListDetails: undefined };

    case G_PRODUCT_SKU:
      return { ...state, skuInfo: payload };

    default:
      return state;
  }
};
