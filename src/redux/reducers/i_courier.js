import { InboundStatus } from "../../constants";
import {
  iCourier_G_DELIVERY_DONE,
  iCourier_G_DELIVERY_FAILED,
  iCourier_G_DELIVERY_START,
  iCourier_G_SHIPMENT_LOGS_START,
  iCourier_G_SHIPMENT_LOGS_FAILED,
  iCourier_G_SHIPMENT_LOGS_DONE,
  iCourier_P_TAB,
} from "../types";

const init_state = {
  tab: InboundStatus.LogisticIncoming,
  deliveryList: [],
  shipmentLogs: [],

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case iCourier_G_DELIVERY_START:
      return { ...state, loading: true, error: "", deliveryList: [] };
    case iCourier_G_DELIVERY_FAILED:
      return { ...state, loading: false, error: payload, deliveryList: [] };
    case iCourier_G_DELIVERY_DONE:
      return { ...state, loading: false, error: "", deliveryList: payload };

    case iCourier_G_SHIPMENT_LOGS_START:
      return { ...state, loading: true, error: "" };
    case iCourier_G_SHIPMENT_LOGS_FAILED:
      return { ...state, loading: false, error: payload };
    case iCourier_G_SHIPMENT_LOGS_DONE:
      return { ...state, loading: false, error: "", shipmentLogs: payload };

    case iCourier_P_TAB:
      return { ...state, tab: payload };

    default:
      return state;
  }
};
