import { OutboundStatus } from "../../constants";
import {
  oCourier_G_DELIVERY_START,
  oCourier_G_DELIVERY_FAILED,
  oCourier_G_DELIVERY_DONE,
  oCourier_G_SHIPMENT_LOGS_FAILED,
  oCourier_G_SHIPMENT_LOGS_DONE,
  oCourier_P_TAB, oCourier_START, oCourier_FAILED, oCourier_G_OQC_OFFICER_DONE,oCourier_G_DETAILS_DONE
} from "../types";

const init_state = {
  tab: OutboundStatus.CourierIncoming,
  deliveryList: [],
  
  deliveryDetails: undefined,
  shipmentLogs: undefined,
  oqc_officer: undefined,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case oCourier_START:
      return { ...state, loading: true, error: "" }
    case oCourier_FAILED:
      return { ...state, loading: false, error: payload }

    case oCourier_G_DELIVERY_START:
      return { ...state, loading: true, error: "", deliveryList: [] };
    case oCourier_G_DELIVERY_FAILED:
      return { ...state, loading: false, error: payload, deliveryList: [] };
    case oCourier_G_DELIVERY_DONE:
      return { ...state, loading: false, error: "", deliveryList: payload };

    case oCourier_G_SHIPMENT_LOGS_FAILED:
      return { ...state, loading: false, error: payload };
    case oCourier_G_SHIPMENT_LOGS_DONE:
      return { ...state, loading: false, error: "", shipmentLogs: payload };

    case oCourier_G_OQC_OFFICER_DONE:
      return { ...state, loading: false, error: "", oqc_officer: payload };

    case oCourier_G_DETAILS_DONE:
      return { ...state, loading: false, error: "", deliveryDetails: payload };

    case oCourier_P_TAB:
      return { ...state, tab: payload };

    default:
      return state;
  }
};
