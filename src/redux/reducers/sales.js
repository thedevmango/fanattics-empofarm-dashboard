import { OutboundStatus } from "../../constants";
import {
  SALES_SET_TAB,
  SALES_START,
  SALES_FAILED,
  SALES_LIST,
  SALES_LIST_DETAILS,
  SALES_TRX_DETAILS,
} from "../types/sales";

const init_state = {
  tab: OutboundStatus.FinanceIncoming,
  salesList: [],
  salesListDetails: undefined,
  sale_transaction_details: [],

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case SALES_SET_TAB:
      return { ...state, tab: payload };

    case SALES_START:
      return { ...state, loading: true, error: "" };
    case SALES_FAILED:
      return { ...state, loading: false, error: payload };

    case SALES_LIST:
      return { ...state, loading: false, error: "", salesList: payload };
    case SALES_LIST_DETAILS:
      return { ...state, loading: false, error: "", salesListDetails: payload };
    case SALES_TRX_DETAILS:
      return { ...state, loading: false, error: "", sale_transaction_details: payload };

    default:
      return state;
  }
};
