import { FINANCE_CHANGE_VALUE, FINANCE_RESET_DROPDOWN } from "../types/finance";

const init_state = {
  timeframeCostByProduct: "Bulan Ini",
  timeframeGrossMargin: "Hari Ini",
  timeframeJumlahTransaksi: "Hari Ini",
  timeframePeakHour: "Hari Ini",
  timeframeRevenue: "Hari Ini",
  timeframeRevenueByProduct: "Bulan Ini",

  dropdownCostByProduct: false,
  dropdownGrossMargin: false,
  dropdownJumlahTransaksi: false,
  dropdownPeakHour: false,
  dropdownRevenue: false,
  dropdownRevenueByProduct: false,
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case FINANCE_CHANGE_VALUE:
      return { ...state, [payload.name]: payload.value };

    case FINANCE_RESET_DROPDOWN:
      return {
        ...state,
        dropdownCostByProduct: false,
        dropdownGrossMargin: false,
        dropdownJumlahTransaksi: false,
        dropdownPeakHour: false,
        dropdownRevenue: false,
        dropdownRevenueByProduct: false,
      };

    default:
      return state;
  }
};
