import { combineReducers } from "redux";
import admin from "./admin";
import auth from "./auth";
import sidebar from "./sidebar";
import pengaturanProduk from "./pengaturanProduk";
import product from "./product";
import resetPass from "./resetPass";
import dashboardFinance from "./dashboardFinance";
import purchasing from "./purchasing";
import finance from "./finance";
import inventory from "./inventory";
import sales from "./sales";
import i_courier from "./i_courier";
import o_courier from "./o_courier";
import i_qc from "./i_qc";
import o_qc from "./o_qc";
import logs from "./logs";
import cancellation from "./cancellation";

export default combineReducers({
  cancellation,
  logs,
  o_qc,
  i_qc,
  o_courier,
  i_courier,
  sales,
  inventory,
  finance,
  purchasing,
  dashboardFinance,
  resetPass,
  product,
  pengaturanProduk,
  sidebar,
  auth,
  admin,
});
