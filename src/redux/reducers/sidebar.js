import {
  SIDEBAR_SELECT,
  SIDEBAR_OPEN,
  SIDEBAR_CLOSE,
  SIDEBAR_SELECT_CHILD,
  SIDEBAR_RESET,
} from "../types/sidebar";

const init_state = {
  selected: "",
  selectedMenu: "",
  selectedChild: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case SIDEBAR_SELECT:
      return {
        ...state,
        selected: payload,
        selectedMenu: payload,
        selectedChild: "",
      };
    case SIDEBAR_OPEN:
      return {
        ...state,
        selectedMenu: payload,
      };
    case SIDEBAR_CLOSE:
      return {
        ...state,
        selectedMenu: "",
      };
    case SIDEBAR_SELECT_CHILD:
      return {
        ...state,
        selected: payload[0],
        selectedMenu: payload[0],
        selectedChild: payload[1],
      };
    case SIDEBAR_RESET:
      return init_state;

    default:
      return state;
  }
};
