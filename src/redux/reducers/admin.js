import { AdminRoles } from "../../constants";
import { ADMIN_FAILED, ADMIN_LIST, ADMIN_LIST_ALL, ADMIN_START, ADMIN_TAB } from "../types";

const init_state = {
  tab: AdminRoles.roleAdmin,
  adminListAll: [],
  adminList: [],

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case ADMIN_START:
      return { ...state, loading: true, error: "" };
    case ADMIN_FAILED:
      return { ...state, loading: false, error: payload };

    case ADMIN_TAB:
      return { ...state, loading: false, error: "", tab: payload };

    case ADMIN_LIST:
      return { ...state, loading: false, error: "", adminList: payload };
    case ADMIN_LIST_ALL:
      return { ...state, loading: false, error: "", adminListAll: payload };

    default:
      return state;
  }
};
