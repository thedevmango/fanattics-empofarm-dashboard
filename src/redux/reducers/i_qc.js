import { InboundStatus } from "../../constants";
import { IQC_P_TAB, IQC_START, IQC_FAILED, IQC_G_TASKLIST, IQC_G_REPORTS } from "../types";

const init_state = {
  tab: InboundStatus.QCIncoming,
  taskList: [],
  iqc_reports: null,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case IQC_START:
      return { ...state, loading: true, error: "" };
    case IQC_FAILED:
      return { ...state, loading: false, error: "" };

    case IQC_G_TASKLIST:
      return { ...state, taskList: payload };
    case IQC_G_REPORTS:
      return { ...state, iqc_reports: payload };

    case IQC_P_TAB:
      return { ...state, tab: payload };

    default:
      return state;
  }
};
