import {
  PRODUCT_FETCH_SUCCESS,
  PRODUCT_FETCH_START,
  PRODUCT_FETCH_FAILED,
  PRODUCT_DELETE_SUCCESS,
  PRODUCT_SET_PAGE,
} from "../types/product";

const init_state = {
  productList: [],
  page: 1,

  loading: false,
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case PRODUCT_FETCH_START:
      return { ...state, loading: true };
    case PRODUCT_FETCH_SUCCESS:
      return { ...state, loading: false, productList: payload };
    case PRODUCT_FETCH_FAILED:
      return { ...state, loading: false };

    case PRODUCT_DELETE_SUCCESS:
      return {
        ...state,
        productList: state.productList.filter((val) => val.id !== payload),
      };

    case PRODUCT_SET_PAGE:
      return { ...state, page: payload };

    default:
      return state;
  }
};
