import { OutboundStatus } from "../../constants";
import {
  INVENTORY_FAILED,
  INVENTORY_G_DETAILS,
  INVENTORY_G_LIST,
  INVENTORY_P_TAB,
  INVENTORY_START,
} from "../types/inventory";

const init_state = {
  tab: OutboundStatus.TPCIncoming,

  TPCList: [],
  TPCDetails: undefined,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case INVENTORY_P_TAB:
      return { ...state, tab: payload };

    case INVENTORY_START:
      return { ...state, loading: true, error: "" };
    case INVENTORY_FAILED:
      return { ...state, loading: false, error: payload };

    case INVENTORY_G_LIST:
      return { ...state, loading: false, error: "", TPCList: payload };
    case INVENTORY_G_DETAILS:
      return { ...state, loading: false, error: "", TPCDetails: payload };

    default:
      return state;
  }
};
