import {
  CANCELLATION_DETAILS,
  CANCELLATION_FAILED,
  CANCELLATION_LIST,
  CANCELLATION_START,
  CANCELLATION_TAB,
} from "../types";

const init_state = {
  tab: false,

  list: [],
  details: undefined,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case CANCELLATION_TAB:
      return { ...state, tab: payload };

    case CANCELLATION_START:
      return { ...state, loading: true, error: "" };
    case CANCELLATION_FAILED:
      return { ...state, loading: false, error: payload };

    case CANCELLATION_LIST:
      return { ...state, loading: false, error: "", list: payload };
    case CANCELLATION_DETAILS:
      return { ...state, loading: false, error: "", details: payload };

    default:
      return state;
  }
};
