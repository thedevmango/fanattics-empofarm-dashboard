import { OutboundStatus } from "../../constants";
import { oQC_P_TAB, oQC_G_TASK_START, oQC_G_TASK_FAILED, oQC_G_TASK_DONE } from "../types";

const init_state = {
  tab: OutboundStatus.QCIncoming,
  taskList: [],

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case oQC_G_TASK_START:
      return { ...state, loading: true, error: "", taskList: [] };
    case oQC_G_TASK_FAILED:
      return { ...state, loading: false, error: payload, taskList: [] };
    case oQC_G_TASK_DONE:
      return { ...state, loading: false, error: "", taskList: payload };

    case oQC_P_TAB:
      return { ...state, loading: false, error: "", tab: payload };

    default:
      return state;
  }
};
