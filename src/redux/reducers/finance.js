import { InboundStatus, OutboundStatus } from "../../constants";
import { FINANCE_P_TAB, FINANCE_S_TAB } from "../types/finance";

const init_state = {
  p_tab: InboundStatus.FinancePre,
  s_tab: OutboundStatus.FinanceIncoming,
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case FINANCE_P_TAB:
      return { ...state, p_tab: payload };

    case FINANCE_S_TAB:
      return { ...state, s_tab: payload };

    default:
      return state;
  }
};
