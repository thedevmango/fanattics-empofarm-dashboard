import {
  PENGATURAN_PRODUK_SET_KATEGORI,
  PENGATURAN_PRODUK_SET_PRODUK,
  PENGATURAN_PRODUK_RESET,
  PENGATURAN_PRODUK_TOGGLE_INPUT,
  PENGATURAN_PRODUK_CHANGE_KATEGORI,
  PENGATURAN_PRODUK_CHANGE_PRODUK,
  PENGATURAN_PRODUK_CHANGE_SUBPRODUK,
} from "../types/pengaturanProduk";

const init_state = {
  isAddingKategori: false,
  isAddingProduk: false,
  isAddingSubproduk: false,
  inputKategori: "",
  inputProduk: "",
  inputSubproduk: "",

  selectedKategori: undefined,
  selectedProduk: undefined,

  dataKategori: [
    {
      id: 1,
      name: "Hard",
    },
    {
      id: 2,
      name: "Soft",
    },
    {
      id: 3,
      name: "Liquid",
    },
  ],

  dataProduk: [
    {
      id: 1,
      kategoriId: 1,
      name: "Pupuk",
    },
    {
      id: 2,
      kategoriId: 2,
      name: "Mentega",
    },
    {
      id: 3,
      kategoriId: 3,
      name: "Susu",
    },
  ],

  dataSubproduk: [
    {
      id: 1,
      kategoriId: 1,
      produkId: 1,
      name: "Pupuk Urea",
    },
    {
      id: 2,
      kategoriId: 1,
      produkId: 1,
      name: "Pupuk Kompos",
    },
    {
      id: 3,
      kategoriId: 2,
      produkId: 2,
      name: "Mentega Sawit",
    },
    {
      id: 4,
      kategoriId: 2,
      produkId: 2,
      name: "Mentega Nabati",
    },
    {
      id: 5,
      kategoriId: 3,
      produkId: 3,
      name: "Susu Coklat",
    },
    {
      id: 6,
      kategoriId: 3,
      produkId: 3,
      name: "Susu Vanilla",
    },
  ],

  loading: false,
  error: false,
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case PENGATURAN_PRODUK_TOGGLE_INPUT:
      return { ...state, [payload]: !state[payload] };

    case PENGATURAN_PRODUK_CHANGE_KATEGORI:
      return { ...state, inputKategori: payload };
    case PENGATURAN_PRODUK_CHANGE_PRODUK:
      return { ...state, inputProduk: payload };
    case PENGATURAN_PRODUK_CHANGE_SUBPRODUK:
      return { ...state, inputSubproduk: payload };

    case PENGATURAN_PRODUK_SET_KATEGORI:
      return {
        ...state,
        selectedKategori: payload,
        selectedProduk: undefined,
      };
    case PENGATURAN_PRODUK_SET_PRODUK:
      return {
        ...state,
        selectedProduk: payload,
      };
    case PENGATURAN_PRODUK_RESET:
      return init_state;

    case "PENGATURAN_PRODUK_ADD_KATEGORI":
      return {
        ...state,
        dataKategori: [...state.dataKategori, payload],
        isAddingKategori: false,
        inputKategori: "",
      };
    case "PENGATURAN_PRODUK_ADD_PRODUK":
      return {
        ...state,
        dataProduk: [...state.dataProduk, payload],
        isAddingProduk: false,
        inputProduk: "",
      };
    case "PENGATURAN_PRODUK_ADD_SUBPRODUKK":
      return {
        ...state,
        dataSubproduk: [...state.dataSubproduk, payload],
        isAddingSubproduk: false,
        inputSubproduk: "",
      };

    default:
      return state;
  }
};
