import {
  AUTH_START,
  AUTH_START_FORGOT,
  AUTH_FAILED,
  AUTH_SUCCESS,
  AUTH_RESET,
  AUTH_MODAL,
  AUTH_TOAST,
  AUTH_ONCHANGE,
} from "../types/auth";

const init_state = {
  email: "",
  emailForgot: "",
  password: "",

  id: 0,
  profilePicture: undefined,
  name: "",
  role: "",

  modalForgot: false,
  toastForgot: false,

  loadingLogin: false,
  loadingForgot: false,
  login: false,
  error: false,
  message: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case AUTH_START:
      return { ...state, loadingLogin: true, error: false, message: "" };
    case AUTH_START_FORGOT:
      return { ...state, loadingForgot: true, error: false, message: "" };
    case AUTH_FAILED:
      return { ...state, loading: false, error: true, message: payload };
    case AUTH_SUCCESS:
      return { ...init_state, login: true, ...payload };
    case AUTH_RESET:
      return init_state;

    case AUTH_ONCHANGE:
      return { ...state, [payload.name]: payload.value };

    case AUTH_MODAL:
      return { ...state, modalForgot: payload };
    case AUTH_TOAST:
      return { ...state, toastForgot: payload };

    default:
      return state;
  }
};
