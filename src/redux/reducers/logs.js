import { LOGS_FAILED, LOGS_G_SHIPMENT, LOGS_START } from "../types";

const init_state = {
  shipmentLogs: undefined,

  loading: false,
  error: "",
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case LOGS_START:
      return { ...state, loading: true, error: "" };
    case LOGS_FAILED:
      return { ...state, loading: false, error: payload };

    case LOGS_G_SHIPMENT:
      return { ...state, loading: false, error: "", shipmentLogs: payload };

    default:
      return state;
  }
};
