import {
  RESET_PASS_START,
  RESET_PASS_FAILED,
  RESET_PASS_SUCCESS,
  RESET_PASS_ONCHANGE,
} from "../types/resetPass";

const init_state = {
  newPassword: "",
  newPassword2: "",

  success: false,
  loading: false,
  error: false,
};

export default (state = init_state, { type, payload }) => {
  switch (type) {
    case RESET_PASS_START:
      return { ...state, loading: true, error: false };
    case RESET_PASS_FAILED:
      return { ...state, loading: false, error: true };
    case RESET_PASS_SUCCESS:
      return { ...init_state, success: true };

    case RESET_PASS_ONCHANGE:
      return { ...state, [payload.name]: payload.value };

    default:
      return state;
  }
};
