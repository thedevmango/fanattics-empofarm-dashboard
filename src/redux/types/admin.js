export const ADMIN_TAB = "ADMIN_TAB";

export const ADMIN_START = "ADMIN_START";
export const ADMIN_FAILED = "ADMIN_FAILED";

export const ADMIN_LIST = "ADMIN_LIST";
export const ADMIN_LIST_ALL = "ADMIN_LIST_ALL";
