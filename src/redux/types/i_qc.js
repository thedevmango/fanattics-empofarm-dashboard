export const IQC_START = "IQC_START";
export const IQC_FAILED = "IQC_FAILED";

export const IQC_G_TASKLIST = "IQC_G_TASKLIST";
export const IQC_G_REPORTS = "IQC_G_REPORTS";

export const IQC_P_TAB = "IQC_P_TAB";