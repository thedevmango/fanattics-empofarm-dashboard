export const AUTH_START = "AUTH_START";
export const AUTH_START_FORGOT = "AUTH_START_FORGOT";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAILED = "AUTH_FAILED";
export const AUTH_RESET = "AUTH_RESET";

export const AUTH_ONCHANGE = "AUTH_ONCHANGE";

export const AUTH_MODAL = "AUTH_MODAL";
export const AUTH_TOAST = "AUTH_TOAST";
