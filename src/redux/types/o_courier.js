export const oCourier_START = "oCourier_START";
export const oCourier_FAILED = "oCourier_FAILED";

export const oCourier_G_DELIVERY_START = "oCourier_G_DELIVERY_START";
export const oCourier_G_DELIVERY_FAILED = "oCourier_G_DELIVERY_FAILED";
export const oCourier_G_DELIVERY_DONE = "oCourier_G_DELIVERY_DONE";

export const oCourier_G_SHIPMENT_LOGS_START = "oCourier_G_SHIPMENT_LOGS_START";
export const oCourier_G_SHIPMENT_LOGS_FAILED = "oCourier_G_SHIPMENT_LOGS_FAILED";
export const oCourier_G_SHIPMENT_LOGS_DONE = "oCourier_G_SHIPMENT_LOGS_DONE";

export const oCourier_G_OQC_OFFICER_DONE = "oCourier_G_OQC_OFFICER_DONE";
export const oCourier_G_DETAILS_DONE = "oCourier_G_DETAILS_DONE";

export const oCourier_P_TAB = "oCourier_P_TAB";
