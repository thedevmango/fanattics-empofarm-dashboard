export const iCourier_G_DELIVERY_START = "iCourier_G_DELIVERY_START";
export const iCourier_G_DELIVERY_FAILED = "iCourier_G_DELIVERY_FAILED";
export const iCourier_G_DELIVERY_DONE = "iCourier_G_DELIVERY_DONE";

export const iCourier_G_SHIPMENT_LOGS_START = "iCourier_G_SHIPMENT_LOGS_START";
export const iCourier_G_SHIPMENT_LOGS_FAILED = "iCourier_G_SHIPMENT_LOGS_FAILED";
export const iCourier_G_SHIPMENT_LOGS_DONE = "iCourier_G_SHIPMENT_LOGS_DONE";

export const iCourier_P_TAB = "iCourier_P_TAB";
