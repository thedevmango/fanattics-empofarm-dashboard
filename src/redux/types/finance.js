export const FINANCE_CHANGE_VALUE = "FINANCE_CHANGE_VALUE";
export const FINANCE_RESET_DROPDOWN = "FINANCE_RESET_DROPDOWN";

export const FINANCE_P_TAB = "FINANCE_P_TAB";
export const FINANCE_S_TAB = "FINANCE_S_TAB";
