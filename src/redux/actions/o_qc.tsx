/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import { API_URL } from "../../constants";
import { oQC_G_TASK_DONE, oQC_G_TASK_FAILED, oQC_G_TASK_START } from "../types";

interface Props {
  status?: string[];
}

export const getTaskOQC = ({ status }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: oQC_G_TASK_START });
    try {
      let promises: any[] = [];
      let result: any[] = [];
      const resTask = await Axios.get(`${API_URL}/sales`, {
        params: { status: status },
      });

      resTask.data.result.forEach(async (task: any) => {
        if (task.oqcOfficerId !== null) {
          promises.push(
            Axios.get(`${API_URL}/users/admins`, {
              params: { id: task.oqcOfficerId },
            }).then((val) => {
              result.push({ ...task, oqc_officer: { ...val.data.result[0] } });
            })
          );
        } else {
          result.push({ ...task, oqc_officer: {} });
        }
      });

      Promise.all(promises).then(() => {
        result.sort((a: any, b: any) => b.updatedAt - a.updatedAt);
        dispatch({ type: oQC_G_TASK_DONE, payload: result });
        // console.log("OQC", result);
      });
    } catch (err) {
      dispatch({ type: oQC_G_TASK_FAILED });
      console.log(err);
    }
  };
};
