/* eslint-disable no-unused-vars */
import Cookies from "js-cookie";
import { CreateJWTToken, DecodeToken } from "../../configs/jwt";
import {
  AUTH_START,
  AUTH_START_FORGOT,
  AUTH_FAILED,
  AUTH_SUCCESS,
  AUTH_TOAST,
  AUTH_MODAL,
  AUTH_RESET,
} from "../types/auth";

interface Props {
  payload?: any;
}

export const handleLogin = ({ payload }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: AUTH_START });
    try {
      setTimeout(() => {
        dispatch({ type: AUTH_SUCCESS, payload });
        Cookies.set("cookie", CreateJWTToken(payload), { expires: 1 });
      }, 2000);
    } catch (err) {
      dispatch({ type: AUTH_FAILED });
      console.log("err", err);
    }
  };
};

export const ReLoginAction = (token: any) => {
  return (dispatch: any) => {
    if (token !== undefined) {
      let payload = DecodeToken(token);
      // console.log("payload", payload);
      return dispatch({ type: AUTH_SUCCESS, payload });
    } else {
      return dispatch({ type: AUTH_FAILED, payload: "User not authorized" });
    }
  };
};

export const LogoutAction = () => {
  return (dispatch: any) => {
    Cookies.remove("cookie");
    localStorage.removeItem("token");
    dispatch({ type: AUTH_RESET });
  };
};

export const handleForgotPassword = () => {
  return async (dispatch: any) => {
    dispatch({ type: AUTH_START_FORGOT });
    try {
      setTimeout(() => {
        dispatch({ type: AUTH_MODAL, payload: false });
        dispatch({ type: AUTH_TOAST, payload: true });
        setTimeout(() => {
          dispatch({ type: AUTH_TOAST, payload: false });
          dispatch({ type: AUTH_RESET });
        }, 2000);
      }, 2000);
    } catch (err) {
      dispatch({ type: AUTH_FAILED });
      console.log("err", err);
    }
  };
};
