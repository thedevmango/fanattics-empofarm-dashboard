/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import { API_URL, InboundStatus } from "../../constants";
import {
  iCourier_G_DELIVERY_DONE,
  iCourier_G_DELIVERY_FAILED,
  iCourier_G_DELIVERY_START,
  iCourier_G_SHIPMENT_LOGS_START,
  iCourier_G_SHIPMENT_LOGS_FAILED,
  iCourier_G_SHIPMENT_LOGS_DONE,
} from "../types";

interface Props {
  deliveryStatus?: string;
  include: string[];
  status: string[];
}

export const getDeliveryList = ({ deliveryStatus, include, status }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: iCourier_G_DELIVERY_START });
    try {
      const { data } = await Axios.get(`${API_URL}/purchases/list`, {
        params: {
          status: status,
          include: ["product_variant", "farmer", ...include],
          // deliveryStatus: deliveryStatus,
        },
      });
      const result = data.result;
      result.sort((a: any, b: any) => b.updatedAt - a.updatedAt);
      dispatch({ type: iCourier_G_DELIVERY_DONE, payload: result });
    } catch (err) {
      dispatch({ type: iCourier_G_DELIVERY_FAILED });
      console.log(err);
    }
  };
};

export const getShipmentLogs = (id: number) => {
  return async (dispatch: any) => {
    dispatch({ type: iCourier_G_SHIPMENT_LOGS_START });
    try {
      const { data } = await Axios.get(`${API_URL}/logs/shipments`, {
        params: { purchasingTransactionId: id },
      });
      const result = data.result;
      result.sort((a: any, b: any) => a.id - b.id);

      dispatch({ type: iCourier_G_SHIPMENT_LOGS_DONE, payload: result });
    } catch (err) {
      dispatch({ type: iCourier_G_SHIPMENT_LOGS_FAILED });
      console.log(err);
    }
  };
};
