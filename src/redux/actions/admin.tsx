import Axios from "axios";
import { API_URL } from "../../constants";
import { ADMIN_FAILED, ADMIN_LIST, ADMIN_LIST_ALL, ADMIN_START } from "../types";

export const getAdminList = (role?: string[]) => {
  return async (dispatch: any) => {
    dispatch({ type: ADMIN_START });
    try {
      const allUsers = await Axios.get(`${API_URL}/users/admins`);;;
      const { data } = await Axios.get(`${API_URL}/users/admins`, {
        params: { role: role },
      });

      dispatch({ type: ADMIN_LIST, payload: data.result });
      dispatch({ type: ADMIN_LIST_ALL, payload: allUsers.data.result });
    } catch (err) {
      dispatch({ type: ADMIN_FAILED });
      console.log(err);
    }
  };
};
