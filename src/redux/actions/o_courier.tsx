import Axios from "axios";
import { API_URL } from "../../constants";
import {
  oCourier_G_DELIVERY_DONE,
  oCourier_G_SHIPMENT_LOGS_START,
  oCourier_G_SHIPMENT_LOGS_FAILED,
  oCourier_G_SHIPMENT_LOGS_DONE,
  oCourier_START,
  oCourier_FAILED,
  oCourier_G_OQC_OFFICER_DONE,
  oCourier_G_DETAILS_DONE
} from "../types";

interface Props {
  status?: string[];
  id?: number;
}

export const getSalesDelivery = ({ status, id }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: oCourier_START });
    
    if (id) {
      try {
        const resSales = await Axios.get(`${API_URL}/sales`, {
          params: { id: id },
        });
        const result = resSales.data.result[0];
        dispatch({ type: oCourier_G_DETAILS_DONE, payload: result });
      } catch (err) {
        dispatch({ type: oCourier_FAILED });
        console.log(err);
      }
    } else {
      try {
        const resSales = await Axios.get(`${API_URL}/sales`, {
          params: { status: status },
        });
        const result = resSales.data.result;
        result.sort((a: any, b: any) => b.updatedAt - a.updatedAt);
        dispatch({ type: oCourier_G_DELIVERY_DONE, payload: result });
      } catch (err) {
        dispatch({ type: oCourier_FAILED });
        console.log(err);
      }
    }

  };
};


export const getShipmentLogs = (data: any) => {
  return async (dispatch: any) => {
    dispatch({ type: oCourier_G_SHIPMENT_LOGS_START });
    try {
      const courier = await Axios.get(`${API_URL}/users/couriers?id=${data.courierId}`);
      const logs = await Axios.get(`${API_URL}/logs/shipments`, {
        params: { salesTransactionId: data.id },
      });
      const result = { ...courier.data.result[0], logs: logs.data.result };
      dispatch({ type: oCourier_G_SHIPMENT_LOGS_DONE, payload: result });
    } catch (err) {
      dispatch({ type: oCourier_G_SHIPMENT_LOGS_FAILED });
      console.log(err);
    }
  };
};

export const getOQCOfficer = (id: number) => {
  return async (dispatch: any) => {
    dispatch({ type: oCourier_START });
    try {
      const { data } = await Axios.get(`${API_URL}/users/admins`, {
        params: { id: id },
      });
      dispatch({
        type: oCourier_G_OQC_OFFICER_DONE,
        payload: data.result[0],
      });
    } catch (err) {
      dispatch({ type: oCourier_FAILED, payload: "error" });
      console.log(err);
    }
  };
};