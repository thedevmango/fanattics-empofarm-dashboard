/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import { API_URL } from "../../constants";
import { INVENTORY_FAILED, INVENTORY_G_DETAILS, INVENTORY_G_LIST, INVENTORY_START } from "../types";

interface Props {
  id?: number;
  status?: string[];
}

export const getTPCList = ({ id, status }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: INVENTORY_START });
    try {
      if (id) {
        const resDetails = await Axios.get(`${API_URL}/sales?id=${id}`);
        dispatch({ type: INVENTORY_G_DETAILS, payload: resDetails.data.result[0] });
      } else {
        const resList = await Axios.get(`${API_URL}/sales`, {
          params: { status: status },
        });

        if (resList.data.result.length < 1) {
          return dispatch({ type: INVENTORY_G_LIST, payload: [] });
        }

        let promises: any[] = [];
        let results: any[] = [];
        resList.data.result.map((list: any) => {
          if (list.oqcOfficerId) {
            return promises.push(
              Axios.get(`${API_URL}/users/admins?id=${list.oqcOfficerId}`)
                .then((resOfficer) => {
                  results.push({ ...list, oqc_officer: resOfficer.data.result[0] });
                })
                .catch((err) => console.log(err.response))
            );
          } else {
            return results.push({ ...list, oqc_officer: {} });
          }
        });
        Promise.all(promises).then(() => {
          dispatch({ type: INVENTORY_G_LIST, payload: results });
        });
      }
    } catch (err) {
      dispatch({ type: INVENTORY_G_LIST, payload: [] });
      dispatch({ type: INVENTORY_FAILED });
      console.log(err.response.status);
    }
  };
};
