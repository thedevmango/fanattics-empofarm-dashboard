import Axios from "axios";
import { API_URL } from "../../constants";
import { IQC_START, IQC_FAILED, IQC_G_TASKLIST, IQC_G_REPORTS } from "../types";

export const getTaskList = ({ include = [], status }: { include?: string[]; status?: string }) => {
  return async (dispatch: any) => {
    dispatch({ type: IQC_START });
    try {
      let promises: any[] = [];
      let result: any[] = [];
      const resTask = await Axios.get(`${API_URL}/purchases/details`, {
        params: {
          status: status,
          include: ["product_variant", "farmer", ...include],
        },
      });
      resTask.data.result.forEach(async (task: any) => {
        promises.push(
          Axios.get(`${API_URL}/users/admins`, {
            params: { id: task.iqcOfficerId },
          }).then((val) => {
            result.push({ ...task, iqcOfficer: { ...val.data.result[0] } });
          })
        );
      });
      Promise.all(promises).then(() => {
        result.sort((a: any, b: any) => b.updatedAt - a.updatedAt);
        dispatch({ type: IQC_G_TASKLIST, payload: result });
      });
    } catch (err) {
      dispatch({ type: IQC_FAILED });
      console.log(err);
    }
  };
};

export const getIQCReports = ({ data }: { data: any }) => {
  return async (dispatch: any) => {
    dispatch({ type: IQC_START });
    if (!data.iqcOfficerId) return dispatch({ type: IQC_G_REPORTS, payload: null });
    try {
      const resOfficer = await Axios.get(`${API_URL}/users/admins?id=${data.iqcOfficerId}`);
      const resReports = await Axios.get(`${API_URL}/quality-controls/inbounds`, {
        params: { purchasingTransactionId: data.id },
      });
      const admin = resOfficer.data.result[0];
      const reports = resReports.data.result[resReports.data.result.length - 1];
      dispatch({ type: IQC_G_REPORTS, payload: { admin, reports } });
    } catch (err) {
      dispatch({ type: IQC_FAILED });
      console.log(err);
    }
  };
};