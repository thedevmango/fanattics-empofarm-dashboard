import Axios from "axios";
import { API_URL } from "../../constants";
import { CANCELLATION_FAILED, CANCELLATION_LIST, CANCELLATION_START } from "../types";

interface IList {
  id?: number;
  isRejected?: boolean | null;
  type?: "sales" | "purchases";
}

interface IListParams {
  id?: number;
  isRejected?: boolean;
  purchasingTransactionId?: number | null;
  saleTransactionId?: number | null;
}

export const getCancellationList = ({ id, isRejected = null, type }: IList) => {
  return async (dispatch: any) => {
    dispatch({ type: CANCELLATION_START });
    let params: IListParams = {};
    try {
      if (id) {
        params.id = id;
      } else {
        if (isRejected !== null) params.isRejected = isRejected;
        if (type === "sales") params.purchasingTransactionId = null;
        if (type === "purchases") params.saleTransactionId = null;
      }
      const resCancel = await Axios.get(`${API_URL}/cancellation-cases`, {
        params: params,
      });
      // console.log(resCancel.data.result);

      //* GET TRANSACTION (PURCHASES/SALES) DETAILS
      let promises: any[] = [];
      let promises2: any[] = [];
      let resultTrxDetails: any = [];
      resCancel.data.result.forEach(async (cancelList: any) => {
        promises.push(
          cancelList.saleTransactionId !== null
            ? Axios.get(`${API_URL}/sales?id=${cancelList.saleTransactionId}`)
                .then((resSales) => {
                  // console.log("resSales", resSales);
                  resSales.data.result.forEach(async (salesDetails: any) => {
                    promises2.push(
                      Axios.get(
                        `${API_URL}/sales/sales-details?salesTransactionId=${salesDetails.id}`
                      )
                        .then((resSalesDetails) => {
                          resultTrxDetails.push({
                            ...cancelList,
                            transaction: {
                              ...resSales.data.result[0],
                              sale_transaction_details: resSalesDetails.data.result,
                            },
                          });
                        })
                        .catch((errSalesDetails) => console.log("errSalesDetails", errSalesDetails))
                    );
                  });
                })
                .catch((errSales) => console.log("errSales", errSales))
            : cancelList.purchasingTransactionId !== null
            ? promises.push(
                Axios.get(`${API_URL}/purchases/details?id=${cancelList.purchasingTransactionId}`)
                  .then((resPurchase) => {
                    resultTrxDetails.push({
                      ...cancelList,
                      transaction: resPurchase.data.result[0],
                    });
                  })
                  .catch((errPurchase) => console.log("errPurchase", errPurchase))
              )
            : null
        );
      });

      Promise.all(promises).then(() => {
        Promise.all(promises2).then(() => {
          //* GET REQUEST ADMIN
          let promises3: any[] = [];
          let resultAdmin: any[] = [];
          resultTrxDetails.forEach(async (resultCancellation: any) => {
            promises3.push(
              Axios.get(`${API_URL}/users/admins?id=${resultCancellation.requestedByAdminId}`)
                .then((resRequestAdmin) => {
                  resultAdmin.push({
                    ...resultCancellation,
                    requestedByAdmin: resRequestAdmin.data.result[0],
                  });
                })
                .catch((errRequestAdmin) => console.log("errRequestAdmin", errRequestAdmin))
            );
          });

          Promise.all(promises3).then(() => {
            console.log("CANCEL ADMIN", resultAdmin);
            dispatch({ type: CANCELLATION_LIST, payload: resultAdmin });
          });
        });
      });
    } catch (err) {
      console.log("err", err);
      dispatch({ type: CANCELLATION_FAILED });
    }
  };
};
