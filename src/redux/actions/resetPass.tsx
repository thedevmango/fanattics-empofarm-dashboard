import { RESET_PASS_START, RESET_PASS_FAILED, RESET_PASS_SUCCESS } from "../types/resetPass";

export const submitResetPassword = () => {
  return async (dispatch: any) => {
    dispatch({ type: RESET_PASS_START });
    try {
      setTimeout(() => {
        dispatch({ type: RESET_PASS_SUCCESS });
      }, 2000);
    } catch (err) {
      dispatch({ type: RESET_PASS_FAILED });
      console.log("err", err);
    }
  };
};
