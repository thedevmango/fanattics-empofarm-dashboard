import Axios from "axios";
import { API_URL } from "../../constants";
import {
  G_PURCHASING_TRX_START,
  G_PURCHASING_TRX_DONE,
  G_PURCHASING_TRX_FAILED,
  G_PURCHASING_TRX_DETAILS_START,
  G_PURCHASING_TRX_DETAILS_DONE,
  G_PURCHASING_TRX_DETAILS_FAILED,
  G_PRODUCT_SKU,
} from "../types";

interface Props {
  id?: number;
  include?: string[];
  isFailed?: boolean;
  status: string[];
  type?: "DS" | "PAR";
}

export const getPurchasingTransaction = ({ include, isFailed, status, type = "DS" }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: G_PURCHASING_TRX_START });
    try {
      const { data } = await Axios.get(`${API_URL}/purchases/list`, {
        params: {
          include,
          status,
          isFailed,
          type: type,
        },
      });

      setTimeout(() => {
        dispatch({ type: G_PURCHASING_TRX_DONE, payload: data.result });
      }, 250);
    } catch (err) {
      dispatch({ type: G_PURCHASING_TRX_FAILED });
      console.log(err);
    }
  };
};

export const getPurchasingTransactionDetails = ({ id }: { id: number }) => {
  return async (dispatch: any) => {
    dispatch({ type: G_PURCHASING_TRX_DETAILS_START });
    try {
      const { data } = await Axios.get(`${API_URL}/purchases/details`, {
        params: {
          id,
        },
      });

      setTimeout(() => {
        dispatch({ type: G_PURCHASING_TRX_DETAILS_DONE, payload: data.result[0] });
      }, 250);
    } catch (err) {
      dispatch({ type: G_PURCHASING_TRX_DETAILS_FAILED });
      console.log(err);
    }
  };
};

export const getSKUInfo = (id: number) => {
  return async (dispatch: any) => {
    try {
      const { data } = await Axios.get(`${API_URL}/products/skus?id=${id}`);
      dispatch({ type: G_PRODUCT_SKU, payload: data.result[0] });
    } catch (err) {
      console.log(err);
    }
  };
};
