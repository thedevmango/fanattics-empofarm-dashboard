import Axios from "axios";
import { API_URL } from "../../constants";
import {
  SALES_START,
  SALES_FAILED,
  SALES_LIST,
  SALES_LIST_DETAILS,
  SALES_TRX_DETAILS,
} from "../types/sales";

interface Props {
  id?: number;
  status?: any;
}

export const getSalesTransaction = ({ id, status }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: SALES_START });
    try {
      if (id) {
        const resDetails = await Axios.get(`${API_URL}/sales?id=${id}`);
        dispatch({ type: SALES_LIST_DETAILS, payload: resDetails.data.result[0] });
      } else {
        const resList = await Axios.get(`${API_URL}/sales`, {
          params: { status: status },
        });

        if (resList.data.result.length < 1) {
          return dispatch({ type: SALES_LIST, payload: [] });
        }

        let promises: any[] = [];
        let results: any[] = [];
        resList.data.result.map((list: any) => {
          if (list.oqcOfficerId) {
            return promises.push(
              Axios.get(`${API_URL}/users/admins?id=${list.oqcOfficerId}`)
                .then((resOfficer) => {
                  results.push({ ...list, oqc_officer: resOfficer.data.result[0] });
                })
                .catch((err) => console.log(err.response))
            );
          } else {
            return results.push({ ...list, oqc_officer: {} });
          }
        });
        Promise.all(promises).then(() => {
          dispatch({ type: SALES_LIST, payload: results });
        });
      }
    } catch (err) {
      dispatch({ type: SALES_LIST, payload: [] });
      dispatch({ type: SALES_FAILED });
      console.log(err.response.status);
    }
  };
};

export const getSaleTrxDetails = ({ id }: { id: number }) => {
  return async (dispatch: any) => {
    dispatch({ type: SALES_START });
    try {
      const { data } = await Axios.get(`${API_URL}/sales/sales-details?salesTransactionId=${id}`);
      dispatch({ type: SALES_TRX_DETAILS, payload: data.result });
    } catch (err) {
      dispatch({ type: SALES_TRX_DETAILS, payload: [] });
      dispatch({ type: SALES_FAILED });
      console.log(err.response);
    }
  };
};

// export const getSalesTransaction = ({ status }: Props) => {
//   return async (dispatch: any) => {
//     dispatch({ type: SALES_START });
//     try {
//       const { data } = await Axios.get(`${API_URL}/sales`, {
//         params: { status: status },
//       });

//       setTimeout(() => {
//         dispatch({ type: SALES_TRX_LIST, payload: data.result });
//       }, 250);
//     } catch (err) {
//       dispatch({ type: SALES_FAILED });
//       console.log(err);
//     }
//   };
// };

// export const getSalesTransactionDetails = ({ id }: Props) => {
//   return async (dispatch: any) => {
//     dispatch({ type: SALES_START });
//     try {
//       const { data } = await Axios.get(`${API_URL}/sales?id=${id}`);

//       setTimeout(() => {
//         dispatch({ type: SALES_TRX_DETAILS, payload: data.result[0] });
//       }, 250);
//     } catch (err) {
//       dispatch({ type: SALES_FAILED });
//       console.log(err);
//     }
//   };
// };
