import Axios from "axios";
import { API_URL } from "../../constants";
import { LOGS_FAILED, LOGS_G_SHIPMENT, LOGS_START } from "../types";

interface Props {
  data: any;
  type: "inbound" | "outbound";
}

export const getShipmentLogs = ({ data, type }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: LOGS_START });
    try {
      const resShipment = await Axios.get(`${API_URL}/logs/shipments`, {
        params:
          type === "inbound"
            ? { purchasingTransactionId: data.id }
            : { salesTransactionId: data.id },
      });
      const shipment = resShipment.data.result;

      if (data.courrierId || data.courierId) {
        try {
          const resCourier = await Axios.get(`${API_URL}/users/couriers`, {
            params: type === "inbound" ? { id: data.courrierId } : { id: data.courierId },
          });
          const results = { logs: shipment, ...resCourier.data.result[0] };
          dispatch({ type: LOGS_G_SHIPMENT, payload: results });
        } catch (err) {
          console.log("courier", err.response);
        }
      } else {
        dispatch({ type: LOGS_G_SHIPMENT, payload: undefined });
      }
    } catch (err) {
      dispatch({ type: LOGS_FAILED });
      console.log(err.response);
    }
  };
};
