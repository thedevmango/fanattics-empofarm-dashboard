const outboundStatus = {
  salesNew: "sales-new",
  salesUnconfirmed: "sales-unconfirmed",
  salesConfirmed: "sales-confirmed",

  salesComposition: "sales-composition",
  salesConsolidation: "sales-consolidation",
  salesConsolidated: "sales-consolidated",

  outboundOutboundQC: "outbound-outboundqc",
  oqcApproval: "oqc-approval",
  oqcRejected: "oqc-rejected",
  outboundChamber: "outbound-chamber",

  outboundDelivery: "outbound-delivery",
  salesCompleted: "sales-completed",
  salesFailed: "sales-failed",
};

const CompositionAll = [
  outboundStatus.salesComposition,
  outboundStatus.salesConsolidation,
  outboundStatus.salesConsolidated,
];

const QCAll = [
  outboundStatus.outboundOutboundQC,
  outboundStatus.oqcApproval,
  outboundStatus.outboundChamber,
];
const CourierAll = [
  outboundStatus.outboundDelivery,
  outboundStatus.salesCompleted,
  outboundStatus.salesFailed
];

export default {
  ...outboundStatus,

  SalesIncoming: [outboundStatus.salesNew],
  SalesUnconfirmed: [outboundStatus.salesUnconfirmed],
  SalesConfirmed: [outboundStatus.salesConfirmed],

  FinanceIncoming: [outboundStatus.salesNew, outboundStatus.salesUnconfirmed],
  FinanceOrder: [outboundStatus.salesConfirmed],
  FinanceFinished: [...CompositionAll, ...QCAll, ...CourierAll],

  QCIncoming: [outboundStatus.salesConsolidated],
  QCInprocess: [outboundStatus.outboundOutboundQC],
  QCDone: [
    // outboundStatus.oqcRejected,
    outboundStatus.oqcApproval,
    outboundStatus.outboundChamber,
    ...CourierAll,
  ],

  TPCIncoming: [outboundStatus.salesConfirmed, outboundStatus.salesComposition],
  TPCInprocess: [outboundStatus.salesConsolidation],
  TPConQC: [
    outboundStatus.salesConsolidated,
    outboundStatus.outboundOutboundQC,
    outboundStatus.oqcApproval,
  ],
  TPCDone: [outboundStatus.outboundChamber, ...CourierAll],

  CourierIncoming: [outboundStatus.outboundChamber],
  CourierInProcess: [outboundStatus.outboundDelivery],
  CourierDone: [outboundStatus.salesCompleted, outboundStatus.salesFailed],
};
