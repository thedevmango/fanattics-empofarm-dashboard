const inboundStatus = {
  onPurchasing: "on-purchasing",
  logisticInboundHold: "logistic-inbound-hold",
  onLogisticInbound: "on-logistic-inbound",
  onArrivalTray: "on-arrival-tray",
  onQcInbound: "on-qc-inbound",
  iqcPassed: "iqc-passed",
  iqcRejected: "iqc-rejected",

  //* BELOM THIS WAS AUTO DONE
  nominalValueInput: "nominal-value-input",
  financeApproval: "finance-approval",
  paymentRequest: "payment-request",
  finalValueSubmitted: "final-value-submitted",
  itemStored: "item-stored",
  failed: "failed",
};

const inboundMasuk = [inboundStatus.onPurchasing];
const inboundDiproses = [
  inboundStatus.logisticInboundHold,
  inboundStatus.onLogisticInbound,
  inboundStatus.onArrivalTray,
  inboundStatus.onQcInbound,
  inboundStatus.iqcPassed,
  inboundStatus.iqcRejected,
];
const inboundPembayaran = [
  inboundStatus.nominalValueInput,
  inboundStatus.financeApproval,
  inboundStatus.paymentRequest,
  inboundStatus.finalValueSubmitted,
];
const inboundSelesai = [inboundStatus.itemStored];

export default {
  ...inboundStatus,

  TenderMasuk: [...inboundMasuk],
  TenderDiproses: [...inboundDiproses],
  TenderPembayaran: [...inboundPembayaran],
  TenderSelesai: [...inboundSelesai],

  FinancePre: [...inboundMasuk, ...inboundDiproses, inboundStatus.nominalValueInput],
  FinanceOrder: [inboundStatus.financeApproval],
  FinancePayment: [inboundStatus.paymentRequest],
  FinanceSuccess: [inboundStatus.finalValueSubmitted, ...inboundSelesai],
  FinanceFailed: [inboundStatus.itemStored],

  LogisticIncoming: [inboundStatus.logisticInboundHold],
  LogisticOnProcess: [inboundStatus.onLogisticInbound],
  LogisticDone: [
    inboundStatus.onArrivalTray,
    inboundStatus.onQcInbound,
    inboundStatus.iqcPassed,
    inboundStatus.iqcRejected,
    ...inboundPembayaran,
    ...inboundSelesai,
  ],

  QCIncoming: [inboundStatus.onArrivalTray],
  QCOnProcess: [inboundStatus.onQcInbound],
  QCDone: [
    inboundStatus.iqcPassed,
    inboundStatus.iqcRejected,
    ...inboundPembayaran,
    ...inboundSelesai,
  ],
};
