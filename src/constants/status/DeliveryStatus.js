//* INBOUND
const inboundDeliveryStatus = {
  waitingLogisticConfirmation: "waiting-logistic-confirmation",
  logisticOnPickup: "logistic-on-pickup",
  arrivedOnFarm: "arrived-on-farm",
  logisticLoading: "logistic-loading",
  logisticHauling: "logistic-hauling",
  arrivedOnDestination: "arrived-on-destination",
};
const incomingInbound = [inboundDeliveryStatus.waitingLogisticConfirmation];
const onprocessInbound = [
  inboundDeliveryStatus.logisticOnPickup,
  inboundDeliveryStatus.arrivedOnFarm,
  inboundDeliveryStatus.logisticLoading,
  inboundDeliveryStatus.logisticHauling,
];
const doneInbound = [inboundDeliveryStatus.arrivedOnDestination];

//
//
//* OUTBOUND
const outboundDeliveryStatus = {
  waitingLogisticConfirmation: "waiting-logistic-confirmation",
  logisticOnPickup: "logistic-on-pickup",
  logisticHaulingPort: "logistic-hauling-port",
  arrivedOnPort: "arrived-on-port",
  logisticUnloading: "logistic-unloading",
  logisticHaulingBuyer: "logistic-hauling-buyer",
  arrivedOnDestination: "arrived-on-destination",
};
const incomingOutbound = [outboundDeliveryStatus.waitingLogisticConfirmation];
const onprocessOutbound = [
  outboundDeliveryStatus.logisticOnPickup,
  outboundDeliveryStatus.logisticHaulingPort,
  outboundDeliveryStatus.arrivedOnPort,
  outboundDeliveryStatus.logisticUnloading,
  outboundDeliveryStatus.logisticHaulingBuyer,
];
const doneOutbound = [outboundDeliveryStatus.arrivedOnDestination];

export default {
  inbound: {
    ...inboundDeliveryStatus,
    incoming: incomingInbound,
    onprocess: onprocessInbound,
    done: doneInbound,
  },
  outbound: {
    ...outboundDeliveryStatus,
    incoming: incomingOutbound,
    onprocess: onprocessOutbound,
    done: doneOutbound,
  },
};
