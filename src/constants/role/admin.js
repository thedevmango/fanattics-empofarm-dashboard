const roleManager = {
  super: "super",
  inventory: "inventory",
  logistic: "logistic",
  iqcManager: "iqc-manager",
  oqcManager: "oqc-manager",
};

const roleStaff = {
  iqc: "iqc",
  oqc: "oqc",
};

export default {
  ...roleManager,
  ...roleStaff,

  roleAdmin: [
    roleManager.super,
    roleManager.inventory,
    roleManager.logistic,
    roleManager.iqcManager,
    roleManager.oqcManager,
  ],
  roleStaff: [roleStaff.iqc, roleStaff.oqc],
};
