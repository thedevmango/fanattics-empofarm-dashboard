export * from "./API_URL";
export const GMAPS_API = "AIzaSyCH-uG3wFO-e_eQd_DdV0j6ifTNPwcdZLk";

export { default as Colors } from "./Colors";
export { default as DeliveryStatus } from "./status/DeliveryStatus";
export { default as InboundStatus } from "./status/InboundStatus";
export { default as OutboundStatus } from "./status/OutboundStatus";
export { default as AdminRoles } from "./role/admin";